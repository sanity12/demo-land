<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sanity extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('URL', 'DATE', 'URI', 'FORM');
        $this->load->library('form_validation');
        $this->load->helper('custom');
        $this->load->library('upload');
        $this->load->model('Crud');
    }

    public function index() {
    $admin_id = $this->session->userdata('admin_id');
        if(isset($admin_id) && $admin_id != ''){
        $this->load->view('sanity/header');
        $this->load->view('sanity/dashboard');
        $this->load->view('sanity/footer');
        }else{
            redirect('sanity/login');
        }
    }

    public function login() {
        
            $this->load->view('sanity/header');
            $this->load->view('sanity/login');
            $this->load->view('sanity/footer');
        
    }

    public function signin() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $this->db->where('username', $username);
        $this->db->where('password', md5($password));
        $query = $this->db->get('admin');
//        echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {

            foreach ($query->result() as $rows) {
                //add all data to session
                $newdata = array(
                    'admin_id' => $rows->id,
                    'admin_username' => $rows->username,
                    'admin_logged_in' => TRUE
                );
            }
            $this->session->set_userdata($newdata);
            redirect("sanity");
        } else {
            $this->session->set_flashdata('error', 'Error,Your username and password are wrong, please try again !');
            redirect("sanity/login");
        }
    }

    public function logout() {
        $newdata = array(
            'admin_id' => '',
            'admin_username' => '',
            'admin_logged_in' => FALSE,
        );
        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();
        $this->load->helper('url');
        redirect('sanity/login');
    }

    public function users() {
        $this->db->where('status', 'Active');
        $query = $this->db->get('users');
        $data['users'] = $query->result_array();

        $this->load->view('sanity/header');
        $this->load->view('sanity/users', $data);
        $this->load->view('sanity/footer');
    }

    public function viewDetail() {
        $id = $this->uri->segment(3);
        $this->db->where('user_id', $id);
        $query = $this->db->get('users');
        $data['users'] = $query->result_array();
        $this->load->view('sanity/header');
        $this->load->view('sanity/user-details', $data);
        $this->load->view('sanity/footer');
    }

    public function delUser() {
        $id = $this->uri->segment(3);
        $dataU = array('status' => 'Deactive');
        $this->db->where('user_id', $id);
        $this->db->update('users', $dataU);
        $this->session->set_flashdata('success', 'Success, User deleted successfully!');
        redirect("sanity/users");
    }

    public function chngPwd() {
        $this->load->view('sanity/header');
        $this->load->view('sanity/change-pass');
        $this->load->view('sanity/footer');
    }

    public function updatePwd() {
        $admin_id = $this->session->userdata('admin_id');
        if ($this->input->post('pass')) {
            $pass = $this->input->post('pass');
            $dataU = array('password' => md5($pass));
        }
        if ($this->input->post('Masterpass')) {
            $Masterpass = $this->input->post('Masterpass');
            $dataU = array('master_password' => md5($Masterpass));
        }
        $this->db->where('id', $admin_id);
        $this->db->update('admin', $dataU);
        redirect('sanity/logout');
    }

    public function propertyList() {

        $this->db->where('user_type', 'public');
        $this->db->where('property_type', 'public');
        $query = $this->db->get('property_details');
        $data['propertyRes'] = $query->result_array();
        $this->load->view('sanity/header');
        $this->load->view('sanity/property_list', $data);
        $this->load->view('sanity/footer');
    }

    public function p_propertyList() {

        //$this->db->where('user_type', 'private');
        $this->db->where('property_type', 'private');
        $query = $this->db->get('property_details');
        //echo $this->db->last_query();
        $data['propertyRes'] = $query->result_array();
        //print_r($data);exit;
        
//      $this->db->where('user_type', 'private');
        $query = $this->db->get('recom_property_details');
        $data['EditedpropertyRes'] = $query->result_array();

        $this->load->view('sanity/header');
        $this->load->view('sanity/pri_property_list', $data);
        $this->load->view('sanity/footer');
    }

    public function applyFormList() {
        //$id = $this->uri->segment(3);
        //$this->db->where('user_id', $id);
        $this->db->order_by('is_payment', 'DESC');
        $query = $this->db->get('applyproperty_private');
        $data['applyRes'] = $query->result_array();

//        $this->db->where('property_id', $pro_id);
//        $query = $this->db->get("recom_property_details");
//        $data['ProRes'] = $query->result_array();

        $this->load->view('sanity/header');
        $this->load->view('sanity/applyFormList', $data);
        $this->load->view('sanity/footer');
    }

    public function ProEditDetails() {
        $pro_id = $this->uri->segment(3);
        $public_id = $this->uri->segment(4);
        $public_property_id = $this->uri->segment(5);
        $this->db->where('property_id', $pro_id);
        $query = $this->db->get("property_details");
        $data['ProRes'] = $query->result_array();

        $query = $this->db->get("preference_details");
        $data['PreferRes'] = $query->result_array();

        $this->db->where('pro_id', $pro_id);
        $query = $this->db->get("gallery");
        $data['GalleryListRes'] = $query->result_array();

        $this->db->where('proper_id', $pro_id);
        $this->db->where('user_public_id', $public_id);
        $query = $this->db->get("applyproperty_private");
        $data['applyproperty_private'] = $query->result_array();

        $data['pro_id'] = $pro_id;
        $data['public_id'] = $public_id;
        $data['public_property_id'] = $public_property_id;

        $this->load->view('sanity/header');
        $this->load->view('sanity/ProPereditDetail', $data);
        $this->load->view('sanity/footer');
    }

    public function updateProper() {
        $propertyid = $this->input->post('property_id');
            echo $public_id = $this->input->post('public_id');
            echo $public_property_id = $this->input->post('public_property_id');
            
            $this->db->where('public_property_id', $public_property_id);
            $this->db->where('public_id', $public_id);
            $query = $this->db->get('recom_property_details');
            
            date_default_timezone_set('Asia/Kolkata');
            $today = date("Y-m-d H:i:s");
            //        echo $today = date('Y-m-d H:i:s'); 

            if ($this->input->post('days')) {
                $days = $this->input->post('days');
            } else {
                $days = 14;
            }

            if ($this->input->post('hrs')) {
                $hrs = $this->input->post('hrs');
            } else {
                $hrs = 7;
            }
            $date = new DateTime($today);
            $date->modify("+" . $hrs . " hours");
            $today = $date->format("Y-m-d H:i:s");

            $next_date = date('Y-m-d H:i:s', strtotime($today . ' + ' . $days . ' days'));

            $propertyidArr = explode(',', $propertyid);
            foreach ($propertyidArr as $value) {
                if ($query->num_rows() > 0) {
                    $public_id = $this->input->post('public_id');
                        $data = array(
                        'property_id' => $value,                        
                        'you_are' => '-',
                        'city' => $this->input->post('city'),
                        'project_name' => $this->input->post('project_name'),
                        'locality' => $this->input->post('locality'),
                        'address' => '-',
                        'plot_area' => $this->input->post('plot_area'),
                        'length' => '0',
                        'breadth' => '0',
                        'no_of_bathrooms' => $this->input->post('no_of_bathrooms'),
                        'no_of_bedrooms' => $this->input->post('no_of_bedrooms'),
                        'no_of_balcony' => $this->input->post('no_of_balcony'),
                        'complete_address' => '-',
                        'Garden' => $this->input->post('Garden'),
                        'water_borewell' => $this->input->post('water_borewell'),
                        'electricity_backup' => $this->input->post('electricity_backup'),
                        'swimming_pool' => $this->input->post('swimming_pool'),
                        'age_of_property' => $this->input->post('age_of_property'),
                        'status1' => $this->input->post('status1'),
                        'electricity' => '-',
                        'pro_photo' => $this->input->post('pro_photo'),
                        'created_date' => date('Y-m-d H:i:s'),
                        'deal_end_date' => $next_date
                    );
                    $this->db->where('property_id', $value);
                    $this->db->where('public_id', $public_id);
                    $this->db->where('public_property_id', $public_property_id);
                    $insert_id = $this->db->update('recom_property_details', $data);
                } else {
                        $data = array(
                        'property_id' => $value,
                        'user_id' => $this->input->post('user_id'),
                        'public_id' => $this->input->post('public_id'),
                        'public_property_id' => $this->input->post('public_property_id'),
                        'user_type' => 'private',
                        'you_are' => '-',
                        'city' => $this->input->post('city'),
                        'project_name' => $this->input->post('project_name'),
                        'locality' => $this->input->post('locality'),
                        'address' => '-',
                        'plot_area' => $this->input->post('plot_area'),
                        'length' => '0',
                        'breadth' => '0',
                        'no_of_bathrooms' => $this->input->post('no_of_bathrooms'),
                        'no_of_bedrooms' => $this->input->post('no_of_bedrooms'),
                        'no_of_balcony' => $this->input->post('no_of_balcony'),
                        'complete_address' => '-',
                        'Garden' => $this->input->post('Garden'),
                        'water_borewell' => $this->input->post('water_borewell'),
                        'electricity_backup' => $this->input->post('electricity_backup'),
                        'swimming_pool' => $this->input->post('swimming_pool'),
                        'age_of_property' => $this->input->post('age_of_property'),
                        'status1' => $this->input->post('status1'),
                        'electricity' => '-',
                        'pro_photo' => $this->input->post('pro_photo'),
                        'created_date' => date('Y-m-d H:i:s'),
                        'deal_end_date' => $next_date
                    );
                    $this->db->insert('recom_property_details', $data);
                    $insert_id = $this->db->insert_id();
                }
            }
            $updateData1 = array(
                'status' => '2',
                'created_date' => date('Y-m-d H:i:s'),
                'updated_date' => $next_date
            );
            $this->db->where('own_property', $propertyid);                
            $this->db->update('applyproperty_private', $updateData1);
            redirect("sanity/applyFormList");

    }

    public function viewDetailPro() {
        $pro_id = $this->uri->segment(3);

        $this->db->where('property_id', $pro_id);
        $query = $this->db->get("property_details");
        $data['ProRes'] = $query->result_array();
        $data['pro_id'] = $pro_id;
        $this->load->view('sanity/header');
        $this->load->view('sanity/viewDetailPro', $data);
        $this->load->view('sanity/footer');
    }

    public function viewDetailProEdited() {
        $pro_id = $this->uri->segment(3);

        $this->db->where('rpd_id', $pro_id);
        $query = $this->db->get("recom_property_details");
        $data['Recm_ProRes'] = $query->result_array();
        $data['pro_id'] = $pro_id;
        $this->load->view('sanity/header');
        $this->load->view('sanity/viewDetailProEdited', $data);
        $this->load->view('sanity/footer');
    }

    public function message(){
        $data_myacc['users'] = $this->Crud->retrive();
        $data_myacc['message'] = $this->Crud->retrive_message();

        $this->load->view('sanity/header');
        $this->load->view('sanity/message', $data_myacc);
        $this->load->view('sanity/footer');
    }

    public function sent_message(){
        $this->load->view('sanity/header');
        $this->load->view('sanity/message', $data_myacc);
        $this->load->view('sanity/footer');
    }

    public function add_message() {
        $this->load->helper('ckeditor');
        $data['id'] = $this->uri->segment(3);

        $this->load->view('sanity/header');
        //$this->load->view('sanity/ckeditor',$this->data); 
        $this->load->view('sanity/add_message', $data);
        $this->load->view('sanity/footer');
    }

    public function user_message() {
        $this->load->helper('ckeditor');
        $data['id'] = $this->uri->segment(3);

        $this->load->view('sanity/header');
        $data['users'] = $this->Crud->retrive();
        //$this->load->view('sanity/ckeditor',$this->data); 
        $this->load->view('sanity/user_message', $data);
        $this->load->view('sanity/footer');
    }

    public function add_msg() {
        $this->load->library('form_validation');
        // field name, error message, validation rules
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('msg_type', 'Message Type', 'trim|required');
        $this->form_validation->set_rules('message', 'Message Description', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            //print_r(validation_errors());exit;            
            $this->add_message();
        } else {
            $this->Crud->msgInsert();
            redirect('sanity/message');
        }
    }

    public function coupon(){
        $this->load->view('sanity/header');
        $data['users'] = $this->Crud->retrive();

        $data['type1'] = $this->Crud->retrive_type('type1');
        $data['type2'] = $this->Crud->retrive_type('type2');
        $data['type3'] = $this->Crud->retrive_type('type3');
        $data['coupontext'] = $this->Crud->retrive_text('coupontext');

        $this->load->view('sanity/coupon', $data);
        $this->load->view('sanity/footer');
    }

    public function coupon1(){
        $data['type1'] = $this->Crud->retrive_type('type1');
        if(empty($data['type1'])){
            $data = array(
                    'code' => $this->input->post('code1'),
                    'coupon_type' => $this->input->post('coupon_type1'),
                    'frequency' => $this->input->post('frequency1'),
                    'duration' => $this->input->post('time1'),
                    'created_date' => date('Y-m-d H:i:s'),
                    'status' => '0' // 0-> enable
                );

                $this->db->insert('coupon', $data);

        }else{

                $updatedata = array(
                    'status' => '1' // 1-> disable
                );
                $this->db->where("c_id", $data['type1'][0]['c_id']);
                $this->db->update('coupon', $updatedata);

                $data = array(
                    'code' => $this->input->post('code1'),
                    'coupon_type' => $this->input->post('coupon_type1'),
                    'frequency' => $this->input->post('frequency1'),
                    'duration' => $this->input->post('time1'),
                    'created_date' => date('Y-m-d H:i:s'),
                    'status' => '0' // 0-> enable
                );
                $this->db->insert('coupon', $data);

        }
        redirect('sanity/coupon');
    }

    public function coupon2(){
        $data['type2'] = $this->Crud->retrive_type('type2');
        if(empty($data['type2'])){
            $data = array(
                'code' => $this->input->post('code2'),
                'coupon_type' => $this->input->post('coupon_type2'),
                'frequency' => $this->input->post('frequency2'),
                'duration' => $this->input->post('time2'),
                'created_date' => date('Y-m-d H:i:s'),
                'status' => '0' // 0-> enable
            );
            $this->db->insert('coupon', $data);
        }else{
            $updatedata = array(
                'status' => '1' // 1-> disable
            );
            $this->db->where("c_id", $data['type2'][0]['c_id']);
            $this->db->update('coupon', $updatedata);

            $data = array(
                'code' => $this->input->post('code2'),
                'coupon_type' => $this->input->post('coupon_type2'),
                'frequency' => $this->input->post('frequency2'),
                'duration' => $this->input->post('time2'),
                'created_date' => date('Y-m-d H:i:s'),
                'status' => '0' // 0-> enable
            );

            $this->db->insert('coupon', $data);
        }
        redirect('sanity/coupon');
    }


    public function coupon3(){
        $data['type3'] = $this->Crud->retrive_type('type3');
        if(empty($data['type3'])){
            $data = array(
                'code' => $this->input->post('code3'),
                'coupon_type' => $this->input->post('coupon_type3'),
                'frequency' => $this->input->post('frequency3'),
                'duration' => $this->input->post('time3'),
                'created_date' => date('Y-m-d H:i:s'),
                'status' => '0' // 0-> enable
            );
            $this->db->insert('coupon', $data);
        }else{
            $updatedata = array(
                'status' => '1' // 1-> disable
            );
            $this->db->where("c_id", $data['type3'][0]['c_id']);
            $this->db->update('coupon', $updatedata);

            $data = array(
                'code' => $this->input->post('code3'),
                'coupon_type' => $this->input->post('coupon_type3'),
                'frequency' => $this->input->post('frequency3'),
                'duration' => $this->input->post('time3'),
                'created_date' => date('Y-m-d H:i:s'),
                'status' => '0' // 0-> enable
            );
            $this->db->insert('coupon', $data);
        }
        redirect('sanity/coupon');
    }
    
    public function addText() {
        $data['coupontext'] = $this->Crud->retrive_text('coupontext');
        if(empty($data['coupontext'])){
            $data = array(
                    'status' => 'coupontext',
                    'description' => $this->input->post('sampletext'),
                );

            $this->db->insert('settings', $data);
        }else{
            $data = array(
                    'description' => $this->input->post('sampletext'),
                );
            $this->db->where('status','coupontext');
            $this->db->update('settings', $data);
        }
        
        redirect('sanity/coupon');
    }

}
