<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Postproperty Extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('URL', 'DATE', 'URI', 'FORM');
        $this->load->library('form_validation');
        $this->load->helper('custom');
        $this->load->library('upload');
        $this->load->model('Crud');
    }

    public function index() {
        $this->load->view('header');
        // $this->load->view('property_details');

        $property_id = $this->uri->segment(3);
        //-------------------------------------------
        //  print_r( $data['user_res']);
        if (isset($property_id) && $property_id != '') {
            $data['mod'] = '1';
            $this->load->view('property/property_details', $data);
        } else {
            $data['mod'] = '0';
            $this->load->view('property/property_details', $data);
        }
        //-------------------------------------------
        $this->load->view('footer');
    }

    public function prefrnces() {
        $this->load->view('header');
        // $this->load->view('property_details');

        $property_id = $this->uri->segment(3);
        //-------------------------------------------
        //  print_r( $data['user_res']);
        if (isset($property_id) && $property_id != '') {
            $data['mod'] = 'pre';
            $this->load->view('property/property_details', $data);
        } else {
            $data['mod'] = '0';
            $this->load->view('property/property_details', $data);
        }
        //-------------------------------------------
        $this->load->view('footer');
    }

    public function udi() {
        $this->load->view('user_general_info');
    }

    public function loadCityname() {
        $loadType = $_POST['loadType'];
        $loadId = $_POST['loadId'];

        $this->db->select('*');
        $this->db->from('cities');
        $this->db->order_by('city_name', 'asc');

        $query = $this->db->get();
        $HTML = "";
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            foreach ($query->result() as $list) {
                $HTML .= "<option value='" . $list->city_name . "'>" . $list->city_name . "</option>";
            }
            echo $HTML;
        } else {
            // echo "hi";
        }
    }

    public function save_property_info() {
        $this->load->model('Crud');
        $property_type = $this->input->post('list_property');
        $property_id = $this->Crud->save_property_details();

        //--------------------------------------------
        $image1 = $this->input->post('pro_photo');

        $config['upload_path'] = 'assets/Pro_Imgupload';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 10000000;
        $config['max_width'] = 102400;
        $config['max_height'] = 102400;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('pro_photo')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data = array('upload_data' => $this->upload->data());
            $img = $data['upload_data']['file_name'];
            $data = array(
                'pro_photo' => $img
                    // 'pro_id'=> $property_id
            );
            $data_propert_image = $this->Crud->add_prop_image_in_property_details($data, $property_id);
            
            $data1 = array(
                    'photo' => $img,
                    'pro_id' => $property_id
                );
            
            $data_propert_image = $this->Crud->add_property_image($data1);
        }

        if (!$this->upload->do_upload('pro_photo_1')) {
            //  $error = array('error' => $this->upload->display_errors());
            if ($this->upload->do_upload('pro_photo')) {
                $data = array('upload_data' => $this->upload->data());
                $data = array(
                    'photo' => $data['upload_data']['file_name'],
                    'pro_id' => $property_id
                );
                $data_propert_image = $this->Crud->add_property_image($data);
            }
        } else {
            $data = array('upload_data' => $this->upload->data());
            $data = array(
                'photo' => $data['upload_data']['file_name'],
                'pro_id' => $property_id
            );
            $data_propert_image = $this->Crud->add_property_image($data);
        }
        if (!$this->upload->do_upload('pro_photo_2')) {
            // $error = array('error' => $this->upload->display_errors());
            if ($this->upload->do_upload('pro_photo')) {
                $data = array('upload_data' => $this->upload->data());
                $data = array(
                    'photo' => $data['upload_data']['file_name'],
                    'pro_id' => $property_id
                );
                $data_propert_image = $this->Crud->add_property_image($data);
            }
        } else {
            $data = array('upload_data' => $this->upload->data());
            $data = array(
                'photo' => $data['upload_data']['file_name'],
                'pro_id' => $property_id
            );
            $data_propert_image = $this->Crud->add_property_image($data);
        }
        if (!$this->upload->do_upload('pro_photo_3')) {
            // $error = array('error' => $this->upload->display_errors());
            if ($this->upload->do_upload('pro_photo')) {
                $data = array('upload_data' => $this->upload->data());
                $data = array(
                    'photo' => $data['upload_data']['file_name'],
                    'pro_id' => $property_id
                );
                $data_propert_image = $this->Crud->add_property_image($data);
            }
        } else {
            $data = array('upload_data' => $this->upload->data());
            $data = array(
                'photo' => $data['upload_data']['file_name'],
                'pro_id' => $property_id
            );
            $data_propert_image = $this->Crud->add_property_image($data);
        }

        //-------------------------------------------------
         if ($property_type == "public") {
            redirect('postproperty/prefrnces/' . $property_id . '/mod');
        } else {
            redirect('home/OwnP');
        }
    }

    public function loadData() {
        $loadType = $_POST['loadType'];
        $loadId = $_POST['loadId'];

        $this->load->model('Crud');
        $result = $this->Crud->getData($loadType, $loadId);
        $HTML = "";

        if ($result->num_rows() > 0) {
            foreach ($result->result() as $list) {
                $HTML .= "<option value='" . $list->id . "'>" . $list->name . "</option>";
            }
        }
        echo $HTML;
    }

    public function getZipcode() {
        $id = $_POST['loadId'];
        $address = $_POST['loadName'];
        if (!empty($address)) {

            $this->load->model('Crud');
            $result = $this->Crud->getDataZipcode($address, $id);
            $HTML = "";
            if ($result->num_rows() > 0) {

//                foreach ($result->result() as $list) {
//                    $HTML .= "<option value='" . $list->zip_id . "'>" . /* $list->subdist_name ." , ". */ $list->pincode . "</option>";
//                }
                $list_id = array();
                $pincode = array();
                $counter = 0;
                foreach ($result->result() as $list) {
                    // $HTML .= "<option value='" . $list->zip_id . "'>" . /* $list->subdist_name ." , ". */ $list->pincode . "</option>";
                    $list_id[$counter] = $list->zip_id;
                    $pincode[$counter] = $list->pincode;
                    $counter++;
                }

                echo(json_encode(array('list_id' => $list_id, 'pincode' => $pincode)));
            }
            //echo $HTML;
        }
    }

    public function save_preference_info() {
        $this->load->model('Crud');
        $this->Crud->save_preference_details();
        redirect('home/OwnP');
    }

    public function formAsPrivate() {
        $pro_id = $this->uri->segment(3);
        $newpro_id = 10;
        // Insert Images of property in to Gallary Table
        
        // Duplicate in Property Table
        $insert = DuplicateMySQLRecord('property_details', 'property_id', $pro_id);
        $updateData = array(
            'property_type' => 'private'
        );
        $this->db->where('property_id', $insert);
        $this->db->update('property_details', $updateData);

        $updateData1 = array(
            'change_status' => '1',
            'primary_id' => $insert
            
        );
        $this->db->where('property_id', $pro_id);
        $this->db->update('property_details', $updateData1);
        
        $insert1 = DuplicateMySQLRecord1('gallery', 'pro_id', $pro_id, $insert);
    

        $this->session->set_flashdata('Success,Successfully Duplicate As Private Form!');
        redirect('home/OwnP');
    }

    public function formAsPublic() {
        $pro_id = $this->uri->segment(3);
        
        $this->db->where('primary_id', $pro_id);
        $query = $this->db->get("property_details");
        $previous = $query->result_array();

        $this->db->where("FIND_IN_SET('" . $pro_id . "',`own_property`) !=", 0);
        $query = $this->db->get("applyproperty_private");
        $data['applyproperty_privateRes'] = $query->result_array();

        if (!empty($data['applyproperty_privateRes'])) {
            $this->session->set_flashdata('Error', '2123');
            redirect('home/message');
        } else {
            if(!empty($previous)){
                $insert1 = DuplicateMySQLRecord('property_details', 'property_id', $pro_id);
                $updateData = array(
                    'property_type' => 'public',
                );
                $this->db->where('property_id', $insert1);
                $this->db->update('property_details', $updateData);

                $updateData1 = array(
                    'change_status' => '1',
                    'primary_id' => $insert1

                );
                $this->db->where('property_id', $pro_id);
                $this->db->update('property_details', $updateData1);

                DuplicateMySQLRecord1('gallery', 'pro_id', $pro_id, $insert1);
                
                $this->session->set_flashdata('success', 'Success,Successfully Duplicate As Public Form!');
                redirect('home/OwnP');
                
            }else{
                $this->session->set_flashdata('Error,You dnt have prevoius public property!');
                redirect('home/OwnP');
            }
            
            
        }
    }

    public function myCheckOut() {
        $user_id = $this->session->userdata('user_id');
        if (isset($user_id)) {
//            if(!empty($_POST['checkbox1'])){
//                $api_ids = $_POST['checkbox1']; 
//                $string_version = implode(',', $api_ids);
//                $subQuery = "app_id IN (".$string_version.")";
//                $data['ApplyProRes'] = $this->Crud->get_applyPro_notPaid_id($subQuery);
//            }else{
            $data['ApplyProRes'] = $this->Crud->get_applyPro_notPaid();
//            }  

            $data['type1'] = $this->Crud->retrive_type('type1');
            $data['type2'] = $this->Crud->retrive_type('type2');
            $data['type3'] = $this->Crud->retrive_type('type3');
            $data['coupontext'] = $this->Crud->retrive_text('coupontext');

            $this->load->view('header');
            $this->load->view('user/checkout', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('error', 'Error,Only logged in user access this page!');
            redirect('user/signIn');
        }
    }

    public function get_value() {
        $code = $this->input->post('code');
        $user_id = $this->session->userdata('user_id');
        $this->db->where("code", $code);
        $this->db->where("status", '0');
        $query = $this->db->get("coupon");
        $quyREs = $query->result_array();
        if (!empty($quyREs)) {
            echo $quyREs[0]['frequency'];
        }
    }
    
    public function send_autoMsg() {
//        SELECT * FROM table WHERE DATEDIFF(NOW(), duedate) < 2
        $sql = "SELECT * FROM bookings " .
            "WHERE DATE(date) > DATE(NOW()) " .
            "AND dateofquote != '' " .
            "AND email != '' " .
            "AND confirmed = 0";
        $result = mysql_query($sql);
        $num_rows = mysql_numrows($result);
        $today = date('Y-m-d');
        $count = 2;
        if($num_rows){
            while($row = mysql_fetch_assoc($result)){
                $date_of_quote = $row['dateofquote'];
                $datetime1 = new DateTime($today);
                $datetime2 = new DateTime($date_of_quote);
                $interval = $datetime1->diff($datetime2);
                $diff = $interval->format('%a');
                if($diff == '2'){
                    // send email
                } else {
                    echo 'something went wrong';
                }
            }
        }
    }
}
