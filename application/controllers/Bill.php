<?php
/**
 * Created by PhpStorm.
 * User: Renuka Construction
 * Date: 08-01-2018
 * Time: AM 11:03
 */
defined('BASEPATH') OR exit('No direct script access allowed');

Class Bill Extends CI_Controller
{

    public $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('URL', 'DATE', 'URI', 'FORM');
        $this->load->library('form_validation');
        $this->load->helper('custom');
        $this->load->library('upload');
        $this->load->model('Crud');
    }

    public function index()
    {
        $this->load->view('header');
        $this->load->view('bill_details_1');
        $this->load->view('footer');
    }
}