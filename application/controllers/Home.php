<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('URL', 'DATE', 'URI', 'FORM');
        $this->load->library('form_validation');
        $this->load->helper('custom');
        $this->load->library('upload');
        $this->load->model('Crud');
        $this->load->model('MAutocomplete');
    }

    public function index() {
//        print_r($_SESSION);
        $data['ProRes'] = $this->Crud->get_products();
        $this->load->view('header');
        $this->load->view('home', $data);
        $this->load->view('footer');
    }

    public function about_us() {
        $this->load->view('header');
        $this->load->view('footer_page/about_us');
        $this->load->view('footer');
    }

    public function contact_us() {
        $this->load->view('header');
        $this->load->view('footer_page/about_us');
        $this->load->view('footer');
    }

    public function faq() {
        $this->load->view('header');
        $this->load->view('footer_page/faqone');
        $this->load->view('footer');
    }

    public function verify_txno() {
        $txn_no = $this->input->post('txn_no');
        $this->db->where('txno', $txn_no);
        $this->db->limit(1);
        $query = $this->db->get('transactions');
        $this->db->last_query();
        $data['TranRes'] = $query->result_array();

        header('Content-Type: application/json');
        $success = '';
        if (empty($data)) {
            $success = '0';
            $data = array('success' => $success, 'result' => '0');
        } else {
            $success = '1';
            $data = array('success' => $success, 'result' => $data['TranRes'][0]['amount']);
        }
        echo json_encode($data);
    }

    public function insert_txno() {
        $user_id = $this->session->userdata('user_id');
        $txnid = $this->input->post('txnid');
        $amount = $this->input->post('amount');
        $insertDate = array(
            'user_id' => $user_id,
            'pro_status' => '1',
            'amount' => $amount,
            'tid' => $txnid
        );
        $this->db->insert('invest_product', $insertDate);
        $newdata = array(
            'txnid' => $txnid,
            'txn_status' => '1'
        );
        $this->session->set_userdata($newdata);
    }

    public function insert_investPro() {
        $user_id = $this->session->userdata('user_id');
        $txnid = $this->session->userdata('txnid');
        $pro_id = $this->input->post('pro_id');
        $invest_pro = $this->input->post('invest_pro');

        $this->db->where('tid', $txnid);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('invest_product');
        $check_pro = $query->result_array();
        if (isset($check_pro[0]['pro_id']) && $check_pro[0]['pro_id'] == '0') {
            $updateData = array(
                'pro_id' => $pro_id
            );
            $this->db->where('tid', $txnid);
            $this->db->update('invest_product', $updateData);
        } else {
            $insertDate = array(
                'user_id' => $user_id,
                'pro_status' => '1',
                'amount' => $check_pro[0]['amount'],
                'tid' => $txnid,
                'pro_id' => $pro_id
            );
            $this->db->insert('invest_product', $insertDate);
        }
    }

    public function delete_investPro() {
        $user_id = $this->session->userdata('user_id');
        $pro_id = $this->session->userdata('pro_id');
        $delete_invest_pro = $this->input->post('delete_invest_pro');
        $index = $this->input->post('index');
        $updateData = array(
            'pro_status' => '2'
        );
        $this->db->where('ip_id', $index);
        $this->db->update('invest_product', $updateData);
    }

    public function paynow() {
        $this->load->library('user_agent');
        $url = $_SERVER['HTTP_REFERER'];
        $checkurl = base_url() . 'user/myaccount';
        if ($url == $checkurl) {
            $this->session->set_flashdata('error', 'Please send a request as the following seletion has expired');
            redirect('home/message');
        }
    }

    public function message() {

        $user_id = $this->session->userdata('user_id');
        if (isset($user_id)) {
            $data = array(
                'msg_status' => '1'
            );
            $this->db->where("msg_status", '0');
            $this->db->where("user_id", $user_id);
            $this->db->update('message', $data);

            $this->load->view('header');

            $result['msgList_admin'] = $this->Crud->msgList_admin($user_id);
            $result['msgList'] = $this->Crud->msgList($user_id);

            $this->load->view('user/message', $result);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('error', 'Error,Only logged in user access this page!');
            redirect('user/signIn');
        }
    }

    public function sent_msg_admin() {
        $user_id = $this->session->userdata('user_id');
        $data = array(
            'user_id' => $user_id,
            'msg_type' => $this->input->post('msg_type'),
            'message' => $this->input->post('message'),
            'created_date' => date('Y-m-d H:i:s'),
            'msg_status' => '5' // 0-> admin message
        );

        $this->db->insert('message', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            redirect("home/message");
        }
    }

//    public function RoI() {
//        $user_id = $this->session->userdata('user_id');
//        if (isset($user_id)) {
//            
//            $this->db->where("user_id", $user_id);
//            $query = $this->db->get('roi_products');
//            $data['ProRes'] = $query->result_array();
//            $this->load->view('header');
//
////            $result['msgList_admin'] = $this->Crud->msgList_admin($user_id);
//            $result['msgList'] = $this->Crud->msgList($user_id);
//            $this->load->view('user/RoI', $data);
//            $this->load->view('footer');
//        }
//    }

    public function proDetail() {
        $pro_id = $this->uri->segment(3);
        $this->db->where('property_id', $pro_id);
        $this->db->where('property_type', 'public');
        $query = $this->db->get("property_details");
        $data['ProRes'] = $query->result_array();

        $this->db->where('user_id', $data['ProRes'][0]['user_id']);
        $query = $this->db->get("users");
        $data['UserRes'] = $query->result_array();

        $this->db->where('property_id', $pro_id);
        $query = $this->db->get("preference_details");
        $data['PreferRes'] = $query->result_array();

        $this->db->where('pro_id', $pro_id);
        $query = $this->db->get("gallery");
        $data['GalleryListRes'] = $query->result_array();

        $this->db->where('pro_id', $pro_id);
        $query = $this->db->get("message");

        $data['MsgRes'] = $query->result_array();
        $data['MsgResN'] = $query->num_rows();
        $data['pro_id'] = $pro_id;

        $this->load->view('header');
        $this->load->view('property/pro_details', $data);
        $this->load->view('footer');
    }

    public function privateDetail() {
        $pro_id = $this->uri->segment(3);
        $this->db->where('property_id', $pro_id);
        // $this->db->where('user_type','private');        
        $query = $this->db->get("property_details");
        $data['ProRes'] = $query->result_array();

        if (!empty($data['ProRes'])) {
            $this->db->where('user_id', $data['ProRes'][0]['user_id']);
            $query = $this->db->get("users");
            $data['UserRes'] = $query->result_array();
        }
        $this->db->where('property_id', $pro_id);
        $query = $this->db->get("preference_details");
        $data['PreferRes'] = $query->result_array();

        $this->db->where('pro_id', $pro_id);
        $query = $this->db->get("gallery");
        $data['GalleryListRes'] = $query->result_array();

        $this->db->where('pro_id', $pro_id);
        $query = $this->db->get("message");

        $data['MsgRes'] = $query->result_array();
        $data['MsgResN'] = $query->num_rows();
        $data['pro_id'] = $pro_id;

        $this->load->view('header');
        $this->load->view('property/privateDetail', $data);
        $this->load->view('footer');
    }

    public function recomproDetail() {
        $pro_id = $this->uri->segment(3);
        $public_id = $this->uri->segment(4);
        $public_property_id = $this->uri->segment(5);

        $this->db->where('property_id', $pro_id);
        $this->db->where('public_property_id', $public_property_id);
        $query = $this->db->get("recom_property_details");
        $data['ProRes'] = $query->result_array();

        $this->db->where('property_id', $pro_id);
        $query = $this->db->get("property_details");
        $data['MainProRes'] = $query->result_array();

        $this->db->where("FIND_IN_SET('" . $pro_id . "',`own_property`) !=", 0);
        $this->db->where('proper_id', $public_property_id);
        $this->db->where('user_public_id', $public_id);
        $query = $this->db->get("applyproperty_private");
        $data['applyproperty_privateRes'] = $query->result_array();

        $this->db->where('user_id', $data['ProRes'][0]['user_id']);
        $query = $this->db->get("users");
        $data['UserRes'] = $query->result_array();

        $query = $this->db->get("preference_details");
        $data['PreferRes'] = $query->result_array();

        $this->db->where('pro_id', $pro_id);
        $query = $this->db->get("gallery");
        $data['GalleryListRes'] = $query->result_array();

        $this->db->where('pro_id', $pro_id);
        $query = $this->db->get("message");
        $data['MsgRes'] = $query->result_array();
        $data['MsgResN'] = $query->num_rows();

        $this->load->view('header');
        $this->load->view('property/Recom_Pro_detail', $data);
        $this->load->view('footer');
    }

    public function dosearch() {
        // process posted form data  
        $keyword = $this->input->post('term');
        $data['response'] = 'false'; //Set default response  
        $query = $this->MAutocomplete->lookup($keyword); //Search DB  
        if (!empty($query)) {
            $data['response'] = 'true'; //Set response  
            $data['message'] = array(); //Create array  
            foreach ($query as $row) {
                $data['message'][] = array(
                    'id' => $row->sc_id,
                    'value' => $row->name,
                    ''
                );  //Add a row to array  
            }
        }
        if ('IS_AJAX') {
            echo json_encode($data); //echo json string if ajax request  
        } else {
            $this->load->view('show', $data); //Load html view of search results  
        }
    }

    public function searchData() {
        if ($this->input->post('landrent') != '') {
            $this->landrATE();
        } else {
            $this->search_result_data();
        }
    }

    public function landrATE() {
        $this->load->view('header');
        $this->load->view('property/landrent');
        $this->load->view('footer');
    }

    public function ROIP() {
        $user_id = $this->session->userdata('user_id');
        if (isset($user_id)) {
            if ($this->uri->segment(4) != '') {
                $data['proper_id'] = $this->uri->segment(3);
                $data['user_public_id'] = $this->uri->segment(4);
                $this->db->where('user_id', $user_id);
                $this->db->where('property_type', 'private');
                $this->db->where("primary_id", '0');
                $query = $this->db->get("property_details");
                $data['ProperRes'] = $query->result_array();
            } else {
                $this->db->where('user_id', $user_id);
                $this->db->where('property_type', 'private');
                $this->db->where("primary_id", '0');
                $query = $this->db->get("property_details");
                $data['ProRes'] = $query->result_array();

                $this->db->where('user_id', $user_id);
                $this->db->where('property_type', 'public');
                $query = $this->db->get("property_details");
                $data['ProResPublic'] = $query->result_array();
            }
            $this->load->view('header');
            $this->load->view('roip', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('error', 'Error,Only logged in user access this page!');
            redirect('user/signIn');
        }
    }

    public function ROI() {
        $user_id = $this->session->userdata('user_id');
        if (isset($user_id)) {

            $this->db->where('user_public_id', $user_id);
            $this->db->where("status", '2');
            $this->db->order_by('is_payment', 'DESC');
            $query = $this->db->get("applyproperty_private");
            $data['PrivateUsers'] = $query->result_array();


            $this->db->where('user_private_id', $user_id);
            $this->db->where("status", '2');
            $this->db->order_by('is_payment', 'DESC');
            $query = $this->db->get("applyproperty_private");
            $data['PublicUsers'] = $query->result_array();


            $this->load->view('header');
            $this->load->view('roi', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('error', 'Error,Only logged in user access this page!');
            redirect('user/signIn');
        }
    }

    public function OwnP() {
        $user_id = $this->session->userdata('user_id');
        if (isset($user_id)) {
            $this->db->where('user_id', $user_id);
            $this->db->where('property_type', 'private');
            $this->db->where("primary_id", '0');
            $query = $this->db->get("property_details");
            $data['ProResPrivate'] = $query->result_array();

            $this->db->where('user_id', $user_id);
            $this->db->where('property_type', 'public');
            $this->db->where("primary_id", '0');
            $query = $this->db->get("property_details");
            $data['ProResPublic'] = $query->result_array();

            $this->load->view('header');
            $this->load->view('ownp', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('error', 'Error,Only logged in user access this page!');
            redirect('user/signIn');
        }
    }

    public function insert_applyIds() {
        //print_r($_POST);
        $user_id = $this->session->userdata('user_id');
        $property_ids = $this->input->post('properids');
        $proper_id = $this->input->post('properid');
        $user_public_id = $this->input->post('user_public_id');
//        $property_idsArr = explode(",", $property_ids);
//        for ($i = 0; $i < count($property_idsArr); $i++) {
        $insertDate = array(
            'user_private_id' => $user_id,
            'proper_id' => $proper_id,
            'user_public_id' => $user_public_id,
            'own_property' => $property_ids,
            'created_date' => date('Y-m-d H:i:s'),
            'updated_date' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('applyproperty_private', $insertDate);
//        }
        $insert_id = $this->db->insert_id();
        if (isset($insert_id)) {
            $url =  base_url() . "home/ROI";
            $url_full = "<a href='".$url."'>$url</a>";
            sent_msg_admin($user_public_id, "Applied User's Message", $url_full, $proper_id);
            redirect();
        }
    }

    public function rating() {
//        print_r($_GET);//
        $rate = $_GET['rating'];
        $rpd_id = $_GET['rpd_id'];
        $data = array(
            'rating' => $rate
        );
        $this->db->where("rpd_id", $rpd_id);
        $q = $this->db->update('recom_property_details', $data);
        if ($q) {
            $success = '1';
            $data = array('success' => $success, 'result' => 'success');
        }
        echo json_encode($data);
    }

    public function udi() {
        $user_id = $this->session->userdata('user_id');
        if ($user_id) {
            $this->load->view('header');

            //get user info
            $this->db->where("user_id", $user_id);
            $query = $this->db->get("users");
            $resotp = $query->result_array();
            $data['otp'] = $resotp;
            if (!empty($resotp) && $resotp[0]['verified_mobile'] != 'verified') {
                $res = sendOTP($resotp[0]['contact_no']);
                if ($res['message'] != '') {
                    $data = array(
                        'otp' => $res['otp'],
                        'verified_mobile' => 'sent',
                        'otp_msgid' => $res['message']
                    );
                    $this->db->where('user_id', $user_id);
                    $this->db->update('users', $data);
                }
//                $data['otpverify'] = '1';
            } else {
                $data['otpverify'] = '0';
            }
            $resotp[0]['contact_no'];
            $data['profile'] = $this->Crud->retrive_user($user_id);
//            $data['subformList'] = $this->Crud->subformList($user_id);
            $data['bankinfo'] = $this->Crud->retrive_bankinfo($user_id);
            $data['contact'] = $resotp[0]['contact_no'];
            $this->load->view('user_general_info', $data);
            // 	$data['otpverify'] = '0';
            //	$this->load->view('user_general_info',$data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('error', 'Error, Only Logged in user access that page! ');
            redirect('user/sign');
        }
    }

    public function verifyOtpForAccount() {
//    print_r($_POST);
        $user_id = $this->session->userdata('user_id');
        $user_mobile = $this->input->post('contact');
        $user_otp = $this->input->post('user_otp');
        $this->db->where("otp", $user_otp);
        $query = $this->db->get("users");
        //print_r()
        if ($query->num_rows() == '1') {
            $data = array(
                'verified_mobile' => 'verified',
                'contact_no' => $user_mobile,
            );
            $this->db->where('user_id', $user_id);
            $data['otpverify'] = $this->db->update('users', $data);
            $this->session->set_flashdata('success', 'Successfully verified.!');
            redirect('home/udi');
        } else {
            $data = array(
                'otp' => "0",
                'verified_mobile' => 'not_verified',
                'otp_msgid' => ""
            );
            $this->db->where('user_id', $user_id);
            $data['otpverify'] = $this->db->update('users', $data);
            $this->session->set_flashdata('error', 'Wrong OTP, Please Refersh Page and Try again.!');
            redirect('home/udi');
        }
    }

    public function UpdateAccount() {
        $user_id = $this->session->userdata('user_id');
        $pwd = $this->input->post('pass');
        $data = array(
            'user_password' => md5($pwd)
        );
        $this->db->where('user_id', $user_id);
        $this->db->update('users', $data);
        $this->session->set_flashdata('success', 'Your New Password has been updated!');
        redirect('user/logout');
    }

    public function applyFormInsertUpdateAccount() {

        $user_id = $this->session->userdata('user_id');
        $data = array(
            'otp' => '0',
            'verified_mobile' => '',
            'otp_msgid' => ''
        );
        $this->db->where('user_id', $user_id);
        $data['otpverify'] = $this->db->update('users', $data);
        $this->load->view('header');
        //get user info
        $this->db->where("user_id", $user_id);
        $query = $this->db->get("users");
        $resotp = $query->result_array();
        $cno = $this->input->post('contact_no');
        if (!empty($resotp) && $resotp[0]['verified_mobile'] != 'verified') {
            $res = sendOTP($this->input->post('contact_no'));
            if ($res['message'] != '') {
                $data = array(
                    'otp' => $res['otp'],
                    'verified_mobile' => 'sent',
                    'otp_msgid' => $res['message']
                );
                $this->db->where('user_id', $user_id);
                $data['otpverify'] = $this->db->update('users', $data);
            }
        } else {
            $data['otpverify'] = '0';
        }
        $data['profile'] = $this->Crud->retrive_user($user_id);
        //$data['subformList'] = $this->Crud->subformList($user_id);
        $data['bankinfo'] = $this->Crud->retrive_bankinfo($user_id);
        $data['contact'] = $cno;
        $this->load->view('user_general_info', $data);
        $this->load->view('footer');
    }

    public function cart() {
        $user_id = $this->session->userdata('user_id');
        if (isset($user_id)) {
//            $this->db->where("user_id", $user_id);
            $this->db->select('*,c.created_date as date');
            $this->db->from('cart c');
            $this->db->join('property_details p', 'p.property_id = c.property_id', 'left');
            $this->db->where("c.user_id", $user_id);
            $this->db->order_by('c.cart_id', 'DESC');
            $query = $this->db->get();
            $data['cartRes'] = $query->result_array();
            $this->load->view('header');
            $this->load->view('cart', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('error', 'Error, Only Logged in user access that page! ');
            redirect('user/sign');
        }
    }

    public function get_city_of_property_details() {
        $keyword = $this->input->post('term');
        $data['response'] = 'false'; //Set default response
        $query = $this->MAutocomplete->get_city_of_property($keyword); //Search DB
        if (!empty($query)) {
            $data['response'] = 'true'; //Set response
            $data['message'] = array(); //Create array
            foreach ($query as $row) {
                $data['message'][] = array(
                    'id' => $row->city,
                    'value' => $row->city,
                    ''
                );  //Add a row to array
            }
        }
        if ('IS_AJAX') {
            echo json_encode($data); //echo json string if ajax request
        } else {
            $this->load->view('show', $data); //Load html view of search results
        }
    }

    public function search_result_data() {
        $s_city = $this->input->post('keyword');
        $r_type = array();
        $r_type = $this->input->post('r_type');
        $c_type = array();
        $c_type = $this->input->post('c_type');
        $r_result = array();
        if (count($r_type) != 0) {
            $this->db->select('*');
            $this->db->from('property_details');
            $this->db->where('city', $s_city);
            $this->db->where('property_type', 'public');
            $this->db->where_in('residential_type', $r_type);
            // $this->db->where_in('commercial_type',$c_type);
            $query = $this->db->get();
            $r_result = $query->result_array();
        }
        $c_result = array();
        if (count($c_type) != 0) {
            $this->db->select('*');
            $this->db->from('property_details');
            $this->db->where('city', $s_city);
            $this->db->where('property_type', 'public');
            $this->db->where_in('commercial_type', $c_type);
            $query1 = $this->db->get();
            $c_result = $query1->result_array();
        }
        $res_array = array_merge($r_result, $c_result);
//            print_r(count($res_array));
//            echo "</br>";
//            print_r($res_array);

        $data['ProRes'] = $res_array;
        $this->load->view('header');
        $this->load->view('property/search_property', $data);
        $this->load->view('footer');
    }

    public function property_type_info() {
        $property = $_GET['prop'];
        // echo "in propert_info";
        $property_type = $_GET['property_type'];
        if ($property == 'residential') {
            $this->db->select('*');
            $this->db->from('property_details');
            $this->db->where('property_type', 'public');
            $this->db->where('residential_type', $property_type);
            $query1 = $this->db->get();
            $result = $query1->result_array();
            $data['ProRes'] = $result;


            $result = array(
                'view' => $this->load->view('property/search_property', $data),
            );
            $this->output->set_output_type('application/json');
            $this->output->set_output(json_decode($result));
        } else {
            $this->db->select('*');
            $this->db->from('property_details');
            $this->db->where('property_type', 'public');
            $this->db->where('commercial_type', $property_type);
            $query = $this->db->get();
            $result = $query->result_array();
            $data['ProRes'] = $result;
//            $this->load->view('header');
//            $this->load->view('search_property',$data);
//            $this->load->view('footer');
        }
    }

    public function display_search_result($result) {
        $data['ProRes'] = $result;
        $this->load->view('header');
        $this->load->view('property/search_property', $data);
        $this->load->view('footer');
    }

    public function search_custom_property_info() {
        $property = $this->input->post('property_type');
        $property_type = $this->input->post('property_sub_type');
        if ($property == 'residential') {
            $this->db->select('*');
            $this->db->from('property_details');
            $this->db->where('property_type', 'public');
            $this->db->where('residential_type', $property_type);
            $query1 = $this->db->get();
            $result = $query1->result_array();
            $data['ProRes'] = $result;

            $this->load->view('header');
            $this->load->view('property/search_property', $data);
            $this->load->view('footer');
        } else {
            $this->db->select('*');
            $this->db->from('property_details');
            $this->db->where('property_type', 'public');
            $this->db->where('commercial_type', $property_type);
            $query = $this->db->get();
            $result = $query->result_array();
            $data['ProRes'] = $result;
            $this->load->view('header');
            $this->load->view('property/search_property', $data);
            $this->load->view('footer');
        }
    }

}
