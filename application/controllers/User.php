<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('URL', 'DATE', 'URI', 'FORM');
        $this->load->library('form_validation');
        $this->load->helper('custom');
        $this->load->library('upload');
        $this->load->model('Crud');
    }

    public function index() {
        $this->load->view('header');
        $this->load->view('home');
        $this->load->view('footer');
    }

    public function contactus() {
        $user_id = $this->session->userdata('user_id');
        $this->load->view('header');

        $this->db->where("user_id", $user_id);
        $query = $this->db->get("users");
        $data['loggedin'] = $query->result_array();

        $this->load->view('footer_page/contactus', $data);
        $this->load->view('footer');
    }

    public function check_user() {


        $this->load->view('header');
        $this->load->view('user/check_user');
        $this->load->view('footer');
    }

    public function register() {
        if ($this->input->post('user_type')) {
            $data['user_type'] = $this->input->post('user_type');
            $this->load->view('header');
            $this->load->view('user/register', $data);
            $this->load->view('footer');
        } else {
            $user_type = $this->check_user();
        }
    }

    public function registration() {
        $this->load->library('form_validation');
        // field name, error message, validation rules
//        $this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
//        $this->form_validation->set_rules('first_name', 'Firstname', 'trim|required');
//        $this->form_validation->set_rules('last_name', 'Lastname', 'trim|required');
//        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
//        $this->form_validation->set_rules('contact_no', 'mobileNumber', 'required|regex_match[/^[0-9]{10}$/]');
//        $this->form_validation->set_rules('company_register', 'Firstname', 'trim|required');
//        $this->form_validation->set_rules('user_password', 'Password', 'trim|min_length[4]|max_length[12]');
//        $this->form_validation->set_rules('confirm_password', 'ConfirmPassword', 'trim|matches[user_password]');
//        if ($this->form_validation->run() == FALSE) {
//            print_r(validation_errors());
//            exit();
//            $this->index();
//        } else {
        $this->load->model('Crud');
        $insertid = $this->Crud->register();
        $this->session->set_flashdata('success', 'Success, Registration completed Successfully ! ');
        $submit = $this->input->post('submit');
        if ($submit == 'submit') {
            redirect('user/signIn/' . $insertid);
        } else {
            redirect('user/accDetails/' . $insertid);
        }
        // $this->load->view('thank');
        //  		//$this->load->site_url('');
//        }
    }

    public function signIn() {
        if ($this->input->post('signin')) {
            $user_name = $this->input->post('email');
            $user_password = $this->input->post('password');
            $where = "user_name ='" . $user_name . "' AND user_password='" . md5($user_password) . "' AND status='Active'";
            $data['UserRes'] = $this->Crud->login($where);
            $user_id = $this->session->userdata('user_id');
            if (!empty($data['UserRes'])) {
                $this->session->set_flashdata('success', 'Success, Logged In Successfully ');
                $this->load->helper('URL');
                redirect();
            } else {
                $this->session->set_flashdata('error', 'Error,Your username and password are wrong, please try again');
                redirect('user/signIn');
            }
        } else {
            if ($this->session->userdata('user_id') != '') {
                redirect();
            } else {
                $this->load->view('header');
                $this->load->view('user/signin');
                $this->load->view('footer');
            }
        }
    }

    public function accDetails() {
        $user_id = $this->session->userdata('user_id');
        if(isset($user_id) && $user_id != ''){
            if ($this->uri->segment(3) != '') {
                $edit_id = $this->uri->segment(3);
                $data['edit_id'] = $edit_id;
            }


            $data['user_res'] = $this->Crud->retrive_user($user_id);
            $this->load->view('header');
            if (!empty($data['user_res']) && $data['user_res'][0]['verified_mobile'] != 'verified') {
                $res = sendOTP($data['user_res'][0]['contact_no']);
                $data1 = array(
                    'otp' => $res['otp'],
                    'verified_mobile' => 'sent',
                    'otp_msgid' => $res['message']
                );
                $this->db->where('user_id', $user_id);
                $data['otpverify'] = $this->db->update('users', $data1);
                $this->load->view('user/accDetails', $data);
            } else {
                $data['otpverify'] = '0';
                $this->db->where("user_id", $user_id);
                $query = $this->db->get("bank_details");
                $data['query'] = $query->result_array();

                $this->load->view('user/accDetails', $data);
            }
            $this->load->view('footer');
        }else{
            $this->session->set_flashdata('error', 'Error,Only logged in user access this page!');
            redirect('user/signIn');
        }
    }

    public function updateBankinfo() {
        $user_id = $this->session->userdata('user_id');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('bank_acc_no', 'Bank Account Number ', 'trim|required|numeric|min_length[11]');

        $this->form_validation->set_rules('bank_holder_name', 'Bank Holder Name ', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            redirect('user/bank_detail');
        } else {
            $data = array(
                'bank_acc_no' => $this->input->post('bank_acc_no'),
                'bank_holder_name' => $this->input->post('bank_holder_name'),
                'ifsc_code' => $this->input->post('ifsc_code'),
                'branch_code' => $this->input->post('branch_code'),
                'status' => '1' // 1-> enable
            );
            $this->db->where("user_id", $user_id);
            $result = $this->db->update('bank_details', $data);
//            $insert_id = $this->db->insert_id();
            if ($result) {
                $this->session->set_flashdata('success', 'Success, Bank Account Details updated Successfully! ');
                redirect("home/udi");
            }
        }
    }

    public function verifyOtp() {
        $user_id = $this->input->post('user_id');
        $ism = $this->input->post('ism');
        $user_mobile = $this->input->post('user_mobile');
        $user_otp = $this->input->post('user_otp');
        $this->db->where("otp", $user_otp);
        $query = $this->db->get("users");
        $this->db->last_query();

        if ($query->num_rows() == '1') {
            $data = array(
                'verified_mobile' => 'verified',
            );
            $this->db->where('user_id', $user_id);
            $data['otpverify'] = $this->db->update('users', $data);
            $this->session->set_flashdata('success', 'Successfully verified, Now You can edit the bank details!');

            redirect('user/accDetails/' . $user_id);
        } else {
            $data = array(
                'otp' => "0",
                'verified_mobile' => 'not_verified',
                'otp_msgid' => ""
            );
            $this->db->where('user_id', $user_id);
            $data['otpverify'] = $this->db->update('users', $data);
            $this->session->set_flashdata('error', 'Wrong OTP, Please Refersh Page and Try again.!');
            redirect('user/accDetails/' . $user_id);
        }
    }

    public function bank_detail_otp() {
        $user_id = $this->session->userdata('user_id');
        $data = array(
            'otp' => '0',
            'verified_mobile' => '',
            'otp_msgid' => ''
        );
        $this->db->where('user_id', $user_id);
        $data['otpverify'] = $this->db->update('users', $data);
        redirect('user/accDetails');
    }

    public function inserBankinfo() {
        $user_id = $this->input->post('user_id');
        $data = array(
            'user_id' => $user_id,
            'bank_acc_no' => $this->input->post('bank_acc_no'),
            'bank_holder_name' => $this->input->post('bank_holder_name'),
            'ifsc_code' => $this->input->post('ifsc_code'),
            'branch_code' => $this->input->post('branch_code'),
            'status' => '1' // 1-> enable
        );

        $this->db->insert('bank_details', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            redirect("user/signIn");
        }
    }

    public function logout() {
        $newdata = array(
            'user_id' => '',
            'user_name' => '',
            'email' => '',
            'logged_in' => FALSE,
        );
        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();
        $this->load->helper('url');
        redirect('user/signIn');
    }

    public function profile() {
        $user_id = $this->session->userdata('user_id');
        $data['user_res'] = $this->Crud->retrive_user($user_id);
        //$data['city'] = $this->Crud->retrive_city($data['user_res'][0]['city_id']);
        //$data['state'] = $this->Crud->retrive_state($data['user_res'][0]['state_id']);
        $this->load->view('header');
        $this->load->view('user/profile', $data);
        $this->load->view('footer');
    }

    public function editprofile() {
        $user_id = $this->session->userdata('user_id');
        $this->load->model('Crud');
        $this->Crud->editprofile($user_id);
        $this->session->set_flashdata('success', 'Success, Your Profile has been updated! ');
        redirect('home/udi');
    }

    public function orders() {
        $user_id = $this->session->userdata('user_id');
        $data['investPro'] = $this->Crud->retrive_investPro($user_id);
        $this->load->view('header');
        $this->load->view('user/order', $data);
        $this->load->view('footer');
    }

    public function setting() {
        $user_id = $this->session->userdata('user_id');
        $data['investPro'] = $this->Crud->retrive_investPro($user_id);
        $this->load->view('header');
        $this->load->view('user/setting', $data);
        $this->load->view('footer');
    }

    public function updatePassword() {
        $user_id = $this->session->userdata('user_id');
        $pass = $this->input->post('pass');
        $updateData = array(
            'password' => md5($pass)
        );
        $this->db->where('user_id', $user_id);
        $this->db->update('users', $updateData);
        redirect('user/logout');
    }

    public function myaccount() {
        $user_id = $this->session->userdata('user_id');
        if (isset($user_id)) {
            $data['ApplyProRes'] = $this->Crud->get_applyPro();

            $this->load->view('header');
            $this->load->view('user/myacc', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('error', 'Error,Only logged in user access this page!');
            redirect('user/signIn');
        }
    }

    public function user_name_exists() {
        $user_name = $this->input->post('user_name');
        $this->db->where('user_name', $user_name);
        $query = $this->db->get('users');
        // print_r($query);
        if ($query->num_rows() == '1') {
            // $this->form_validation->set_message('username_check', 'Sorry, This username is already used by another user please select another one');
            //return FALSE;
            echo 'exist';
        } else {
            //return TRUE;
            echo 'not_exist';
        }
    }

    public function upload_pic() {
        $data = $_POST['image'];

        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);

        $data = base64_decode($data);
//$imageName = time().'.png';
        $imageName = time() . "abcd.png";
        file_put_contents('assets/profile_images/' . $imageName, $data);
        echo $imageName;
    }

    public function edit_preference_details() {
        $user_id = $this->session->userdata('user_id');
        $this->load->view('header');
        $this->load->view('property/edit_preference_details');
        $this->load->view('footer');
    }

    public function editpreferences() {
        $property_id = $this->input->post('property_id');
        $user_id = $this->session->userdata('user_id');
        $this->load->model('Crud');
        $this->Crud->editpreference_details($user_id, $property_id);
        //  $this->session->set_flashdata('success', 'Success, Your Profile has been updated! ');
        redirect('home/proDetail/' . $property_id);
    }

    public function edit_property_images() {
        $user_id = $this->session->userdata('user_id');
        $this->load->view('header');
        $this->load->view('property/edit_property_images');
        $this->load->view('footer');
    }

    public function update_property_images() {
        $property_id = $this->input->post('property_id');
        $property_type = $this->input->post('property_type');
        $pro_photo_1_id = $this->input->post('pro_photo_1_id');
        $pro_photo_2_id = $this->input->post('pro_photo_2_id');
        $pro_photo_3_id = $this->input->post('pro_photo_3_id');
        //--------------------------------------------
        $image1 = $this->input->post('pro_photo');

        $config['upload_path'] = 'assets/Pro_Imgupload';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 10000000;
        $config['max_width'] = 102400;
        $config['max_height'] = 102400;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('pro_photo')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data = array('upload_data' => $this->upload->data());
            $data = array(
                'pro_photo' => $data['upload_data']['file_name']
                    // 'pro_id'=> $property_id
            );
            $data_propert_image = $this->Crud->add_prop_image_in_property_details($data, $property_id);
        }
        if (!$this->upload->do_upload('pro_photo_1')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data = array('upload_data' => $this->upload->data());
            $data = array(
                'photo' => $data['upload_data']['file_name']
                    // 'pro_id'=> $property_id
            );
            $data_propert_image = $this->Crud->update_prop_image_in_gallery($data, $property_id, $pro_photo_1_id);
        }
        if (!$this->upload->do_upload('pro_photo_2')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data = array('upload_data' => $this->upload->data());
            $data = array(
                'photo' => $data['upload_data']['file_name']
                    // 'pro_id'=> $property_id
            );
            $data_propert_image = $this->Crud->update_prop_image_in_gallery($data, $property_id, $pro_photo_2_id);
        }
        if (!$this->upload->do_upload('pro_photo_3')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data = array('upload_data' => $this->upload->data());
            $data = array(
                'photo' => $data['upload_data']['file_name']
                    // 'pro_id'=> $property_id
            );
            $data_propert_image = $this->Crud->update_prop_image_in_gallery($data, $property_id, $pro_photo_3_id);
        }
        if ($property_type == "public") {
            redirect('home/proDetail/' . $property_id);
        } else {
            redirect('home/privateDetail/' . $property_id);
        }
    }

}

?>