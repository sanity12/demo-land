<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/theme-script.js"></script>
<style>
    .star-rating {
        line-height:32px;
        font-size:1.25em;
    }
    .rate .fa-star{color: #d0d04e;}
    .star-rating .fa-star{color: #d0d04e;}
    .star-rating1 .fa-star{color: #d0d04e;}
</style>
<section id="product-details" style="background-color: #f0f0f0;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>            
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li class="active">
                        <?php if($applyproperty_privateRes[0]['is_payment'] == '0'){?>
                            <?php echo html_entity_decode($ProRes[0]['project_name']); ?>
                        <?php }else{ ?>
                            <?php echo html_entity_decode($MainProRes[0]['project_name']); ?>
                        <?php } ?>
                        
                    </li>
                </ul>
            </div>
            <?php if($applyproperty_privateRes[0]['is_payment'] == '0'){?>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="product-image">
                    <div class="product-full">
                        <img id="product-zoom" src='<?php echo base_url(); ?>/assets/Pro_Imgupload/land.jpg' 
                             class="img-responsive set-img-imp" data-zoom-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/land.jpg"/>
                    </div>
                    <div class="product-img-thumb" id="gallery_02">
                        <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="false">
                            <li>
                                <a class="set-img-imp-details" href="#" data-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/land.jpg" data-zoom-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/land.jpg">
                                    <img id="product-zoom"  src="<?php echo base_url(); ?>/assets/Pro_Imgupload/land.jpg" /> 
                                </a>
                            </li>
                            <li>
                                <a class="set-img-imp-details" href="#" data-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/land.jpg" data-zoom-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/land.jpg">
                                    <img id="product-zoom"  src="<?php echo base_url(); ?>/assets/Pro_Imgupload/land.jpg" /> 
                                </a>
                            </li>
                            <li>
                                <a class="set-img-imp-details" href="#" data-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/land.jpg" data-zoom-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/land.jpg">
                                    <img id="product-zoom"  src="<?php echo base_url(); ?>/assets/Pro_Imgupload/land.jpg" /> 
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php }else{ ?>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="product-image">
                    <div class="product-full">
                        <img id="product-zoom" src='<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRes[0]['pro_photo']; ?>' 
                              class="img-responsive set-img-imp" data-zoom-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRes[0]['pro_photo']; ?>"/>
                     </div>
                     <div class="product-img-thumb" id="gallery_02">
                         <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="false">
                                     <?php
                         //                            print_r($GalleryListRes);exit;
                         //                                $GalleryList = mysqli_query($con, "SELECT * FROM gallery WHERE pro_id='" . $_SESSION['usr_pro'] . "'");
                                     foreach ($GalleryListRes as $GalleryListRe) {
                                         ?>
                                         <li>
                                             <a class="set-img-imp-details" href="#" data-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $GalleryListRe['photo']; ?>" data-zoom-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $GalleryListRe['photo']; ?>">
                                                 <img id="product-zoom"  src="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $GalleryListRe['photo']; ?>" /> 
                                             </a>
                                         </li>
                             <?php } ?>
                         </ul>
                    </div>
                </div>
                <?php if($applyproperty_privateRes[0]['is_payment'] == '1'){
                    if($ProRes[0]['rating'] != '0'){?>               
                                <div class="star-rating rate" style="font-size: x-large;" >
                                    <span class="fa fa-star-o" data-rating="1"></span>                                                                
                                    <span class="fa fa-star-o" data-rating="2"></span>
                                    <span class="fa fa-star-o" data-rating="3"></span>
                                    <span class="fa fa-star-o" data-rating="4"></span>
                                    <span class="fa fa-star-o" data-rating="5"></span>
                                    <input type="hidden" name="rating" class="rating-value" value="<?php echo $ProRes[0]['rating'];?>">
                                </div>
                    <script>
                        var $star_rating = $('.star-rating .fa');
                        var SetRatingStar = function () {
                            return $star_rating.each(function () {
                                if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                                    return $(this).removeClass('fa-star-o').addClass('fa-star');
                                } else {
                                    return $(this).removeClass('fa-star').addClass('fa-star-o');
                                }
                            });
                        };
                    </script>
                    <script> SetRatingStar();</script>
            <?php  } else {  ?>
                            <div class="star-rating rate1" style="font-size: x-large;">
                                <span class="fa fa-star-o" data-rating="1"></span>                                                                
                                <span class="fa fa-star-o" data-rating="2"></span>
                                <span class="fa fa-star-o" data-rating="3"></span>
                                <span class="fa fa-star-o" data-rating="4"></span>
                                <span class="fa fa-star-o" data-rating="5"></span>
                                <input type="hidden" name="rating" class="rating-value" value="">
                                <input type="hidden" name="rpd_id" class="rpd_id" value="<?php echo $ProRes[0]['rpd_id']; ?>" >
                            </div>
                <script>
                    var $star_rating2 = $('.star-rating .fa');

                    var SetRatingStar1 = function () {
                        return $star_rating2.each(function () {
                            if (parseInt($star_rating2.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                                return $(this).removeClass('fa-star-o').addClass('fa-star');
                            } else {
                                return $(this).removeClass('fa-star').addClass('fa-star-o');
                            }
                        });
                    };

                    $star_rating2.on('click', function () {

                        $star_rating2.siblings('input.rating-value').val($(this).data('rating'));

                        SetRatingStar1();

                        $.ajax({
                            url: "<?php echo base_url(); ?>home/rating",
                            // type:post,
                            dataType: "json",
                            data: {rating: $(this).data('rating'), rpd_id: $(".rpd_id").val()},
                            success: function (data) {
                                alert("Rating saved successfully! Thank You");
                            },
                            error: function (e) {
                                // Handle error here
                                console.log(e);
                            },
                            timeout: 30000
                        });
                    });

                    SetRatingStar1();
                </script>
                <?php } } ?>
            </div>            
            <?php } ?>
            
             <?php if($applyproperty_privateRes[0]['is_payment'] == '0'){?>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pdMainFacts type2">
                <table>
                    <tbody>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7">
                                    <img src="<?php echo base_url(); ?>assets/img/Plot_area.jpg" style="width: 25px;height: 25px;">
                                    <span class="areaType">Plot Area(l*b)</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['plot_area']; ?></span></div>
                            </td>
                            &nbsp;
                            <td>
                                <div class="pdFactHead top7"><img src="<?php echo base_url(); ?>assets/img/balcony.png" style="width: 25px;height: 25px;">
                                    <span class="areaType"><?php echo $ProRes[0]['no_of_bedrooms']; ?>BHK <?php echo $ProRes[0]['no_of_balcony']; ?>Balcony, <?php echo $ProRes[0]['no_of_bathrooms']; ?>Bathrooms</span></div>
                                <div class="pdFactHead"><span class="areaType1"></span></div>
                            </td>
                        </tr>
                        <tr class="separator">
                            <td>
                                <div class="pdFactHead top7"><img src="<?php echo base_url(); ?>assets/img/address.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Complete address</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['complete_address']; ?></span></div>
                            </td>
                            <td style="background: #597959">
                                <div class="pdFactHead" style="background: #597959;color: white;"><img src="<?php echo base_url(); ?>assets/img/Water 24-7.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Water(Borewell/Not) 24/7</span></div>
                                <div class="pdFactHead" style="color: white;"><b><span class="areaType1"><?php echo $ProRes[0]['water_borewell']; ?></b></span></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Garden.jpg" style="width: 25px;height: 25px;">
                                    <span class="areaType">Garden ?</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['Garden']; ?></span></div>
                            </td>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Electricity.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Electricity/generator?</span></div>
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['electricity_backup']; ?></span></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Swimming-pool</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['swimming_pool']; ?></span></div>
                            </td>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/1_fast.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Age of property</span></div>
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['age_of_property']; ?></span></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">city</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['city']; ?></span></div>
                            </td>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/1_fast.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">status</span></div>
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['status1']; ?></span></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <?php }else{ ?>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pdMainFacts type2 privateInfo">
                <table>
                    <tbody>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7">
                                    <img src="<?php echo base_url(); ?>assets/img/Plot_area.jpg" style="width: 25px;height: 25px;">
                                    <span class="areaType">Plot Area(l*b)</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $MainProRes[0]['plot_area']; ?></span></div>
                            </td>
                            &nbsp;
                            <td>
                                <div class="pdFactHead top7"><img src="<?php echo base_url(); ?>assets/img/balcony.png" style="width: 25px;height: 25px;">
                                    <span class="areaType"><?php echo $MainProRes[0]['no_of_bedrooms']; ?>BHK <?php echo $MainProRes[0]['no_of_balcony']; ?>Balcony, <?php echo $MainProRes[0]['no_of_bathrooms']; ?>Bathrooms</span></div>
                                <div class="pdFactHead"><span class="areaType1"></span></div>
                            </td>
                        </tr>
                        <tr class="separator">
                            <td>
                                <div class="pdFactHead top7"><img src="<?php echo base_url(); ?>assets/img/address.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Complete address</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $MainProRes[0]['complete_address']; ?></span></div>
                            </td>
                            <td style="background: #597959">
                                <div class="pdFactHead" style="background: #597959;color: white;"><img src="<?php echo base_url(); ?>assets/img/Water 24-7.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Water(Borewell/Not) 24/7</span></div>
                                <div class="pdFactHead" style="color: white;"><b><span class="areaType1"><?php echo $MainProRes[0]['water_borewell']; ?></b></span></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Garden.jpg" style="width: 25px;height: 25px;">
                                    <span class="areaType">Garden ?</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $MainProRes[0]['Garden']; ?></span></div>
                            </td>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Electricity.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Electricity/generator?</span></div>
                                <div class="pdFactHead"><span class="areaType1"><?php echo $MainProRes[0]['electricity_backup']; ?></span></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Swimming-pool</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $MainProRes[0]['swimming_pool']; ?></span></div>
                            </td>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/1_fast.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Age of property</span></div>
                                <div class="pdFactHead"><span class="areaType1"><?php echo $MainProRes[0]['age_of_property']; ?></span></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">city</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $MainProRes[0]['city']; ?></span></div>
                            </td>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/1_fast.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">status</span></div>
                                <div class="pdFactHead"><span class="areaType1"><?php echo $MainProRes[0]['status1']; ?></span></div>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Possession</span></div>
                                <div class="pdFactHead"><span class="areaType1"><?php echo $MainProRes[0]['possession']; ?></span></div>
                            </td>
                            <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <?php } ?>
            
            <?php if($applyproperty_privateRes[0]['is_payment'] == '1'){?>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 pdMainFacts type2" style="margin-top: 15px">
                <?php
                $p_user_id = $applyproperty_privateRes[0]['user_private_id'];
                $CListRes = get_property_user_details($p_user_id);
                ?>
                <div style="background-color:white;padding:5px;">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <img id="product-zoom"  src="<?php echo base_url(); ?>/assets/Profile_images/<?php echo $CListRes[0]['profile_picture']; ?>" />
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4" style="text-align:center; font-size:13px;">
                            <label class="label-control"><?php echo $CListRes[0]['user_status']; ?></label>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4">
                            <span style="font-weight: normal;font-size: 13px;">Name: </span>
                        </div>
                        <div class="col-lg-5 areaType">
                            <span style="font-weight: normal;font-size: 13px;"><?php echo $CListRes[0]['first_name']; ?></span>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4">
                            <span style="font-weight: normal;font-size: 13px;">Website link: </span>
                        </div>
                        <div class="col-lg-5">
                            <span style="font-weight: normal;font-size: 13px;"><a href="<?php echo $CListRes[0]['website_link']; ?>" ><?php echo $CListRes[0]['website_link']; ?></a> </span>
                        </div>
                    </div>
                    </br>
                     <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4">
                            <span style="font-weight: normal;font-size: 13px;">Email: </span>
                        </div>
                        <div class="col-lg-5">
                            <span style="font-weight: normal;font-size: 13px;">	<?php echo $CListRes[0]['email']; ?> </span>
                        </div>
                    </div>
                    </br>
                     <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4">
                            <span style="font-weight: normal;font-size: 13px;">Mobile No: </span>
                        </div>
                        <div class="col-lg-5">
                            <span style="font-weight: normal;font-size: 13px;">	<?php echo $CListRes[0]['contact_no']; ?> </span>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-4">
                            <span style="font-weight: normal;font-size: 13px;">Addtional Description:</span>
                        </div>
                        <div class="col-lg-5">
                            <span style="font-weight: normal;font-size: 13px;">ABC<?php ///echo $CListRes[0]['first_name'];  ?></span>
                        </div>
                    </div>
                    </br>
                </div>
                </div>
                <?php } ?>
            </div>
            <?php if($applyproperty_privateRes[0]['is_payment'] == '0'){?>
                <div class="col-lg-12">
                    <?php
                    date_default_timezone_set('Asia/Kolkata');
                    $CurrDate = date('Y-m-d H:i:s');

                    $date = new DateTime($ProRes[0]['deal_end_date']);
                    $sdate1 = $date->format('M d, Y H:i:s');

                    $date = new DateTime($ProRes[0]['created_date']);
                    $credate1 = $date->format('M d, Y H:i:s');
                    ?>
                </div>
                <div class="col-lg-12">
                    <h4><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;<strong>Deal Counter</strong></h4>
                    <div id="clockdiv" style="float: left">
                        <div>
                            <span class="days"></span>
                            <div class="smalltext">Days</div>
                        </div>
                        <div>
                            <span class="hours"></span>
                            <div class="smalltext">Hours</div>
                        </div>
                        <div>
                            <span class="minutes"></span>
                            <div class="smalltext">Minutes</div>
                        </div>
                        <div>
                            <span class="seconds"></span>
                            <div class="smalltext">Seconds</div>
                        </div>
                    </div>
                    <div id="note_div" style="float: left">
                        <!--<p>Amount will be locked after 5 days from investment.</p>-->
                    </div>
                </div>
            <?php } else{ ?>
            <div class="col-lg-12">
                    <h4><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;<strong>Deal Counter</strong></h4>
                    <div id="clockdiv" style="float: left">
                        <div>
                            <span class="days">00</span>
                            <div class="smalltext">Days</div>
                        </div>
                        <div>
                            <span class="hours">00</span>
                            <div class="smalltext">Hours</div>
                        </div>
                        <div>
                            <span class="minutes">00</span>
                            <div class="smalltext">Minutes</div>
                        </div>
                        <div>
                            <span class="seconds">00</span>
                            <div class="smalltext">Seconds</div>
                        </div>
                    </div>
                    <div id="note_div" style="float: left">
                        <!--<p>Amount will be locked after 5 days from investment.</p>-->
                    </div>
                </div>
            <?php } ?>
        </div><!--/row-->            
    </div>
</section>

<?php //  echo $this->session->userdata('user_mobileno');
            //$res = sendSMS($this->session->userdata('user_mobileno'), $sdate1);
            //print_r($res);?>
<script>
    $(document).ready(function () {
        function getTimeRemaining(endtime) {
            var t = Date.parse(endtime) - Date.parse(new Date());
            //var t = 435600000;
            var days = Math.floor(t / (1000 * 60 * 60 * 24));
            var hours = Math.floor((t % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((t % (1000 * 60)) / 1000);

            return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        }

        function initializeClock(id, endtime) {

            var clock = document.getElementById(id);
            var daysSpan = clock.querySelector('.days');
            var hoursSpan = clock.querySelector('.hours');
            var minutesSpan = clock.querySelector('.minutes');
            var secondsSpan = clock.querySelector('.seconds');

            function updateClock() {
                var t = getTimeRemaining(endtime);
                daysSpan.innerHTML = t.days;
                hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
                console.log(t.total);
                if(t.total == 86400000){
                    console.log(t.total);
                        <?php $res = sendSMS($this->session->userdata('user_mobileno'), $sdate1); ?>
                        var res = "<?php print_r($res);?>";
                        console.log(res);alert(res);
                }
                if (t.total <= 0) {
                    location.reload();
                    clearInterval(timeinterval);
                }
            }

            // updateClock();
            var timeinterval = setInterval(updateClock, 1000);

        }


// var deadline = new Date(Date.parse(new Date()) + <?php //echo $a;     ?> * <?php //echo $b;     ?> * <?php //echo $c;     ?> * <?php //echo $d;     ?> * 1000);
// var countDownDate = new Date(value).getTime();

        var deadline = "<?php echo $sdate1; ?>";
        
        var credate1 = "<?php echo $credate1; ?>";
        var tm = Date.parse(deadline) - Date.parse(new Date());
        var initime = Date.parse(deadline) - Date.parse(credate1);
//          console.log(initime);
        //console.log(initime / 4);

         
//        if (tm > 0 ) {
//            alert(tm);
//           
//            initializeClock('clockdiv', deadline);
//        } else 
        if (tm >= 0) {
            
            initializeClock('clockdiv', deadline);
        } else {
            alert('else');
            var clock = document.getElementById('clockdiv');
            var daysSpan = clock.querySelector('.days');
            var hoursSpan = clock.querySelector('.hours');
            var minutesSpan = clock.querySelector('.minutes');
            var secondsSpan = clock.querySelector('.seconds');
            daysSpan.innerHTML = '00';
            hoursSpan.innerHTML = '00';
            minutesSpan.innerHTML = '00';
            secondsSpan.innerHTML = '00';

            var product_id = 1;
            $.ajax({
                type: 'POST',
                url: 'date-fun.php',
                data: {product_id: product_id, pro_stock: 'Closed'},
                success: function (response) {//response is value returned from php (for your example it's "bye bye"
                    //alert(response);
                }
            });
            //  echo update_stock($ProRes[0]['pro_id']);
        }
    });

</script>  

