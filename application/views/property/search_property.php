<?php
/**
 * Created by PhpStorm.
 * User: Renuka Construction
 * Date: 21-12-2017
 * Time: PM 12:44
 */
?>

<section id="featured-product">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 set-bg-clr">
                <div class="title"><h3 class="text-uppercase">Results</h3></div>
            </div>
            <div class="col-lg-12">
                <div id="carousel-multi-item" class="carousel slide multiitem-car">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row text-center">
                                <?php
                                $i = 1;
                                foreach ($ProRes as $ProRe) {
                                    // print_r($ProRe);
                                    ?>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-card">
                                        <div class="card hoverable">
                                            <div class="card-image">
                                                <div class="view overlay hm-white-slight z-depth-1 zoom-img">
                                                    <a href="<?php echo base_url(); ?>home/proDetail/<?php echo $ProRe['property_id']; ?>" target="_blank">
                                                        <img src="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRe['pro_photo']; ?>" class="img-responsive set-img-imp" alt="">
                                                        <div class="mask waves-effect"></div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="card-content">
                                                <h3><a target="_blank" href="<?php echo base_url(); ?>home/proDetail/<?php echo $ProRe['property_id']; ?>"><?php echo html_entity_decode($ProRe['project_name']); ?></a></h3>

                                                <?php //if ($ProRe['stock'] == 'Open For Investment') { ?>
                                                <h5><?php echo $ProRe['locality']; ?></h5>
                                                <?php //} elseif ($ProRe['stock'] == 'Almost Closing') { ?>
                                                <!--<h5 style="color:yellow;"><?php echo $ProRe['locality']; ?></h5>-->
                                                <?php //} else { ?>
                                                <!--<h5 style="color:#f00;"><?php echo $ProRe['locality']; ?></h5>-->
                                                <?php //} ?>

                                                <!--                                                <div class="star-rating rating-xs rating-active">
                                                                                                    <div class="rating-container rating-uni" data-content="★★★★★">-->
                                                <?php
                                                //                                                        $num = $ProRe['rating'];
                                                //                                                        $restar = '';
                                                //                                                        if ($num == 5) {
                                                //                                                            $restar = '100';
                                                //                                                        }
                                                //                                                        if ($num == 4.5) {
                                                //                                                            $restar = '90';
                                                //                                                        }
                                                //                                                        if ($num == 4) {
                                                //                                                            $restar = '80';
                                                //                                                        }
                                                //                                                        if ($num == 3.5) {
                                                //                                                            $restar = '70';
                                                //                                                        }
                                                //                                                        if ($num == 3) {
                                                //                                                            $restar = '60';
                                                //                                                        }
                                                //                                                        if ($num == 2.5) {
                                                //                                                            $restar = '50';
                                                //                                                        }
                                                //                                                        if ($num == 2) {
                                                //                                                            $restar = '40';
                                                //                                                        }
                                                //                                                        if ($num == 1.5) {
                                                //                                                            $restar = '30';
                                                //                                                        }
                                                //                                                        if ($num == 1) {
                                                //                                                            $restar = '20';
                                                //                                                        }
                                                //                                                        if ($num == 0.5) {
                                                //                                                            $restar = '10';
                                                //                                                        }
                                                ?>
                                                <!--                                                        <div class="rating-stars" data-content="★★★★★" style="width:<?php //echo $restar;  ?>%;">
                                                <?php //echo $restar; ?>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>-->


                                                <!--                                                <h4>
                                                                                                    <i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<span style="text-decoration:line-through;" class="text-danger"><?php echo $ProRe['pro_mrp']; ?>/-</span>&nbsp;&nbsp;&nbsp;
                                                                                                    <i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<span class="text-success"><?php echo $ProRe['pro_price']; ?>/-</span>
                                                                                                </h4>                                                    -->
                                            </div>

                                            <div id="GetCartCountSuccess"></div>
                                            <div class="card-btn text-center">
                                                <?php //if($this->session->userdata('user_type') == 'private'){?>

                                                <a class="btn btn-default btn-block" href="<?php echo base_url(); ?>home/proDetail/<?php echo $ProRe['property_id']; ?>" style="color:black;border: solid 1px black">Click Here to View</a>
                                                <?php //} ?>
                                            </div>
                                            <div class="card-btn text-center">
                                                <?php //if($this->session->userdata('user_type') == 'private'){?>

                                                <a class="btn btn-primary btn-block" href="<?php echo base_url(); ?>home/ROIP/<?php echo $ProRe['property_id']; ?>/<?php echo $ProRe['user_id']; ?>">Request exchanges</a>
                                                <?php //} ?>
                                            </div>
                                        </div>
                                    </div>

                                    <?php if ($i % 4 == 0) { ?><div class="col-md-12" style="padding-bottom:15px;">&nbsp;</div><?php } ?>

                                    <?php
                                    $i++;
                                }
                                ?>

                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>