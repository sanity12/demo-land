<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/theme-script.js"></script>
<section id="product-details" style="background-color: #f0f0f0;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>            
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <!--<li><a href="<?php echo base_url(); ?>product/product_list/<?php //echo $ProRes[0]['cat_id'];  ?>"><?php //echo get_catname($ProRes[0]['cat_id']);  ?></a></li>-->
                    <li class="active"><?php echo html_entity_decode($ProRes[0]['project_name']); ?></li>
                </ul>
                <span style="color: cornflowerblue;font-size:15px">Property Type:
                <?php 
                    if($ProRes[0]['residential_type'] != null){
                        echo $ProRes[0]['residential_type'];
                    }else{
                        echo $ProRes[0]['commercial_type'];
                    }
                ?>
                </span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="product-image">
                    <div class="product-full">
                        <img id="product-zoom" src='<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRes[0]['pro_photo']; ?>' 
                             class="img-responsive set-img-imp" data-zoom-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRes[0]['pro_photo']; ?>"/>
                    </div>
                    <div class="product-img-thumb" id="gallery_02">
                        <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="false">
                            <?php
//                            print_r($GalleryListRes);exit;
//                                $GalleryList = mysqli_query($con, "SELECT * FROM gallery WHERE pro_id='" . $_SESSION['usr_pro'] . "'");
                            foreach ($GalleryListRes as $GalleryListRe) {
                                ?>
                                <li>
                                    <a class="set-img-imp-details" href="#" data-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $GalleryListRe['photo']; ?>" data-zoom-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $GalleryListRe['photo']; ?>">
                                        <img id="product-zoom"  src="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $GalleryListRe['photo']; ?>" /> 
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                 <?php $property_id=$ProRes[0]['property_id'];
                $property_type= $ProRes[0]['property_type'];?>
                <a class="btn btn-warning" style="float:right;" href="<?php echo base_url();?>user/edit_property_images/<?php echo $ProRes[0]['property_id']; ?>/<?php echo $property_type ;?>">Edit Property Images</a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pdMainFacts type2">
                <table>
                    <tbody>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7">
                                    <img src="<?php echo base_url(); ?>assets/img/Plot_area.jpg" style="width: 25px;height: 25px;">
                                    <span class="areaType">Plot Area(l*b)</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['plot_area']; ?></span></div>
                            </td>
                            &nbsp;
                            <td>
                                <div class="pdFactHead top7"><img src="<?php echo base_url(); ?>assets/img/balcony.png" style="width: 25px;height: 25px;">
                                    <span class="areaType"><?php echo $ProRes[0]['no_of_bedrooms']; ?>BHK <?php echo $ProRes[0]['no_of_balcony']; ?>Balcony, <?php echo $ProRes[0]['no_of_bathrooms']; ?>Bathrooms</span></div>
                                <div class="pdFactHead"><span class="areaType1"></span></div>
                            </td>
                        </tr>
                        <tr class="separator">
                            <td>
                                <div class="pdFactHead top7"><img src="<?php echo base_url(); ?>assets/img/address.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Complete address</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['complete_address']; ?></span></div>
                            </td>
                            <td style="background: #597959">
                                <div class="pdFactHead" style="background: #597959;color: white;"><img src="<?php echo base_url(); ?>assets/img/Water 24-7.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Water(Borewell/Not) 24/7</span></div>
                                <div class="pdFactHead" style="color: white;"><b><span class="areaType1"><?php echo $ProRes[0]['water_borewell']; ?></b></span></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Garden.jpg" style="width: 25px;height: 25px;">
                                    <span class="areaType">Garden ?</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['Garden']; ?></span></div>
                            </td>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Electricity.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Electricity/generator?</span></div>
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['electricity_backup']; ?></span></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Swimming-pool</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['swimming_pool']; ?></span></div>
                            </td>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/1_fast.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Age of property</span></div>
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['age_of_property']; ?></span></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">city</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['city']; ?></span></div>
                            </td>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/1_fast.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">status</span></div>
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['status1']; ?></span></div>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
<!--           <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 pdMainFacts type2" style="margin-top: 15px;margin-bottom: 34px;">
                <table style="width:100%">
                    <tbody>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">Name</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['first_name']; ?></span></div></div>
                            </td>
                        </tr>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">Profile pic</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['first_name']; ?></span></div></div>
                            </td>
                        </tr>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">website link</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['website_link']; ?></span></div></div>
                            </td>
                        </tr>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">email address</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['email']; ?></span></div></div>
                            </td>
                        </tr>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">addtional Description</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['first_name']; ?></span></div></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                 
                <?php if(!empty($MsgRes)){?>
                <div class="message">
                    <div class="pull-left" style="margin-top: 35px;">
                        <a href="<?php echo base_url(); ?>home/ROI" class="btn btn-warning btn-flat" style="color:black !important;">Applied user Messages<span class="badge label label-success custom_label" style=""><?php echo $MsgResN;?></span></a>
                    </div>
                </div>
                <?php } ?>

            </div>-->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 pdMainFacts type2" style="margin-top: 15px">
                 <?php $p_user_id= $ProRes[0]['user_id'];
                $CListRes=get_property_user_details($p_user_id);
                ?>
                <div style="background-color:white;padding:5px;">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <img id="product-zoom"  src="<?php echo base_url(); ?>/assets/Profile_images/<?php echo $CListRes[0]['profile_picture'];  ?>" />
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4" style="text-align:center; font-size:13px;">
                            <label class="label-control"><?php echo $CListRes[0]['user_status']; ?></label>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-4">
<!--                            <label class="label-control">Name:</label>-->
                            <span style="font-weight: normal;font-size: 13px;">Name: </span>
                        </div>
                        <div class="col-lg-5 areaType">
                            <span style="font-weight: normal;font-size: 13px;"><?php echo $CListRes[0]['first_name']; ?></span>
                        </div>
                    </div>
                    </br>
                <div class="row">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg-4">
                        <span style="font-weight: normal;font-size: 13px;">Website link: </span>
                    </div>
                    <div class="col-lg-5">
                        <span style="font-weight: normal;font-size: 13px;">	<?php echo $CListRes[0]['website_link']; ?> </span>
                    </div>
                </div>
                    </br>
                    <div class="row">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg-4">
                        <span style="font-weight: normal;font-size: 13px;">Email:</span>
                    </div>
                    <div class="col-lg-5">
                        <span style="font-weight: normal;font-size: 13px;"><?php echo $CListRes[0]['email']; ?></span>
                    </div>
                </div>
                    </br>
                <div class="row">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg-4">
                        <span style="font-weight: normal;font-size: 13px;">Addtional Description:</span>
                    </div>
                    <div class="col-lg-5">
                        <span style="font-weight: normal;font-size: 13px;">ABC<?php ///echo $CListRes[0]['first_name']; ?></span>
                    </div>
                </div>
                    </br>
                    
                    <div class="message">
                    <div class="pull-left" style="margin-top: 35px;">
                        <a href="<?php echo base_url(); ?>postproperty/formAsPublic/<?php echo $pro_id;?>" class="btn btn-primary btn-flat" style="color:white !important;">Duplicate Property Form As Public<span class="badge label label-danger custom_label" style=""></span></a>
                    </div>
                </div> 
<!--                <table style="width:100%">
                    <tbody>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">Name</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['first_name']; ?></span></div></div>
                            </td>
                        </tr>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">Profile pic</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['first_name']; ?></span></div></div>
                            </td>
                        </tr>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">website link</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['website_link']; ?></span></div></div>
                            </td>
                        </tr>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">email address</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['email']; ?></span></div></div>
                            </td>
                        </tr>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">addtional Description</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['first_name']; ?></span></div></div>
                            </td>
                        </tr>
                    </tbody>
                </table>-->           
<!--                <div class="message">
                    <div class="pull-left" style="margin-top: 35px;">
                        <a href="<?php echo base_url(); ?>postproperty/formAsPrivate/<?php echo $pro_id; ?>" class="btn btn-primary btn-flat" style="color:white !important;">Transfer Form As Private<span class="badge label label-danger custom_label" style=""></span></a>
                    </div>
                </div>-->
                
                <?php if (!empty($MsgRes)) { ?>
                    <div class="message">
                        <div class="pull-left" style="margin-top: 35px;">
                            <a href="<?php echo base_url(); ?>home/ROI" class="btn btn-warning btn-flat" style="color:black !important;">Applied user Messages<span class="badge label label-success custom_label" style=""><?php echo $MsgResN; ?></span></a>
                        </div>
                    </div>
                <?php } ?>
            </div>
            </div>
<!--            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pdMainFacts type2" style="margin-bottom: 34px;">
                <h3><u>Preference Details</u></h3>
                <table style="width:100%">
                    <tbody>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7">
                                    <img src="<?php echo base_url(); ?>assets/img/Plot_area.jpg" style="width: 25px;height: 25px;">
                                    <span class="areaType">Plot Area(l*b)</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['plot_area']; ?></span></div>
                            </td>
                            &nbsp;
                            <td>
                                <div class="pdFactHead top7"><img src="<?php echo base_url(); ?>assets/img/balcony.png" style="width: 25px;height: 25px;">
                                    <span class="areaType"><?php echo $PreferRes[0]['no_of_bedrooms']; ?>BHK <?php echo $PreferRes[0]['no_of_balcony']; ?>Balcony, <?php echo $PreferRes[0]['no_of_bathrooms']; ?>Bathrooms</span></div>
                                <div class="pdFactHead"><span class="areaType1">
                                    
                                    </span></div>
                            </td>
                        </tr>
                        <tr class="separator">
                            <td>
                                <div class="pdFactHead top7"><img src="<?php echo base_url(); ?>assets/img/address.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">zipcode</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['zipcode']; ?></span></div>
                            </td>
                            <td style="background: #597959">
                                <div class="pdFactHead" style="background: #597959;color: white;"><img src="<?php echo base_url(); ?>assets/img/Water 24-7.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Water(Borewell/Not) 24/7</span></div>
                                <div class="pdFactHead" style="color: white;"><b><span class="areaType1"><?php echo $PreferRes[0]['water_borewell']; ?></b></span></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Garden.jpg" style="width: 25px;height: 25px;">
                                    <span class="areaType">City</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['city']; ?></span></div>
                            </td>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Electricity.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Electricity/generator?</span></div>
                                <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['electricity_backup']; ?></span></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Swimming-pool</span></div>     
                                <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['swimming_pool']; ?></span></div>
                            </td>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/1_fast.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Age of property</span></div>
                                <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['age_of_property']; ?></span></div>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>-->
        </div><!--/row-->            

    </div>
</section>

<!--<section id="product-details">
    <div style="border: solid 1px #efefef;" class="container">
        <div class="row">
            <h4><strong>Description</strong></h4>
            <h6 style="padding-left:10px; padding-right:10px;"><?php echo html_entity_decode($ProRes[0]['pro_desc']); ?></h6>
        </div>
    </div>
</section>      

<section id="product-details">
    <div style="border: solid 1px #efefef;" class="container">
        <div class="row">
            <h4><strong>Rating</strong></h4>
           <div class="star-rating rating-xs rating-active">
                <div class="rating-container rating-uni" data-content="★★★★★">
<?php
$num = $ProRes[0]['rating'];
$restar = '';
if ($num == 5) {
    $restar = '100';
}
if ($num == 4.5) {
    $restar = '90';
}
if ($num == 4) {
    $restar = '80';
}
if ($num == 3.5) {
    $restar = '70';
}
if ($num == 3) {
    $restar = '60';
}
if ($num == 2.5) {
    $restar = '50';
}
if ($num == 2) {
    $restar = '40';
}
if ($num == 1.5) {
    $restar = '30';
}
if ($num == 1) {
    $restar = '20';
}
if ($num == 0.5) {
    $restar = '10';
}
?>
                    <div class="rating-stars" data-content="★★★★★" style="width:<?php echo $restar; ?>%;">
<?php echo $restar; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->
<script>
    function getTimeRemaining(endtime) {
        var t = Date.parse(endtime) - Date.parse(new Date());

        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        var hours = Math.floor((t % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((t % (1000 * 60)) / 1000);

        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }

    function initializeClock(id, endtime) {

        var clock = document.getElementById(id);
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);
            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
            console.log(t.total);
            // if(){
            //     location.reload();

            // }
            if (t.total <= 0) {
                location.reload();
                clearInterval(timeinterval);
            }
        }

        // updateClock();
        var timeinterval = setInterval(updateClock, 1000);

    }


// var deadline = new Date(Date.parse(new Date()) + <?php //echo $a;  ?> * <?php //echo $b;  ?> * <?php //echo $c;  ?> * <?php //echo $d;  ?> * 1000);
// var countDownDate = new Date(value).getTime();

    var deadline = "<?php echo $sdate1; ?>";
    var credate1 = "<?php echo $credate1; ?>";
    var tm = Date.parse(deadline) - Date.parse(new Date());
    var initime = Date.parse(deadline) - Date.parse(credate1);
//   console.log(initime);
    console.log(initime / 4);

    console.log(tm);
// console.log((tm / 4))
    if (tm > 0 && tm < (initime / 4)) {

        var product_id = <?php echo $ProRes[0]['pro_id']; ?>;
        // alert(product_id);
        $.ajax({
            type: 'POST',
            url: 'date-fun.php',
            data: {product_id: product_id, pro_stock: 'Almost Closing'},
            success: function (response) {//response is value returned from php (for your example it's "bye bye"
                console.log(response);
            }
        });
        initializeClock('clockdiv', deadline);
    } else if (tm >= 0) {

        initializeClock('clockdiv', deadline);


    } else {
        var clock = document.getElementById('clockdiv');
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');
        daysSpan.innerHTML = '00';
        hoursSpan.innerHTML = '00';
        minutesSpan.innerHTML = '00';
        secondsSpan.innerHTML = '00';

        var product_id = <?php echo $ProRes[0]['pro_id']; ?>;
        $.ajax({
            type: 'POST',
            url: 'date-fun.php',
            data: {product_id: product_id, pro_stock: 'Closed'},
            success: function (response) {//response is value returned from php (for your example it's "bye bye"
                //alert(response);
            }
        });
        //  echo update_stock($ProRes[0]['pro_id']);
    }


</script>  

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

