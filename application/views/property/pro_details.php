<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/theme-script.js"></script>

<section id="product-details" style="background-color: #f0f0f0;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>            
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <!--<li><a href="<?php echo base_url(); ?>product/product_list/<?php //echo $ProRes[0]['cat_id'];     ?>"><?php //echo get_catname($ProRes[0]['cat_id']);     ?></a></li>-->
                    <li class="active"><?php echo html_entity_decode($ProRes[0]['project_name']); ?></li>
                </ul>
                <div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <span style="color:cornflowerblue;font-size:17px ; text-align:center;">Property Type:
                            <?php if($ProRes[0]['residential_type']!=null)
                            {
                                echo $ProRes[0]['residential_type'];
                            }
                            else
                            {
                                echo $ProRes[0]['commercial_type'];
                            }

                            ?></span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="product-image">
                    <div class="product-full">
                        <img id="product-zoom" src='<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRes[0]['pro_photo']; ?>' 
                             class="img-responsive set-img-imp" data-zoom-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRes[0]['pro_photo']; ?>"/>
                    </div>
                    <div class="product-img-thumb" id="gallery_02">
                        <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="false">
                            <?php
//                            print_r($GalleryListRes);exit;
//                                $GalleryList = mysqli_query($con, "SELECT * FROM gallery WHERE pro_id='" . $_SESSION['usr_pro'] . "'");
                            foreach ($GalleryListRes as $GalleryListRe) {
                                ?>
                                <li>
                                    <a class="set-img-imp-details" href="#" data-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $GalleryListRe['photo']; ?>" data-zoom-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $GalleryListRe['photo']; ?>">
                                        <img id="product-zoom"  src="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $GalleryListRe['photo']; ?>" /> 
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <?php $property_id=$ProRes[0]['property_id'];
                $property_type=$ProRes[0]['property_type'];?>
                <a class="btn btn-warning" style="float:right;" href="<?php echo base_url();?>user/edit_property_images/<?php echo $ProRes[0]['property_id']; ?>/<?php echo $property_type; ?>">Edit Property Images</a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pdMainFacts type2">
                <table>
                    <tbody>
                        <tr class="trfacts">
                            <td style="background: #597959;border: solid;border-color: white;">
                                <div class="pdFactHead top7" style="background: #597959;color: white;">
                                    <img src="<?php echo base_url(); ?>assets/img/Plot_area.jpg" style="width: 25px;height: 25px;">
                                    <span class="areaType" style="color: white;">Plot Area(l*b)</span></div>     
                                <div class="pdFactHead" >
                                    <?php $plot_area = $ProRes[0]['plot_area'];
                                    $plot_area_array = explode(" ",$plot_area);?>
                                    <span class="areaType1" style="color: white;" id="parea"><?php echo $plot_area_array[0]; ?></span>
                                    <select name="select_measurement"  class="parea_sel" style="height:60%;width:50%;color:black" >
                                        <option>select</option>
                                        <option <?php if ($plot_area_array[1] == "Sq.Feet" ) echo 'selected';?> value="sf">Sq.Feet</option>
                                        <option <?php if ($plot_area_array[1] == "Sq.Meter" ) echo 'selected';?> value="sm">Sq.Meter</option>
                                        <option <?php if ($plot_area_array[1] == "Acre" ) echo 'selected';?> value="ac">Acre</option>
                                        <option <?php  if ($plot_area_array[1] == "Hectare" ) echo 'selected';?> value="hc">Hectare</option>
                                    </select>
                                </div>
                            </td>
                            &nbsp;<p style=""></p>
                    <td style="background: #597959;border: solid;border-color: white;">
                        <div class="pdFactHead top7" style="background: #597959;color: white;"><img src="<?php echo base_url(); ?>assets/img/balcony.png" style="width: 25px;height: 25px;">
                            <span class="areaType" style="color: white;"><?php echo $ProRes[0]['no_of_bedrooms']; ?>BHK <?php echo $ProRes[0]['no_of_balcony']; ?>Balcony, <?php echo $ProRes[0]['no_of_bathrooms']; ?>Bathrooms</span></div>
                        <div class="pdFactHead" style="color: white;"><span class="areaType1"></span></div>
                    </td>
                    </tr>
                    <tr class="separator">
                        <td>
                            <div class="pdFactHead top7"><img src="<?php echo base_url(); ?>assets/img/length.png" style="width: 25px;height: 25px;">
                                <span class="areaType">Length</span></div>     
                            <div class="pdFactHead">
                                <?php $length= $ProRes[0]['length'];
                                if($length!="")
                                {

                                    $length_array=explode(" ",$length);
                                    if(!empty($length_array)){
                                        $length_value=$length_array[0];
                                        $length_measure=$length_array[1];
                                    }
                                }
                                else
                                {
                                    $length_value="-";
                                    $length_measure="";
                                }
                                 ?>
                                <span class="areaType1" id="len"><?php echo $length_value; ?></span>
                                <select  name="select_measurement" class="len_sel" style="height:60%;width:50%;" >
                                    <option <?php if ($length_measure == "Feet" ) echo 'selected';?> value="sf">Feet</option>
                                    <option <?php if ($length_measure == "Meter" ) echo 'selected';?> value="sm">Meter</option>
                                </select>
                            </div>
                        </td>
                        <td >
                            <div class="pdFactHead" ><img src="<?php echo base_url(); ?>assets/img/breadth.png" style="width: 25px;height: 25px;">
                                <span class="areaType">breadth</span>
                            </div>
                            <?php  $breadth= $ProRes[0]['length'];
                            if($breadth!="")
                            {

                                $breadth_array=explode(" ",$breadth);
                                $breadth_value=$breadth_array[0];
                                $breadth_measure=$breadth_array[1];
                            }
                            else
                            {
                                $breadth_value="-";
                                $breadth_measure="";
                            }
                                        ?>
                                <div class="pdFactHead" ><span class="areaType1" id="brd"><?php echo $breadth_value; ?></span>
                                    <select name="select_measurement" class="brd_sel" style="height:60%;width:50%;" >
                                        <option <?php if ($breadth_measure == "Feet" ) echo 'selected';?> value="sf">Feet</option>
                                        <option  <?php if ($breadth_measure == "Meter" ) echo 'selected';?> value="sm">Meter</option>
                                        
                                    </select>
                            </div>
                        </td>
                    </tr>
                    <tr class="separator">
                        <td style="background: #597959;border: solid;border-color: white;">
                            <div class="pdFactHead top7" style="background: #597959;color: white;"><img src="<?php echo base_url(); ?>assets/img/address.png" style="width: 25px;height: 25px;">
                                <span class="areaType">Complete address</span></div>     
                            <div class="pdFactHead" style="color: white;"><span class="areaType1"><?php echo $ProRes[0]['complete_address']; ?></span></div>
                        </td>
                        <td>
                            <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Water 24-7.png" style="width: 25px;height: 25px;">
                                <span class="areaType">Water(Borewell/Not) 24/7</span></div>
                            <div class="pdFactHead"><b><span class="areaType1"><?php echo $ProRes[0]['water_borewell']; ?></b></span></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Garden.jpg" style="width: 25px;height: 25px;">
                                <span class="areaType">Garden ?</span></div>     
                            <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['Garden']; ?></span></div>
                        </td>
                        <td>
                            <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Electricity.png" style="width: 25px;height: 25px;">
                                <span class="areaType">Electricity/generator?</span></div>
                            <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['electricity_backup']; ?></span></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                                <span class="areaType">Swimming-pool</span></div>     
                            <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['swimming_pool']; ?></span></div>
                        </td>
                        <td>
                            <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/1_fast.png" style="width: 25px;height: 25px;">
                                <span class="areaType">Age of property</span></div>
                            <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['age_of_property']; ?></span></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                                <span class="areaType">city</span></div>     
                            <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['city']; ?></span></div>
                         </td>
                        <td>
                            <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/1_fast.png" style="width: 25px;height: 25px;">
                                <span class="areaType">Legal status</span></div>
                            <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['status1']; ?></span></div>
                        </td>
                    </tr>
                    <tr>
                            <td>
                                <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                                    <span class="areaType">Possession</span></div>
                                <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['possession']; ?></span></div>
                            </td>
                            <td></td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 pdMainFacts type2" style="margin-top: 15px">
                 <?php $p_user_id= $ProRes[0]['user_id'];
                $CListRes=get_property_user_details($p_user_id);
                ?>
                <div style="background-color:white;padding:5px;">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <img id="product-zoom"  src="<?php echo base_url(); ?>/assets/Profile_images/<?php echo $CListRes[0]['profile_picture'];  ?>" />
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4" style="text-align:center; font-size:13px;">
                            <label class="label-control"><?php echo $CListRes[0]['user_status']; ?></label>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-4">
<!--                            <label class="label-control">Name:</label>-->
                            <span style="font-weight: normal;font-size: 13px;">Name: </span>
                        </div>
                        <div class="col-lg-5 areaType">
                            <span style="font-weight: normal;font-size: 13px;"><?php echo $CListRes[0]['first_name']; ?></span>
                        </div>
                    </div>
                    </br>
                <div class="row">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg-4">
                        <span style="font-weight: normal;font-size: 13px;">Website link: </span>
                    </div>
                    <div class="col-lg-5">
                        <span style="font-weight: normal;font-size: 13px;">	<?php echo $CListRes[0]['website_link']; ?> </span>
                    </div>
                </div>
                    </br>
                <div class="row">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg-4">
                        <span style="font-weight: normal;font-size: 13px;">Addtional Description:</span>
                    </div>
                    <div class="col-lg-5">
                        <span style="font-weight: normal;font-size: 13px;">ABC<?php ///echo $CListRes[0]['first_name']; ?></span>
                    </div>
                </div>
                    </br>
<!--                <table style="width:100%">
                    <tbody>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">Name</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['first_name']; ?></span></div></div>
                            </td>
                        </tr>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">Profile pic</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['first_name']; ?></span></div></div>
                            </td>
                        </tr>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">website link</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['website_link']; ?></span></div></div>
                            </td>
                        </tr>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">email address</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['email']; ?></span></div></div>
                            </td>
                        </tr>
                        <tr class="trfacts">
                            <td>
                                <div class="pdFactHead top7"><span class="areaType">addtional Description</span></div>     
                            </td>
                            <td>
                                <div class="pdFactHead top7"><div class="pdFactHead"><span class="areaType1"><?php echo $UserRes[0]['first_name']; ?></span></div></div>
                            </td>
                        </tr>
                    </tbody>
                </table>-->           
                <div class="message">
                    <div class="pull-left" style="margin-top: 35px;">
                        <a href="<?php echo base_url(); ?>postproperty/formAsPrivate/<?php echo $pro_id; ?>" class="btn btn-primary btn-flat" style="color:white !important;">Transfer Form As Private<span class="badge label label-danger custom_label" style=""></span></a>
                    </div>
                </div>
                
                <?php if (!empty($MsgRes)) { ?>
                    <div class="message">
                        <div class="pull-left" style="margin-top: 35px;">
                            <a href="<?php echo base_url(); ?>home/ROI" class="btn btn-warning btn-flat" style="color:black !important;">Applied user Messages<span class="badge label label-success custom_label" style=""><?php echo $MsgResN; ?></span></a>
                        </div>
                    </div>
                <?php } ?>
            </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pdMainFacts type2" style="margin-bottom: 34px;">
                <h3><u>Preference Details</u></h3>
                <div class="col-lg-12">
                    <?php $property_id=$ProRes[0]['property_id']; ?>
                    <a class="btn btn-warning" style="float:right;" href="<?php echo base_url();?>user/edit_preference_details/<?php echo $ProRes[0]['property_id']; ?>">Edit Preference Details</a>

                </div>
                 <table style="width:100%">
                            <tbody>
                                <tr class="trfacts">
                                    <td style="background: #597959;border: solid;border-color: white;">
                                        <div class="pdFactHead top7" style="background: #597959;color: white;">
                                            <img src="<?php echo base_url(); ?>assets/img/Plot_area.jpg" style="width: 25px;height: 25px;">
                                            <span class="areaType">Plot Area(l*b)</span></div>
                                    
                                        <?php if(!empty($PreferRes['plot_area'])) {?>
                                             <div class="pdFactHead" style="color: white;"><span class="areaType1"><?php echo $PreferRes[0]['plot_area']; ?></span></div>
                                        <?php }else{ ?>
                                             <div class="pdFactHead" style="color: white;"><span class="areaType1"></span></div>
                                        <?php } ?>
                                    </td>
                                    &nbsp;
                                    <td style="background: #597959;border: solid;border-color: white;">
                                        <div class="pdFactHead top7" style="background: #597959;color: white;"><img src="<?php echo base_url(); ?>assets/img/balcony.png" style="width: 25px;height: 25px;">
                                            <span class="areaType"><?php if(!empty($PreferRes[0]['no_of_bedrooms'])){echo $PreferRes[0]['no_of_bedrooms'];} ?>BHK <?php if(!empty($PreferRes[0]['no_of_bathrooms'])){ echo $PreferRes[0]['no_of_balcony'];} ?>Balcony</span></div>
                                        <div class="pdFactHead"><span class="areaType1" style="color: white;">
                                            , <?php if(!empty($PreferRes[0]['no_of_bathrooms'])){ echo $PreferRes[0]['no_of_bathrooms'];} ?>Bathrooms
                                            </span></div>

                                    </td>
                                </tr>
                                <tr class="separator">
                                    <td>
                                        <div class="pdFactHead top7"><img src="<?php echo base_url(); ?>assets/img/address.png" style="width: 25px;height: 25px;">
                                            <span class="areaType">zipcode</span></div>
                                            <?php
                                            if(!empty($PreferRes[0]['zipcode']))
                                            {
                                                $zipcode_id=$PreferRes[0]['zipcode'];
                                                $zipcode_id_array=explode(",",$zipcode_id);
                                                $zipcode_result=get_pincode_for_preference($zipcode_id_array);
                                                $zipcode_data=" ";
                                                for($i=0;$i<count($zipcode_result);$i++)
                                                {
                                                    if($zipcode_data==" ")
                                                    {
                                                        $zipcode_data=$zipcode_data."".$zipcode_result[$i]['pincode'];
                                                    }
                                                    else
                                                    {
                                                        $zipcode_data=$zipcode_data.",".$zipcode_result[$i]['pincode'];
                                                    }

                                                }
                                            }
                                            else{
                                                $zipcode_data="";
                                            }

                                            ?>
                                        <div class="pdFactHead"><span class="areaType1"><?php echo $zipcode_data; ?></span></div>
                                    </td>
                                    <td>
                                        <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Water 24-7.png" style="width: 25px;height: 25px;">
                                            <span class="areaType">Water(Borewell/Not) 24/7</span></div>
                                        <?php if(!empty($PreferRes[0]['water_borewell'])) {?>
                                        <div class="pdFactHead"><b><span class="areaType1"><?php echo $PreferRes[0]['water_borewell']; ?></b></span></div>
                                        <?php}else{ ?>
                                        <div class="pdFactHead"><b><span class="areaType1"><?php echo ""; ?></b></span></div>
                                        <?php } ?>

                                    </td>
                                </tr>
                                <tr>
                                    <td style="background: #597959;border: solid;border-color: white;">
                                        <div class="pdFactHead" style="background: #597959;color: white;"><img src="<?php echo base_url(); ?>assets/img/Garden.jpg" style="width: 25px;height: 25px;">
                                            <span class="areaType">City</span></div>
                                            <?php
                                            if(!empty($PreferRes[0]['city']))
                                            {
                                                $city_id=$PreferRes[0]['city'];
                                                $city_id_array=explode(",",$city_id);
                                                $city_result=get_city_name_for_preference($city_id_array);
                                                $city_data=" ";
                                                for($i=0;$i<count($city_result);$i++)
                                                {
                                                    if($city_data==" ")
                                                    {
                                                        $city_data=$city_data."".$city_result[$i]['city_name'];
                                                    }
                                                    else
                                                    {
                                                        $city_data=$city_data.",".$city_result[$i]['city_name'];
                                                    }

                                                }
                                            }
                                            else
                                            {
                                                $city_data="";
                                            }

                                            ?>
                                        <div class="pdFactHead"><span class="areaType1" style="color: white;"><?php echo $city_data;  ?></span></div>
                                    </td>
                                    <td>
                                        <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Electricity.png" style="width: 25px;height: 25px;">
                                            <span class="areaType">Electricity/generator?</span></div>
                                        <?php if(!empty($PreferRes[0]['electricity_backup'])) {?>
                                        <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['electricity_backup']; ?></span></div>
                                        <?php}else{ ?>
                                        <div class="pdFactHead"><span class="areaType1"><?php echo ""; ?></span></div>
                                        <?php  }?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                                            <span class="areaType">Swimming-pool</span></div>
                                        <?php if(!empty($PreferRes[0]['swimming_pool'])) {?>

                                        <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['swimming_pool']; ?></span></div>
                                        <?php}else{ ?>
                                        <div class="pdFactHead"><span class="areaType1"><?php echo ""; ?></span></div>
                                        <?php  }?>
                                    </td>
                                    <td>
                                        <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/1_fast.png" style="width: 25px;height: 25px;">
                                            <span class="areaType">Age of property</span></div>
                                            <?php if(!empty($PreferRes[0]['age_of_property'])) {?>
                                                <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['age_of_property']; ?></span></div>
                                            <?php}else{ ?>
                                                 <div class="pdFactHead"><span class="areaType1"><?php echo ""; ?></span></div>
                                            <?php  }?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                                            <span class="areaType">Possession</span></div>
                                        <?php if(!empty($PreferRes[0]['possession'])) {?>
                                            <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['possession']; ?></span></div>
                                        <?php}else{ ?>
                                        <div class="pdFactHead"><span class="areaType1"><?php echo ""; ?></span></div>
                                        <?php  }?>


                                    </td>
                                    <td>
                                        <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/1_fast.png" style="width: 25px;height: 25px;">
                                            <span class="areaType">Legal Status</span></div>
                                         <?php if(!empty($PreferRes[0]['status1'])) {?>
                                             <div class="pdFactHead"><span class="areaType1"><?php echo $ProRes[0]['status1']; ?></span></div>
                                         <?php}else{ ?>
                                        <div class="pdFactHead"><span class="areaType1"><?php echo ""; ?></span></div>
                                         <?php  }?>
                                    </td>
                                    <td></td>
                                </tr>
                    </tbody>
                </table>

            </div>
           <!--  <div class="message">
                <div class="pull-right" style="margin-top: 0px; margin-bottom:10px;">
                    <a href="<?php echo base_url(); ?>postproperty/formAsPrivate/<?php echo $pro_id; ?>" class="btn btn-primary btn-flat" style="color:white !important;">Duplicate Property Form As Private<span class="badge label label-danger custom_label" style=""></span></a>
                </div>
            </div> -->
        </div><!--/row-->            

    </div>
</section>

<script type="text/javascript">
    var areaD = new Array()
    areaD['sm']      = 1.0;
    areaD['hc']      = 0.0001;
    areaD['sf']       = 10.76391;
    areaD['ac']         = 0.0002471054;
    //    areaD['sqmile']       = 0.0000003861022;
//    areaD['sqmillimeter'] = 1000000.0;
//    areaD['sqcentimeter'] = 10000.0;
//    areaD['sqkilometer']  = 0.000001;
//    areaD['sqinch']       = 1550.003;
//    areaD['sqyard']       = 1.19599;
    function convertArea(x, unit1, unit2){
         if (jQuery.inArray(unit1, areaD) && jQuery.inArray(unit2, areaD)){
            factor1 = areaD[unit1];
            factor2 = areaD[unit2];
            return factor2*x/factor1;
        }else{
            return False;
        }
    }
    var unit1;
    var x = $("#parea").html();
    $('.parea_sel').focus(function () {
        unit1 = $(this).val();
    }).change(function () {
        $(this).unbind('focus');
            var unit2 = $(this).val();
            var res = convertArea(x, unit1, unit2);
            $("#parea").html(parseFloat(res).toFixed(2));
    });
    
    var y = $("#len").html();
    $('.len_sel').focus(function () {
        unit1y = $(this).val();
    }).change(function () {
        $(this).unbind('focus');
            var unit2y = $(this).val();
            var res = convertArea(y, unit1y, unit2y);
            $("#len").html(parseFloat(res).toFixed(2));
    });
    
    var z = $("#brd").html();
    $('.brd_sel').focus(function () {
        unit1z = $(this).val();
    }).change(function () {
        $(this).unbind('focus');
            var unit2z = $(this).val();
            var res = convertArea(z, unit1z, unit2z);
            $("#brd").html(parseFloat(res).toFixed(2));
    });
</script>
<script>
//    function getTimeRemaining(endtime) {
//        var t = Date.parse(endtime) - Date.parse(new Date());
//
//        var days = Math.floor(t / (1000 * 60 * 60 * 24));
//        var hours = Math.floor((t % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
//        var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
//        var seconds = Math.floor((t % (1000 * 60)) / 1000);
//
//        return {
//            'total': t,
//            'days': days,
//            'hours': hours,
//            'minutes': minutes,
//            'seconds': seconds
//        };
//    }
//
//    function initializeClock(id, endtime) {
//
//        var clock = document.getElementById(id);
//        var daysSpan = clock.querySelector('.days');
//        var hoursSpan = clock.querySelector('.hours');
//        var minutesSpan = clock.querySelector('.minutes');
//        var secondsSpan = clock.querySelector('.seconds');
//
//        function updateClock() {
//            var t = getTimeRemaining(endtime);
//            daysSpan.innerHTML = t.days;
//            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
//            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
//            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
//            console.log(t.total);
//            // if(){
//            //     location.reload();
//
//            // }
//            if (t.total <= 0) {
//                location.reload();
//                clearInterval(timeinterval);
//            }
//        }
//
//        // updateClock();
//        var timeinterval = setInterval(updateClock, 1000);
//
//    }
//
//
//// var deadline = new Date(Date.parse(new Date()) + <?php //echo $a;     ?> * <?php //echo $b;     ?> * <?php //echo $c;     ?> * <?php //echo $d;     ?> * 1000);
//// var countDownDate = new Date(value).getTime();
//
//    var deadline = "<?php //echo $sdate1; ?>";
//    var credate1 = "<?php //echo $credate1; ?>";
//    var tm = Date.parse(deadline) - Date.parse(new Date());
//    var initime = Date.parse(deadline) - Date.parse(credate1);
////   console.log(initime);
//    console.log(initime / 4);
//
//    console.log(tm);
//// console.log((tm / 4))
//    if (tm > 0 && tm < (initime / 4)) {
//
//        var product_id = <?php //echo $ProRes[0]['pro_id']; ?>;
//        // alert(product_id);
//        $.ajax({
//            type: 'POST',
//            url: 'date-fun.php',
//            data: {product_id: product_id, pro_stock: 'Almost Closing'},
//            success: function (response) {//response is value returned from php (for your example it's "bye bye"
//                console.log(response);
//            }
//        });
//        initializeClock('clockdiv', deadline);
//    } else if (tm >= 0) {
//
//        initializeClock('clockdiv', deadline);
//
//
//    } else {
//        var clock = document.getElementById('clockdiv');
//        var daysSpan = clock.querySelector('.days');
//        var hoursSpan = clock.querySelector('.hours');
//        var minutesSpan = clock.querySelector('.minutes');
//        var secondsSpan = clock.querySelector('.seconds');
//        daysSpan.innerHTML = '00';
//        hoursSpan.innerHTML = '00';
//        minutesSpan.innerHTML = '00';
//        secondsSpan.innerHTML = '00';
//
//        var product_id = <?php //echo $ProRes[0]['pro_id']; ?>;
//        $.ajax({
//            type: 'POST',
//            url: 'date-fun.php',
//            data: {product_id: product_id, pro_stock: 'Closed'},
//            success: function (response) {//response is value returned from php (for your example it's "bye bye"
//                //alert(response);
//            }
//        });
//        //  echo update_stock($ProRes[0]['pro_id']);
//    }


</script>  

