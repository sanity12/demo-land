<div id="content" class="content-container ng-scope">
    <section class="view-container animate-fade-up">
        <div class="container wid-init">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px">
                        <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Edit Your Preference Details </h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="container wid-init ce-form">
             <!-- -----------------------------  -->

             <?php
             $property_id=$this->uri->segment(3);
             $user_id = $this->session->userdata('user_id');
             $pre_res= retrive_preference_details($property_id,$user_id);
             ?>
             <form class="well form-horizontal" action="<?php echo site_url('user/editpreferences'); ?>" method="post"  id="edit_preference_form" name="edit_preference_form">

                <div id="preferences" class="tab-pane fade in ">
<!--             </br>-->
<!--             </br>-->
             <div id="outer_div" style="border:1px solid #eaeaea;background-color: #eaedf1;">
<!--             </br>-->
<!--             </br>-->
<!--             <form name="preference_details_info" class="" action="--><?php //echo site_url('postproperty/save_preference_info'); ?><!--" method="post">-->

             <input type="hidden" name="property_id" id="property_id" value="<?php echo $this->uri->segment(3); ?>"/>
             <div class="row">
                 <div class="col-lg-1"></div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Plot Area:</label>
                 </div>
                 <div class="col-lg-3">
                     <div class="col-lg-6">
                         <input type="text" name="plot_area" id="plot_area" class="form-control text_input" style="" value="<?php echo $pre_res[0]['plot_area']; ?>"/>
                     </div>
                     <div class="col-lg-6">
                         <select  name="select_measurement" class="form-control" style=""/>>
                         <option>Sq.Feet</option>
                         <option>Sq.Meter</option>
                         </select>

                     </div>

                 </div>
                 <div class="col-lg-2">
                     <label class=" control-label label_font label_style">No of Bathrooms:</label>
                 </div>
                 <div class="col-lg-3">
                     <!--                        <input type="text" name="length" id="" class=" form-control text_input" style="height:60%;" />-->
                     <select class="form-control" name="no_of_bathrooms" style="">
                         <option <?php if ($pre_res[0]['no_of_bathrooms'] == "1" ) echo 'selected';?> value="1">1</option>
                         <option <?php if ($pre_res[0]['no_of_bathrooms'] == "2" ) echo 'selected';?> value="2">2</option>
                         <option <?php if ($pre_res[0]['no_of_bathrooms'] == "3" ) echo 'selected';?> value="3">3</option>
                         <option <?php if ($pre_res[0]['no_of_bathrooms'] == "4" ) echo 'selected';?> value="4">4</option>
                         <option <?php if ($pre_res[0]['no_of_bathrooms'] == "5" ) echo 'selected';?>  value="5">5</option>
                         <option <?php if ($pre_res[0]['no_of_bathrooms'] == "6" ) echo 'selected';?> value="6">6</option>
                         <option <?php if ($pre_res[0]['no_of_bathrooms'] == "7" ) echo 'selected';?>  value="7">7</option>
                         <option <?php if ($pre_res[0]['no_of_bathrooms'] == "8" ) echo 'selected';?> value="8">8</option>
                         <option <?php if ($pre_res[0]['no_of_bathrooms'] == "9" ) echo 'selected';?> value="9">9</option>
                         <option <?php if ($pre_res[0]['no_of_bathrooms'] == "9+" ) echo 'selected';?> value="9+">9+</option>
                     </select>

                 </div>
             </div>
             </br>
             <div class="row">
                 <div class="col-lg-1"></div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">No of Bedrooms:</label>
                 </div>
                 <div class="col-lg-3">
                     <select class="form-control" name="no_of_bedrooms" style="">
                         <option <?php if ($pre_res[0]['no_of_bedrooms'] == "1" ) echo 'selected';?> value="1">1</option>
                         <option <?php if ($pre_res[0]['no_of_bedrooms'] == "2" ) echo 'selected';?> value="2">2</option>
                         <option <?php if ($pre_res[0]['no_of_bedrooms'] == "3" ) echo 'selected';?> value="3">3</option>
                         <option <?php if ($pre_res[0]['no_of_bedrooms'] == "4" ) echo 'selected';?> value="4">4</option>
                         <option <?php if ($pre_res[0]['no_of_bedrooms'] == "5" ) echo 'selected';?>  value="5">5</option>
                         <option <?php if ($pre_res[0]['no_of_bedrooms'] == "6" ) echo 'selected';?> value="6">6</option>
                         <option <?php if ($pre_res[0]['no_of_bedrooms'] == "7" ) echo 'selected';?>  value="7">7</option>
                         <option <?php if ($pre_res[0]['no_of_bedrooms'] == "8" ) echo 'selected';?> value="8">8</option>
                         <option <?php if ($pre_res[0]['no_of_bedrooms'] == "9" ) echo 'selected';?> value="9">9</option>
                         <option <?php if ($pre_res[0]['no_of_bedrooms'] == "9+" ) echo 'selected';?> value="9+">9+</option>

                     </select>
                 </div>
                 <div class="col-lg-2">
                     <label class=" control-label label_font label_style">No of Balcony:</label>
                 </div>
                 <div class="col-lg-3">
                     <select class="form-control" name="no_of_balcony" style="">
                         <option <?php if ($pre_res[0]['no_of_balcony'] == "1" ) echo 'selected';?> value="1">1</option>
                         <option <?php if ($pre_res[0]['no_of_balcony'] == "2" ) echo 'selected';?> value="2">2</option>
                         <option <?php if ($pre_res[0]['no_of_balcony'] == "3" ) echo 'selected';?> value="3">3</option>
                         <option <?php if ($pre_res[0]['no_of_balcony'] == "4" ) echo 'selected';?> value="4">4</option>
                         <option <?php if ($pre_res[0]['no_of_balcony'] == "5" ) echo 'selected';?>  value="5">5</option>
                         <option <?php if ($pre_res[0]['no_of_balcony'] == "6" ) echo 'selected';?> value="6">6</option>
                         <option <?php if ($pre_res[0]['no_of_balcony'] == "7" ) echo 'selected';?>  value="7">7</option>
                         <option <?php if ($pre_res[0]['no_of_balcony'] == "8" ) echo 'selected';?> value="8">8</option>
                         <option <?php if ($pre_res[0]['no_of_balcony'] == "9" ) echo 'selected';?> value="9">9</option>
                         <option <?php if ($pre_res[0]['no_of_balcony'] == "9+" ) echo 'selected';?> value="9+">9+</option>
                     </select>
                 </div>
             </div>
             </br>
             <div class="row">
                 <div class="col-lg-1"></div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">No of Hall:</label>
                 </div>
                 <div class="col-lg-3">
                     <select class="form-control" name="no_of_hall" style="">
                         <option <?php if ($pre_res[0]['no_of_hall'] == "1" ) echo 'selected';?> value="1">1</option>
                         <option <?php if ($pre_res[0]['no_of_hall'] == "2" ) echo 'selected';?> value="2">2</option>
                         <option <?php if ($pre_res[0]['no_of_hall'] == "3" ) echo 'selected';?> value="3">3</option>
                         <option <?php if ($pre_res[0]['no_of_hall'] == "4" ) echo 'selected';?> value="4">4</option>
                         <option <?php if ($pre_res[0]['no_of_hall'] == "5" ) echo 'selected';?>  value="5">5</option>
                         <option <?php if ($pre_res[0]['no_of_hall'] == "6" ) echo 'selected';?> value="6">6</option>
                         <option <?php if ($pre_res[0]['no_of_hall'] == "7" ) echo 'selected';?>  value="7">7</option>
                         <option <?php if ($pre_res[0]['no_of_hall'] == "8" ) echo 'selected';?> value="8">8</option>
                         <option <?php if ($pre_res[0]['no_of_hall'] == "9" ) echo 'selected';?> value="9">9</option>
                         <option <?php if ($pre_res[0]['no_of_hall'] == "9+" ) echo 'selected';?> value="9+">9+</option>
                     </select>
                 </div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">No of Kitchen:</label>
                 </div>
                 <div class="col-lg-3">
                     <select class="form-control" name="no_of_kitchen" style="">
                         <option <?php if ($pre_res[0]['no_of_kitchen'] == "1" ) echo 'selected';?> value="1">1</option>
                         <option <?php if ($pre_res[0]['no_of_kitchen'] == "2" ) echo 'selected';?> value="2">2</option>
                         <option <?php if ($pre_res[0]['no_of_kitchen'] == "3" ) echo 'selected';?> value="3">3</option>
                         <option <?php if ($pre_res[0]['no_of_kitchen'] == "4" ) echo 'selected';?> value="4">4</option>
                         <option <?php if ($pre_res[0]['no_of_kitchen'] == "5" ) echo 'selected';?>  value="5">5</option>
                         <option <?php if ($pre_res[0]['no_of_kitchen'] == "6" ) echo 'selected';?> value="6">6</option>
                         <option <?php if ($pre_res[0]['no_of_kitchen'] == "7" ) echo 'selected';?>  value="7">7</option>
                         <option <?php if ($pre_res[0]['no_of_kitchen'] == "8" ) echo 'selected';?> value="8">8</option>
                         <option <?php if ($pre_res[0]['no_of_kitchen'] == "9" ) echo 'selected';?> value="9">9</option>
                         <option <?php if ($pre_res[0]['no_of_kitchen'] == "9+" ) echo 'selected';?> value="9+">9+</option>
                     </select>
                 </div>
             </div>
             </br>
             <div class="row">
                 <div class="col-lg-1"></div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Electricity(24/7):</label>
                 </div>
                 <div class="col-lg-3">
                     <div class="col-lg-4">
                         <input type="radio" name="electricity" <?php if ($pre_res[0]['electricity'] == "yes" ) echo 'checked';?> value="yes"  style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                     </div>
                     <div class="col-lg-4">
                         <input type="radio" name="electricity" <?php if ($pre_res[0]['electricity'] == "no" ) echo 'checked';?> value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                     </div>
                 </div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Garden:</label>
                 </div>
                 <div class="col-lg-3">
                     <div class="col-lg-4">
                         <input type="radio" name="garden" <?php if ($pre_res[0]['garden'] == "yes" ) echo 'checked';?>  value="yes" style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                     </div>
                     <div class="col-lg-4">
                         <input type="radio" name="garden" <?php if ($pre_res[0]['garden'] == "no" ) echo 'checked';?> value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                     </div>
                 </div>
             </div>
             </br>
             <div class="row">
                 <div class="col-lg-1"></div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Water(Borewell):</label>
                 </div>
                 <div class="col-lg-3">
                     <div class="col-lg-4">
                         <input type="radio" name="water_borewell" <?php if ($pre_res[0]['water_borewell'] == "yes" ) echo 'checked';?> value="yes" style="margin-bottom: 9%;"  /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                     </div>
                     <div class="col-lg-4">
                         <input type="radio" name="water_borewell" <?php if ($pre_res[0]['water_borewell'] == "no" ) echo 'checked';?> value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                     </div>
                 </div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Electricity/Generator:</label>
                 </div>
                 <div class="col-lg-3">
                     <div class="col-lg-4">
                         <input type="radio" name="electricity_backup" value="yes" <?php if ($pre_res[0]['electricity_backup'] == "yes" ) echo 'checked';?> style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                     </div>
                     <div class="col-lg-4">
                         <input type="radio" name="electricity_backup" value="no" <?php if ($pre_res[0]['electricity_backup'] == "no" ) echo 'checked';?> style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                     </div>
                 </div>
             </div>
             </br>
             <div class="row">
                 <div class="col-lg-1"></div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Swimming-pool:</label>
                 </div>
                 <div class="col-lg-3">
                     <div class="col-lg-4">
                         <input type="radio" name="swimming_pool"  <?php if ($pre_res[0]['swimming_pool'] == "yes" ) echo 'checked';?> value="yes" style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                     </div>
                     <div class="col-lg-4">
                         <input type="radio" name="swimming_pool" <?php if ($pre_res[0]['swimming_pool'] == "no" ) echo 'checked';?> value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                     </div>
                 </div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Age of Property:</label>
                 </div>
                 <div class="col-lg-3">
                     <select class="form-control" name="age_of_property" >
                         <option <?php if ($pre_res[0]['age_of_property'] == "1" ) echo 'selected';?> value="1">1</option>
                         <option <?php if ($pre_res[0]['age_of_property'] == "2" ) echo 'selected';?> value="2">2</option>
                         <option <?php if ($pre_res[0]['age_of_property'] == "3" ) echo 'selected';?> value="3">3</option>
                         <option <?php if ($pre_res[0]['age_of_property'] == "4" ) echo 'selected';?> value="4">4</option>
                         <option <?php if ($pre_res[0]['age_of_property'] == "5" ) echo 'selected';?>  value="5">5</option>
                         <option <?php if ($pre_res[0]['age_of_property'] == "6" ) echo 'selected';?> value="6">6</option>
                         <option <?php if ($pre_res[0]['age_of_property'] == "7" ) echo 'selected';?>  value="7">7</option>
                         <option <?php if ($pre_res[0]['age_of_property'] == "8" ) echo 'selected';?> value="8">8</option>
                         <option <?php if ($pre_res[0]['age_of_property'] == "9" ) echo 'selected';?> value="9">9</option>
                         <option <?php if ($pre_res[0]['age_of_property'] == "9+" ) echo 'selected';?> value="9+">9+</option>
                     </select>
                 </div>
             </div>
             </br>
             <div class="row">
                 <div class="col-lg-1"></div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Possession:</label>
                 </div>
                 <div class="col-lg-3">
                     <select class="form-control" name="possession" id="possession" style="">
                         <option value="">Select Possession</option>
                         <option value="Immediate">Immediate</option>
                         <option <?php if ($pre_res[0]['possession'] == "1 Month" ) echo 'selected';?>  value="1 Month">1 Month</option>
                         <option <?php if ($pre_res[0]['possession'] == "2 Month" ) echo 'selected';?> value="2 Month">2 Month</option>
                         <option <?php if ($pre_res[0]['possession'] == "3 Month" ) echo 'selected';?> value="3 Month">3 Month</option>
                         <option <?php if ($pre_res[0]['possession'] == "4 Month" ) echo 'selected';?> value="4 Month">4 Month</option>
                         <option <?php if ($pre_res[0]['possession'] == "5 Month" ) echo 'selected';?> value="5 Month">5 Month</option>
                         <option <?php if ($pre_res[0]['possession'] == "6 Month" ) echo 'selected';?> value="6 Month">6 Month</option>
                         <option <?php if ($pre_res[0]['possession'] == "7 Month" ) echo 'selected';?> value="7 Month">7 Month</option>
                         <option <?php if ($pre_res[0]['possession'] == "8 Month" ) echo 'selected';?> value="8 Month">8 Month</option>
                         <option <?php if ($pre_res[0]['possession'] == "9 Month" ) echo 'selected';?> value="9 Month">9 Month</option>
                         <option <?php if ($pre_res[0]['possession'] == "10 Month" ) echo 'selected';?> value="10 Month">10 Month</option>
                         <option <?php if ($pre_res[0]['possession'] == "11 Month" ) echo 'selected';?> value="11 Month">11 Month</option>
                         <option <?php if ($pre_res[0]['possession'] == "12 Month" ) echo 'selected';?> value="12 Month">12 Month</option>

                     </select>
                 </div>

             </div>
             </br>
             <!-- ------ start code of display coutry state zipcode----- -->
             <div class="row">
                 <div class="col-lg-1"></div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Select Country:</label>
                 </div>
                 <div class="col-lg-5">
                     <select class="form-control" name="country" id="country" style="" onchange="selectState(this.options[this.selectedIndex].value)">
                         <option value="-1">Select country</option>
                         <option value="1">India</option>
                     </select>
                     <input type="hidden" name="pre_country" id="pre_country" />
                 </div>
             </div>
             </br>
             <div class="row">
                 <div class="col-lg-1"></div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Select state:</label>
                 </div>
                 <div class="col-lg-5">
                     <select class="form-control" name="state" id="state_dropdown" style="" onchange="selectCity(this.options[this.selectedIndex].value)">
                         <option value="-1">Select State</option>
                     </select>
                     <input type="hidden" name="pre_state" id="pre_state"/>
                 </div>
             </div>
             </br>
             <div class="row">
                 <div class="col-lg-1"></div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Select City:</label>
                 </div>
                 <div class="col-lg-5">
                     <select class="form-control" name="city"  id="city_dropdown" onchange="getZipcode(this.options[this.selectedIndex].text, this.options[this.selectedIndex].value)" style="">
                         <option></option>
                     </select>
                 </div>
             </div>
             </br>
             <div class="row">
                 <div class="col-lg-1"></div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Select Pincode :</label>
                 </div>
                 <div class="col-lg-5">
                     <!--                        <select  class="form-control ruleparameter" size="5" multiple  name="HIGHEST_RCP_RP" id="zip_dropdown" style="" >-->
                     <!--                            <option>411038</option>-->
                     <!--                            <option>411035</option>-->
                     <!--                            <option>411036</option>-->
                     <!--                            <option>411078</option>-->
                     <!--                            <option>411090</option>-->
                     <!--                            <option>411290</option>-->
                     <!---->
                     <!--                        </select>-->
                     <select  class="form-control" size=""  name="" id="zip_dropdown" style="" onchange="append_zipcode_value(this.value)" >
                     </select>

                     <!--                        <div id="show_zipcode"></div>-->
                 </div>
             </div>
                </br>
             <div class="row">
                 <div class="col-lg-1"></div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Selected Zipcode:</label>
                 </div>
                 <div class="col-lg-5">
                     <?php
                     $zipcode_id=$pre_res[0]['zipcode'];
                     $zipcode_id_array=explode(",",$zipcode_id);
                     $zipcode_result=get_pincode_for_preference($zipcode_id_array);
                     $zipcode_data=" ";
                     for($i=0;$i<count($zipcode_result);$i++)
                     {
                         if($zipcode_data==" ")
                         {
                             $zipcode_data=$zipcode_data."".$zipcode_result[$i]['pincode'];
                         }
                         else
                         {
                             $zipcode_data=$zipcode_data.",".$zipcode_result[$i]['pincode'];
                         }

                     }
                     ?>
                     <input  type="text" name="selected_zipcode" id="selected_zipcode" class="form-control" value="<?php //echo $zipcode_data;?>" style="height:100%;" />
                     <input type="hidden" name="selected_zipcode_id" id="selected_zipcode_id"  value="<?php echo $pre_res[0]['zipcode']; ?>"/>

                 </div>
             </div>
             </br>
             <div class="row">
                 <div class="col-lg-1"></div>
                 <div class="col-lg-2">
                     <label class="control-label label_font label_style">Selected City:</label>
                 </div>
                 <div class="col-lg-5">
                     <?php
                     $city_id=$pre_res[0]['city'];
                     $city_id_array=explode(",",$city_id);
                     $city_result=get_city_name_for_preference($city_id_array);
                     $city_data=" ";
                     for($i=0;$i<count($city_result);$i++)
                     {
                         if($city_data==" ")
                         {
                             $city_data=$city_data."".$city_result[$i]['city_name'];
                         }
                         else
                         {
                             $city_data=$city_data.",".$city_result[$i]['city_name'];
                         }

                     }
                     ?>

                     <input  type="text" name="selected_city" id="selected_city" class="form-control" value="<?php //echo $city_data; ?>" style="height:100%;"  />
                     <input type="hidden" name="selected_city_id" id="selected_city_id" value="<?php echo $pre_res[0]['city']; ?>"/>
                 </div>
             </div>

             <!-- ------end code of display coutry state zipcode----- -->
</br>
</br>
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="error col-lg-4" style="color:red"></div>
            </div>
             </br>
             </br>
             <div class="row">
                 <div class="col-lg-5"></div>
                 <div class="col-lg-3">
                     <input type="button" value="save" class="btn btn-primary" onclick="submit_edit_preference_form();" />
                 </div>
             </div>
             </br></br>
<!--             </form>-->
             </div>
             </div>

             </form>

            <!-- --------------------------------------- -->

        </div>
        </section>
    </div>



<script>

    function selectState(country_id) {

        //pre state and city assign to empty
        $('#selected_zipcode_id').val("");
        $('#selected_city_id').val("");

        if (country_id != "-1") {
            loadData('state', country_id);
            $("#city_dropdown").html("<option value='-1'>Select city</option>");
        } else {
            $("#state_dropdown").html("<option value='-1'>Select state</option>");
            $("#city_select").html("<option value=''>Select city</option>");
        }
    }

    function loadData(loadType, loadId) {
        var dataString = 'loadType=' + loadType + '&loadId=' + loadId;
        // $("#" + loadType + "_loader").show();
        // $("#" + loadType + "_loader").fadeIn(400).html('Please wait... <img src="<?php echo base_url(); ?>assets/images/loading.gif" />');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Postproperty/loadData",
            data: dataString,
            cache: false,
            success: function (result) {
                // $("#" + loadType + "_loader").hide();
                $("#" + loadType + "_dropdown").html("<option value='-1'>Select " + loadType + "</option>");
                $("#" + loadType + "_dropdown").append(result);
                //alert('getZipcode');
            }
        });
    }
    function selectCity(state_id) {

        if (state_id != "-1") {
            loadData('city', state_id);
        } else {
            $("#city_dropdown").html("<option value='-1'>Select city</option>");
        }
    }
    function getZipcode(city_name, city_id) {
        // $("#checkbox").prop('checked', '');
        // $("#checkboxDele").prop('checked', '');
        var dataString = 'loadName=' + city_name + '&loadId=' + city_id;
        //  $("#zip_loader").show();
        // $("#zip_loader").fadeIn(400).html('Please wait... <img src="<?php echo base_url(); ?>assets/images/loading.gif" />');

        // $("#zip_dropdown option").remove();
        var data=$('#selected_city').val();

        if(data=="")
        {
            var data1=data+''+city_name;
        }
        else
        {
            var data1=data+','+' '+city_name;
        }
        $('#selected_city').val(data1);
        var selected_city_id=$('#selected_city_id').val();
        if(selected_city_id=="")
        {
            var data2=selected_city_id+''+city_id;
        }
        else
        {
            var data2=selected_city_id+','+' '+city_id;
        }
        $('#selected_city_id').val(data2);


        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Postproperty/getZipcode",
            data: dataString,
            cache: false,
            dataType:"json",
            success: function (result) {
                // $("#zip_loader").hide();
                //------------------------------------
                var all_data_div='';
                var pincode_length=result.pincode.length;
                for(var i=0;i< pincode_length; i++)
                {
                    all_data_div+='<option value="'+result.list_id[i]+'" >'+result.pincode[i]+'</option>';
                }
                //-------------------------------------
                $("#zip_dropdown").html("<option value='-1'>Select Zipcode</option>");
                $("#zip_dropdown").append(all_data_div);

            }
        });
    }


    //-----------------------------
    function append_zipcode_value(selected_zipcode)
    {
        //alert(selected_zipcode);
        var zipcode = $("#zip_dropdown option:selected").text();
        var data=$('#selected_zipcode').val();
        if(data=="")
        {
            var data1=data+''+zipcode;
        }
        else
        {
            var data1=data+','+' '+zipcode;
        }

        $('#selected_zipcode').val(data1);
        var selected_zipcode_id=$('#selected_zipcode_id').val();
        if(selected_zipcode_id=="")
        {
            var data3=selected_zipcode_id+''+selected_zipcode;
        }
        else
        {
            var data3=selected_zipcode_id+','+' '+selected_zipcode;
        }

        $('#selected_zipcode_id').val(data3);

    }

    function submit_edit_preference_form()
    {
        $('.error').html("");
        var country=$('#country').val();
        var state=$('#state_dropdown').val();

        var pre_country=<?php echo $pre_res[0]['country']; ?>;
        var pre_state=<?php echo $pre_res[0]['state']; ?>;

        var selected_zipcode=$('#selected_zipcode').val();
        var selected_city=$('#selected_city').val();
        if(country=="-1")
        {
            $('#pre_country').val(pre_country);
            if(state=="-1")
            {
                $('#pre_state').val(pre_state);
            }
            //submit Edit Preference form
            $('#edit_preference_form').submit();

        }
        else
        {
            if(state=="-1")
            {
                $('.error').html($('<span class="error" style="padding-left:10px;">Please select the State </span>'));
                return false;
            }
            else if(selected_city=="")
            {
                $('.error').html($('<span class="error" style="padding-left:10px;">Please select the  City </span>'));
                return false;
            }
            else if(selected_zipcode=="")
            {
                $('.error').html($('<span class="error" style="padding-left:10px;">Please select the Pincode </span>'));
                return false;
            }
            else
            {
                //submit Edit Preference form
                $('#edit_preference_form').submit();
            }
        }


    }

</script>