<style type="text/css">
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
        font-weight: bold;
    }
</style>
<!--<link href="--><?php //echo base_url(); ?><!--assets/css/multiselect.css" media="screen" rel="stylesheet" type="text/css">-->
<link href="<?php echo base_url(); ?>assets/css/property_details.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300"rel="stylesheet">
<!--<link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->
<!--<link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/croppie.css">-->



<div class="container" id="">
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px">
                <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;USER DASHBOARD</h4>
            </div>
            <!--<h4><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp; </h4>-->           	
        </div>
    </div>
   <ul class="nav nav-tabs" style="font-size: 16px;" id="myTab">
        <li id="profile_li" class="active"><a data-toggle="tab" href="#profile"><i class="fa fa-user" aria-hidden="true"></i> Basic Details</a></li>
        <li id="location_ul"><a data-toggle="tab" href="#location"><i class="fa fa-check-square-o location_li" aria-hidden="true"></i> Location</a></li>
        <li><a data-toggle="tab" href="#property_details"><i class="fa fa-gears" aria-hidden="true"></i>Property Details</a></li>
<!--        <li><a data-toggle="tab" href="#my_acc"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Pricing</a></li>-->
        <?php if($this->session->userdata('user_type') == 'public'){ ?>
        <li  class="preference_li"><a data-toggle="tab" href="#preferences"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Preferences</a></li>
        <?php }else{?>
        <li style="display: none" class="preference_li"><a data-toggle="tab" href="#preferences"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Preferences</a></li>

        <?php }?>
<!--        <li style="display:none" class="account_details_li"><a data-toggle="tab" href="#account_details"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Account Details</a></li>-->
    </ul>
    <form name="property_info"  id="property_info" class="" action="<?php echo site_url('postproperty/save_property_info'); ?>" method="post" enctype="multipart/form-data">

    <div class="tab-content" id="tabs">
<!--        <form name="property_info" class="" action="--><?php //echo site_url('postproperty/save_property_info'); ?><!--">-->


        <div id="profile" class="tab-pane fade in active">
<!--            <h3> User Profile</h3>-->
            </br>
            </br>
            <div id="outer_div" style="border:1px solid #eaeaea;background-color: #eaedf1;">
                </br>
                </br>

<!--                <ul class="userOptions" ng-class="{'userOptionsError':ClassError}">-->
<!--                    <li><div class="owner active" ng-click="setClass('O')" ng-class="(formObj.Class == 'O') ? 'active' : ''"><i></i><span class="imgowe"><label  style="font-size:13px;font-weight:normal;">Owner</label> </span ></div></li>-->
<!--                    <li><div class="dealer" ng-click="setClass('A')" ng-class="(formObj.Class == 'A') ?'active': ''"><i></i><span></span><label  style="font-size:13px;font-weight:normal;">Dealer</label> </div></li>-->
<!--                    <li><div class="builder" ng-click="setClass('B')" ng-class="(formObj.Class == 'B') ?'active': ''"><i></i><span></span><label  style="font-size:13px;font-weight:normal;">Builder</label> </div></li>-->
<!--                </ul>-->

                <!-- ---------------------------------------------------- -->
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font">You are<span style="color:red;">*</span>:</label>
                    </div>
                    <div class="col-lg-2">
                        <input type="radio" name="you_are" value="Owner" style="margin-bottom: 3%;">  <label style="font-size: 16px;font-weight: normal;">Owner</label>
                    </div>
                    <div class="col-lg-2">
                        <input type="radio" name="you_are" value="Dealer" style="margin-bottom: 3%;"><label style="font-size: 16px;font-weight: normal;">Dealer</label>
                    </div>
                    <div class="col-lg-2">
                        <input type="radio" name="you_are" value="Builder" style="margin-bottom: 3%;"> <label style="font-size: 16px;font-weight: normal;">Builder</label>
                    </div>
                </div>
                </br>
                <!-- ---------------------------------------------------- -->

                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font">List property for<span style="color:red;">*</span>:</label>
                    </div>
                    <div class="col-lg-3">
                        <div  id="private_icon_div" class="col-lg-1" style="display:none">
                            <i class="fa fa-user-secret" aria-hidden="true" style="font-size: 20px !important;color: black;padding-top: 10px;"></i>
                        </div>
                        <div  id="public_icon_div" class="col-lg-1" style="display:none">
                            <i class="fa fa-users" aria-hidden="true" style="font-size: 20px !important;color: black;padding-top: 10px;"></i>
                        </div>
                        <div class="col-lg-10">
                            <select name="list_property" id="list_property" class="select_input form-control" onchange="display_preference_div(this.value);" style="" >
                                <option value="">Select</option>
                                <option value="public" label="Public">Public</option>
                                <option value="private" label="Private">Private</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-1">

                        <span  id="public_t_id" class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="" style="padding-top: 13%;cursor:pointer;font-size:17px; display:none"></span>
                        <span  id="private_t_id" class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="" style="padding-top: 13%;cursor:pointer;font-size:17px;display:none"></span>
                    </div>
                    </div>
                </br>

                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font" style="">Property Type<span style="color:red;">*</span>:

                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10">
                        <!--  ----------------------------------  -->
                        <div id="disply_resi_comm" style="background-color: #fdfdfc;">
                            <ul class="nav nav-tabs" id="property_type" style="font-size: 12px;">
                                <li class="active"><a data-toggle="tab" href="#residentail"> Residential</a></li>
                                <li><a data-toggle="tab" href="#Commercial">Commercial</a></li>
                            </ul>

                            <input type="hidden" name="selected_property_type" id="selected_property_type" />
                            <div class="tab-content">
                            <div id="residentail" class="tab-pane fade in active">
                                <!--                        <h2>Hi</h2>-->
                                <div class="row_select">

                                    <!-- --------------- -->
                                    <div class=" row multi-columns-row " style="padding-top: 5px;">
                                        <div class="col-sm-6 col-md-3 col-lg-2">
                                            <div class="select_type" style="">
                                                <div class="col-lg-12">
                                                    <img src="<?php echo base_url(); ?>assets/images/apartment.jpg" class="" alt="User Image" height="50" width="50">
                                                 </div>
                                                <div class="col-lg-1">
                                                    <input type="radio" name="residential_type" value="Residential Apartment" style="padding-top:10px;">
                                                </div>
                                                <div class="col-lg-4">
                                                    <p style="font-size:12px!important;">Residential Apartment</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-3 col-lg-2">
                                            <div class="select_type" style="">
                                                <div class="col-lg-12">
                                                     <img src="<?php echo base_url(); ?>assets/images/residence.jpg" class="" alt="User Image" height="50" width="50">
                                                 </div>
                                                <div class="col-lg-1">
                                                    <input type="radio" name="residential_type" value="Residential Land" style="padding-top:10px;">
                                                </div>
                                                <div class="col-lg-4">
                                                    <p style="font-size:12px!important;">Residentail Land</p>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-6 col-md-3 col-lg-2">
                                            <div class="select_type" style="">
                                                <div class="col-lg-12">
                                                    <img src="<?php echo base_url(); ?>assets/images/Independent_House.jpg" class="" alt="User Image" height="50" width="50">
                                                </div>
                                                <div class="col-lg-1">
                                                    <input type="radio" name="residential_type" value="Independent House/Villa" style="padding-top:10px;">
                                                </div>
                                                <div class="col-lg-4">
                                                    <p style="font-size:12px!important;">Independent House/Villa</p>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-6 col-md-3 col-lg-2">
                                            <div class="select_type" style="">
                                                <div class="col-lg-12">
                                                     <img src="<?php echo base_url(); ?>assets/images/independent_builder.png" class="" alt="User Image" height="50" width="50">
                                                </div>
                                                <div class="col-lg-1">
                                                        <input type="radio" name="residential_type" value="Independent Builder Floor" style="padding-top:10px;">
                                                </div>
                                                <div class="col-lg-4">
                                                            <p style="font-size:12px!important;">Independent Builder Floor</p>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-6 col-md-3 col-lg-2">
                                            <div class="select_type" style="">
                                                <div class="col-lg-12">
                                                     <img src="<?php echo base_url(); ?>assets/images/farm_house.png" class="" alt="User Image" height="50" width="50">
                                                </div>
                                                <div class="col-lg-1">
                                                    <input type="radio" name="residential_type" value="Farm House" style="padding-top:10px;">
                                                </div>
                                                <div class="col-lg-4">
                                                    <p style="font-size:12px!important;">Farm House</p>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-6 col-md-3 col-lg-2">
                                            <div class="select_type" style="">
                                                <div class="col-lg-12">
                                                    <img src="<?php echo base_url(); ?>assets/images/studio_apartment.png" class="" alt="User Image" height="50" width="50">
                                                    </div>
                                                <div class="col-lg-1">
                                                    <input type="radio" name="residential_type" value="Studio Apartment" style="padding-top:10px;">
                                                </div>
                                                <div class="col-lg-4">
                                                    <p style="font-size:12px!important;">Studio Apartment</p>
                                                </div>
                                            </div>
                                        </div>


                                    </div> <!-- end of div class row--- -->


                                    <div class=" row multi-columns-row ">
                                        <div class="col-sm-6 col-md-3 col-lg-2">
                                            <div class="select_type" style="">
                                                <div class="col-lg-12">
                                                    <img src="<?php echo base_url(); ?>assets/images/service_apartment.jpg" class="" alt="User Image" height="50" width="50">
                                                </div>
                                                <div class="col-lg-1">
                                                    <input type="radio" name="residential_type" value="Serviced Apartment" style="padding-top:10px;">
                                                </div>
                                                <div class="col-lg-4">
                                                    <p style="font-size:12px!important;">Serviced Apartment</p>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-6 col-md-3 col-lg-2">
                                            <div class="select_type" style="">
                                                <div class="col-lg-12">
                                                    <img src="<?php echo base_url(); ?>assets/images/other.jpg" class="" alt="User Image" height="50" width="50">
                                                </div>
                                                <div class="col-lg-1">
                                                    <input type="radio" name="residential_type" value="Other" style="padding-top:10px;">
                                                </div>
                                                <div class="col-lg-4">
                                                    <p style=" padding-top: 4px;font-size:12px!important;">Other</p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <!-- ---------------------- -->

                                </div>

                            </div>

                            <div id="Commercial" class="tab-pane fade in">
                                <!--                        <h2>Hello</h2>-->
                                <div class=" row multi-columns-row " style="padding-top: 5px;">
                                    <div class="col-sm-6 col-md-3 col-lg-2">
                                        <div class="select_type" style="">
                                            <div class="col-lg-12">
                                                <img src="<?php echo base_url(); ?>assets/images/shop.jpg" class="" alt="User Image" height="50" width="50">
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="radio" name="commercial_type" value="Commercial Shops" style="padding-top:10px;">
                                            </div>
                                            <div class="col-lg-4">
                                                <p style="font-size:12px!important;">Commercial Shops</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-2">
                                        <div class="select_type" style="">
                                            <div class="col-lg-12">
                                                <img src="<?php echo base_url(); ?>assets/images/space_in_office.png" class="" alt="User Image" height="50" width="50">
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="radio" name="commercial_type" value="Commercial Office/Space" style="padding-top:10px;">
                                            </div>
                                            <div class="col-lg-4">
                                                <p style="font-size:12px!important;">Commercial Office/Space</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-2">
                                        <div class="select_type" style="">
                                            <div class="col-lg-12">
                                                <img src="<?php echo base_url(); ?>assets/images/residentail_apartment.png" class="" alt="User Image" height="50" width="50">
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="radio" name="commercial_type" value="Industrial Lands/Plots" style="padding-top:10px;">
                                            </div>
                                            <div class="col-lg-4">
                                                <p style="font-size:12px!important;">Industrial Lands/Plots</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-2">
                                        <div class="select_type" style="">
                                            <div class="col-lg-12">
                                                <img src="<?php echo base_url(); ?>assets/images/Agriculture.jpg" class="" alt="User Image" height="50" width="50">
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="radio" name="commercial_type" value="Agricultural/Farm Land" style="padding-top:10px;">
                                            </div>
                                            <div class="col-lg-4">
                                                <p style="font-size:12px!important;">Agricultural/Farm Land</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-2">
                                        <div class="select_type" style="">
                                            <div class="col-lg-12">
                                                <img src="<?php echo base_url(); ?>assets/images/hotel.jpg" class="" alt="User Image" height="50" width="50">
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="radio" name="commercial_type" value="Hotel/Resorts" style="padding-top:10px;">
                                            </div>
                                            <div class="col-lg-4">
                                                <p style="font-size:12px!important;">Hotel/Resorts</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-2">
                                        <div class="select_type" style="">
                                            <div class="col-lg-12">
                                                <img src="<?php echo base_url(); ?>assets/images/commercial_land.jpg" class="" alt="User Image" height="50" width="50">
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="radio" name="commercial_type" value="Commercial Land/Inst. Land" style="padding-top:10px;">
                                            </div>
                                            <div class="col-lg-4">
                                                <p style="font-size:12px!important;">Commercial Land/Inst. Land</p>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end of row div ---- -->
                                <div class="row">
                                    <div class="col-sm-6 col-md-3 col-lg-2">
                                        <div class="select_type" style="">
                                            <div class="col-lg-12">
                                                <img src="<?php echo base_url(); ?>assets/images/warehouse.png" class="" alt="User Image" height="50" width="50">
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="radio" name="commercial_type" value="Ware House" style="padding-top:10px;">
                                            </div>
                                            <div class="col-lg-4">
                                                <p style="font-size:12px!important;">Ware House</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-2">
                                        <div class="select_type" style="">
                                            <div class="col-lg-12">
                                                <img src="<?php echo base_url(); ?>assets/images/cold_storage.jpg" class="" alt="User Image" height="50" width="50">
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="radio" name="commercial_type" value="Cold Storage" style="padding-top:10px;">
                                            </div>
                                            <div class="col-lg-4">
                                                <p style="font-size:12px!important;">Cold Storage</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-2">
                                        <div class="select_type" style="">
                                            <div class="col-lg-12">
                                                <img src="<?php echo base_url(); ?>assets/images/residentail_apartment.png" class="" alt="User Image" height="50" width="50">
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="radio" name="commercial_type" value="Factory" style="padding-top:10px;">
                                            </div>
                                            <div class="col-lg-4">
                                                <p style=" padding-top: 4px;font-size:12px!important;">Factory</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-2">
                                        <div class="select_type" style="">
                                            <div class="col-lg-12">
                                                <img src="<?php echo base_url(); ?>assets/images/other.jpg" class="" alt="User Image" height="50" width="50">
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="radio" name="commercial_type" value="Other" style="padding-top:10px;">
                                            </div>
                                            <div class="col-lg-4">
                                                <p style=" padding-top: 4px;font-size:12px!important;">Other</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div> <!-- end of div commercial --->
                                </div>


                        </div>


                        <!------------------------------  -->

                    </div>

                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="error col-lg-4" style="color:red;font-size:15px;"></div>
                </div>


            </br>
                <div class="row" style="margin-left: 0%;margin-right: 0%;background-color: #eaedf1;">
                    <div class="col-md-11">
                        <div class="btn-toolbar pull-right">
                            <div class="btn-group">
                                <!--                <button class="btn btn-default change-tab" data-direction="previous" data-target="#myTab"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Last</button>-->
                                <button id="next_button_for_validate" class="btn btn-default"  onclick="validate_basic_details()" data-direction="next"  style="" >Next <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div> <!-- end of outer div -->

        </div> <!-- end of profile div>
        <!-- //End Pofile  -->

        <!-- start the code of location div --->
        <div id="location" class="tab-pane fade in">
<!--            <h2>Hello</h2>-->
            </br>
            </br>
            <div id="location_outer" style="border:1px solid #eaeaea;background-color: #eaedf1;">
                </br>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-4">
                        <p style="font-size:16px">Where is the property located?</p>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-4 form-group">
                            <label class="control-label label_font">City<span style="color:red;">*</span>:</label>
                            <select name="city_dropdown" id="city_select" class=" form-control" style="margin-top: 3%;" onchange="show_city_name(this.value);" >
                                <option value="Pune">Pune</option>
                                <option value="Mumbai">Mumbai</option>
                            </select>

                    </div>
                    <div class="col-lg-1 form-group">
                    </div>
                    <div class="col-lg-4 ">
                            <label class="control-label label_font">Project Name<span style="color:red;">*</span>:</label>
                            <input  type="text" name="project_name" id="project_name" class="form-control" style="" />
                    </div>
                    <div class="col-lg-1 form-group">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-4 form-group map_outer_div">
                        <label class="control-label label_font">Locality<span style="color:red;">*</span>:</label>
<!--                        <input type="text"  class="form-control"/>-->
                        <input id="pac-input" name="locality" id="locality" class="controls" type="text" placeholder="Search Box">
                        <div id="map" style=" width: 94%;height: 187px;"></div>
                    </div>
                    <div class="col-lg-1 form-group">
                    </div>
                    <div class="col-lg-4 " style="margin-left: 3%;">
                        <label class="control-label label_font">Complete Address<span style="color:red;">*</span>:</label>
                        <input type="text"  name="complete_address" id="complete_address" class="form-control" style="" />
                    </div>
                    <div class="col-lg-1 form-group">
                    </div>
                </div>

                </br>
            </div> <!-- end of location inner div --- -->

        </div> <!-- end of location div --->

        <!-- end of  the code of location div --->

        <!--- start the code of property details--------------- -->
        <div id="property_details" class="tab-pane fade in">
            </br>  </br>
            <div id="inner_div_of_property_details" style="border:1px solid #eaeaea;background-color: #eaedf1;">
                </br>

                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Plot Area <span style="color:red;">*</span>:</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="col-lg-6">
                            <input type="text" name="plot_area" id="plot_area" class="form-control text_input" style=""/>
                        </div>
                        <div class="col-lg-6">
                            <select  name="plot_area_measurement" id="plot_area_measurement" class="form-control" style=""/>>
                                <option value="Sq.Feet">Sq.Feet</option>
                                <option value="Sq.Meter">Sq.Meter</option>
                                <option value="Acres">Acres</option>
                                <option value="Hectares">Hectares</option>
                            </select>

                        </div>

                    </div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">No of Bathrooms:</label>
                    </div>
                    <div class="col-lg-3">
<!--                        <input type="text" name="length" id="" class=" form-control text_input" style="height:60%;" />-->
                        <select class="form-control" name="no_of_bathrooms" id="no_of_bathrooms" style="">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="9+">9+</option>
                        </select>

                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Length:</label>
                    </div>
                    <div class="col-lg-3">
<!--                        <input type="text" name="breadth" id="" class="form-control text_input" style="height:60%"/>-->
                        <div class="col-lg-6">
                            <input type="text" name="length" id="length" class="form-control text_input" style=""/>
                        </div>
                        <div class="col-lg-6">
                            <select  name="select_length_measurement" class="form-control" style=""/>
                            <option value="">Select</option>
                            <option value="Feet">Feet</option>
                            <option value="Meter">Meter</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Breadth:</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="col-lg-6">
                            <input type="text" name="breadh" id="breadh" class="form-control text_input" style=""/>
                        </div>
                        <div class="col-lg-6">
                            <select  name="select_breadth_measurement" id="select_breadth_measurement" class="form-control" style=""/>
                            <option value="">Select</option>
                            <option value="Feet">Feet</option>
                            <option value="Meter">Meter</option>
                            </select>
                        </div>

                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">No of Bedrooms<span style="color:red;">*</span>:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="no_of_bedrooms" id="no_of_bedrooms" style="">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="9+">9+</option>
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <label class=" control-label label_font label_style">No of Balcony<span style="color:red;">*</span>:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="no_of_balcony" id="no_of_balcony" style="">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="9+">9+</option>
                        </select>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">No of Hall<span style="color:red;">*</span>:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="no_of_hall" id="no_of_hall" style="">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="9+">9+</option>
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">No of Kitchen <span style="color:red;">*</span>:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="no_of_kitchen" id="no_of_kitchen" style="">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="9+">9+</option>
                        </select>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
<!--                    <div class="col-lg-2">-->
<!--                        <label class="control-label label_font label_style">Complete Address<span style="color:red;">*</span> :</label>-->
<!--                    </div>-->
<!--                    <div class="col-lg-3">-->
<!--                        <input type="text" name="complete_address" id="complete_address" class="form-control text_input" style=""/>-->
<!--                    </div>-->
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Possession:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="possession" id="possession" style="">
                            <option value="">Select Possession</option>
                            <option value="Immediate">Immediate</option>
                            <option value="1 Month">1 Month</option>
                            <option value="2 Month">2 Month</option>
                            <option value="3 Month">3 Month</option>
                            <option value="4 Month">4 Month</option>
                            <option value="5 Month">5 Month</option>
                            <option value="6 Month">6 Month</option>
                            <option value="7 Month">7 Month</option>
                            <option value="8 Month">8 Month</option>
                            <option value="9 Month">9 Month</option>
                            <option value="10 Month">10 Month</option>
                            <option value="11 Month">11 Month</option>
                            <option value="12 Month">12 Month</option>
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Garden:</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="col-lg-4">
                            <input type="radio" name="garden" value="yes" style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                        </div>
                        <div class="col-lg-4">
                            <input type="radio" name="garden" value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                        </div>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Water(Borewell) <span style="color:red;">*</span>:</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="col-lg-4">
                            <input type="radio" name="water_borewell" value="yes" style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                        </div>
                        <div class="col-lg-4">
                            <input type="radio" name="water_borewell" value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Electricity/Generator<span style="color:red;">*</span>:</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="col-lg-4">
                            <input type="radio" name="electricity_backup" value="yes" style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                        </div>
                        <div class="col-lg-4">
                            <input type="radio" name="electricity_backup" value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                        </div>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Swimming-pool:</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="col-lg-4">
                            <input type="radio" name="swimming_pool" value="yes" style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                        </div>
                        <div class="col-lg-4">
                            <input type="radio" name="swimming_pool" value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Age of Property:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="age_of_property" style="">
                            <option>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>9+</option>
                        </select>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Electricity(24/7):</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="col-lg-4">
                            <input type="radio" name="electricity" value="yes" style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                        </div>
                        <div class="col-lg-4">
                            <input type="radio" name="electricity" value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Legal Status:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="legal_status" id="legal_status" style="">
                            <option value="pending">Pending Cases</option>
                            <option value="notpending">Not Pending Cases</option>

                        </select>
                    </div>
                </div>
                <!----------------------------------- -->
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-4">
                        <h3>Property Images</h3>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-3">
                        <label class="control-label label_font label_style">Select File to upload <span style="color:red;">*</span>:</label>

                    </div>
                    <div class="col-lg-5">
                        <!--                                   <span class="btn btn-default btn-file">-->
                        <input name="pro_photo" id="pro_photo" placeholder="Property Image" class="form-control"  type="file" required="" style="margin-bottom: 4%">

                        <!--                                    </span>-->
                    </div>
                </div>
<!--                </br>-->
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-3">
<!--                        <label class="label_font label_style">Select File:</label>-->
                    </div>
                    <div class="col-lg-5">
                        <!--                                   <span class="btn btn-default btn-file">-->
                        <input name="pro_photo_1" id="pro_photo_1" placeholder="Property Image" class="form-control"  type="file">

                        <!--                                    </span>-->
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-3">
<!--                        <label class="label_font label_style">Select File:</label>-->
                    </div>
                    <div class="col-lg-5">
                        <!--                                   <span class="btn btn-default btn-file">-->
                        <input name="pro_photo_2" id="pro_photo_2" placeholder="Property Image" class="form-control"  type="file">

                        <!--                                    </span>-->
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-3">
<!--                        <label class="label_font label_style">Select File:</label>-->
                    </div>
                    <div class="col-lg-5">
                        <!--                                   <span class="btn btn-default btn-file">-->
                        <input name="pro_photo_3" id="pro_photo_3" placeholder="Property Image" class="form-control"  type="file">

                        <!--                                    </span>-->
                    </div>
                </div>
                <!-- --------------------------------- -->
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="error col-lg-4" style="color:red;font-size:15px;"></div>
                </div>

                <!---  start code for save button -------------- -->

                </br>
                </br>
                <div class="row">
                    <div class="col-lg-5"></div>
                    <div class="col-lg-3">
                        <input type="button" value="save" class="btn btn-primary" onclick="validate_property_details();" />
                    </div>
                </div>
                <!---  end code for save button -------------- -->

                </br>
                </br>
            </div>
        </div> <!-- End of the property_details_div -->
    </form>
        <!--- end the code of property details--------------- -->
        <!-- -------start the code of the preferences div ------ -->
        <div id="preferences" class="tab-pane fade in ">
            </br>
            </br>
            <div id="outer_div" style="border:1px solid #eaeaea;background-color: #eaedf1;">
                </br>
                </br>
                <form name="preference_details_info" class="" action="<?php echo site_url('postproperty/save_preference_info'); ?>" method="post">

                <input type="hidden" name="property_id" id="property_id" value="<?php echo $this->uri->segment(3); ?>"/>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Plot Area:</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="col-lg-6">
                            <input type="text" name="plot_area" id="plot_area" class="form-control text_input" style=""/>
                        </div>
                        <div class="col-lg-6">
                            <select  name="plot_area_measurement"  id="plot_area_measurement" class="form-control" style=""/>>
                            <option>Sq.Feet</option>
                            <option>Sq.Meter</option>

                            </select>

                        </div>

                    </div>
                    <div class="col-lg-2">
                        <label class=" control-label label_font label_style">No of Bathrooms:</label>
                    </div>
                    <div class="col-lg-3">
                        <!--                        <input type="text" name="length" id="" class=" form-control text_input" style="height:60%;" />-->
                        <select class="form-control" name="no_of_bathrooms" style="">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>9+</option>
                        </select>

                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">No of Bedrooms:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="no_of_bedrooms" style="">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>9+</option>
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <label class=" control-label label_font label_style">No of Balcony:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="no_of_balcony" style="">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>9+</option>
                        </select>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">No of Hall:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="no_of_hall" style="">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>9+</option>
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">No of Kitchen:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="no_of_kitchen" style="">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>9+</option>
                        </select>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Electricity(24/7):</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="col-lg-4">
                            <input type="radio" name="electricity" value="yes" style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                        </div>
                        <div class="col-lg-4">
                            <input type="radio" name="electricity" value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Garden:</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="col-lg-4">
                            <input type="radio" name="garden" value="yes" style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                        </div>
                        <div class="col-lg-4">
                            <input type="radio" name="garden" value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                        </div>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Water(Borewell):</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="col-lg-4">
                            <input type="radio" name="water_borewell" value="yes" style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                        </div>
                        <div class="col-lg-4">
                            <input type="radio" name="water_borewell" value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Electricity/Generator:</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="col-lg-4">
                            <input type="radio" name="electricity_backup" value="yes" style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                        </div>
                        <div class="col-lg-4">
                            <input type="radio" name="electricity_backup" value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                        </div>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Swimming-pool:</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="col-lg-4">
                            <input type="radio" name="swimming_pool" value="yes" style="margin-bottom: 9%;" /><label class="control-label label_font" style="padding-left:10px;">Yes</label>
                        </div>
                        <div class="col-lg-4">
                            <input type="radio" name="swimming_pool" value="no" style="margin-bottom: 9%;"/><label class="control-label label_font" style="padding-left:10px;">No</label>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <label class="control-label label_font" style="padding-top:9px;">Legal Status:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="legal_status" id="legal_status" style="">
                            <option value="pending">Pending Cases</option>
                            <option value="notpending">Not Pending Cases</option>

                        </select>
                    </div>

                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Possession:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="possession" id="possession" style="">
                            <option value="">Select Possession</option>
                            <option value="Immediate">Immediate</option>
                            <option value="1 Month">1 Month</option>
                            <option value="2 Month">2 Month</option>
                            <option value="3 Month">3 Month</option>
                            <option value="4 Month">4 Month</option>
                            <option value="5 Month">5 Month</option>
                            <option value="6 Month">6 Month</option>
                            <option value="7 Month">7 Month</option>
                            <option value="8 Month">8 Month</option>
                            <option value="9 Month">9 Month</option>
                            <option value="10 Month">10 Month</option>
                            <option value="11 Month">11 Month</option>
                            <option value="12 Month">12 Month</option>

                        </select>
                    </div>
                    <div class="col-lg-2">
                        <label class="control-label" style="padding-top:9px;">Age of Property:</label>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="age_of_property" >
                            <option>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>9+</option>
                        </select>
                    </div>

                </div>
                </br>
                <!-- ------ start code of display coutry state zipcode----- -->
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Select Country:</label>
                    </div>
                    <div class="col-lg-5">
                        <select class="form-control" name="country" style="" onchange="selectState(this.options[this.selectedIndex].value)">
                            <option value="-1">Select country</option>
                            <option value="1">India</option>
                        </select>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Select state:</label>
                    </div>
                    <div class="col-lg-5">
                        <select class="form-control" name="state" id="state_dropdown" style="" onchange="selectCity(this.options[this.selectedIndex].value)">
                            <option></option>
                        </select>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Select City:</label>
                    </div>
                    <div class="col-lg-5">
                        <select class="form-control" name="city"  id="city_dropdown" onchange="getZipcode(this.options[this.selectedIndex].text, this.options[this.selectedIndex].value)" style="">
                            <option></option>
                        </select>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Select Pincode of Prefered Area:</label>
                    </div>
                    <div class="col-lg-5">
<!--                        <select  class="form-control ruleparameter" size="5" multiple  name="HIGHEST_RCP_RP" id="zip_dropdown" style="" >-->
<!--                            <option>411038</option>-->
<!--                            <option>411035</option>-->
<!--                            <option>411036</option>-->
<!--                            <option>411078</option>-->
<!--                            <option>411090</option>-->
<!--                            <option>411290</option>-->
<!---->
<!--                        </select>-->
                        <select  class="form-control" size=""  name="" id="zip_dropdown" style="" onchange="append_zipcode_value(this.value)" >
                        </select>

<!--                        <div id="show_zipcode"></div>-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Selected Zipcode:</label>
                    </div>
                    <div class="col-lg-5">
                        <input  type="text" name="selected_zipcode" id="selected_zipcode" class="form-control" style="height:100%;" />
                        <input type="hidden" name="selected_zipcode_id" id="selected_zipcode_id"/>

                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <label class="control-label label_font label_style">Selected City:</label>
                    </div>
                    <div class="col-lg-5">
                        <input  type="text" name="selected_city" id="selected_city" class="form-control" style="height:100%;" />
                        <input type="hidden" name="selected_city_id" id="selected_city_id"/>
                    </div>
                </div>

                <!-- ------end code of display coutry state zipcode----- -->

                </br>
                </br>
                <div class="row">
                    <div class="col-lg-5"></div>
                    <div class="col-lg-3">
                        <input type="submit" value="save" class="btn btn-primary" />
                    </div>
                </div>
                </br></br>
                </form>
            </div>
        </div> <!-- end of the preferences div>
        <!-- ----------end of the code of preferences dv ------------ -->

        <!-- start the code of account details div-------------- -->

        <!-- end of code of profile details div ----------- -->
    <!--------------- --->
    <div class="row" style="margin-left: 0%;margin-right: 0%;background-color: #eaedf1;">
        <div class="col-md-11">
            <div class="btn-toolbar pull-right">
                <div class="btn-group">
                    <!--                <button class="btn btn-default change-tab" data-direction="previous" data-target="#myTab"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Last</button>-->
                    <button  id="change_tab_next_btn" class="btn btn-default change-tab" data-direction="next" data-target="#myTab" style="display:none" >Next <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
                    </button>
                </div>
            </div>
        </div>

    </div>

    <!--- -  ------------------ -->
    </br>
    </br>

    </div> <!--end of div tab-content -->

<!--      </form>-->




</div> <!-- end of container div---->




<!--<script type="text/javascript" src="--><?php //echo base_url(); ?><!--assets/js/jquery.min.js"></script>-->
<!--    <script type="text/javascript" src="--><?php //echo base_url(); ?><!--assets/js/jquery.min.js"></script>-->

<!--<script type="text/javascript" src="--><?php //echo base_url() ?><!--assets/js/select2.full.min.js"></script>-->
<!--<script type="text/javascript" src="--><?php //echo base_url() ?><!--assets/js/jquery.multi-select.js"></script>-->

<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>-->
<!--<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>-->


<script>


    function disply_content_tooltip(value)
    {
        if(value=="public")
        {
            $('#public_t_id').css('display','block');
            $('#private_t_id').css('display','none');
            $('#public_t_id').tooltip({
                title: 'Public Property Type content'
            });
        }
        else
        {
            $('#public_t_id').css('display','none');
            $('#private_t_id').css('display','block');
            $('#private_t_id').tooltip({
                title: 'Private Property type Content'
            });
        }

    }


</script>

<script>
    $("#property_type li").on("click", function(){
       // alert($(this).text());
        var selected_property_type_value=$(this).text();

        $('#selected_property_type').val(selected_property_type_value);
    });
    
    $(document).ready(function () {
        var selected_property_type_val = $('#property_type').find('li.active').text();
        $('#selected_property_type').val(selected_property_type_val);

    });
    
    function display_preference_div(value)
    {
        if(value=='public')
        {
            $(".preference_li").css("display","block");
            $('#public_icon_div').css('display','block');
            $('#private_icon_div').css('display','none');
            disply_content_tooltip(value);
        }
        else
        {
            $(".preference_li").css("display","none");
            $('#public_icon_div').css('display','none');
            $('#private_icon_div').css('display','block');
            disply_content_tooltip(value);
        }
    }


</script>
<script>

    $('.change-tab').click(function() {
        var $this = $(this);
        var $nav = $($this.data('target'))
        var $tabs = $nav.find('li');
        var $curTab = $tabs.filter('.active');
        var cur = $tabs.index($curTab);
        if ($this.data('direction') == 'next') var $showTab = cur == $tabs.length - 1 ? $tabs.eq(0).find('a') : $tabs.eq(cur + 1).find('a');
        else var $showTab = cur == 0 ? $tabs.eq($tabs.length - 1).find('a') : $tabs.eq(cur - 1).find('a');
        $showTab.tab('show');
    });



    // normal tabs code
    $('#myTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

//    $(function () {
//        $('#myTab a:last').tab('show');
//    })

    function validate_basic_details()
    {

        $('.error').html("");
        var list_property=$('#list_property').val();
//        var property_type=$("#property_type li").text();
      //  var property_type = $('#property_type li a.active').text();
       // var proprty_type=$('#selected_property_type').val();
         var selected_property=$('#selected_property_type').val();
       // alert(selected_property);
        selected_property=selected_property.trim();

        if(!($("input:radio[name='you_are']").is(":checked")))
        {

           //$('.notification_message_type_div').after($('<span class="error">Please Select only one Category for Tips and Tricks</span>'));
           //if((category_id!=undefined) || (category_id!=null))
           //$('.html_content_type_div').css('display','block');
           $('.error').html($('<span class="error" style="padding-left:10px;">Please Select field you are </span>'));
            return false;

       }
        else if((list_property==""))
        {
            $('.error').html($('<span class="error" style="padding-left:10px;">Please Select List Property </span>'));
        }
//        else if((!($("input:radio[name='residential_type']").is(":checked"))))
//        {
//
//            $('.error').html($('<span class="error" style="padding-left:10px;">Please Select Property type </span>'));
//        }
        else if(selected_property=="")
        {
                        $('.error').html($('<span class="error" style="padding-left:10px;">Please Select Property type </span>'));

        }
        else if(selected_property=="Residential")
        {

            if(!($("input:radio[name='residential_type']").is(":checked")))
            {
                $('.error').html($('<span class="error" style="padding-left:10px;">Please Select Residentail  type </span>'));

            }
            else
            {
                $('#location_ul').addClass("enabled");
                $('#change_tab_next_btn').css('display','block');
                $('#next_button_for_validate').css('display','none');
            }
        }
        else if(selected_property=="Commercial")
        {

            if(!($("input:radio[name='commercial_type']").is(":checked")))
            {
                $('.error').html($('<span class="error" style="padding-left:10px;">Please Select Commercial  type </span>'));

            }
            else
            {

                $('#change_tab_next_btn').css('display','block');
                $('#next_button_for_validate').css('display','none');
            }
        }
        else
       {
          // alert("in else");
           $('#change_tab_next_btn').css('display','block');
          $('#next_button_for_validate').css('display','none');

       }

    }

    function validate_property_details()
    {
        //property_info
        $('.error').html("");
        var plot_area=$('#plot_area').val();
        var no_of_bedrooms=$('#no_of_bedrooms').val();
        var no_of_balcony=$('#no_of_balcony').val();
        var no_of_hall=$('#no_of_hall').val();
        var no_of_balcony=$('#no_of_balcony').val();
        var no_of_kitchen=$('#no_of_kitchen').val();
        var complete_address=$('#complete_address').val();
        var pro_photo=$('#pro_photo').val();

        var city_select=$('#city_select').val();
        var locality=$('#pac-input').val();
//        var address=$('#address').val();
        var project_name=$('#project_name').val();

        var length=$('#length').val();
        var breadth=$('#breadh').val();

        if(length!="")
        {
            if(breadth=="")
            {
                $('.error').html($('<span class="error" style="padding-left:10px;">Please Select Breadth </span>'));
                return false;

            }
        }
        if(breadth!="")
        {
            if(length=="")
            {
                $('.error').html($('<span class="error" style="padding-left:10px;">Please Select Length </span>'));
                return false;
            }
        }

        if((city_select==-1)||(city_select==""))
        {
            $('.error').html($('<span class="error" style="padding-left:10px;">Please Select City </span>'));

        }
        else if(project_name=="")
        {
            $('.error').html($('<span class="error" style="padding-left:10px;">Please Select Project Name </span>'));

        }
        else if((locality==undefined)||(locality==""))
        {
            $('.error').html($('<span class="error" style="padding-left:10px;">Please Select Locality </span>'));

        }
        else if(plot_area=="")
        {
            $('.error').html($('<span class="error" style="padding-left:10px;">Please Enter Plot Area </span>'));
            return false;
        }

        else if(no_of_bedrooms=="")
        {
            $('.error').html($('<span class="error" style="padding-left:10px;">Please Select No of Bedrooms </span>'));

        }
        else if(no_of_balcony=="")
        {
            $('.error').html($('<span class="error" style="padding-left:10px;">Please Select No of Balcony </span>'));

        }
        else if(no_of_hall=="")
        {
            $('.error').html($('<span class="error" style="padding-left:10px;">Please Select No of Hall </span>'));

        }
        else if(no_of_kitchen=="")
        {
            $('.error').html($('<span class="error" style="padding-left:10px;">Please Select No of Kitchen </span>'));

        }
        else if(complete_address=="")
        {
            $('.error').html($('<span class="error" style="padding-left:10px;">Please Enter Complete Address </span>'));

        }
        else if(!($("input:radio[name='water_borewell']").is(":checked")))
        {
            $('.error').html($('<span class="error" style="padding-left:10px;">Please Select Water/Boorewell </span>'));

        }
        else if(pro_photo=="")
        {
            $('.error').html($('<span class="error" style="padding-left:10px;">Please Select Property Image </span>'));

        }
        else
        {
           // alert("in else");
           $("#property_info").submit();
        }
    }
</script>


<script type="text/javascript">
    $(document).ready(function () {
//       $(".select2-hidden-accessible").hide();



        //------------------------------
        var mod = '<?php echo $mod; ?>';
        if(mod=="pre")
        {
            $("#preferences").addClass("active");
            $("#profile").removeClass("active");
            $('#profile_li').removeClass("active");
            $('.preference_li').addClass("active");

        }

        //------------------------------

        loadCity('city', 21); //21=maharashtra
        function loadCity(loadType, loadId) {
           // debugger;
            var dataString = 'loadType=' + loadType + '&loadId=' + loadId;
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Postproperty/loadCityname",
                data: dataString,
                cache: false,
                success: function (result) {
                                    //$("#" + loadType + "_loader").hide();
                    $("#" + loadType + "_select").html("<option value='-1'>Select " + loadType + "</option>");
                    $("#" + loadType + "_select").append(result);
                    //alert('getZipcode');
                }
            });
        }

       // $("#tabs").tabs();
    }); //End of document.ready function

    //function for sowing the city name in search input
    function show_city_name(value)
    {
        $('#pac-input').val(value);
    }

    function selectState(country_id) {

        if (country_id != "-1") {
            loadData('state', country_id);
            $("#city_dropdown").html("<option value='-1'>Select city</option>");
        } else {
            $("#state_dropdown").html("<option value='-1'>Select state</option>");
            $("#city_select").html("<option value=''>Select city</option>");
        }
    }

    function loadData(loadType, loadId) {
        var dataString = 'loadType=' + loadType + '&loadId=' + loadId;
          // $("#" + loadType + "_loader").show();
       // $("#" + loadType + "_loader").fadeIn(400).html('Please wait... <img src="<?php echo base_url(); ?>assets/images/loading.gif" />');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Postproperty/loadData",
            data: dataString,
            cache: false,
            success: function (result) {
               // $("#" + loadType + "_loader").hide();
                $("#" + loadType + "_dropdown").html("<option value='-1'>Select " + loadType + "</option>");
                $("#" + loadType + "_dropdown").append(result);
                //alert('getZipcode');
            }
        });
    }
    function selectCity(state_id) {

        if (state_id != "-1") {
            loadData('city', state_id);
        } else {
            $("#city_dropdown").html("<option value='-1'>Select city</option>");
        }
    }
    function getZipcode(city_name, city_id) {
       // $("#checkbox").prop('checked', '');
       // $("#checkboxDele").prop('checked', '');
        var dataString = 'loadName=' + city_name + '&loadId=' + city_id;
      //  $("#zip_loader").show();
       // $("#zip_loader").fadeIn(400).html('Please wait... <img src="<?php echo base_url(); ?>assets/images/loading.gif" />');

       // $("#zip_dropdown option").remove();
        var data=$('#selected_city').val();

        if(data=="")
        {
            var data1=data+''+city_name;
        }
        else
        {
            var data1=data+','+' '+city_name;
        }
        $('#selected_city').val(data1);
        var selected_city_id=$('#selected_city_id').val();
        if(selected_city_id=="")
        {
            var data2=selected_city_id+''+city_id;
        }
        else
        {
            var data2=selected_city_id+','+' '+city_id;
        }
        $('#selected_city_id').val(data2);


        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Postproperty/getZipcode",
            data: dataString,
            cache: false,
            dataType:"json",
            success: function (result) {
               // $("#zip_loader").hide();
                //------------------------------------
                var all_data_div='';
                var pincode_length=result.pincode.length;
                for(var i=0;i< pincode_length; i++)
                {
                    all_data_div+='<option value="'+result.list_id[i]+'" >'+result.pincode[i]+'</option>';
                }
                //-------------------------------------
                $("#zip_dropdown").html("<option value='-1'>Select Zipcode</option>");
                $("#zip_dropdown").append(all_data_div);

            }
        });
    }


    //-----------------------------
    function append_zipcode_value(selected_zipcode)
    {
        //alert(selected_zipcode);
        var zipcode = $("#zip_dropdown option:selected").text();
        var data=$('#selected_zipcode').val();
        if(data=="")
        {
            var data1=data+''+zipcode;
        }
        else
        {
            var data1=data+','+' '+zipcode;
        }

        $('#selected_zipcode').val(data1);
        var selected_zipcode_id=$('#selected_zipcode_id').val();
        if(selected_zipcode_id=="")
        {
            var data3=selected_zipcode_id+''+selected_zipcode;
        }
        else
        {
            var data3=selected_zipcode_id+','+' '+selected_zipcode;
        }

        $('#selected_zipcode_id').val(data3);

    }

</script>

<!-- code of map display start  -------------------------------- -->
<script>
    // This example adds a search box to a map, using the Google Place Autocomplete
    // feature. People can enter geographical searches. The search box will return a
    // pick list containing a mix of places and predicted search terms.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 18.5204, lng: 73.8567},
            zoom:12,
            mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbpTVbsYaSRPuNS6uVk51EvHYwU3E_Yuk&libraries=places&callback=initAutocomplete"
        async defer></script>
<!-- code of map display end  -------------------------------- -->
<!--<h2>hello</h2>-->