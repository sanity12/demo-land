<?php
/**
 * Created by PhpStorm.
 * User: Renuka Construction
 * Date: 31-12-2017
 * Time: AM 11:09
 */
?>
<div id="content" class="content-container ng-scope">
    <section class="view-container animate-fade-up">
<!--        <div class="container wid-init">-->
<!--            <div class="row">-->
<!--                <div class="col-md-12 col-sm-12">-->
<!--                    <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px">-->
<!--                        <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Edit Your Details </h4>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <div class="container wid-init ce-form">
            <!-- -----------------------------  -->
            <form class="well form-horizontal" action="<?php echo site_url('user/update_property_images'); ?>" method="post"  id="edit_property_images_form" enctype="multipart/form-data">

                <!----------------------------------- -->
                </br>
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4">
                        <h3><u>Property Images</u></h3>
                        <?php  ?>
                    </div>

                </div>
                </br>
                <!-- --------------------------------------------------- -->
                <?php
                $property_id =$this->uri->segment(3);
                $property_type=$this->uri->segment(4);
                $image_result=get_property_image($property_id);

                //get property images from gallery table
                $img_res=get_property_images_from_gallery($property_id);
                ?>
                <input type="hidden" id="property_id" name="property_id" value="<?php echo $property_id; ?>" />
                <input type="hidden" id="property_type" name="property_type" value="<?php echo $property_type; ?>" />

                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-3">
                        <img src="<?php echo base_url();?>assets/Pro_Imgupload/<?php echo  $image_result[0]['pro_photo']; ?>" alt="" class="img-responsive">
                    </div>
                    <div class="col-lg-4">
                        <input name="pro_photo" id="pro_photo" placeholder="Property Image" class="form-control"  type="file"  style="margin-bottom: 4%">
                    </div>
                </div>

                </br>
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-3">
                        <img src="<?php echo base_url();?>assets/Pro_Imgupload/<?php echo  $img_res[0]['photo']; ?>" alt="" class="img-responsive">
                    </div>
                    <div class="col-lg-4">
                        <input name="pro_photo_1" id="pro_photo_1" placeholder="Property Image" class="form-control"  type="file"  style="margin-bottom: 4%">
                        <input type="hidden" id="pro_photo_1_id" name="pro_photo_1_id" value="<?php echo $img_res[0]['gallery_id'];?>" />
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-3">
                        <img src="<?php echo base_url();?>assets/Pro_Imgupload/<?php echo  $img_res[1]['photo']; ?>" alt="" class="img-responsive">
                    </div>
                    <div class="col-lg-4">
                        <input name="pro_photo_2" id="pro_photo_2" placeholder="Property Image" class="form-control"  type="file"  style="margin-bottom: 4%">
                        <input type="hidden" id="pro_photo_2_id" name="pro_photo_2_id" value="<?php echo $img_res[1]['gallery_id'];?>" />

                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-3">
                        <img src="<?php echo base_url();?>assets/Pro_Imgupload/<?php echo  $img_res[2]['photo']; ?>" alt="" class="img-responsive">
                    </div>
                    <div class="col-lg-4">
                        <input name="pro_photo_3" id="pro_photo_3" placeholder="Property Image" class="form-control"  type="file"  style="margin-bottom: 4%">
                        <input type="hidden" id="pro_photo_3_id" name="pro_photo_3_id" value="<?php echo $img_res[2]['gallery_id'];?>" />

                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-lg-6"></div>
                    <div class="col-lg-4">
                        <input type="submit" value="Save" class="btn btn-primary" />
                    </div>

                </div>
                </br>
                </br>
                </br>
                </form>

        </div>
        </section>
    </div>

