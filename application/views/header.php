<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>LAND</title>     
        <style>
            .fixed {
                position: fixed;
                top:0; left:0;
                width: 100%; }
            </style>
            <!-- Material Design Icons -->
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">    
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

            <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
            <!--<link href="<?php echo base_url(); ?>assets/css/mdb.css" rel="stylesheet">-->
            <link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet">
            <link href="<?php echo base_url(); ?>assets/css/reset.css" rel="stylesheet">
            <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
            <link href="<?php echo base_url(); ?>assets/css/star-rating.css" rel="stylesheet">            
            <link href="<?php echo base_url(); ?>assets/css/select2.css" rel="stylesheet">   
            <link href="<?php echo base_url(); ?>assets/css/component.css" rel="stylesheet">            
            <link href="<?php echo base_url(); ?>assets/css/resourse/component.css" rel="stylesheet">     
            <link href="<?php echo base_url(); ?>assets/css/gmail-style.css" rel="stylesheet">

            <link href="<?php echo base_url(); ?>assets/css/search.css" rel="stylesheet">
            <link href="<?php echo base_url(); ?>assets/css/AdminLTE.min_user.css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.countdownTimer.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.carousel.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/reset.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/product-main.css" />
            <link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>

            <!-- JQuery -->
            <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.countdownTimer.min.js"></script>
            <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
        </head>
        <body style="overflow-x: hidden;">
        <div class="loader" style="display:none"></div>
        <section id="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4"><h6><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;dvsh.1101@gmail.com&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-phone" aria-hidden="true"></i>&nbsp;020-2461 9091</h6></div>
                    <nav class="navbar navbar-custom <?php
                    if ($this->router->fetch_method() == "index") {
                        echo "navbar-transparent navbar-fixed-top";
                    }
                    ?> one-page" role="navigation">

                        <div class="container">
                            <div class="navbar-header">
                                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>

                            </div>
                            <div class="collapse navbar-collapse" id="custom-collapse">
                                <ul class="nav navbar-nav navbar-right" style="background-color:#40acf2">
                                    <?php if (($this->session->userdata('user_id') != "")) { ?>
                                        <li class="dropdown user user-menu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="height:42px;">
                                                <img src="<?php echo base_url(); ?>assets/profile_images/<?php get_profile_image(); ?>" class="user-image" alt="User Image">  
                                                <span class="hidden-xs"><?php echo $this->session->userdata('user_fname'); ?> <?php echo $this->session->userdata('user_lname'); ?></span>
                                            </a>
                                            <ul class="dropdown-menu" style="height: 285px;">
                                                <li class="user-header" style="background-color: black;">
                                                    <img src="<?php echo base_url(); ?>assets/profile_images/<?php get_profile_image(); ?>" class="user-image" alt="User Image" style="height:100px;margin-left: 81px">                                               
                                                    <p style="text-align: center">
                                                        <?php echo $this->session->userdata('user_fname'); ?> <?php echo $this->session->userdata('user_lname'); ?>
                                                    </p>
                                                </li>
                                                <li class="user-footer" style="background-color: #6491e2 !important;">
                                                    <div class="pull-left">
                                                        <a href="<?php echo base_url(); ?>home/udi" class="btn btn-default btn-flat" style="color:black !important;">Profile</a>
                                                    </div>
                                                    <?php
                                                     $wer = get_oneprivatePro(); if ($wer > 0) { ?>
                                                        <div class="pull-left">
                                                            <a class="btn btn-default btn-flat" href="<?php echo base_url(); ?>user/accDetails" style="color:black !important;margin-left: 17PX;">Account Details</a>
                                                        </div>
                                            <?php } ?>

                                                </li>
                                                <li class="user-footer" style="background-color: #6491e2 !important;">
                                                    <div class="pull-left">
                                                        <a href="<?php echo base_url(); ?>home/message" class="btn btn-default btn-flat" style="color:black !important;">My Messages<span class="badge label label-danger custom_label" style=""><?php sent_but_not_receive(); ?></span></a>
                                                    </div>
                                                    <div class="pull-right">
                                                        <a href="<?php echo base_url(); ?>user/logout" class="btn btn-default btn-flat" style="color:black !important;">Sign out</a>
                                                    </div>

                                                </li>

                                            </ul>
                                        </li>
                                        <span class="badge label label-danger custom_label" style="margin-top: 4%;margin-left:7px; "><?php sent_but_not_receive(); ?></span>
                                        <!--   end of my code-->
<?php } else { ?>
                                        <li><a class="section-scroll" href="<?php echo base_url(); ?>user/signIn">Sign In</a></li>
                                        <li><a class="section-scroll" href="<?php echo base_url(); ?>user/register">Register</a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </section>
        <section id="navigation">
            <div class="container">
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <center><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>/assets/img/logo.png" alt="" title="" class="img-responsive" /></a></center>
                </div>
                <div style="padding-top:12px;" class="col-xs-12 col-sm-1 col-md-1 col-lg-2"></div>
                <div style="padding-top:12px;" class="col-xs-12 col-sm-7 col-md-7 col-lg-8">
                    <form name="search_form" class="search_form" id="search_form" action="<?php echo base_url(); ?>home/searchData" method="post">
                        <div class="col-md-12 col-lg-2">
                            <div class="row">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary search1 btn-block" id="search_btn" type="submit" name="submit" value="Search" style="height: 43px;">
                                        <strong><i style="color:#FFF;" class="fa fa-search" aria-hidden="true"></i>&nbsp;SEARCH</strong></button>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="row">
                               <div id="main_d" class="form-control" onclick="myFunction()" style="padding-top: 10px;">Property Type</div>
                                        <div id="s_property_type" class="s_property_type cScroll dd-list-menu scrollbar96 ddlistOpen ddlstSrp showi propTypvariant flipOpen DynHyt2"
                                         style="z-index:1000;width: 100%;" active="true">
                                        <div class="viewport ptnr" style="height:0px">
                                            <div class="overview" style="top: 0px;">
                                                <a id="NPddli" class="prnt DCheadingOpt" activetab="inactive" style="display: block;">
                                                    <input name="" type="checkbox" id="P" class="pro" onclick="myFun1()" value="Projects"></i> <span  onclick="myFun1()">Projects</span></a>
                                                <div class="child DCnpOpen" id="DCnpOpen">
                                                    <a class="DCpropTypradio defaultvalNp DCchildOpt" val="23"> <input name="" type="radio" id="RP"  class="checkBoxClassP" ><span data-labelfor="RP">Residential Projects </span></a>
                                                    <a class="DCpropTypradio DCchildOpt" val="26"> <input name="" type="radio" id="CP"  class="checkBoxClassP1" ><span data-labelfor="CP">Commercial Projects</span></a>
                                                </div>
                                                <a id="RENTPddli" class="prnt DCheadingOpt" activetab="inactive" style="display: block;">
                                                    <input name="landrent" type="checkbox" id="LR" class="pro" class="checkBoxClassLR" value="Gov.Land Rate"></i> <span data-labelfor="LR">Gov.Land Rate</span></a>
                                                <a id="resddli" val="R" opencls="DCresOpen" class="prnt DCheadingOpt" activetab="inactive" style="display: block;">
                                                    <input name="" type="checkbox" id="AR" onclick="myFun2()" value="All Residential"></i> <span id="ARSpa" onclick="myFun2()">All Residential</span></a>
                                                <div class="child initChecked DCresOpen" id="DCresOpen">
                                                    <a class="DCpropTypOpt DCchildOpt" val="1"> <input name="r_type[]" type="checkbox" class="checkBoxClass" id="RA" value="Residential Apartment"><span data-labelfor="RA">Residential Apartment</span> </a>
                                                    <a class="DCpropTypOpt DCchildOpt" val="4"> <input name="r_type[]" type="checkbox" class="checkBoxClass" id="IB" value="Independent/Builder Floor"><span data-labelfor="IB">Independent/Builder Floor </span> </a>
                                                    <a class="DCpropTypOpt DCchildOpt" val="2"> <input name="r_type[]" type="checkbox" class="checkBoxClass" id="IV" value="Independent House/Villa"><span data-labelfor="IV">Independent House/Villa</span> </a>
                                                    <a class="DCpropTypOpt DCchildOpt" val="3"> <input name="r_type[]" type="checkbox" class="checkBoxClass" id="RL" value="Residential Land"><span data-labelfor="RL">Residential Land </span> </a>
                                                    <a class="DCpropTypOpt DCchildOpt" val="90"> <input name="r_type[]" type="checkbox" class="checkBoxClass" id="SA" value="Studio Apartment"><span data-labelfor="SA">Studio Apartment </span> </a>
                                                    <a class="DCpropTypOpt DCchildOpt" val="5"> <input name="r_type[]" type="checkbox" class="checkBoxClass" id="FH" value="Farm House"><span data-labelfor="FH">Farm House </span> </a>
                                                    <a class="DCpropTypOpt DCchildOpt" val="22"> <input name="r_type[]" type="checkbox" class="checkBoxClass" id="SAs" value="Serviced Apartments"><span data-labelfor="SAs">Serviced Apartments </span> </a>
                                                    <a class="DCpropTypOpt DCchildOpt" val="80"> <input name="r_type[]" type="checkbox" class="checkBoxClass" id="other" value="Other"><span data-labelfor="other">Other </span> </a>
                                                </div>
                                                <a id="comddli" val="C" class="prnt DCopen DCheadingOpt" opencls="DCcomOpen" activetab="active" style="display: block;">
                                                    <input name="" type="checkbox" id="AC" onclick="myFun3()" value="All Commercial"> <span  onclick="myFun3()"> All Commercial</span></a>
                                                <div class="child DCcomOpen" style="" id="DCcomOpen">
                                                    <a class="DCpropTypOpt DCchildOpt" val="6"> <input name="c_type[]" type="checkbox" id="CS" class="checkBoxClassC" value="Commercial Shops" ><span data-labelfor="CS"> Commercial Shops </span></a>
                                                    <!--<a class="DCpropTypOpt DCchildOpt" val="82"> <input name="" type="checkbox" id="CSs" class="checkBoxClassC" ><span data-labelfor="CSs">Commercial Showrooms </span></a>-->
                                                    <a class="DCpropTypOpt DCchildOpt" val="7"> <input name="c_type[]" type="checkbox" id="CO" class="checkBoxClassC" value="Commercial Office/Space" ><span data-labelfor="CO">Commercial Office/Space </span></a>
                                                    <a class="DCpropTypOpt DCchildOpt" val="9"> <input name="c_type[]" type="checkbox" id="CL" class="checkBoxClassC" value="Commercial Land/Inst. Land" ><span data-labelfor="CL">Commercial Land/Inst. Land </span></a>
                                                    <a class="DCpropTypOpt DCchildOpt" val="16"> <input name="c_type[]" type="checkbox" id="IL" class="checkBoxClassC" value="Industrial Lands/Plots" ><span data-labelfor="IL">Industrial Lands/Plots </span></a>
                                                    <a class="DCpropTypOpt DCchildOpt" val="20"> <input name="c_type[]" type="checkbox" id="AL" class="checkBoxClassC" value="Agricultural/Farm Land"><span data-labelfor="AL">Agricultural/Farm Land </span></a>
                                                    <a class="DCpropTypOpt DCchildOpt" val="10"> <input name="c_type[]" type="checkbox" id="HR" class="checkBoxClassC" value="Hotel/Resorts" ><span data-labelfor="HR">Hotel/Resorts </span></a>
                                                    <!--<a class="DCpropTypOpt DCchildOpt" val="83"> <input name="" type="checkbox" id="GB" class="checkBoxClassC"><span data-labelfor="GB">Guest-House/Banquet-Halls </span></a>-->
                                                    <!--<a class="DCpropTypOpt DCchildOpt" val="11"> <input name="" type="checkbox" id="TS" class="checkBoxClassC"><span data-labelfor="TS">Time Share </span></a>-->
                                                    <!--<a class="DCpropTypOpt DCchildOpt" val="12"> <input name="" type="checkbox" id="SM" class="checkBoxClassC"><span data-labelfor="SM">Space in Retail Mall </span></a>-->
                                                    <!--<a class="DCpropTypOpt DCchildOpt" val="13"> <input name="" type="checkbox" id="OB" class="checkBoxClassC"><span data-labelfor="OB">Office in Business Park </span></a>-->
                                                    <!--<a class="DCpropTypOpt DCchildOpt" val="14"> <input name="" type="checkbox" id="OI" class="checkBoxClassC"><span data-labelfor="OI">Office in IT Park </span></a>-->
                                                    <a class="DCpropTypOpt DCchildOpt" val="15"> <input name="c_type[]" type="checkbox" id="WH" class="checkBoxClassC" value="Ware House"><span data-labelfor="WH">Ware House </span></a>
                                                    <a class="DCpropTypOpt DCchildOpt" val="17"> <input name="c_type[]" type="checkbox" id="CST" class="checkBoxClassC" value="Cold Storage"><span data-labelfor="CST">Cold Storage </span></a>
                                                    <a class="DCpropTypOpt DCchildOpt" val="18"> <input name="c_type[]" type="checkbox" id="F" class="checkBoxClassC" value="Factory"><span data-labelfor="F">Factory </span></a>
                                                    <!--<a class="DCpropTypOpt DCchildOpt" val="19"> <input name="" type="checkbox" id="M" class="checkBoxClassC"><span data-labelfor="M">Manufacturing </span> </a>-->
                                                    <!--<a class="DCpropTypOpt DCchildOpt" val="21"> <input name="" type="checkbox" id="BC" class="checkBoxClassC"><span data-labelfor="BC">Business center </span></a>-->
                                                    <a class="DCpropTypOpt DCchildOpt" val="81"> <input name="c_type[]" type="checkbox" id="OT" class="checkBoxClassC" value="Other"><span data-labelfor="OT">Other </span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                        </div> 
                        <div class="col-md-12 col-lg-6">
                            <div class="row testing">
                                <input style="margin:0px;" type="text" class="form-control mysearch" name="keyword" id="mysearch" placeholder="&nbsp;&nbsp;Enter City..." >
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </section>

        <div class="container wid-init">
            <?php
            if ($this->session->flashdata('success') != '') {
                echo'<div class="alert alert-success" role="alert" id="" style="font-size:17px">';
                echo $this->session->flashdata('success');
                echo ' <i class="glyphicon glyphicon-thumbs-up"></i>';
                echo '</div>';
            }
            ?>
            <?php
            if ($this->session->flashdata('error') != '') {
                echo'<div class="alert alert-danger" role="alert" id="" style="font-size:17px">';
                echo $this->session->flashdata('error');
                echo ' <i class="glyphicon glyphicon-thumbs-down"></i>';
                echo '</div>';
            }
            ?>
        </div>
