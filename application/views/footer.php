<section id="footer">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <h5 class="white-text">CONTACT US</h5>
                <h6 class="white-text">C-20,Waluj Industrial Area, MIDC Waluj-4311333, <br/>Aurangabad, Maharashtra, India.<br/>dvsh.1101@gmail.com   |   Call us on 020-2461 9091</h6>

                <h5 class="white-text">CONNECT WITH US</h5>
                <a href="#"><img src="<?php echo base_url();?>assets/img/fb.jpg" alt="" title="" /></a>&nbsp;&nbsp;<a href="#"><img src="<?php echo base_url();?>assets/img/twt.jpg" alt="" title="" /></a>&nbsp;&nbsp;<a href="#"><img src="<?php echo base_url();?>assets/img/in.jpg" alt="" title="" /></a><!--&nbsp;&nbsp;<a href="#"><img src="img/insta.jpg" alt="" title="" /></a>&nbsp;&nbsp;<a href="#"><img src="img/gplus.jpg" alt="" title="" /></a>&nbsp;&nbsp;<a href="#"><img src="img/you.jpg" alt="" title="" /></a>-->                                                
            </div>
            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
                <h5 class="white-text">SITE LINKS</h5>
                <ul>
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li><a href="<?php echo base_url(); ?>home/about_us">About us</a></li>
                    <li><a href="<?php echo base_url(); ?>home/faq">FAQ's</a></li>
                    <li><a href="<?php echo base_url(); ?>user/contactus">Contact</a></li>                                                                        
                </ul>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
                <h5 class="white-text">SITE LINKS</h5>
                <ul>
                    <li><a href="terms.php">Terms &amp; Conditions</a></li>
                    <li><a href="delivery.php">Delivery &amp; Shipping Policy</a></li>                   
                    <li><a href="privacy.php">Privacy Policy</a></li>      
                    <li><a href="refund.php">Refund &amp; Cancellation</a></li>                
                </ul>
            </div>            
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <h5>
                    <div class="fb-page" data-href="https://www.facebook.com/facebook" data-tabs="timeline" data-width="300" data-height="200" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/facebook" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div>
                </h5>
            </div>                                    

        </div>
    </div>
</section>


<section id="btm-footer">
    <div class="container">
        <div class="row">
            <h6 class="text-center">Copyright@2017. All rights reserved. Design By <a target="_blank" href="http://webdeveloperspune.com/">Web Developers Pune</a></h6>
        </div>
    </div>
</section>  
{elapsed_time}
<!-- JQuery -->
<!--<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/mdb.js"></script>
<!--<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/mdb.js"></script>-->
<!--<script src="<?php echo base_url(); ?>/assets/js/jquery-1.8.0.min.js"></script>-->
<script src="<?php echo base_url(); ?>/assets/js/select2.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/validation.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckfinder.js"></script>
<script>
$(document).ready(function() {
$(".alert").hide();   
        $(".alert").fadeTo(7000, 7000).slideUp(7000, function(){
            $(".alert").slideUp(7000);
        });   
});
//$(window).load(function() {
//    $(".loader").fadeOut("slow");
//});
//$(function(){
//  $('.mysearch').select2();
//});
$(window).scroll(function(){
  var sticky = $('.sticky'),
      scroll = $(window).scrollTop();

  if (scroll >= 100) sticky.addClass('fixed');
  else sticky.removeClass('fixed');
});

</script>

<!--POPUP-->

<script>var polyfilter_scriptpath = '/resourse/';</script>
<!--POPUP-->                    

<!----------------js code for search -------------- -->
<script type="text/javascript" >
    $(document).ready(function () {
        $("#mysearch").autocomplete({
            minLength: 1,
            source:
                function (req, add) {
//                        $.ajax({
//                            url: "<?php //echo base_url(); ?>//home/dosearch",
//                            dataType: 'json',
//                            type: 'POST',
//                            data: req,
//                            success:
//                                    function (data) {
//                                        if (data.response == "true") {
//                                            add(data.message);
//                                            console.log(data);
//                                        }
//                                    }
//                        });
                    //-----------------------------------------------
                    $.ajax({
                        url: "<?php echo base_url(); ?>home/get_city_of_property_details",
                        dataType: 'json',
                        type: 'POST',
                        data: req,
                        success:
                            function (data) {
                                if (data.response == "true") {
                                    add(data.message);
                                    console.log(data);
                                }
                            }
                    });
                    //------------------------------------------------
                }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#s_property_type").hide();
        $(".DCnpOpen").hide();
        //    $(".DCresOpen").hide();
        $(".DCcomOpen").hide();
        $("#AR").prop('checked', true);
        $(".checkBoxClass").prop('checked', true);
        $("[data-labelfor]").click(function () {
            $('#' + $(this).attr("data-labelfor")).prop('checked',
                function (i, oldVal) {
                    return !oldVal;
                });
        });
    });
    function myFunction() {
        var x = document.getElementById("s_property_type");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
    function myFun1() {
        var x = document.getElementById("DCnpOpen");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        $(".checkBoxClassLR").prop('checked', $(".checkBoxClassLR").prop('checked'));
    }
    function myFun2() {
        var x = document.getElementById("DCresOpen");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
    function myFun3() {
        var x = document.getElementById("DCcomOpen");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }


    $("#P").click(function () {
        $(".checkBoxClassP").prop('checked', $(this).prop('checked'));

        $("#main_d").html($(this).val());
    });
    $(".checkBoxClassP1").click(function () {
        $(".checkBoxClassP").prop('checked', false);
    });
    $(".checkBoxClassP").click(function () {
        $(".checkBoxClassP1").prop('checked', false);
    });
    $("#AR").click(function () {
        $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        if ($('input#AR').is(':checked')) {
            $("#AC").prop('checked', false);
            $(".checkBoxClassC").prop('checked', false);
            myFun3();
        }
        $("#LR").prop('checked', false);
        $("#main_d").html($(this).val());
        myFunction();
    });
    $("#AC").click(function () {
        $(".checkBoxClassC").prop('checked', $(this).prop('checked'));
        if ($('input#AC').is(':checked')) {
            $("#AR").prop('checked', false);
            $(".checkBoxClass").prop('checked', false);
            myFun2();
        }
        $("#LR").prop('checked', false);
        $("#main_d").html($(this).val());
        myFunction();
    });
    $("#LR").click(function () {
        $(".checkBoxClass").removeAttr('checked');
        $(".checkBoxClassP").removeAttr('checked');
        $(".checkBoxClassP1").removeAttr('checked');
        $(".checkBoxClassC").removeAttr('checked');
        $("#AR").removeAttr('checked');
        $("#P").removeAttr('checked');
        $("#AC").removeAttr('checked');
        $(".checkBoxClassLR").prop('checked', $(this).prop('checked'));
        myFun2();
        myFun3();
        $("#main_d").html($(this).val());
        myFunction();
    });
  $("#mysearch").click(function(){
        var x = document.getElementById("s_property_type");
        if (x.style.display === "block") {
            x.style.display = "none";
        } 
    });
     $("#search_btn").click(function(){
        var x = document.getElementById("s_property_type");
        if (x.style.display === "block") {
            x.style.display = "none";
        } 
    });
    
    
</script>


</body>

</html>
