<?php $act = $this->router->fetch_method(); ?>
<section id="top-banner">
    <div class="container">
        <div class="row">
            <?php
            if ($this->session->userdata('user_id') != '') {
                $user_type = check_user($this->session->userdata('user_id'));
                if ($user_type == 'private') {
                    ?>
                    <!--<div style="padding-top:12px;float:right;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2" style="margin-top: 18px;float: right;">
                            <span class="input-group-btn">
                                <a class="btn btn-success btn-block" href="<?php echo base_url(); ?>postproperty" ><strong>Sell / Rent Property <sup class="Free posSt" style="color:#666">Free</sup></strong></a>
                            </span>            
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2" style="margin-top: 18px;float: right;">
                            <span class="input-group-btn">
                                <a class="btn btn-info btn-block" href="<?php echo base_url(); ?>user/myaccount"><strong>My Account
                                        <span class="badge label label-danger custom_label" style="background-color:green !Important; "><?php get_my_account_count(); ?></span>
                                    </strong></a>
                            </span>            
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2" style="margin-top: 18px;float: right;">
                            <span class="input-group-btn">
                                <a class="btn btn-primary btn-block" href="<?php echo base_url(); ?>home/ROIP" ><strong>Own Property</strong></a>
                            </span>            
                        </div>

                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2" style="margin-top: 18px;float: right;">
                            <span class="input-group-btn">
                                <a class="btn btn-warning btn-block" href="<?php echo base_url(); ?>home/ROI"><strong>ROI</strong></a>
                            </span>            
                        </div>
                    <!--</div>-->
                <?php } else {
                    ?>
                    <!--<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2"></div>-->
                    <!--<div style="padding-top:12px;float:right;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2" style="margin-top: 18px;float: right;">
                            <span class="input-group-btn">
                                <a class="btn btn-success btn-block" href="<?php echo base_url(); ?>postproperty" ><strong>Sell / Rent Property <sup class="Free posSt" style="color:#666">Free</sup></strong></a>
                            </span>            
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2" style="margin-top: 18px;float: right;">
                            <span class="input-group-btn">
                                <a class="btn btn-info btn-block" href="<?php echo base_url(); ?>user/myaccount"><strong>My Account
                                        <span class="badge label label-danger custom_label" style="background-color:green !Important; "><?php get_my_account_count(); ?></span>
                                    </strong></a>
                            </span>            
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2" style="margin-top: 18px;float: right;">
                            <span class="input-group-btn">
                                <a class="btn btn-primary btn-block" href="<?php echo base_url(); ?>home/OwnP"><strong>Own Property</strong></a>
                            </span>            
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2" style="margin-top: 18px;float: right;">
                            <span class="input-group-btn">
                                <a class="btn btn-warning btn-block" href="<?php echo base_url(); ?>home/ROI"><strong>ROI</strong></a>
                            </span>            
                        </div>
                    <!--</div>-->
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</section>


<div id="fb-root"></div>

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">

                <!-- Carousel -->
                <div id="carousel1" class="carousel slide carousel-fade hoverable">

                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel1" data-slide-to="0" class="active">
                        </li>
                        <li data-target="#carousel1" data-slide-to="1"></li>
                        <li data-target="#carousel1" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner z-depth-2" role="listbox">

                        <!-- First slide -->
                        <div class="item active" style="transition: 1.6s ease-in-out left !important;">
                            <div class="view overlay hm-blue-slight">
                                <a><img src="<?php echo base_url(); ?>/assets/img/banner/Slide1.png" class="img-responsive" alt="slide1">
                                    <div class="mask waves-effect waves-light"></div>
                                </a>
                                <div class="carousel-caption hidden-xs">
                                    <div class="animated fadeInDown">
                                        <!--<h5>Lorem ipsum dolor sit amet</h5>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.item -->

                        <!-- Second slide -->
                        <div class="item" style="transition: 1.6s ease-in-out left !important;">
                            <div class="view overlay hm-blue-slight">
                                <a><img src="<?php echo base_url(); ?>/assets/img/banner/Slide2.png" class="img-responsive" alt="slide2">
                                    <div class="mask waves-effect waves-light"></div>
                                </a>
                                <div class="carousel-caption hidden-xs">
                                    <div class="animated fadeInDown">
                                        <!--<h5>Lorem ipsum dolor sit amet</h5>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.item -->

                        <!-- Third slide -->
                        <div class="item" style="transition: 1.6s ease-in-out left !important;">
                            <div class="view overlay hm-blue-slight">
                                <a><img src="<?php echo base_url(); ?>/assets/img/banner/Slide3.png" class="img-responsive" alt="slide3">
                                    <div class="mask waves-effect waves-light"></div>
                                </a>
                                <div class="carousel-caption hidden-xs">
                                    <div class="animated fadeInDown">
                                        <!--<h5>Lorem ipsum dolor sit amet</h5>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- /Forth slide -->
                         <div class="item" style="transition: 1.6s ease-in-out left !important;">
                            <div class="view overlay hm-blue-slight">
                                <a><img src="<?php echo base_url(); ?>/assets/img/banner/nature_picture.png" class="img-responsive" alt="slide3">
                                    <div class="mask waves-effect waves-light"></div>
                                </a>
                                <div class="carousel-caption hidden-xs">
                                    <div class="animated fadeInDown">
                                        <!--<h5>Lorem ipsum dolor sit amet</h5>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.carousel-inner -->

                    <!-- Controls -->
                    <a class="left carousel-control new-control" href="#carousel1" role="button" data-slide="prev">
                        <span class="fa fa fa-angle-left waves-effect waves-light" style="color:black;"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control new-control" href="#carousel1" role="button" data-slide="next">
                        <span class="fa fa fa-angle-right waves-effect waves-light" style="color:black;"></span>
                        <span class="sr-only">Previous</span>
                    </a>

                </div>
                <!-- /.carousel -->

            </div><!--/col-lg-8-->            
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="nav nav-pills nav-stacked top-title"><h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;MAIN CATEGORIES</h4></div>

                <ul class="nav nav-pills nav-stacked">
                    <form id="gov_land_rate_form" name="gov_land_rate_form" action="<?php echo site_url('home/searchData'); ?>" method="post" style="margin-top: 3%;margin-left: 6%;margin-bottom: 3%;font-size:17px;color:black">
                        <input type="hidden" id="landrent" name="landrent" value="Gov.Land Rate" />
                        <li><a onclick="submit_gov_land_rate_form();" style="cursor: pointer;"><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Gov.Land Rate</a></li>
                    </form>
                    <form id="residential_app_form" name="residential_app_form" action="<?php echo site_url('home/search_custom_property_info'); ?>" method="post" style="margin-top: 3%;margin-left: 6%;margin-bottom: 3%;">
                        <input type="hidden" id="property_type" name="property_type" value="residential" />
                        <input type="hidden" id="property_sub_type" name="property_sub_type" value="Residential Apartment" />
                        <li><a onclick="custom_property_search('residential', 'Residential Apartment');" style="cursor: pointer;"><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Residential Apartment</a></li>
                    </form>
                    <form id="factory_form" name="factory_form" action="<?php echo site_url('home/search_custom_property_info'); ?>" method="post" style="margin-top: 3%;margin-left: 6%;margin-bottom: 3%;">
                        <input type="hidden" id="property_type" name="property_type" value="commercial" />
                        <input type="hidden" id="property_sub_type" name="property_sub_type" value="Factory" />
                        <li><a onclick="submit_factory_form();" style="cursor: pointer;"><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Factory</a></li>
                    </form>
                    <form id="independent_house_form" name="independent_house_form" action="<?php echo site_url('home/search_custom_property_info'); ?>" method="post" style="margin-top: 3%;margin-left: 6%;margin-bottom: 3%;">
                        <input type="hidden" id="property_type" name="property_type" value="residential" />
                        <input type="hidden" id="property_sub_type" name="property_sub_type" value="Independent House/Villa" />
                        <li><a onclick="submit_independent_house_form();" style="cursor: pointer;"><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Independent House/Villa</a></li>
                    </form>
                    <form id="residential_land_form" name="residential_land_form" action="<?php echo site_url('home/search_custom_property_info'); ?>" method="post" style="margin-top: 3%;margin-left: 6%;margin-bottom: 3%;">
                        <input type="hidden" id="property_type" name="property_type" value="residential" />
                        <input type="hidden" id="property_sub_type" name="property_sub_type" value="Residential Land" />
                        <li><a onclick="submit_residential_land_form();" style="cursor: pointer;"><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Residential Land</a></li>
                    </form>
                    <form id="farm_house_form" name="farm_house_form" action="<?php echo site_url('home/search_custom_property_info'); ?>" method="post" style="margin-top: 3%;margin-left: 6%;margin-bottom: 3%;">
                        <input type="hidden" id="property_type" name="property_type" value="residential" />
                        <input type="hidden" id="property_sub_type" name="property_sub_type" value="Farm House" />
                        <li><a onclick="submit_farm_house_form();" style="cursor: pointer;"><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Farm House</a></li>
                    </form>
                    <form id="commercial_shop_form" name="commercial_shop_form" action="<?php echo site_url('home/search_custom_property_info'); ?>" method="post" style="margin-top: 3%;margin-left: 6%;margin-bottom: 3%;">
                        <input type="hidden" id="property_type" name="property_type" value="commercial" />
                        <input type="hidden" id="property_sub_type" name="property_sub_type" value="Commercial Shops" />
                        <li><a onclick="submit_commercial_shop_form();" style="cursor: pointer;"><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Commercial Shops</a></li>
                    </form>



<!--                    <li><a href="--><?php //echo base_url();   ?><!--product/product_list/1"><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Factory</a></li>-->
<!--                    <li><a href="--><?php //echo base_url();   ?><!--product/product_list/1"><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Independent House/Villa</a></li>-->
<!--                    <li><a href="--><?php //echo base_url();   ?><!--product/product_list/1"><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Residential Land </a></li>-->
<!--                    <li><a href="--><?php //echo base_url();   ?><!--product/product_list/1"><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Farm House </a></li>-->
<!--                    <li><a href="--><?php //echo base_url();   ?><!--product/product_list/1"><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Commercial Shops </a></li>-->
                </ul>
            </div>
        </div>
    </div>
</section>

<section id="middle-box">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-4 col-lg-4 box-1">
                <div class="box-mn">
                    <center><i class="fa fa-user-secret" aria-hidden="true" style="font-size: 60px !important;color: #40acf2;padding-top: 20px;"></i></center>
                    <h2 class="text-center">Private</h2>
                    <h5 class="text-center">1000+ products available</h5>
                </div>
            </div>

            <div class="col-sm-4 col-md-4 col-lg-4 box-2">
                <div class="box-mn">
                    <center><i class="fa fa-users" aria-hidden="true" style="font-size: 60px !important;color: #40acf2;padding-top: 20px;"></i></center>

                    <h2 class="text-center">Public</h2>
                    <h5 class="text-center">all over india delivery available</h5>
                </div>
            </div>

            <div class="col-sm-4 col-md-4 col-lg-4 box-3">
                <div class="box-mn">
                    <center><i class="fa fa-thumbs-o-up new" aria-hidden="true"></i></center>
                    <h2 class="text-center">ENJOY</h2>
                    <h5 class="text-center">enjoy your product</h5>
                </div>
            </div>                       

        </div>
    </div>
</section>

<section id="featured-product">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 set-bg-clr">
                <div class="title"><h3 class="text-uppercase">Featured Products</h3></div>
            </div>
            <div class="col-lg-12">
                <div id="carousel-multi-item" class="carousel slide multiitem-car">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row text-center">
                                <?php
                                $i = 1;
                                foreach ($ProRes as $ProRe) {
                                    // print_r($ProRe);
                                    ?>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-card">
                                        <div class="card hoverable">
                                            <div class="card-image">
                                                <div class="view overlay hm-white-slight z-depth-1 zoom-img">
                                                    <a href="<?php echo base_url(); ?>home/proDetail/<?php echo $ProRe['property_id']; ?>" target="_blank">
                                                        <img src="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRe['pro_photo']; ?>" class="img-responsive set-img-imp" alt="">
                                                        <div class="mask waves-effect"></div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="card-content text">
                                                <h3><a target="_blank" href="<?php echo base_url(); ?>home/proDetail/<?php echo $ProRe['property_id']; ?>" ><?php echo html_entity_decode($ProRe['project_name']); ?></a></h3>                                                
                                                <h5><?php echo $ProRe['locality']; ?></h5>
                                            </div>

                                            <div id="GetCartCountSuccess"></div>
                                            <div class="card-btn text-center">

                                                <a class="btn btn-default btn-block" href="<?php echo base_url(); ?>home/proDetail/<?php echo $ProRe['property_id']; ?>" style="color:black;border: solid 1px black">Click Here to View</a>
                                            </div>
                                            <div class="card-btn text-center">
                                                <?php $wer = get_oneprivatePro();
                                                if ($wer > 0) { ?>

                                                    <a class="btn btn-primary btn-block" href="<?php echo base_url(); ?>home/ROIP/<?php echo $ProRe['property_id']; ?>/<?php echo $ProRe['user_id']; ?>">Request exchanges</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>

                                    <?php if ($i % 4 == 0) { ?><div class="col-md-12" style="padding-bottom:15px;">&nbsp;</div><?php } ?>

                                    <?php
                                    $i++;
                                }
                                ?>

                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section id="clients">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 set-bg-clr">
                <div class="title2"><h3 class="text-uppercase">Our Clients</h3></div>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 clients-mb-set"><img src="<?php echo base_url(); ?>/assets/img/client.jpg" alt="" title="" class="img-responsive" /></div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 clients-mb-set"><img src="<?php echo base_url(); ?>/assets/img/client.jpg" alt="" title="" class="img-responsive" /></div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 clients-mb-set"><img src="<?php echo base_url(); ?>/assets/img/client.jpg" alt="" title="" class="img-responsive" /></div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 clients-mb-set"><img src="<?php echo base_url(); ?>/assets/img/client.jpg" alt="" title="" class="img-responsive" /></div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 clients-mb-set"><img src="<?php echo base_url(); ?>/assets/img/client.jpg" alt="" title="" class="img-responsive" /></div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 clients-mb-set"><img src="<?php echo base_url(); ?>/assets/img/client.jpg" alt="" title="" class="img-responsive" /></div>

        </div>
    </div>
</section>

<!-- POP UP DESIGN -->
<div class="md-modal" id="modal-1">
    <div class="md-content">
        <h3><i class="fa fa-check"></i>&nbsp;Success</h3>
        <div>
            <h5>Product Successfully Added In My Account <br><br></h5>
            <button class="md-view_cart" onclick="window.location.href = '<?php echo base_url(); ?>home/myaccount'">View My Account</button>                                   
            <button class="md-close">Close</button>                                  
        </div>
    </div>
</div>

<script type="text/javascript" >
    $(document).ready(function () {
        $("#mysearch").autocomplete({ 
            minLength: 1,
            source:
                    function (req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>home/dosearch",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function (data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                            console.log(data);
                                        }
                                    },
                        });
                    },
        });
    });

</script>
<script>
    $("#search_form").submit(function () {

    });
</script>
<script type="text/javascript" >
    $(document).ready(function () {
        $("#s_property_type").hide();
        $(".DCnpOpen").hide();
        //    $(".DCresOpen").hide();
        $(".DCcomOpen").hide();
        $("#AR").prop('checked', true);
        $(".checkBoxClass").prop('checked', true);
        $("[data-labelfor]").click(function () {
            $('#' + $(this).attr("data-labelfor")).prop('checked',
                    function (i, oldVal) {
                        return !oldVal;
                    });
        });
    });
    function myFunction() {
        var x = document.getElementById("s_property_type");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
    function myFun1() {
        var x = document.getElementById("DCnpOpen");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        $(".checkBoxClassLR").prop('checked', $(".checkBoxClassLR").prop('checked'));
    }
    function myFun2() {
        var x = document.getElementById("DCresOpen");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
    function myFun3() {
        var x = document.getElementById("DCcomOpen");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }


</script>
<script>
    function display_property_details(property, property_type)
    {

        $.ajax({url: '<?php echo base_url(); ?>home/property_type_info',
            data: {prop: property, property_type: property_type},
            type: 'GET',
            dataType: 'json',
            success: function (result) {

            }

        });
    }

    function custom_property_search(prop, prop_type)
    {
        //alert("in f");
        $('#residential_app_form').submit();
    }

    function submit_factory_form()
    {
        $('#factory_form').submit();
    }

    function submit_independent_house_form()
    {
        $('#independent_house_form').submit();
    }

    function submit_residential_land_form()
    {
        $('#residential_land_form').submit();
    }
    function submit_farm_house_form()
    {
        $('#farm_house_form').submit();
    }
    function submit_commercial_shop_form()
    {
        $('#commercial_shop_form').submit();
    }

    function submit_gov_land_rate_form()
    {
        $('#gov_land_rate_form').submit();
    }
</script>
<script>
    function MyInvestFun(property_id, type)
    {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url() ?>home/insert_investPro",
            data: {property_id: property_id, invest_pro: type},
            success: function (data)
            {
                location.href = "<?php echo base_url(); ?>home/myaccount";
            }
        });
    }
</script>