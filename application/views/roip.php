<?php if(!empty($ProRes)){ ?>
<section id="top-banner">
    <div class="container" style="padding-bottom: 15px;">
        <div class="row">
            <div style="padding-top:12px;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="nav nav-pills nav-stacked top-title">
                        <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;PRIVATE PROPERTY LIST</h4>
                    </div>
                    <div class="col-lg-12" style="margin-top: 15px;"><div id="carousel-multi-item" class="carousel slide multiitem-car"><div class="carousel-inner">
                    <div class="item active"><div class="row text-center">
                            <?php
                            $i = 1;
                            foreach ($ProRes as $ProRe) {
                               // print_r($ProRe);
                                ?>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-card">
                                    <div class="card hoverable">
                                        <div class="card-image">
                                            <div class="view overlay hm-white-slight z-depth-1 zoom-img">
                                                <a href="<?php echo base_url();?>home/privateDetail/<?php echo $ProRe['property_id']; ?>">
                                                <img src="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRe['pro_photo']; ?>" class="img-responsive set-img-imp" alt="">
                                                    <div class="mask waves-effect"></div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="card-content">
                                            <h3><a href="<?php echo base_url();?>home/privateDetail/<?php echo $ProRe['property_id']; ?>"><?php echo html_entity_decode($ProRe['project_name']); ?></a></h3>                                                
                                                <h5><?php echo $ProRe['locality']; ?></h5>
                                        </div>
<!--                                        <div class="card-btn text-center">
                                            <a class="btn btn-primary btn-block" href="<?php echo base_url();?>home/ROIP/<?php echo $ProRe['property_id']; ?>">APPLY NOW</a>
                                        </div>-->
                                    </div>
                                </div>
                                <?php if ($i % 4 == 0) { ?><div class="col-md-12" style="padding-bottom:15px;">&nbsp;</div><?php } ?>
                                <?php $i++;
                            }
                            ?>

                        </div>
                        <!-- /.row -->
                    </div></div></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php if(!empty($ProResPublic)){ ?>
<section id="top-banner">
    <div class="container" style="padding-bottom: 15px;">
        <div class="row">
            <div style="padding-top:12px;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="nav nav-pills nav-stacked top-title">
                        <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;PUBLIC PROPERTY LIST</h4>
                    </div>
                    <div class="col-lg-12" style="margin-top: 15px;"><div id="carousel-multi-item" class="carousel slide multiitem-car"><div class="carousel-inner">
                    <div class="item active"><div class="row text-center">
                            <?php
                            $i = 1;
                            foreach ($ProResPublic as $ProRe) {
                               // print_r($ProRe);
                                ?>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-card">
                                    <div class="card hoverable">
                                        <div class="card-image">
                                            <div class="view overlay hm-white-slight z-depth-1 zoom-img">
                                                <a href="<?php echo base_url();?>home/proDetail/<?php echo $ProRe['property_id']; ?>">
                                                <img src="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRe['pro_photo']; ?>" class="img-responsive set-img-imp" alt="">
                                                    <div class="mask waves-effect"></div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="card-content">
                                            <h3><a href="<?php echo base_url();?>home/proDetail/<?php echo $ProRe['property_id']; ?>"><?php echo html_entity_decode($ProRe['project_name']); ?></a></h3>                                                
                                                <h5><?php echo $ProRe['locality']; ?></h5>
                                        </div>
                                       
                                    </div>
                                </div>
                                <?php if ($i % 4 == 0) { ?><div class="col-md-12" style="padding-bottom:15px;">&nbsp;</div><?php } ?>
                                <?php $i++;
                            }
                            ?>

                        </div>
                        <!-- /.row -->
                    </div></div></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>
<?php if(isset($proper_id)){ ?>
<section id="top-banner">
    <div class="container" style="padding-bottom: 15px;">
        <div class="row">
            <div style="padding-top:12px;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="nav nav-pills nav-stacked top-title">
                        <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;MAIN CATEGORIES</h4>
                    </div>
                    <div class="col-lg-12" style="margin-top: 15px;"><div id="carousel-multi-item" class="carousel slide multiitem-car"><div class="carousel-inner">
                    <div class="item active"><div class="row text-center">
                            <?php
                            $i = 1;
                            foreach ($ProperRes as $ProperRe) {
                               // print_r($ProRe);
                                ?>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-card">
                                    <div class="card hoverable">
                                        <div class="card-image">
                                            <div class="view overlay hm-white-slight z-depth-1 zoom-img">
                                                <a target="_blank" href="<?php echo base_url();?>home/privateDetail/<?php echo $ProperRe['property_id']; ?>">
                                                <img src="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProperRe['pro_photo']; ?>" class="img-responsive set-img-imp" alt="">
                                                    <div class="mask waves-effect"></div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="card-content">
                                            <h3><a target="_blank" href="<?php echo base_url();?>home/privateDetail/<?php echo $ProperRe['property_id']; ?>"><?php echo html_entity_decode($ProperRe['project_name']); ?></a></h3>                                                
                                                <h5><?php echo $ProperRe['locality']; ?></h5>
                                        </div>
                                        <div class="card-btn text-center">
                                            <a class="btn btn-primary btn-block applynow" href="javascript:void(0)" data-id="<?php echo $ProperRe['property_id']; ?>" id="applynow<?php echo $ProperRe['property_id']; ?>">APPLY NOW</a>
                                        </div>
                                        <div class="card-btn text-center img0" style="" id="img<?php echo $ProperRe['property_id']; ?>">
                                            <img src="<?php echo base_url(); ?>/assets/images/rightgreen.png" style="width:20%">
                                        </div>
                                    </div>
                                </div>
                                <?php if ($i % 4 == 0) { ?><div class="col-md-12" style="padding-bottom:15px;">&nbsp;</div><?php } ?>
                                <?php $i++;
                            }
                            ?>

                        </div>
                        <!-- /.row -->
                    </div></div></div></div>
                    <center><div class="col-lg-12" style="margin-top: 15px;">
                            <form enctype="" method="post" action="<?php echo base_url() ?>home/insert_applyIds">
                                <input type="hidden" name="properid" value="<?php echo $proper_id;?>" id="properid">
                                <input type="hidden" name="properids" value="" id="properids">
                                <input type="hidden" name="user_public_id" value="<?php echo $user_public_id;?>" id="user_public_id">
                                <button class="btn btn-warning btn-block" href="javascript:void(0)" id="submit">SUBMIT
                                </button>
                                <!--<a ></a>-->
                            </form>
                            
                    </div></center>
                </div>
            </div>
            
        </div>
    </div>
</section>
<?php } ?>

<script type="text/javascript" >
    $(document).ready(function(){
        $(".img0").hide();
    });
    var arr = [];
    var proper_id = <?php echo $proper_id; ?>;
    $(".applynow").click(function(){
        var proper_id = $(this).attr("data-id");
        arr.push(proper_id);
        $("#img"+proper_id).show();
        $("#applynow"+proper_id).html("applied");
        $("#properids").val(arr);
    });
//    $("#submit").click(function(){
//        alert(proper_id);
//        debugger;
//        $.ajax({
//            type: 'POST',
//            url: "",
//            data: {'arr': arr,'proper_id':proper_id},
//            dataType: "json",
//            success: function (data)
//            {alert("Yay!");
//                location.href = "<?php echo base_url();?>";
//            }
//        });
//    });
</script>
