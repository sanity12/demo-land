<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"><br>
	<div class="page-subheading page-subheading-md">
	    <ol class="breadcrumb">
	        <li><a href="home.php">Dashboard</a></li> 
	        <li class="active">View All Message</li>
	    </ol>
	</div>
	<div class="page-heading page-heading-md" style="padding-left: 15px;">
	    <h2>View All Message</h2>
	</div>
	<style type="text/css">
	.uid_cont .open{
		display: block;
	}
</style>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/dataTables.bootstrap.js"></script>
    <div class="container" style="width: 100%">
    	<?php if(isset($users)){
    		foreach ($users as $user) {    			
		?>
		<script>
		    $(document).ready(function () {
		        $('#dataTables<?php echo $user['user_id'];?>').dataTable();
		        $('#dataTablesuser<?php echo $user['user_id'];?>').dataTable();

		    });
		</script>		
		<button class="btn btn-success uid_link" data-id="<?php echo $user['user_id'];?>" ><?php echo "User Name : " . $user['user_name'];?></button>
    	<div class="panel panel-default form-container uid_cont"  id="uid_cont<?php echo $user['user_id'];?>" did="<?php echo $user['user_id'];?>">
            <div class="panel-body">	   
				<table class="table table-striped table-bordered table-hover" id="dataTables<?php echo $user['user_id'];?>">
					<thead>
						<tr>
							<th>Sr.No.</th>
							<th>User Name</th>							
							<th>Message Type</th>		
							<th style="width: 44%;">Message</th>
							<th>Status</th>
							<th>Created Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $i=1; foreach ($message as $row){
							if($user['user_id'] == $row['user_id']){
						 ?>
						    					
						<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo get_username($row['user_id']);?></td>
							<td><?php echo $row['msg_type']; ?></td>
							<td><?php $url = $row['message']; echo wordwrap($url, 30, "\n", 1); ?></td>
							<td><?php 
								if($row['msg_status'] == '0'){
									echo "Sent";
								}if($row['msg_status'] == '1'){
									echo "Received";
								}
								if($row['msg_status'] == '2'){
									echo "Unread";
								}
								if($row['msg_status'] == '3'){
									echo "Read";
								}
							 //echo $row['msg_status']; 
							 ?></td>
							<td><?php echo $row['created_date']; ?></td>
							<td>
							<a href="view_message/<?php echo $row['m_id']; ?>" class="btn btn-warning"><i class="fa fa-star"></i></a>
							</td>
						</tr>						
						<?php $i++; } } ?>						
					</tbody>
					<tfoot>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>
							<a href="add_message/<?php echo $user['user_id']; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
		<?php }
			 } ?>
	</div>
</div>
<script type="text/javascript">
	//$('.uid_cont').hide();
	//$(".open").show();	
	// $(".uid_link").click(function(){
	// 	var id = $(this).attr('data-id');
	// 	$("#uid_cont"+id).toggleClass("open");
	// });

	// $(".uid_link").click(function(){		
	// 	var id = $(this).attr('data-id');
	// 	alert(id);
	// //	debugger;	
	// 	//$pre = $(".open").attr('did');	
	// 	if( $(".uid_cont").hasClass("open") ) {
	// 	//if( $(this).find('open') ){
	// 		 // alert('found');
	// 		$pre = $(".open").attr('did');
	// 		alert('found'+$pre);
	// 		if($pre != ''){			
	// 			alert('if');	
	// 			 // $("#uid_cont"+pre).removeClass('open');
	// 			 // $("#uid_cont"+pre).hide();
	// 			$("#uid_cont"+id).addClass('open');
	// 			setTimeout(function(){///workaround
	// 				$("#uid_cont"+pre).removeClass('open');
	// 			}, 10);
		   		
	// 		}
	// 		// else{
	// 		// 	alert('else');
	// 		// 	$("#uid_cont"+pre).removeClass('open');
	// 		// 	 $("#uid_cont"+pre).hide();
	// 		// }
	// 		//alert($(this).find('open').attr('did'));
		  
		   
	// 	}else{
	// 		$("#uid_cont"+id).addClass('open');
	// 		$(".open").show();
	// 	}
			
	// });
</script>
<!-- /.content-wrapper -->
