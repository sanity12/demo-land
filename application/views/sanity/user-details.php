<div class="page-subheading page-subheading-md">
    <ol class="breadcrumb">
        <li><a href="home.php">Dashboard</a></li>
        <li><a href="<?php echo base_url();?>sanity/users">Users List</a></li>
        <li class="active">User Details</li>
    </ol>
</div>

<div class="page-heading page-heading-md">
    <h2>User Details<span style="float:right; font-size:25px; padding-top:15px;"><a href="<?php echo base_url();?>sanity/users">Users List</a></span></h2>
</div>

<div class="container-fluid-md" style="min-height:500px; height:auto;">
    <form class="form-horizontal form-bordered" role="form" name="que_form" action="" method="post" onsubmit="return isempty();" enctype="multipart/form-data">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">

                            <tbody>
                                <tr>
                                    <th width="25%">User Name</th>
                                    <td style="background-color:#fafafa;"><?php echo html_entity_decode($users[0]['name']); ?></td>
                                </tr>
                                <tr>
                                    <th>Mobile No.</th>
                                    <td style="background-color:#fafafa;"><?php echo $users[0]['mobileno']; ?></td>

                                </tr>
                                <tr>
                                    <th>Email ID</th>
                                    <td style="background-color:#fafafa;"><?php echo $users[0]['email']; ?></td>
                                </tr>
                                <tr>
                                    <th>Register Date</th>
                                    <td style="background-color:#fafafa;"><?php echo date('d-M-Y', strtotime($users[0]['reg_date'])); ?></td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td style="background-color:#fafafa;"><?php echo $users[0]['status']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div><!--/.table-responsive-->

                    <br><br>


                </div>
            </div>
    </form>
</div>