<div class="page-subheading page-subheading-md">
    <ol class="breadcrumb">
        <li><a href="home.php">Dashboard</a></li>
        <li class="active">Add Coupon Code</li>
    </ol>
</div>
<div class="page-heading page-heading-md">
    <h2>Coupon Code</h2>
</div>

<div class="container-fluid-md">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                	<form class="well1 form-vertical"  method="post">
                		<div class="form-group">
                            <div class="col-md-2 inputGroupContainer">
                            	<div class="input-group col-md-8">
                        		</div>
                    		</div>
                    		<label class="col-md-2 control-label" >Enter Code</label>
                    		<label class="col-md-2 control-label" >Frequency %</label>  
							<label class="col-md-2 control-label" >Duration (Days)</label>          
                    	</div>
                	</form>
                	<br><br><br>
                	<form class="well1 form-vertical" action="<?php echo site_url('sanity/coupon1'); ?>" method="post"  id="coupon_form" enctype="multipart/form-data">
                		<div class="form-group">
	                        <label class="col-md-2 control-label" >Coupon Code1</label> 
	                        
	                        <input name="coupon_type1"  placeholder="" class="form-control"  type="hidden" value="type1">
                            <div class="col-md-2 inputGroupContainer">
                            	<div class="input-group col-md-8">
                        			<input name="code1"  placeholder="" class="form-control"  type="text" required='' value="<?php if(!empty($type1)){echo $type1[0]['code'];} ?>">
                        		</div>
                    		</div>

                    		<div class="col-md-2 inputGroupContainer">
                        		<div class="input-group col-md-8">
                        			<input name="frequency1"  placeholder="" class="form-control"  type="text" required='' value="<?php if(!empty($type1)){echo $type1[0]['frequency'];} ?>">
                        		</div>
                    		</div>

                        	<div class="col-md-2 inputGroupContainer">
                        		<div class="input-group col-md-8">
                        			<input name="time1"  placeholder="" class="form-control"  type="text" required='' value="<?php if(!empty($type1)){echo $type1[0]['duration'];} ?>">
                        		</div>
                    		</div>
                        	<div class="col-md-2 inputGroupContainer">		
                        		<div class="input-group col-md-8">
                        			<input name="Subcode1" id="Subcode1" class="btn btn-success"  type="submit" required=''>
                        		</div>
                    		</div>
                    	</div>
                	</form>
                	<br><br><br>
                	<form class="well1 form-vertical" action="<?php echo site_url('sanity/coupon2'); ?>" method="post"  id="coupon_form" enctype="multipart/form-data">
                		<div class="form-group">
	                        <label class="col-md-2 control-label" >Coupon Code2</label> 
	                        <input name="coupon_type2"  placeholder="" class="form-control"  type="hidden" value="type2">
                            <div class="col-md-2 inputGroupContainer">
                            	<div class="input-group col-md-8">
                        			<input name="code2"  placeholder="" class="form-control"  type="text" required='' value="<?php if(!empty($type2)){echo $type2[0]['code'];} ?>">
                        		</div>
                    		</div>

                    		<div class="col-md-2 inputGroupContainer">
                        		<div class="input-group col-md-8">
                        			<input name="frequency2"  placeholder="" class="form-control"  type="text" required='' value="<?php if(!empty($type2)){echo $type2[0]['frequency'];} ?>">
                        		</div>
                    		</div>

                        	<div class="col-md-2 inputGroupContainer">
                        		<div class="input-group col-md-8">
                        			<input name="time2"  placeholder="" class="form-control"  type="text" required='' value="<?php if(!empty($type2)){echo $type2[0]['duration'];} ?>">
                        		</div>
                    		</div>

                        	<div class="col-md-2 inputGroupContainer">		
                        		<div class="input-group col-md-8">
                        			<input name="Subcode2" id="Subcode1" class="btn btn-success"  type="submit" required=''>
                        		</div>
                    		</div>
                    	</div>
                	</form>
                	<br><br><br>
                	<form class="well1 form-vertical" action="<?php echo site_url('sanity/coupon3'); ?>" method="post"  id="coupon_form" enctype="multipart/form-data">
                		<div class="form-group">
	                        <label class="col-md-2 control-label" >Coupon Code3</label> 
	                        <input name="coupon_type3"  placeholder="" class="form-control"  type="hidden" value="type3">
                            <div class="col-md-2 inputGroupContainer">
                            	<div class="input-group col-md-8">
                        			<input name="code3"  placeholder="" class="form-control"  type="text" required='' value="<?php if(!empty($type3)){echo $type3[0]['code'];} ?>">
                        		</div>
                    		</div>

                    		<div class="col-md-2 inputGroupContainer">
                        		<div class="input-group col-md-8">
                        			<input name="frequency3"  placeholder="" class="form-control"  type="text" required='' value="<?php if(!empty($type3)){echo $type3[0]['frequency'];} ?>">
                        		</div>
                    		</div>

                        	<div class="col-md-2 inputGroupContainer">
                        		<div class="input-group col-md-8">
                        			<input name="time3"  placeholder="" class="form-control"  type="text" required='' value="<?php if(!empty($type3)){echo $type3[0]['duration'];} ?>">
                        		</div>
                    		</div>

                        	<div class="col-md-2 inputGroupContainer">		
                        		<div class="input-group col-md-8">
                        			<input name="Subcode3" id="Subcode1" class="btn btn-success"  type="submit" required=''>
                        		</div>
                    		</div>
                    	</div>
                	</form>
                	<br><br><br>
                	<form class="well1 form-vertical" action="<?php echo site_url('sanity/addText'); ?>" method="post"  id="coupon_form" enctype="multipart/form-data">
                		<div class="form-group">
	                        <label class="col-md-2 control-label" >Sample Text</label> 
                            <div class="col-md-6 inputGroupContainer">
                            	<div class="input-group col-md-11">
                                    <input name="sampletext"  placeholder="" class="form-control"  type="text" required='' value="<?php echo $coupontext[0]['description'];?>">
                        		</div>
                    		</div>
							<div class="col-md-2 inputGroupContainer">		
                        		<div class="input-group col-md-2">
                        			<input name="submit" id="" class="btn btn-success"  type="submit" >
                        		</div>
                    		</div>
                    	</div>
                	</form>
                </div>
            </div>
        </div>
    </div>
</div>
