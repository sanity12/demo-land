<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Admin Panel</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/iriy-admin.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/demo.css">
        <link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/rickshaw.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/morris.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.carousel.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/product-main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.countdownTimer.css" />
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.countdownTimer.css" />-->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/validation.js"></script>
        
        <style>
            #note_div{
    display: inline-block;
     padding-top:5%;
     padding-left: 5%;
     border-radius: 3px;

 }
 #note_div > p{
     font-size: small;
 }

 .clockdiv{
     color: #fff;
     display: inline-block;
     font-weight: 200;
     text-align: center;
     font-size: 26px;
 }

 .clockdiv > div{
     padding: 0px;
     border-radius: 3px;
     background: #d23b00;
     display: inline-block;
 }

.clockdiv div > span{
     padding: 6px;
     border-radius: 3px;
     background: #9c2c00;
     display: inline-block;
     font-weight:200;
 }
 .smalltext {
     font-size: 10px;
 }
        </style>
    </head>
    <body>
        <?php //$GetAdminInfo = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM admin where id='" . $_SESSION['adminid'] . "'")); ?>
        <header>
            <nav class="navbar navbar-default navbar-static-top no-margin" role="navigation">
                <div class="navbar-brand-group">
                    <a class="navbar-sidebar-toggle navbar-link" data-sidebar-toggle>
                        <i class="fa fa-lg fa-fw fa-bars"></i>
                    </a>
                    <a class="navbar-brand hidden-xxs" href="<?php echo base_url();?>sanity">
                        <span class="sc-visible"><strong>A</strong></span>

                        <span class="sc-hidden">
                            <span class="semi-bold">A</span>dmin Panel
                        </span>
                    </a>
                </div>
                <style type="text/css">
                    .mannual.open>.dropdown-menu {
                        display: block;
                        margin-left: -82px;
                    }
                </style>
                <ul class="nav navbar-nav navbar-nav-expanded pull-right margin-md-right">
                    <li class="dropdown mannual">
                        <a data-toggle="dropdown" class="dropdown-toggle navbar-user" href="javascript:;">
                            <span class="hidden-xs"><?php if(isset($_SESSION['admin_logged_in'])){ echo $this->session->userdata('admin_username');} ?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu pull-right-xs">
                            <li class="arrow"></li>
                            <li style="display:none;"><a href="javascript:;"><span class="badge badge-danger pull-right">2</span> Inbox</a></li>
                            <li style="display:none;"><a href="javascript:;">Messages</a></li>
                            <li><a href="<?php echo base_url();?>sanity/chngPwd">Change Passwords</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url()?>sanity/logout">Log Out</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </header>
        <!--end header-->
        <?php $act = $this->router->fetch_method(); ?>
        <!--start page-wrapper-->
        <?php if($act != 'login'){?>
        <div class="page-wrapper">
            <aside class="sidebar sidebar-default">
                <nav>
                    <ul class="nav nav-pills nav-stacked">
                        <h4>&nbsp;</h4>
                        <li class="nav-dropdown <?php if (isset($act)) {
                            if ($act == 'index') { ?> active open <?php }
                        } ?>">
                            <a href="#"><i class="fa fa-lg fa-fw fa-home"></i>&nbsp;&nbsp;Dashboards</a>
                            <ul class="nav-sub">
                                <li class="active open">
                                    <a href="<?php echo base_url();?>sanity"><i class="fa fa-fw fa-caret-right"></i>My Dashboard</a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-dropdown <?php if (isset($act)) {
                                if (($act == 'propertyList') || ($act == 'p_propertyList')){ ?> active open <?php }
                            }
                            ?>">
                            <a href="#"><i class="fa fa-lg fa-fw fa-shopping-cart" aria-hidden="true"></i>&nbsp;&nbsp;property List</a>
                            <ul class="nav-sub">
                                <li class="<?php if ($act == 'propertyList') { ?> active open <?php } ?>">
                                    <a href="<?php echo base_url();?>sanity/propertyList" title="Dashboard"><i class="fa fa-fw fa-caret-right"></i>Public propertyList</a>
                                </li>
                                <li class="<?php if ($act == 'p_propertyList') { ?> active open <?php } ?>">
                                    <a href="<?php echo base_url();?>sanity/p_propertyList" title="Dashboard"><i class="fa fa-fw fa-caret-right"></i>Private propertyList</a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-dropdown <?php if (isset($act)) {
                            if (($act == 'users') || ($act == 'user-details') || ($act == 'viewDetail')) { ?> active open <?php }
                        } ?>">
                            <a href="#"><i class="fa fa-lg fa-fw fa-users" aria-hidden="true"></i>&nbsp;&nbsp;Users</a>
                            <ul class="nav-sub">
                                <li class="<?php if (($act == 'users') || ($act == 'user-details') || ($act == 'viewDetail')) { ?> active open <?php } ?>">
                                    <a href="<?php echo base_url();?>sanity/users" title="Users"><i class="fa fa-fw fa-caret-right"></i>Users List</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="nav-dropdown <?php if (isset($act)) {
                            if (($act == 'message') || ($act == 'sent_message') || ($act == 'user_message')) { ?> active open <?php }
                        } ?>">
                            <a href="#"><i class="fa fa-lg fa-fw fa-users" aria-hidden="true"></i>&nbsp;&nbsp;Send message</a>
                            <ul class="nav-sub">
                                <li class="<?php if (($act == 'message') || ($act == 'sent_message')) { ?> active open <?php } ?>">
                                    <a href="<?php echo base_url();?>sanity/message" title="Users"><i class="fa fa-fw fa-caret-right"></i>Message</a>
                                </li>
                                <li class="<?php if (($act == 'message') || ($act == 'sent_message') || ($act == 'user_message')) { ?> active open <?php } ?>">
                                    <a href="<?php echo base_url();?>sanity/user_message" title="Users"><i class="fa fa-fw fa-caret-right"></i>Send Message to All</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="nav-dropdown <?php if (isset($act)) {
                                if (($act == 'applyFormList') || ($act == 'ProEditDetails')){ ?> active open <?php }
                            }
                            ?>">
                            <a href="#"><i class="fa fa-lg fa-fw fa-shopping-cart" aria-hidden="true"></i>&nbsp;&nbsp;applyForm List</a>
                            <ul class="nav-sub">
                                <li class="<?php if ($act == 'applyFormList' || ($act == 'ProEditDetails')) { ?> active open <?php } ?>">
                                    <a href="<?php echo base_url();?>sanity/applyFormList" title="Dashboard"><i class="fa fa-fw fa-caret-right"></i>applyFormList</a>
                                </li>
                            </ul>
                        </li>

                       
                        <li class="nav-dropdown <?php if (isset($act)) {
                        if ($act == 'coupon') { ?> active open <?php }
                    } ?>">
                            <a href="#"><i class="fa fa-lg fa-fw fa-cog"></i>&nbsp;&nbsp;Coupon</a>
                            <ul class="nav-sub">
                                <li <?php if ($act == 'coupon') { ?> class="active open" <?php } ?>>
                                    <a href="<?php echo base_url();?>sanity/coupon" title="Form Layouts">
                                        <i class="fa fa-fw fa-caret-right"></i>Add Coupon Code
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>                
                </nav>
            </aside>
        <?php } ?>
    <!--start page-content-->        
    <div class="page-content">
       
    <div class="container wid-init">
    <?php
    if ($this->session->flashdata('success') != '') {
        echo'<div class="alert alert-success" role="alert" id="">';
        echo $this->session->flashdata('success');
        echo '<i class="glyphicon glyphicon-thumbs-up"></i>';
        echo '</div>';
    }
    ?>
    <?php
    if ($this->session->flashdata('error') != '') {
        echo'<div class="alert alert-danger" role="alert" id="">';
        echo $this->session->flashdata('error');
        echo '<i class="glyphicon glyphicon-thumbs-down"></i>';
        echo '</div>';
    }
    ?>  
    </div>
