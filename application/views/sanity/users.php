<div class="page-subheading page-subheading-md">
    <ol class="breadcrumb">
        <li><a href="home.php">Dashboard</a></li>
        <li class="active">Register Users</li>
    </ol>
</div>
<div class="page-heading page-heading-md">
    <h2>Users</h2>
</div>

<div class="container-fluuser_id-md">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive" style="min-height:500px; height:auto;">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th wuser_idth="8%">Sr.No.</th>
                                    <th wuser_idth="18%">User Name</th>
                                    <th wuser_idth="18%">User Type</th>
                                    <th wuser_idth="18%">Profile</th>
                                    <th wuser_idth="18%">Full Name</th>
                                    <th wuser_idth="15%">Mobile No</th>
                                    <th wuser_idth="22%">Email ID</th>
                                    <th wuser_idth="15%">Register Date</th>
                                    <th wuser_idth="10%">Status</th>
                                    <th wuser_idth="22%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
<!--                                <tr>
                                    <td colspan="4" style="text-align:center;">No Users Available</td>
                                </tr>-->
                                <?php foreach ($users as $row){ ?>
                                <tr>
                                    <td><?php echo $row['user_id']; ?></td>
                                    <td style="text-transform: capitalize"><?php echo html_entity_decode($row['user_name']); ?></td>
                                    <td style="text-transform: capitalize"><?php echo $row['user_type']; ?></td>
                                    <td><img src="<?php echo base_url(); ?>assets/profile_images/<?php echo $row['profile_picture']; ?>" width="25%"></td>
                                    <td style="text-transform: capitalize"><?php echo html_entity_decode($row['first_name']." ".$row['last_name']); ?></td>
                                    <td><?php echo $row['contact_no']; ?></td>
                                    <td><?php echo $row['email']; ?></td>
                                    <td><?php echo date('d M, Y', strtotime($row['created_date'])); ?></td>
                                    <td><?php echo $row['status']; ?></td>
                                    <td>
                                        <a href="<?php echo base_url();?>sanity/viewDetail/<?php echo $row['user_id']; ?>" class="btn btn-warning" style="padding:1px 4px; margin-right:5px;" title="View Details"><i class="fa fa-star"></i></a>
                                    <!--<a href="home.php?p=edit-user&updateuser_id=<?php echo $row['user_id']; ?>" class="btn btn-info" style="padding:1px 4px; margin-right:5px;" title="Edit Record"><i class="fa fa-pencil"></i></a>-->
                                    <a href="<?php echo base_url();?>sanity/delUser/<?php echo $row['user_id']; ?>" class="btn btn-danger" style="padding:1px 4px; margin-right:5px;" title="Delete User" onclick="return confirm('Are you sure you want to delete this users?');"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div><!--/table-responsive-->
                </div><!--/.panel-body-->
            </div><!--/.panel-->
        </div><!--/.col-md-12-->
    </div><!--/.row-->
</div><!--/.container-fluuser_id-md--> 