<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.elevatezoom.js"></script>
<!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/theme-script.js"></script>-->
<style>
    .control-label{
        font-weight: 900;
        float: left
    }
</style>
<div class="page-subheading page-subheading-md">
    <ol class="breadcrumb">
        <li><a href="home.php">Dashboard</a></li>
        <li><a href="<?php echo base_url(); ?>sanity/applyFormList">ApplyForm List</a></li>
        <li class="active">Edit Details</li>
    </ol>
</div>

<div class="page-heading page-heading-md">
    <h2>Property Details<span style="float:right; font-size:25px; padding-top:15px;"><a href="<?php echo base_url(); ?>sanity/applyFormList">ApplyForm List </a></span></h2>
</div>
<?php // print_r($ProRes); ?>

<div class="container-fluid-md" style="min-height:500px; height:auto;">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="product-image">
            <div class="product-full">
                <img id="product-zoom" src='<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRes[0]['pro_photo']; ?>' 
                     class="img-responsive set-img-imp" data-zoom-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRes[0]['pro_photo']; ?>"/>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <form class="form-horizontal form-bordered" role="form" name="adminproform" action="<?php echo base_url();?>sanity/updateProper" method="post" >
            <div class="panel panel-default">
<!--                <input type="hidden"  name="property_id"  value="<?php echo $pro_id; ?>">-->
                <input type="hidden" name="user_id"  value="<?php echo $ProRes[0]['user_id']; ?>">
                <input type="hidden" name="public_id" value="<?php echo $public_id; ?>">
                <input type="hidden" name="public_property_id" value="<?php echo $public_property_id; ?>">
                <?php if(!empty($applyproperty_private)){ ?>
                    <input type="hidden" name="property_id" value="<?php echo $applyproperty_private[0]['own_property']; ?>">
                <?php//}else{?>
                    <!--<input type="hidden" name="property_id" value="0">-->
               <?php } ?>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Project Name</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="project_name"  value="<?php echo $ProRes[0]['project_name']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Locality</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="locality"  value="<?php echo $ProRes[0]['locality']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">City</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="city"  value="<?php echo $ProRes[0]['city']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Plot Area(l*b)</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="plot_area"  value="<?php echo $ProRes[0]['plot_area']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">BHK</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="no_of_bedrooms"  value="<?php echo $ProRes[0]['no_of_bedrooms']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Bathroom</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="no_of_bathrooms"  value="<?php echo $ProRes[0]['no_of_bathrooms']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Balcony</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="no_of_balcony"  value="<?php echo $ProRes[0]['no_of_balcony']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Complete address</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="complete_address"  value="<?php echo $ProRes[0]['complete_address']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Water(Borewell/Not) 24/7</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="water_borewell"  value="<?php echo $ProRes[0]['water_borewell']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Garden ?</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="Garden"  value="<?php echo $ProRes[0]['Garden']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Electricity/generator?</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="electricity_backup"  value="<?php echo $ProRes[0]['electricity_backup']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Swimming-pool</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="swimming_pool"  value="<?php echo $ProRes[0]['swimming_pool']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Age of property</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="age_of_property"  value="<?php echo $ProRes[0]['age_of_property']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Status</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="status1"  value="<?php echo $ProRes[0]['status1']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">electricity</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="electricity"  value="<?php echo $ProRes[0]['electricity']; ?>">
                        </div>
                    </div>
                    
                    <input type="hidden" class="form-control" name="pro_photo"  value="<?php echo $ProRes[0]['pro_photo']; ?>">
                    <div class="form-group">
                       <select name="days" class="col-sm-3">
                            <option value="">Days</option>
                            <?php for($j=1;$j<100;$j++){?>
                            <option value="<?php echo $j; ?>"><?php echo $j; ?></option>
                            <?php }?>
                        </select>
                        <select name="hrs" class="col-sm-3">
                            <option value="">Hours</option>
                            <?php for($i=1;$i<=24;$i++){?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="controls col-sm-9">
                            <input type="submit" class="btn btn-primary" name="submit"  value="Update">
                            <input type="button" class="btn btn-warning" name="button"  value="Accept">
                        </div>
                    </div>
                </div>
            </div>
        </form>
     </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pdMainFacts type2">
        <h3><u>Preference Details</u></h3>
        <table style="width:100%">
            <tbody>
                <tr class="trfacts" style="height:75px">
                    <td>
                        <div class="pdFactHead top7">
                            <img src="<?php echo base_url(); ?>assets/img/Plot_area.jpg" style="width: 25px;height: 25px;">
                            <span class="areaType">Plot Area(l*b)</span></div>     
                        <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['plot_area']; ?></span></div>
                    </td>
                    &nbsp;
                    <td>
                        <div class="pdFactHead top7"><img src="<?php echo base_url(); ?>assets/img/balcony.png" style="width: 25px;height: 25px;">
                            <span class="areaType">BHK and Balcony,Bathrooms</span></div>
                        <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['no_of_bedrooms']; ?>BHK <?php echo $PreferRes[0]['no_of_balcony']; ?>Balcony, <?php echo $PreferRes[0]['no_of_bathrooms']; ?>Bathrooms</span></div>
                    </td>
                </tr>
                <tr class="separator" style="height:75px">
                    <td>
                        <div class="pdFactHead top7"><img src="<?php echo base_url(); ?>assets/img/address.png" style="width: 25px;height: 25px;">
                            <span class="areaType">zipcode</span></div>     
                        <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['zipcode']; ?></span></div>
                    </td>
                    <td style="background: #597959">
                        <div class="pdFactHead" style="background: #597959;color: white;"><img src="<?php echo base_url(); ?>assets/img/Water 24-7.png" style="width: 25px;height: 25px;">
                            <span class="areaType">Water(Borewell/Not) 24/7</span></div>
                        <div class="pdFactHead" style="color: white;"><b><span class="areaType1"><?php echo $PreferRes[0]['water_borewell']; ?></b></span></div>
                    </td>
                </tr>
                <tr style="height:75px">
                    <td>
                        <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Garden.jpg" style="width: 25px;height: 25px;">
                            <span class="areaType">City</span></div>     
                        <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['city']; ?></span></div>
                    </td>
                    <td>
                        <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/Electricity.png" style="width: 25px;height: 25px;">
                            <span class="areaType">Electricity/generator?</span></div>
                        <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['electricity_backup']; ?></span></div>
                    </td>
                </tr>
                <tr style="height:75px">
                    <td>
                        <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/swimming.png" style="width: 25px;height: 25px;">
                            <span class="areaType">Swimming-pool</span></div>     
                        <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['swimming_pool']; ?></span></div>
                    </td>
                    <td>
                        <div class="pdFactHead"><img src="<?php echo base_url(); ?>assets/img/1_fast.png" style="width: 25px;height: 25px;">
                            <span class="areaType">Age of property</span></div>
                        <div class="pdFactHead"><span class="areaType1"><?php echo $PreferRes[0]['age_of_property']; ?></span></div>
                    </td>
                </tr>
            </tbody>
        </table>

    </div>
</div>    