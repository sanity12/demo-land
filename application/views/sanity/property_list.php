<div class="page-subheading page-subheading-md">
    <ol class="breadcrumb">
        <li><a href="home.php">Dashboard</a></li>
        <li class="active">All Property Details</li>
    </ol>
</div>
<div class="page-heading page-heading-md">
    <h2>Property List</h2>
</div>

<div class="container-fluuser_id-md">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive" style="min-height:500px; height:auto;">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th wuser_idth="8%">Sr.No.</th>
                                    <th width="8%">Status</th>
                                    <th wuser_idth="18%">User Name</th>
                                    <th wuser_idth="15%">user_type</th>
                                    <th wuser_idth="15%">project_name</th>
                                    <th wuser_idth="22%">pro_photo</th>
                                    <th wuser_idth="15%">locality</th>
                                    <th wuser_idth="10%">you_are</th>
                                    <th wuser_idth="22%">Date</th>
                                    <th wuser_idth="22%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
<!--                                <tr>
                                    <td colspan="4" style="text-align:center;">No Users Available</td>
                                </tr>-->
                                <?php foreach ($propertyRes as $row){ ?>
                                <tr>
                                    <td><?php echo $row['property_id']; ?></td>
                                    <td>
                                        <?php if (isset($row['status']) && $row['status'] == '0') { ?>
                                            <a href="javascript:void(0)"  class="btn btn-primary">Submitted Form</a>
                                        <?php } else if ($row['status'] == '1') {
                                            ?><a href="javascript:void(0)" class="btn btn-warning">Recommended Form</a>
                                        <?php } else if ($row['status'] == '2') {
                                            ?><a href="javascript:void(0)" class="btn btn-success">Accepted Form</a>
                                        <?php } else {
                                            ?><a href="javascript:void(0)" class="btn btn-danger">Deleted Form</a>
                                        <?php } ?> 
                                    </td>
                                    <td style="text-transform: capitalize"><?php echo get_username($row['user_id']); ?></td>
                                    <td style="text-transform: capitalize"><?php echo html_entity_decode($row['user_type']); ?></td>
                                    <td style="text-transform: capitalize"><?php echo $row['project_name']; ?></td>
                                    <td><img src="<?php echo base_url(); ?>assets/Pro_Imgupload/<?php echo $row['pro_photo']; ?>" width="25%"></td>
                                    <td><?php echo $row['locality']; ?></td>
                                    <td style="width:10%"><?php echo $row['you_are']; ?></td>
                                    <td style="width:10%"><?php echo date('d M, Y', strtotime($row['created_date'])); ?></td>
                                    
                                    <td>
                                        <a href="<?php echo base_url();?>sanity/viewDetail/<?php echo $row['user_id']; ?>" class="btn btn-warning" style="padding:1px 4px; margin-right:5px;" title="View Details"><i class="fa fa-star"></i></a>
                                    <!--<a href="home.php?p=edit-user&updateuser_id=<?php echo $row['user_id']; ?>" class="btn btn-info" style="padding:1px 4px; margin-right:5px;" title="Edit Record"><i class="fa fa-pencil"></i></a>-->
                                    <a href="<?php echo base_url();?>sanity/delUser/<?php echo $row['user_id']; ?>" class="btn btn-danger" style="padding:1px 4px; margin-right:5px;" title="Delete User" onclick="return confirm('Are you sure you want to delete this users?');"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div><!--/table-responsive-->
                </div><!--/.panel-body-->
            </div><!--/.panel-->
        </div><!--/.col-md-12-->
    </div><!--/.row-->
</div><!--/.container-fluuser_id-md--> 

