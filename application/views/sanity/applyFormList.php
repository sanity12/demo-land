<div class="page-subheading page-subheading-md">
    <ol class="breadcrumb">
        <li><a href="home.php">Dashboard</a></li>
        <li class="active">Apply Form Details</li>
    </ol>
</div>
<div class="page-heading page-heading-md">
    <h2>Form List</h2>
</div>

<div class="container-fluid-md">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive" style="min-height:500px; height:auto;">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="8%">Sr.No.</th>
                                    <th width="8%">public name</th>
                                    <th width="10%">main Form</th>
                                    <th width="12%">Private User Name</th>
                                    <th style="width:18%"><h4><strong>Payment</strong></h4></th> 
                                    <th width="10%" style="width:10%">applied form</th>
                                    <th width="10%">Date</th>
                                    <th width="25%" >LastDate</th>
                                </tr>
                            </thead>
                            <tbody>

<!--                                <tr>
    <td colspan="4" style="text-align:center;">No Users Available</td>
</tr>-->
                                <?php foreach ($applyRes as $row) { ?>
                                    <tr>
                                        <td><?php echo $row['app_id']; ?></td>
                                         <td><?php echo get_username($row['user_public_id']); ?></td>
                                        <td><?php $p = get_photo($row['proper_id']); ?>
                                            <a href="<?php echo base_url(); ?>home/proDetail/<?php echo $row['proper_id']; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/Pro_Imgupload/<?php echo $p; ?>" width="40%" style="width: 100%;"></a></td>
                                       
                                        <td><?php echo get_username($row['user_private_id']); ?>
                                            <a href="<?php echo base_url(); ?>sanity/ProEditDetails/<?php echo $row['proper_id']; ?>/<?php echo $row['user_public_id']; ?>/<?php echo $row['proper_id']; ?>" class="btn btn-warning" style="padding:1px 4px; margin-right:5px;" title="View Details"><i class="fa fa-pencil"></i></a></td>
                                        <?php if ($row['is_payment'] == '1') { ?>
                                            <td data-th="Price"><h4><strong>&nbsp;<span class="btn btn-success">PAID</span></strong></h4></td>
                                        <?php } else { ?>
                                            <td data-th="Price"><h4><strong>&nbsp;<a class="btn btn-primary col-md-7" href="<?php echo base_url(); ?>postproperty/paynow/<?php //echo $warr[$i]; ?>">PAY NOW</a></strong></h4></td>
                                        <?php } ?>
                                        <td><?php
                                            $qw = $row['own_property'];
                                            $warr = explode(",", $qw);
                                            for ($i = 0; $i < count($warr); $i++) {
                                                $ph = get_photo($warr[$i]);
                                                ?>
                                                <a href="<?php echo base_url(); ?>home/proDetail/<?php echo $warr[$i]; ?>" target="_blank">
                                                    <img src="<?php echo base_url(); ?>assets/Pro_Imgupload/<?php echo $ph; ?>" width="25%" style="width: 100%;">
                                                </a>
                                                
                                            <?php }?>
                                        </td>
                                        <td><?php echo date('d M, Y', strtotime($row['created_date'])); ?></td>
                                        <?php if ($row['is_payment'] == '0') { ?>
                                        <td class="cntdwn" style="background:#c0392b;color:white;border: solid;">
                                            <?php
                                            date_default_timezone_set('Asia/Kolkata');
                                            $CurrDate = date('Y-m-d H:i:s');

                                            $date = new DateTime($row['updated_date']);
                                            $sdate1 = $date->format('M d, Y H:i:s');

                                            $date = new DateTime($row['created_date']);
                                            $credate1 = $date->format('M d, Y H:i:s');

                                            ?>
                                            <div class="col-lg-12">
                                                <h4><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;<strong>Deal Counter</strong></h4>
                                                <div id="clockdiv<?php echo $row['app_id']; ?>" style="float: left" class="clockdiv">
                                                    <div>
                                                        <span class="days"></span>
                                                        <div class="smalltext">Days</div>
                                                    </div>
                                                    <div>
                                                        <span class="hours"></span>
                                                        <div class="smalltext">Hours</div>
                                                    </div>
                                                    <div>
                                                        <span class="minutes"></span>
                                                        <div class="smalltext">Minutes</div>
                                                    </div>
                                                    <div>
                                                        <span class="seconds"></span>
                                                        <div class="smalltext">Seconds</div>
                                                    </div>
                                                </div>
                                                <div id="note_div" style="float: left">
                                                    <!--<p>Amount will be locked after 5 days from investment.</p>-->
                                                </div>
                                            </div>
                                        </td>
                                        
                                        
                                        <script>
                                            $(document).ready(function () {
                                                function getTimeRemaining(endtime) {
                                                    var t = Date.parse(endtime) - Date.parse(new Date());
                                                    //var t = 435600000;
                                                    var days = Math.floor(t / (1000 * 60 * 60 * 24));
                                                    var hours = Math.floor((t % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                                    var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
                                                    var seconds = Math.floor((t % (1000 * 60)) / 1000);

                                                    return {
                                                        'total': t,
                                                        'days': days,
                                                        'hours': hours,
                                                        'minutes': minutes,
                                                        'seconds': seconds
                                                    };
                                                }

                                                function initializeClock(id, endtime) {

                                                    var clock = document.getElementById(id);
                                                    var daysSpan = clock.querySelector('.days');
                                                    var hoursSpan = clock.querySelector('.hours');
                                                    var minutesSpan = clock.querySelector('.minutes');
                                                    var secondsSpan = clock.querySelector('.seconds');

                                                    function updateClock() {
                                                        var t = getTimeRemaining(endtime);
                                                        daysSpan.innerHTML = t.days;
                                                        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                                                        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                                                        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
                                                        console.log(t.total);
                                                        // if(){
                                                        //     location.reload();

                                                        // }
                                                        if (t.total <= 0) {
                                                            location.reload();
                                                            clearInterval(timeinterval);
                                                        }
                                                    }

                                                    // updateClock();
                                                    var timeinterval = setInterval(updateClock, 1000);

                                                }


                                        // var deadline = new Date(Date.parse(new Date()) + <?php //echo $a;    ?> * <?php //echo $b;    ?> * <?php //echo $c;    ?> * <?php //echo $d;    ?> * 1000);
                                        // var countDownDate = new Date(value).getTime();

                                                var deadline = "<?php echo $sdate1; ?>";

                                                var credate1 = "<?php echo $credate1; ?>";
                                                var tm = Date.parse(deadline) - Date.parse(new Date());
                                                var initime = Date.parse(deadline) - Date.parse(credate1);
                                        //   console.log(initime);
                                                console.log(initime / 4);

                                                console.log(tm);
                                        // console.log((tm / 4))
                                                if (tm > 0 && tm < (initime / 4)) {

                                                    var product_id = 1;
                                                    // alert(product_id);
                                                    $.ajax({
                                                        type: 'POST',
                                                        url: 'date-fun.php',
                                                        data: {product_id: product_id, pro_stock: 'Almost Closing'},
                                                        success: function (response) {//response is value returned from php (for your example it's "bye bye"
                                                            console.log(response);
                                                        }
                                                    });
                                                    initializeClock('clockdiv<?php echo $row['app_id']; ?>', deadline);
                                                } else if (tm >= 0) {

                                                    initializeClock('clockdiv<?php echo $row['app_id']; ?>', deadline);


                                                } else {
                                                    var clock = document.getElementById('clockdiv<?php echo $row['app_id']; ?>');
                                                    var daysSpan = clock.querySelector('.days');
                                                    var hoursSpan = clock.querySelector('.hours');
                                                    var minutesSpan = clock.querySelector('.minutes');
                                                    var secondsSpan = clock.querySelector('.seconds');
                                                    daysSpan.innerHTML = '00';
                                                    hoursSpan.innerHTML = '00';
                                                    minutesSpan.innerHTML = '00';
                                                    secondsSpan.innerHTML = '00';

                                                    var product_id = 1;
                                                    $.ajax({
                                                        type: 'POST',
                                                        url: 'date-fun.php',
                                                        data: {product_id: product_id, pro_stock: 'Closed'},
                                                        success: function (response) {//response is value returned from php (for your example it's "bye bye"
                                                            //alert(response);
                                                        }
                                                    });
                                                    //  echo update_stock($ProRes[0]['pro_id']);
                                                }
                                            });

                                        </script> 
                                        <?php }else{?>
                                        <td class="cntdwn" style="background:#c0392b;color:white;border: solid;">
                                            <div class="col-lg-12">
                                                <h4><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;<strong>Deal Counter</strong></h4>
                                                <div id="clockdiv<?php echo $row['app_id']; ?>" style="float: left" class="clockdiv">
                                                    <div>
                                                        <span class="days">00</span>
                                                        <div class="smalltext">Days</div>
                                                    </div>
                                                    <div>
                                                        <span class="hours">00</span>
                                                        <div class="smalltext">Hours</div>
                                                    </div>
                                                    <div>
                                                        <span class="minutes">00</span>
                                                        <div class="smalltext">Minutes</div>
                                                    </div>
                                                    <div>
                                                        <span class="seconds">00</span>
                                                        <div class="smalltext">Seconds</div>
                                                    </div>
                                                </div>
                                                <div id="note_div" style="float: left">
                                                    <!--<p>Amount will be locked after 5 days from investment.</p>-->
                                                </div>
                                            </div>
                                        </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div><!--/table-responsive-->
                </div><!--/.panel-body-->
            </div><!--/.panel-->
        </div><!--/.col-md-12-->
    </div><!--/.row-->
</div><!--/.container-fluuser_id-md--> 
<script type="text/javascript" src="http://rendro.github.io/countdown/javascripts/jquery.countdown.js"></script>
<!--<script type="text/javascript">
$(function() {
    $('.cntdwn').countdown({
        date: "<?php echo date('d M, Y', strtotime($row['updated_date'])); ?>",
        render: function(data) {
            console.log(data);
          $(this.el).text(this.leadingZeros(data.days, 2) + " days " + this.leadingZeros(data.hours, 2) + " hrs " + this.leadingZeros(data.sec, 2) + " sec");
        },
    });
});
</script>-->

