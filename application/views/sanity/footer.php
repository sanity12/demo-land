<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.navgoco.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/js/dashboard.js"></script>-->
<script>
    $(document).ready(function () {
        $(".alert").hide();
        $(".alert").fadeTo(2000, 2000).slideUp(2000, function(){
            $(".alert").slideUp(2000);
        });
    });
</script>
</body>
</html>
