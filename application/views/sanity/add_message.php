<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/ckeditor/samples/css/samples.css">
<div class="content-wrapper"><br>
     <h3 class="text-center margin-xl-bottom"></h3>
     <div id="content" class="content-container ng-scope">
    <section class="view-container animate-fade-up">
        <div class="container wid-init">
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <div class="details_div">
                        <div class="centerblock land_header_pclass"> <h2>Add Message<span style="float:right; font-size:25px; padding-right:123px;"><a href="<?php echo base_url();?>sanity/message">View Messages</a></span></h2>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        
        <div class="container wid-init ce-form">

            <form class="well form-horizontal" action="<?php echo site_url('sanity/add_msg'); ?>" method="post"  id="msg_form" enctype="multipart/form-data">
                <fieldset>
                    <h4>New Message</h4>
                    <div class="form-group">
                        <label class="col-md-4 control-label">To</label>  
                        <div class="col-md-8 inputGroupContainer">
                        <div class="input-group col-md-8">
                            <input  name="user_id" id="user_id" type="hidden" value="<?php echo $id; ?>">
                            <input  name="to" placeholder="" class="form-control"  id="to" type="text" value="<?php echo get_username($id); ?>" readonly disable="" >
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" > Message Type*</label> 
                            <div class="col-md-8 inputGroupContainer">
                            <div class="input-group col-md-8">
                        <input name="msg_type" id="msg_type" placeholder="Message Type" class="form-control"  type="text">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Product Description*</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group col-md-8">
                                <span class="input-group col-md-8-addon"></span>                           
                                <textarea class="ckeditor" name="message" id="editor1"></textarea>                  
                            </div>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-4"><br>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-warning" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                        </div>
                    </div>
                </fieldset>
            </form>
            
        </div><!-- /.container -->
    </section>
</div> 
</div>
<script src="<?php echo base_url();?>assets/js/ckeditor.js"></script>
<script src="<?php echo base_url();?>assets/js/ckfinder.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('editor1', {
fullPage: true,
allowedContent: true,
extraPlugins: 'wysiwygarea'
 
});
</script>
<!-- /.content-wrapper -->
