<div class="page-heading page-heading-md">
    <h2>Property Details<span style="float:right; font-size:25px; padding-top:15px;"><a href="<?php echo base_url(); ?>sanity/p_propertyList">Property List </a></span></h2>
</div>
<?php // print_r($ProRes); ?>

<div class="container-fluid-md" style="min-height:500px; height:auto;">
    
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <form class="form-horizontal form-bordered" role="form" name="adminproform" action="<?php echo base_url();?>sanity/updateProper" method="post" >
            <div class="panel panel-default">
                <input type="hidden"  name="property_id"  value="<?php echo $pro_id; ?>">
                <input type="hidden" name="user_id"  value="<?php echo $ProRes[0]['user_id']; ?>">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Project Name</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="project_name"  value="<?php echo $ProRes[0]['project_name']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Locality</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="locality"  value="<?php echo $ProRes[0]['locality']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">City</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="city"  value="<?php echo $ProRes[0]['city']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Plot Area(l*b)</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="plot_area"  value="<?php echo $ProRes[0]['plot_area']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">BHK</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="no_of_bedrooms"  value="<?php echo $ProRes[0]['no_of_bedrooms']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Bathroom</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="no_of_bathrooms"  value="<?php echo $ProRes[0]['no_of_bathrooms']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Balcony</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="no_of_balcony"  value="<?php echo $ProRes[0]['no_of_balcony']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Complete address</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="complete_address"  value="<?php echo $ProRes[0]['complete_address']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Water(Borewell/Not) 24/7</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="water_borewell"  value="<?php echo $ProRes[0]['water_borewell']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Garden ?</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="Garden"  value="<?php echo $ProRes[0]['Garden']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Electricity/generator?</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="electricity_backup"  value="<?php echo $ProRes[0]['electricity_backup']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Swimming-pool</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="swimming_pool"  value="<?php echo $ProRes[0]['swimming_pool']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Age of property</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="age_of_property"  value="<?php echo $ProRes[0]['age_of_property']; ?>" readonly disable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Electricity</label>
                        <div class="controls col-sm-9">
                            <input type="text" class="form-control" name="electricity"  value="<?php echo $ProRes[0]['electricity']; ?>" readonly disable>
                        </div>
                    </div>
<!--                    <div class="form-group">
                        <div class="controls col-sm-9">
                            <input type="submit" class="btn btn-primary" name="submit"  value="Update">
                            <input type="button" class="btn btn-warning" name="button"  value="Accept">
                        </div>
                    </div>-->
                </div>
            </div>
        </form>
     </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="product-image">
            <div class="product-full">
                <img id="product-zoom" src='<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRes[0]['pro_photo']; ?>' 
                     class="img-responsive set-img-imp" data-zoom-image="<?php echo base_url(); ?>/assets/Pro_Imgupload/<?php echo $ProRes[0]['pro_photo']; ?>"/>
            </div>
        </div>
    </div>
</div>