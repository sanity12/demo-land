<!doctype html>
<html class="no-js">
    <head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Admin Panel</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <!--<link rel="shortcut icon" href="/favicon.ico">-->
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="dist/css/iriy-admin.min.css">
        <link rel="stylesheet" href="demo/css/demo.css">
        <link rel="stylesheet" href="dist/assets/font-awesome/css/font-awesome.css">

        <!--[if lt IE 9]>
        <script src="dist/assets/libs/html5shiv/html5shiv.min.js"></script>
        <script src="dist/assets/libs/respond/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" >
            function isempty() {
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

                if (document.loginfrm.username.value == "")
                {
                    alert("Please enter username");
                    document.loginfrm.username.focus();
                    document.loginfrm.username.select();
                    return false;
                }

                if (document.loginfrm.password.value == "")
                {
                    alert("Please enter password");
                    document.loginfrm.password.focus();
                    return false;
                }
            }
        </script>
    </head>
    <body class="body-sign-in">
        <div class="container">
            <div class="panel panel-default form-container">
                <div class="panel-body">
                    <form role="form" name="loginfrm" action="<?php echo base_url();?>sanity/signin" method="post" onSubmit="return isempty();">
                        <h3 class="text-center margin-xl-bottom">Welcome To Admin Panel</h3>

                        <?php if (isset($error)) {
                            if ($error == '1') { ?>
                                <p class="text-center" style="color:#f00;">Wrong email id or password</p>
    <?php }
} ?>

                        <div class="form-group text-center">
                            <label class="sr-only" for="username">User Name</label>
                            <input type="text" class="form-control input-lg" name="username" placeholder="User Name">
                        </div>
                        <div class="form-group text-center">
                            <label class="sr-only" for="password">Password</label>
                            <input type="password" class="form-control input-lg" name="password" placeholder="Password">
                        </div>
                        <input type="submit" class="btn btn-primary btn-block btn-lg" name="login" value="SIGN IN">
                    </form>
                </div>
                <div class="panel-body text-center">

                </div>
            </div>
        </div>
    </body>

</html>
