<div class="page-subheading page-subheading-md">
    <ol class="breadcrumb">
        <li><a href="">Dashboard</a></li>
        <li class="active"><a href="">Admin Passwords</a></li>
    </ol>
</div>
<div class="page-heading page-heading-md">
    <h2>Change Passwords</h2>
</div>

<div class="container-fluid-md">
    <form class="form-horizontal form-bordered" role="form" name="adminform" action="<?php echo base_url();?>sanity/updatePwd" method="post" onchange="return isempty();">
        <h3>Admin Password</h3>
        <div class="panel panel-default">
            <div class="panel-body">


                <div class="form-group">
                    <label class="control-label col-sm-2">Enter New Password</label>

                    <div class="controls col-sm-9">
                        <input type="password" class="form-control" name="pass" placeholder="New Password">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Re-Enter Password</label>

                    <div class="controls col-sm-9">
                        <input type="password" class="form-control" name="repass" placeholder="Re-Enter New Password">
                    </div>
                </div>


                <div class="form-group has-success">
                    <label class="control-label col-sm-2"></label>

                    <div class="controls col-sm-9">
                        <button type="submit" class="btn btn-primary" name="admin_submit" value="Update">Update</button>
                    </div>
                </div>

            </div>
        </div>
    </form>


    <form class="form-horizontal form-bordered" role="form" name="iform" action="" method="post" onsubmit="return validation();">
        <h3>Master Password</h3>
        <div class="panel panel-default">
            <div class="panel-body">


                <div class="form-group">
                    <label class="control-label col-sm-2">Enter New Password</label>

                    <div class="controls col-sm-9">
                        <input type="password" class="form-control" name="Masterpass" placeholder="New Password">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Re-Enter Password</label>

                    <div class="controls col-sm-9">
                        <input type="password" class="form-control" name="repass" placeholder="Re-Enter New Password">
                    </div>
                </div>


                <div class="form-group has-success">
                    <label class="control-label col-sm-2"></label>

                    <div class="controls col-sm-9">
                        <button type="submit" class="btn btn-primary" name="master_submit" value="Update">Update</button>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>    



<div style="min-height:200px;">&nbsp;</div>
<script type="text/javascript" >
    function isempty() {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (document.adminform.pass.value == "")
        {
            alert("Please enter new password");
            document.adminform.pass.focus();
            document.adminform.pass.select();
            return false;
        }

        if (document.adminform.repass.value == "")
        {
            alert("Please re-enter new password");
            document.adminform.repass.focus();
            document.adminform.repass.select();
            return false;
        }

        if (document.adminform.pass.value != document.adminform.repass.value)
        {
            alert("Password does not match");
            document.adminform.repass.focus();
            document.adminform.repass.select();
            return false;
        }
    }


    function validation() {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (document.iform.pass.value == "")
        {
            alert("Please enter new password");
            document.iform.pass.focus();
            document.iform.pass.select();
            return false;
        }

        if (document.iform.repass.value == "")
        {
            alert("Please re-enter new password");
            document.iform.repass.focus();
            document.iform.repass.select();
            return false;
        }

        if (document.iform.pass.value != document.iform.repass.value)
        {
            alert("Password does not match");
            document.iform.repass.focus();
            document.iform.repass.select();
            return false;
        }
    }

</script>
