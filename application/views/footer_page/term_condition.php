<div id="content" class="content-container ng-scope">
	<style type="text/css">
		p {
    line-height: 200%;
}
	</style>
	<section class="view-container animate-fade-up" id="one">
		<div class="container wid-init">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="details_div">
						<div class="centerblock land_header_pclass"></div>
						<div class="centerblock land_header_pcls"><h3><b>Paraskrit Terms And Conditions</b></h3></div>
					</div>
				</div>
			</div>
		</div>
		<div class="container wid-init ce-form"><h4 style="font-family: initial;"><p>
		•	  &nbsp;&nbsp;I/We confirm that we have carefully read and fully understood all the terms of use of availing financing that are listed on Paraskrit Proprietorship website at https://www.Paraskrit.com/terms_use & https://www.Paraskrit.com/privacy and applicable to any advances/credit availed through the Website. <br><br>
		•	  &nbsp;&nbsp;I/We accept the terms & conditions unconditionally and agree that these terms and conditions may be amended or modified at any time and I/we will be bound by the amended terms & conditions that are in force.<br><br>
		•	&nbsp;&nbsp;I/We understand that sanction of the credit is at the sole discretion of the Company and is subject to my/our execution of necessary documents, providing any security(ies) and other closing formalities to the satisfaction of the concerned Lender. I/We agree that Company at its sole discretion may reject my application without providing any reasons, and that I/We reserve no right to appeal against this decision.<br><br>
		•	&nbsp;&nbsp;I/We declare that I am/we are citizens of/resident in India and I/we declare, warrant and represent that all the particulars and information and details given / filled in this application form and documents given by me/us to are true, correct and accurate and I/we have not withheld / suppressed any material information. I/We undertake to furnish any additional information/ documents that may be required by PARASKRIT Proprietorship from time to time. PARASKRIT Proprietorship reserves the right to retain the photograph & the documents submitted with the applications & may not return the same to the applicant.<br><br>
		•	&nbsp;&nbsp;I/We confirm that there are no insolvency proceedings against me/us nor have I/we ever been adjudicated insolvent. I/We also undertake to inform PARASKRIT Proprietorship of changes in my/our occupation/employment and to provide any further information that the company may require. I/We shall advise PARASKRIT Proprietorship on the change in my/our resident status.<br><br>
		•	&nbsp;&nbsp;I/We agree that my/our advances/credit will not be used for any speculative or anti-social purpose.<br><br>
		•	&nbsp;&nbsp;As a pre-condition, relating to grant of the advances/credit facilities by PARASKRIT Proprietorship to me/us, I/We agree and give consent for the disclosure by PARASKRIT Proprietorship of: I. information and data relating to me/us and our directors and Company companies; II. the information or data relating to any credit facility availed of/to be availed, by me/us; and III. Default, if any, committed by me/us, in discharge of any our obligations, to all or any third parties as may be deemed necessary at the sole discretion of PARASKRIT Proprietorship.<br><br>
		•	&nbsp;&nbsp;I/We further authorize PARASKRIT Proprietorship to keep me/us informed (vide telephone, SMS, mail, E-mail etc.) of all promotional schemes and/or activities. </p></h4>

		</div>
	</section>
</div>
		