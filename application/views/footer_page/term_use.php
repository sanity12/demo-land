<div id="content" class="content-container ng-scope">
	<section class="view-container animate-fade-up" id="one">
		<div class="container wid-init">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="details_div">
						<div class="centerblock land_header_pclass"></div>
						<div class="centerblock land_header_pcls"><h3><b>TERMS OF USE</b></h3></div>
					</div>
				</div>
			</div>
		</div>
		<div class="container wid-init ce-form">
			
			<p>Paraskrit Website http://paraskrit.com/ (this Website) is owned, provided and maintained by Paraskrit Proprietorship, a company incorporated under the Companies Act, 2017, having its registered office at Nobel Manchester Building, opp. Narhe Police Station, Narhe, Pune- 411041, Maharashtra.</p><br>			
			<p>In these Terms of Use (ToU),“we”,“our” and “us” means Paraskrit Proprietorship. “you” and “your” means any person who accesses and uses this Website.</p><br>

			<h4><b>General Terms</b></h4><br>
			
			<p>Access to and use of this Website is subject to these ToU and our Privacy Policy. By accessing and using this Website you agree to be bound by and to act in accordance with these ToU and our Privacy Policy. If you do not agree to these ToU or our Privacy Policy, you are not permitted to access and use this Website and you should cease such access and/or use immediately.<br><br>

			If you breach any term of these ToU, your right to access and use this Website shall cease immediately.<br><br>

			We reserve the right to amend these ToU from time to time without notice by amending this page. The amended ToU will be effective from the date they are posted on this Website. As these ToU may be amended from time to time, you should check them whenever you visit this Website. Your continued use of this Website will constitute your acceptance of the amended ToU.</p><br>
			
			<h4><b>Eligibility</b></h4>
			<p>The Service is not available to minors under the age of 18 or to any users suspended or removed from the system by Paraskrit Group for any reason. Additionally, users are prohibited from selling, trading, or otherwise transferring their accounts to another party.
			</p><br>

			<h4><b>Right to use Personal/Contact Details</b></h4>
			<p>By making use of this Website and furnishing your personal/contact details, you hereby agree that you are interested in availing and/or purchasing the products/services that you have selected. You hereby agree that we may contact you either electronically or through phone, to understand your interest in our products and/or services and to fulfil your demand. You also agree that we reserve the right to make your details available to any of our authorized members/agents or partners and you may be contacted by them for information and for sales through email, telephone and/or SMS. You agree to receive promotional materials and/or special offers from us through email or SMS. If you disagree with this ToU, please do not use this Website. <br><br>

			Please be aware that information provided on this Website is provided for general information purposes only and is provided so that you can avail the product and/or service that you feel is appropriate to meet your needs. You should always check the suitability, adequacy and appropriateness of the product and/or service that is of interest to you and it is your sole decision whether to obtain or refrain from obtaining any product and/or service. If you are in any doubt as to the suitability, adequacy or appropriateness of any product and/or service referred to on this Website, we suggest that you seek independent professional advice before you obtain any product and/or service via this Website.
			<br><br>
			Please be aware that nothing on this Website is, or shall be deemed to constitute, an offer by us or any third party to sell to you any product and/or service or to enter into any contract with you in respect of any product and/or service. By submitting your details, you are making an offer to obtain the relevant product and/or service that may be accepted or rejected. The contract for the product and/or service will only be concluded once your offer has been accepted. You will receive confirmation when your offer has been accepted.
			<br><br>
			We aim to provide uninterrupted access to this Website but we give no warranty as to the uninterrupted availability of this Website. We reserve the right to suspend, restrict or terminate your access to this Website at any time.
			<br><br>
			We reserve the right to add, amend, delete, edit, remove or modify any information, content, material or data displayed on this Website and without notice from time to time.
			</p><br>

			<h4><b>Permission To Use</b></h4>
			<p>Any unauthorized use by you or any illegal or unlawful activity by you in or for this Website shall terminate the permission or license granted to you by us to use this Website and may give rise to a claim for damages and/or be considered a criminal offence.
				<br><br>
			You may operate a link to this Website provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it, as determined by us. You must not operate a link to this Website in such a way as to suggest or imply any form of association, approval or endorsement by us, unless otherwise agreed between you and us through a separate legal agreement. We reserve the right to require you to immediately remove any link to this Website at any time and we may withdraw any linking permission at any time.
			</p><br>

			<h4><b>Intellectual Property Rights</b></h4>
			<p>The copyright in the information, content, material or data displayed on this Website belongs to us or our licensors or our licensees or authorized or permitted agents/members. You may temporarily print, copy, download or store extracts of information, content, material or data displayed on this Website for your own personal use but should not use for any commercial purpose.<br><br>

			This Website contains material which is owned by or licensed to us and are protected by copyright. This material includes, but is not limited to, data, documents, information, details, design, layout, appearance and graphics; logos, business names, trading names, service marks and trade-marks, internet domain names, moral rights, rights specifications, know how, processes and business methods; software programs (in all cases whether registered or unregistered and including all rights to apply for registration) and all such rights are reserved. Reproduction of any material of our Website is prohibited for any commercial use.<br><br>

			Business Secret / Protection of Business Method: You must not transmit, or input into the Website, any: files that may damage any other person's computing devices or software, content that may be offensive, or material or Data in violation of any law (including Data or other material protected by copyright or trade secrets/ business method on which you do not have the right to use)
			</p><br>
			<h4><b>Our Limited Liabilities</b></h4>
			<p>We use strive to ensure that the data, material and information on this Website is accurate and to correct any errors or omissions as soon as practicable after being notified of them. However, we are not guaranteeing that the data, material and information on this Website are accurate or that there are no errors or omissions in the data, material and information.<br><br>

			We do not guarantee the accuracy of, and disclaim all liability for any errors or other inaccuracies relating to the information and description of the content, products, and services. All such information and services are provided "AS IS" without warranty of any kind.
			<br><br>
			We disclaim that all warranties and conditions that this Website, its services or any email sent from us, our affiliates, and / or their respective agents are free of viruses or other harmful components.<br><br>

			We are not responsible for any losses or damages arising from an inability to access the Website, from any use of the Website or from reliance on the data transmitted using the Website where such losses or damages are caused by any event beyond our reasonable control including as a result of the nature of electronic transmission of data over the internet.<br><br>

			We are not responsible or liable for any direct and indirect losses or damages suffered or incurred by you which were not foreseeable by us when you accessed or used the Website.<br><br>

			Please be aware that by submitting your details, you are making an offer to obtain the relevant product or service from us and our affiliates on the terms and conditions that may be accepted or rejected. The contract for the product or service will only be concluded once your offer has been accepted. You will receive confirmation when your offer has been accepted. In case your offer for any product and/or service is rejected even after acceptance for any reason, we are not responsible or liable for any losses or damages suffered or incurred by you in the process.<br><br>

			You may also be able to access user reviews directly on the Website. The views expressed therein do not represent our views or the views of our affiliates and we are not responsible or liable for the accuracy or content of any such views or expressions. We are not responsible or liable for any loss or damage you may suffer or incur in connection with your use of user reviews on the Website.<br><br>

			Please be aware that the information and descriptions of products and/or services on this Website may not represent the complete descriptions of all the features and terms and conditions of those products and/or services. You must ensure that you carefully read all the features and terms and conditions of any product and/or service before availing it.<br><br>

			If you apply for and obtain any product and/or service through this Website, it is your responsibility to ensure that you understand and agree with the prescribed terms and conditions before entering into a contract to obtain that product or service.<br><br>

			Any views, opinions, advice or assistance which is given or provided to you by a third party after you have used this Website do not represent our views, opinions, advice or assistance and are not checked, monitored, reviewed, verified or endorsed by us. We do not endorse, recommend or take responsibility for any third party who provides you with any views, opinions advice or assistance. You act or refrain from acting on any third party's views, opinions, advice or assistance at your sole risk and sole discretion and you are solely responsible for any decision to act or refrain from acting on such views, opinions, advice or assistance. We are not responsible or liable for any loss or damage you may suffer or incur in connection with such views, opinions, advice or assistance including in relation to their accuracy, truthfulness or completeness or for any acts, omissions, errors or defaults of any third party in connection with such views, opinions, advice or assistance.<br><br>

			In no event shall we and/or our affiliates be liable for any direct, indirect, punitive, incidental, special, or consequential damages arising out of, or in any way connected with, your access to, display of or use of this Website or with the delay or inability to access, display or use this Website (including, but not limited to, your reliance upon opinions appearing on this Website; any computer viruses, information, software, linked Sites, products, and services obtained through this Site; or otherwise arising out of the access to, display of or use of this Site) whether based on a theory of negligence, contract, tort, strict liability, or otherwise.
			</p><br>
			<h4><b>Responsibilities of Users</b></h4>
			<p>You must take all reasonable precautions (including using appropriate virus checking software) to ensure that any information, content, material or data you provide is free from viruses, spyware, malicious software, trojans, worms, logic bombs and anything else which may have a contaminating, harmful or destructive effect on any part of this Website or any other technology.<br><br>
			You may complete a registration process as part of your use of this Website which may include the creation of a username, password and/or other identification information. Any username, password and/or other identification information must be kept confidential by you and must not be disclosed to, or shared with, anyone. Where you do disclose to or share with anyone your username, password and/or other identification information, you are solely responsible for all activities undertaken on this Website using your username, password and/or other identification information.<br><br>
			It is your responsibility to check and ensure that all information, content, material or data you provide on this Website is correct, complete, accurate and not misleading and that you disclose all relevant facts. We do not accept any responsibility or liability for any loss or damage you may suffer or incur if any information, content, material or data you provide on this Website is not correct, complete and accurate or if it is misleading or if you fail to disclose all relevant facts.<br><br>
			You must get permission from any other person about whom you propose to provide information before you provide it. In submitting any other person's details, you are confirming to us that you have their permission to do so and that they understand how their details will be used.
			You are solely liable for any impersonation or fraud that you may commit while submitting any information on this Website and the resultant damage or injury that such impersonation or fraud may cause to us or to any third party. We, and such affected third parties shall have the rights to initiate such legal action against you as deemed fit.<br><br>
			You undertake to comply with all laws, rules and regulations in force at all times during the subsistence of these terms within the territory of India.
			You are solely responsible and liable for your conduct on this Website and for your User Content.<br>
			</p><br>

			<h4><b>Indemnity</b></h4>
			<p>You agree that you will be liable to us for any damage, loss, claim, demand, liability or expense that we may suffer or incur arising out of or in connection with your conduct on this Website and/or your User Content.<br><br>

			You agree to indemnify us for and hold us and our officers, directors, agents, subsidiaries, joint ventures, and employees harmless from any claims, causes of action, damages, losses, demands, liabilities, recoveries, fines, penalties or other costs or expenses of any kind or nature, including reasonable attorneys' fees arising out of or related to your breach of this ToU or arising out of your violation of any law or the rights of a third party, or your use/conduct on this Website.
			</p><br>

			<h4><b>Recording your Calls</b></h4>
			<p>Telephone calls that you may make to or receive from our customer service help lines may be monitored and/or recorded. This will help us to train our staff and improve our service to you. A recording will only be used under proper and careful supervision.</p>

			<h4><b>Complaints/Grievances</b></h4>
			<p>Our aim is to provide you with an excellent service at all times. If you are unhappy with our service for any reason or have complain or grievance of any kind, you can inform us about the same by sending email at customercare@Paraskrit.com<br><br>

			We will aim to resolve your complaint as soon as possible and if we are not able to do so within two working days, we will provide you with an acknowledgement. After we have had an opportunity to investigate your concerns, we will issue you with a final response.<br><br>

			If you remain dissatisfied with our response or in case of any kind of disputes, differences, controversies relating to or arising out of our products or services or acts or relating to the use of this Website all such disputes, differences and controversies shall be finally, exclusively and conclusively settled by Arbitration in accordance with the Rules of Arbitration under the provisions of Arbitration and Conciliation Act, 1996. The Arbitration shall be conducted in English Language and venue for such arbitration shall be Ahmedabad, India. The Award of Arbitral Tribunal shall be final and binding on the parties to the dispute. The cost of the Arbitration shall be borne by the respective parties.
			</p><br>

			<h4><b>Privacy</b></h4>
			<p>We are committed to protecting your privacy and we treat your privacy very seriously. We process information about you in line with our Privacy Policy. By using this Website, you agree to the way in which we process and deal with your personal information.<br><br>

			We may disclose your personal information or access your account if required to do so by law, any court, the Financial Services Authority, or any other applicable regulatory, compliance, Governmental or law enforcement agency.
			</p><br>

			<h4><b>Miscellaneous</b></h4>
			<p>This Website is only intended for use by residents of India. We make no warranty or representation that any product or service referred to on this Website and/or any service we provide is available or otherwise appropriate for use outside India. If you choose to use this Website from locations outside India, you do so at your sole risk and you are responsible for compliance with all applicable local laws.<br><br>

			If any provision of these ToU is held to be unlawful, invalid or unenforceable, that provision shall be deemed deleted from these ToU and the validity and enforceability of the remaining provisions of these ToU shall not be affected.<br><br>

			These ToU constitute the entire agreement between you and us relating to your access to and use of this Website and supersedes any prior agreements (including any previous terms of use of this Website).<br><br>

			No failure or delay by us in exercising any right under these ToU will operate as a waiver of that right nor will any single or partial exercise by us of any right preclude any further exercise of any right.
			</p><br>

			<h4><b>Governing Law</b></h4>
			<p>These ToU and your access to and use of this Website shall be governed by and interpreted in accordance with Indian laws.Each of you and us submits to the exclusive jurisdiction of the courts of Aurangabad, India in connection with these ToU and your access to and use of this Website (including any claims or disputes).</p><br>

			<h4><b>Survival Of Terms</b></h4>
			<p>Notwithstanding any other provisions of this ToU, or any general legal principles to the contrary, any provision of this ToU that imposes or contemplates continuing obligations on a party will survive the expiration or termination of this ToU.</p>


		</div>
	</section>
</div>
		