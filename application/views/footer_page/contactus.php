<div id="content" class="content-container ng-scope">
    <section style="margin-bottom:40px;" id="product-details" class="view-container animate-fade-up">
        <div class="container">
            <div class="row">
                <div style="border:solid 1px #aac6f5; padding:20px;" class="col-lg-12 form-set">            
                    <div class="col-lg-8">
                        <h5>
                            <?php if(!empty($loggedin)){ ?>
                            <form class="form-horizontal" action="<?php echo site_url('user/insert_contactus'); ?>" method="post"  id="contact_form">
                                <div class="col-lg-12" ><strong style="font-size:22px"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;Enquiry Form</strong></div>
                                <div class="col-sm-6" style="padding: 11px;">
                                    <input type="text" class="form-control" placeholder="First Name" name="fname" value="<?php echo $loggedin[0]['first_name']; ?>">
                                </div>
                                <div class="col-sm-6" style="padding: 11px;">
                                    <input type="text" class="form-control" placeholder="Last Name" name="lname" value="<?php echo $loggedin[0]['last_name']; ?>" >
                                </div>
                                <div class="col-sm-6" style="padding: 11px;">
                                    <input type="text" class="form-control" placeholder="Email Id" name="email" value="<?php echo $loggedin[0]['email']; ?>">
                                </div>
                                <div class="col-sm-6" style="padding: 11px;">
                                    <input type="text" class="form-control" placeholder="Contact Number" name="contact_num" value="<?php echo $loggedin[0]['contact_no']; ?>"> 
                                </div>
                                <div class="col-sm-12">
                                    <textarea style="height:140px; margin-top:10px;" type="text" id="form76" class="form-control" placeholder="Message" name="message"></textarea>	
                                </div>
                                <div class="col-sm-12" style="padding: 13px;">
                                    <button style="margin-left:0px;" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                            <?php }else{ ?>
                                 <form class="well form-horizontal" action="<?php echo site_url('user/insert_contactus'); ?>" method="post"  id="contact_form">
                                <div class="col-lg-12" style="padding: 13px;"><strong><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;Enquiry Form</strong></div>
                                <div class="col-sm-6" style="padding: 11px;">
                                    <input type="text" class="form-control" placeholder="First Name" name="fname" >
                                </div>
                                <div class="col-sm-6" style="padding: 11px;">
                                    <input type="text" class="form-control" placeholder="Last Name" name="lname" >
                                </div>
                                <div class="col-sm-6" style="padding: 11px;">
                                    <input type="text" class="form-control" placeholder="Email Id" name="email" >
                                </div>
                                <div class="col-sm-6" style="padding: 11px;">
                                    <input type="text" class="form-control" placeholder="Contact Number" name="contact_num" > 
                                </div>
                                <div class="col-sm-12">
                                    <textarea style="height:140px; margin-top:10px;" type="text" id="form76" class="form-control" placeholder="Message" name="message"></textarea>	
                                </div>
                                <div class="col-sm-12" style="padding: 13px;">
                                    <button style="margin-left:0px;" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                          <?php  } ?>
                        </h5> 
                    </div>                    
                    <div class="col-lg-4" style="padding-top: 96px;">
                        <h5 class="text-center">
                            <strong><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp;Contact Details</strong>
                        </h5>
                        <h5 class="text-center">Nobel Manchester Building, <br/>Ambegaon-411046, <br/>Pune, <br/>Maharashtra, India.<br/><br/><strong>support@paraskrit.com   |   Call us on 020-2461 9091</strong></h5>
                    </div>                         
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>                   
                </div><!-- End col-lg-12 -->
            </div>
        </div>
    </section>
</div>