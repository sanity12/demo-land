<div id="content" class="content-container ng-scope">
	<section class="view-container animate-fade-up" id="one">
		<div class="container wid-init">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="details_div">
						<div class="centerblock land_header_pclass"></div>
						<div class="centerblock land_header_pcls"><h3>Frequently Asked Questions</h3></div>
					</div>
				</div>
			</div>
		</div>
		<div class="container wid-init ce-form">
			<h4><b>1) Do I have to provide Bank Guarantee ?</b></h4>
			<p>Yes, the bank guarantee provides security over the transfer of credit to the retailers/vendors/distributors and also ensures the returns are met in timely manner.</p><br>
			<h4><b>2) How is the profit shared if above M.G.R met? </b></h4>
			<p>Any profit earned above the MGR is divided such that 65% of the additional profit is to be deposited for Paraskrit and 35% to the Vendor/Retailer/Distributor.</p><br>
			<h4><b>3) What is the grace period for returns?</b></h4>
			<p>There will be fine imposed per day for 15 days after the due date for returns has expired after which legal action may be taken.</p><br>
			<h4><b>4) What is the minimum/maximum amount that I can get?</b></h4>
			<p>The amount one can receive is disclosed beforehand and is specific to the type of product one is selling/trading.</p><br>
			<h4><b>5) What all documents do I need to submit?</b></h4>
			<p> Copy of the Invoice received from the dealer/merchant/tradesman with its UID number, copy of the bill receipt of the products sold to end customer.</p><br>
			<h4><b>6) How do I pay the returns? </b></h4>
			<p>The payment is to be made to the account details provided in the Agreement form.</p><br>
			<h4><b>7) Are there any hidden charges?</b></h4>
			<p>There are no hidden charges.</p><br>
			<!-- <span style="float: right;"><h4><a href="<?php echo site_url('welcome/faqtwo'); ?>">Click here for next page </a><i class="fa fa-arrow-right" aria-hidden="true"></i>
			</span></h4> -->
		</div>
	</section>
</div>
		