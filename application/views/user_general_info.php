<style type="text/css">
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
        font-weight: bold;
    }
</style>
<div class="container" id="top-banner" style="margin-bottom: 10px">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px">
        <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;USER DASHBOARD</h4>
    </div>
    <!--<h4> <i class="fa fa-sign-in" aria-hidden="true"></i>  </h4><Br><br>-->
    <!--<p></p>-->

    <ul class="nav nav-tabs" style="font-size: medium">
        <li class="active" ><a data-toggle="tab" href="#profile"><i class="fa fa-user" aria-hidden="true"></i> My Profile</a></li>
        <!--<li><a data-toggle="tab" href="#order_history"><i class="fa fa-check-square-o" aria-hidden="true"></i> Order History</a></li>-->
        <li><a data-toggle="tab" href="#account_set"><i class="fa fa-gears" aria-hidden="true"></i> Profile Setting</a></li>
        <li><a data-toggle="tab" href="#my_acc"><i class="fa fa-plus-square-o" aria-hidden="true"></i> My Account</a></li>
    </ul>
    <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">          
            <!-- Modal content-->
            <div class="modal-content modal-dialog modal-sm" style="/*! margin-right: 30px; */width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <h4 class="modal-title">Mobile Verification</h4>
                </div>
                <div class="modal-body">
                    <form class="well form-horizontal" action="<?php echo site_url('home/verifyOtpForAccount'); ?>" method="post"  id="contact_form">
                        <fieldset>
                            <center><span><h5>Sent OTP to your mobile number please check</h5></span></center>      
                            <div class="form-group">
                                <label class="col-md-4 control-label">Mobile Number</label>  
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"></span>
                                        <input  name="user_mobile" class="form-control"  type="text" value="<?php if (isset($contact)) {
    echo $contact;
} ?>" disabled="">
                                        <input  name="contact"  type="hidden" value="<?php if (isset($contact)) {
    echo $contact;
} ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" >OTP <span style="color: red">*</span></label> 
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"></span>
                                        <input name="user_otp" placeholder="OTP to verify" class="form-control"  type="text" required="">
                                    </div>
                                </div>
                            </div>
                            <table style="border:0px;float: right;">
                                <tr>
                                    <td style="text-align:center;">Min</td>
                                    <td style="text-align:center;">Sec</td>
                                </tr>
                                <tr>
                                    <td colspan="4"><span id="ms_timer"></span></td>
                                </tr>
                            </table>
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4"><br>
                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-warning" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspVERIFY<span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>     
                </div>
            </div>
            <!-- Modal content-->
        </div>          
    </div>
    <div class="tab-content">
        <div id="profile" class="tab-pane fade in active">
            <h3 style="text-transform: capitalize;"> User Profile (<?php echo $profile[0]['user_type']; ?>)</h3>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="4">
                                <h4><strong><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Basic Information</strong>
                                    <div class="col-md-12 col-sm-12"><a class="btn btn-warning" style="float:right;" href="<?php echo base_url(); ?>user/profile">Edit Profile</a>
                                </h4>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="30%" style="border: solid;border-color: #dbdbdb;"><h4><?php echo $profile[0]['first_name'] . " " . $profile[0]['last_name']; ?></h4></td>
                            <td width="20%" style="border: solid;border-color: #dbdbdb;"><h4><?php echo $profile[0]['contact_no']; ?> &nbsp;&nbsp; <?php if ($profile[0]['verified_mobile'] == 'verified') { ?><img src="<?php echo base_url(); ?>/assets/images/rightgreen.png"  style="height: 30px; width: 30px;background:#dbdbdb" title="Verified"/><?php } ?></h4></td> 
                            <td width="30%" style="border: solid;border-color: #dbdbdb;"><h4><?php echo $profile[0]['email']; ?></h4></td>   
                            <td width="50%" style="border: solid;border-color: #dbdbdb;"><h4><?php echo $profile[0]['user_name']; ?></h4></td>
                        </tr>                                                    
                    </tbody>
                </table>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="4"><h4><strong><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;Company Details</strong></h4></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="20%" style="border: solid;border-color: #dbdbdb;"><h4>Pan NO. :<?php echo $profile[0]['panno'] ?></h4></td>
                            <td width="30%" style="border: solid;border-color: #dbdbdb;"><h4> Website Link : <a href="<?php echo $profile[0]['website_link'] ?>" ><?php echo $profile[0]['website_link'] ?></a></h4></td>                                
                            <td width="20%" style="border: solid;border-color: #dbdbdb;text-transform: capitalize"><h4>User Status. :<?php echo $profile[0]['user_status'] ?></h4></td>
                            <td width="20%" style="border: solid;border-color: #dbdbdb;"><h4>Company Register : <select name="company_register" class="form-control selectpicker" disabled="">
                                        <option value="proprietorship" <?php if ($profile[0]['company_register'] == 'proprietorship') {
    echo 'selected';
} ?> >Proprietorship</option>
                                        <option value="partnership" <?php if ($profile[0]['company_register'] == 'partnership') {
    echo 'selected';
} ?> >Partnership</option>
                                        <option value="pvtltd" <?php if ($profile[0]['company_register'] == 'pvtltd') {
    echo 'selected';
} ?> >Pvt. Ltd.</option>
                                        <option value="onepercom" <?php if ($profile[0]['company_register'] == 'onepercom') {
    echo 'selected';
} ?> >One Person Company</option>
                                        <option value="llp" <?php if ($profile[0]['company_register'] == 'llp') {
    echo 'selected';
} ?> >LLP</option>
                                        <option value="lmtcom" <?php if ($profile[0]['company_register'] == 'lmtcom') {
    echo 'selected';
} ?> >Limited Company</option>
                                        <option value="not_regi" <?php if ($profile[0]['company_register'] == 'not_regi') {
                                        echo 'selected';
                                    } ?> >Not Registered</option>
                                    </select></h4></td>                                
                        </tr>                                                                                   
                    </tbody>
                </table> 

            </div>
        </div>
        <!-- //End Pofile  -->
        <div id="order_history" class="tab-pane fade">
            <h3>My Order</h3>
            <section id="featured-product">
                <div class="container" style="width: 100%">
                    <div class="panel panel-default form-container">
                        <div class="panel-body">                    
                            <script>
                                $(document).ready(function () {
                                    $('#dataTables4').dataTable({
                                        //  "order": [[2, "asc" ]]
                                    });
                                });
                            </script>
                            <table class="table table-striped table-bordered table-hover" id="dataTables4">
                                <thead>
                                    <tr>
                                        <th>Sr.No.</th>
                                        <th>Type</th>
                                        <th>Product Name</th>
                                        <th>Product Image</th>                      
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php
$index = 1;
foreach ($subformList as $row) {
    ?>

                                        <tr>
                                            <td><?php echo $index; ?></td>
                                            <td>
    <?php if (isset($row['Is_edited']) && $row['Is_edited'] == '0') { ?><a href="viewapplyform/<?php echo $row['product_id']; ?>/<?php echo $row['Is_edited']; ?>"  class="btn btn-primary">Submitted Form</a>

    <?php } else if ($row['Is_edited'] == '2') {
        ?><a href="viewapplyform/<?php echo $row['product_id']; ?>/<?php echo $row['Is_edited']; ?>" class="btn btn-success">Approved Form</a>

    <?php } else if ($row['Is_edited'] == '3') {
        ?><a href="viewapplyform/<?php echo $row['product_id']; ?>/<?php echo $row['Is_edited']; ?>" class="btn btn-danger">Disapproved Form</a>

    <?php } else {
        ?><a href="viewapplyform/<?php echo $row['product_id']; ?>/<?php echo $row['Is_edited']; ?>" class="btn btn-warning">Recommended Form</a>
    <?php } ?></td>
                                            <td><?php echo $row['pro_name']; ?></td>
                                            <td><?php $url = base_url() . 'assets/Pro_Imgupload/' . $row['photo']; ?>
                                                <img src="<?php echo $url; ?>" width="50px" height="50px">
                                            </td>   
                                            <td><?php echo $row['c_date']; ?></td>              
                                            <!-- <td>
                                            <a href="delete_product?pro_id=<?php echo $row['api_id']; ?>" onclick="return confirm('are you sure for delete this product?');" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                            </td> -->
                                        </tr>                       
    <?php $index++;
} ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- //End Order History  -->
        <div id="account_set" class="tab-pane fade">
            <h3><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Change Password</h3>
            <div style="width: 100%;height: 250px;">
                <form class="" action="<?php echo site_url('home/UpdateAccount'); ?>" method="post"  id="account_form">

                    <div style="border:solid 1px #aac6f5; margin-bottom:40px;" class="col-lg-9 form-set">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div> 
                        <!--Password-->
                        <div class="col-lg-6">
                            <div class="md-form">
                                <input id="pwd" class="form-control masked" name="pass" placeholder="Enter New Password" type="password" required="">
                            </div>
                        </div> 
                        <div class="col-lg-6">
                            <button class="btn btn-info btn-sm" type="button" id="eye">
                                <i class="fa fa-eye" aria-hidden="true" style="font-size: 21px;"></i>
                            </button>
                        </div>  
                        <!--Password-->
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>         
                        <!--Re-Password-->
                        <div class="col-lg-6">
                            <div class="md-form">
                                <input id="pwds" class="form-control masked" name="repass" placeholder="Re-Enter New Password" type="password" required="">
                            </div>
                        </div> 
                        <div class="col-lg-6">
                            <button class="btn btn-info btn-sm" type="button" id="eyes">
                                <i class="fa fa-eye" aria-hidden="true" style="font-size: 21px;"></i>
                            </button>
                        </div>  
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>  
                        <!--Re-Password-->
                        <div class="col-lg-12">               
                            <button style="margin-left:0px;" type="submit" class="btn btn-success" name="submit" value="Update"><strong>Change Password</strong></button>                
                        </div>                                
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>
                    </div>
                </form><br>
            </div>  
            <form class="" action="<?php echo site_url('home/applyFormInsertUpdateAccount'); ?>" method="post"  id="chn_mobile_form">
                <p><h3><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Update Contact Number</h3></p>
                <div style="border:solid 1px #aac6f5; margin-bottom:40px;" class="col-lg-9 form-set">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div> 
                    <!--Password-->
                    <div class="col-lg-6">
                        <div class="md-form">
                            <input id="contact_no" class="form-control masked" name="contact_no" placeholder="Enter New Mobile No" type="text">
                        </div>
                    </div> 
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>  
                    <!--Re-Password-->
                    <div class="col-lg-12">               
                        <button style="margin-left:0px;" type="submit" class="btn btn-success" name="submit" value="Update"><strong>Change Contact</strong></button>                
                    </div>                                
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>
                </div>
            </form>
        </div>
        <!-- //End Account Setting  -->
        <div id="my_acc" class="tab-pane fade">
            <h3>Bank Details</h3>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="4">
                            <h4><strong><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Account Information</strong>
                                <div class="col-md-12 col-sm-12"><a class="btn btn-warning" style="float:right;" href="<?php echo base_url(); ?>user/bank_detail_otp">Edit Bank Details</a>
                            </h4>
                        </th>
                    </tr>
                </thead>
                <tbody>
<?php if (!empty($bankinfo)) { ?> 
                        <tr>
                            <td width="30%"><h4>Account No: </h4> <?php echo $bankinfo[0]['bank_acc_no']; ?></td>
                            <td width="20%"><h4>Bank Holder Name: </h4><?php echo $bankinfo[0]['bank_holder_name']; ?></td> 
                        </tr>
                        <tr>
                            <td width="30%"><h4>IFSC Code: </h4><?php echo $bankinfo[0]['ifsc_code']; ?></td>   
                            <td width="50%"><h4>Branch Code: </h4><?php echo $bankinfo[0]['branch_code']; ?></td>
                        </tr>  
<?php } else { ?>
                        <tr><td width="30%">
                                <h4>
                                    <center>
                                        <a href="<?php echo base_url(); ?>user/bank_detail_otp" class="btn btn-warning" >PROCEED FOR BANK ACCOUNT DETAILS</a>
                                    </center>
                                </h4></td>
                        </tr>
<?php } ?>                                                  
                </tbody>
            </table>
        </div>
        <!-- //End My Account  -->
    </div>
</div>
        </div>
<!-- Password View -->    
<script>
    function show() {
        var p = document.getElementById('pwd');
        p.setAttribute('type', 'text');
    }

    function hide() {
        var p = document.getElementById('pwd');
        p.setAttribute('type', 'password');
    }

    var pwShown = 0;

    document.getElementById("eye").addEventListener("click", function () {
        if (pwShown == 0) {
            pwShown = 1;
            show();
        } else {
            pwShown = 0;
            hide();
        }
    }, false);


    function shows() {
        var ps = document.getElementById('pwds');
        ps.setAttribute('type', 'text');
    }

    function hides() {
        var ps = document.getElementById('pwds');
        ps.setAttribute('type', 'password');
    }

    var pwShowns = 0;

    document.getElementById("eyes").addEventListener("click", function () {
        if (pwShowns == 0) {
            pwShowns = 1;
            shows();
        } else {
            pwShowns = 0;
            hides();
        }
    }, false);
</script>
<!-- Password View -->     
<script type="text/javascript">
<?php if ($otpverify == '1') { ?>
        $(window).on('load', function () {
            $('#myModal').modal('show');
        });

        $(document).ready(function () {
            $("#ms_timer").countdowntimer({
                minutes: 5,
                size: "md"
            });
        });
        var ajax_call = function () {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>home/DeleteOTP",
                data: {},
                // dataType: "json",
                success: function (data) {
                    // alert(data);
                    location.reload(true);
                }
            });
            location.reload(true);
        };
        var interval = 1000 * 60 * 5; // where X is your every X minutes
        setInterval(ajax_call, interval);
<?php } ?>
</script>