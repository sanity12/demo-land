<?php //print_r($rows);    ?>
<style>
    .star-rating {
        line-height:32px;
        font-size:1.25em;
    }
    .rate .fa-star{color: #d0d04e;}
    .star-rating .fa-star{color: #d0d04e;}
    .star-rating1 .fa-star{color: #d0d04e;}
</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px">
                <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;User Applied Property List</h4>
            </div>
            <!--<h4><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp; </h4>-->           	
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>               
    </div>
</div>
<section style="margin-bottom:30px;" id="product-details">
    <div class="container" >
        <div class="row">
            <div style="border:solid 1px #aac6f5;padding: 0px !important;" class="col-lg-12 form-set">
                <div style="margin-left:20px;" class="page-header">
                    <h3><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;Private User List
                      <!--<small>Frequently Asked Questions</small>-->
                    </h3>
                </div>
                <div id="faq" class="col-md-12">
                    <div class="panel-group" id="accordion">
                        <?php
                        $j = 0;
                        foreach ($PrivateUsers as $row) { ?>
                            <div class="panel panel-default" style="margin-bottom: 53px;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapse-<?php echo $row['app_id']; ?>">
                                            
                                            
                                            <p><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;
                                                <img src="<?php echo base_url(); ?>assets/Pro_Imgupload/<?php echo get_photo($row['proper_id']); ?>" style="width:10%">
                                                &nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>
                                                <?php if ($row['is_payment'] == '1') { ?>
                                            <span class="btn btn-primary" style="text-transform: uppercase;margin-left: 416px;">View Now</span>
                                            <?php } ?> 
                                            </p>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse-<?php echo $row['app_id']; ?>" class="panel-collapse collapse">
                                    <?php
                                    $qw = $row['own_property'];
                                    $warr = explode(",", $qw);
                                    $r = $row['rating'];
                                    $rwarr = explode(",", $r);
                                    ?>
                                    <div class="panel-body" >
                                        <?php if ($row['is_payment'] == '1') { ?>
                                                <a href="javascript:void(0)" class="btn btn-primary"><span style="text-transform: uppercase;"><b><?php echo get_flname($row['user_private_id']); ?></span></a>
                                            <?php } else { ?>
                                                <a href="javascript:void(0)" class="btn btn-primary"><span style="text-transform: uppercase;"><b><?php echo 'User_' . $row['user_private_id']; ?></b></span></a>
                                            <?php } ?>
                                        <br><br>
                                        <?php
                                        for ($i = 0; $i < count($warr); $i++) {
                                            $ph = get_photo($warr[$i]);
                                            ?>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-card">
                                                <?php if ($row['is_payment'] == '1') { ?>
                                                
                                                        <a href="<?php if ($row['user_private_id'] == $row['user_public_id']) { ?><?php echo base_url(); ?>home/privateDetail/<?php echo $warr[$i]; ?><?php } else { ?><?php echo base_url(); ?>home/recomproDetail/<?php echo trim($warr[$i]); ?>/<?php echo $row['user_public_id']; ?>/<?php echo $row['proper_id']; ?><?php } ?>">
                                                            <img src="<?php echo base_url(); ?>assets/Pro_Imgupload/<?php echo $ph; ?>" style="width: 48%;">
                                                        </a>
                                                        <a class="btn btn-default" href="<?php if ($row['user_private_id'] == $row['user_public_id']) { ?><?php echo base_url(); ?>home/privateDetail/<?php echo $warr[$i]; ?><?php } else { ?><?php echo base_url(); ?>home/recomproDetail/<?php echo trim($warr[$i]); ?>/<?php echo $row['user_public_id']; ?>/<?php echo $row['proper_id']; ?><?php } ?>">
                                                        Click here to view
                                                        </a>
                                                        <h3><a target="_blank" href="<?php echo base_url(); ?>home/recomproDetail/<?php echo $warr[$i]; ?>/<?php echo $row['user_public_id']; ?>/<?php echo $row['proper_id'];?>"><?php echo html_entity_decode(get_proname($warr[$i])); ?></a></h3>
                                                <?php } else { ?>
                                                        <a href="<?php if ($row['user_private_id'] == $row['user_public_id']) { ?><?php echo base_url(); ?>home/privateDetail/<?php echo $warr[$i]; ?><?php } else { ?><?php echo base_url(); ?>home/recomproDetail/<?php echo trim($warr[$i]); ?>/<?php echo $row['user_public_id']; ?>/<?php echo $row['proper_id']; ?><?php } ?>">
                                                            <img src="<?php echo base_url(); ?>assets/Pro_Imgupload/land.jpg" width="25%" style="width: 48%;">
                                                        </a>
                                                        <a class="btn btn-default" href="<?php if ($row['user_private_id'] == $row['user_public_id']) { ?><?php echo base_url(); ?>home/privateDetail/<?php echo $warr[$i]; ?><?php } else { ?><?php echo base_url(); ?>home/recomproDetail/<?php echo trim($warr[$i]); ?>/<?php echo $row['user_public_id']; ?>/<?php echo $row['proper_id']; ?><?php } ?>">
                                                        Click here to view
                                                        </a>
                                                <?php } ?>                                                
                                            </div>                                                
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php if ($row['is_payment'] == '0') { ?>
                                <div class="card-btn text-center">
                                    <a class="btn btn-primary col-md-4" href="<?php echo base_url(); ?>user/payment/" style="margin-left: 41px;float: right;">PAY NOW</a>
                                </div>  
                            <?php } ?>
                            </div>                            
                        <?php } ?>   
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div><!-- End col-lg-12 -->
</div>
</section>
<section style="margin-bottom:30px;" id="product-details">
    <div class="container" >
        <div class="row">
            <div style="border:solid 1px #aac6f5;padding: 0px !important;" class="col-lg-12 form-set">
                <div style="margin-left:20px;" class="page-header">
                    <h3><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;Public's Applied List
                      <!--<small>Frequently Asked Questions</small>-->
                    </h3>
                </div>
                <div id="faq" class="col-md-12">
                    <div class="panel-group" id="accordion">
                        <?php
                        $j = 0;
                        foreach ($PublicUsers as $row) { ?>
                            <div class="panel panel-default" style="margin-bottom: 53px;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapse-<?php echo $row['app_id']; ?>">
                                            <p><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;
                                                <img src="<?php echo base_url(); ?>assets/Pro_Imgupload/<?php echo get_photo($row['proper_id']); ?>" style="width:10%">
                                                &nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>
                                                <?php if ($row['is_payment'] == '1') { ?>
                                            <span class="btn btn-primary" style="text-transform: uppercase;margin-left: 416px;">View Now</span>
                                            <?php } ?> 
                                            </p>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse-<?php echo $row['app_id']; ?>" class="panel-collapse collapse">
                                    <?php
                                    $qw = $row['own_property'];
                                    $warr = explode(",", $qw);
                                    $r = $row['rating'];
                                    $rwarr = explode(",", $r);
                                    ?>
                                    <div class="panel-body" >
                                        <?php if ($row['is_payment'] == '1') { ?>
                                                <a href="javascript:void(0)" class="btn btn-primary"><span style="text-transform: uppercase;"><b><?php echo get_flname($row['user_private_id']); ?></span></a>
                                            <?php } else { ?>
                                                <a href="javascript:void(0)" class="btn btn-primary"><span style="text-transform: uppercase;"><b><?php echo 'User_' . $row['user_private_id']; ?></b></span></a>
                                            <?php } ?>
                                        <br><br>
                                        <?php
                                        for ($i = 0; $i < count($warr); $i++) {
                                            $ph = get_photo($warr[$i]);
                                            ?>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-card">
                                                <?php if ($row['is_payment'] == '1') { ?>
                                                
                                                        <a href="<?php if ($row['user_private_id'] == $row['user_public_id']) { ?><?php echo base_url(); ?>home/privateDetail/<?php echo $warr[$i]; ?><?php } else { ?><?php echo base_url(); ?>home/recomproDetail/<?php echo trim($warr[$i]); ?>/<?php echo $row['user_public_id']; ?>/<?php echo $row['proper_id']; ?><?php } ?>">
                                                            <img src="<?php echo base_url(); ?>assets/Pro_Imgupload/<?php echo $ph; ?>" style="width: 48%;">
                                                        </a>
                                                        <a class="btn btn-default" href="<?php if ($row['user_private_id'] == $row['user_public_id']) { ?><?php echo base_url(); ?>home/privateDetail/<?php echo $warr[$i]; ?><?php } else { ?><?php echo base_url(); ?>home/recomproDetail/<?php echo trim($warr[$i]); ?>/<?php echo $row['user_public_id']; ?>/<?php echo $row['proper_id']; ?><?php } ?>">
                                                        Click here to view
                                                        </a>
                                                        <h3><a target="_blank" href="<?php echo base_url(); ?>home/recomproDetail/<?php echo $warr[$i]; ?>/<?php echo $row['user_public_id']; ?>/<?php echo $row['proper_id'];?>"><?php echo html_entity_decode(get_proname($warr[$i])); ?></a></h3>
                                                <?php } else { ?>
                                                        <a href="<?php if ($row['user_private_id'] == $row['user_public_id']) { ?><?php echo base_url(); ?>home/privateDetail/<?php echo $warr[$i]; ?><?php } else { ?><?php echo base_url(); ?>home/recomproDetail/<?php echo trim($warr[$i]); ?>/<?php echo $row['user_public_id']; ?>/<?php echo $row['proper_id']; ?><?php } ?>">
                                                            <img src="<?php echo base_url(); ?>assets/Pro_Imgupload/land.jpg" width="25%" style="width: 48%;">
                                                        </a>
                                                        <a class="btn btn-default" href="<?php if ($row['user_private_id'] == $row['user_public_id']) { ?><?php echo base_url(); ?>home/privateDetail/<?php echo $warr[$i]; ?><?php } else { ?><?php echo base_url(); ?>home/recomproDetail/<?php echo trim($warr[$i]); ?>/<?php echo $row['user_public_id']; ?>/<?php echo $row['proper_id']; ?><?php } ?>">
                                                        Click here to view
                                                        </a>
                                                <?php } ?>                                                
                                            </div>                                                
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php if ($row['is_payment'] == '0') { ?>
                                <div class="card-btn text-center">
                                    <a class="btn btn-primary col-md-4" href="<?php echo base_url(); ?>user/payment/" style="margin-left: 41px;float: right;">PAY NOW</a>
                                </div>  
                            <?php } ?>
                            </div>                            
                        <?php } ?>   
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div><!-- End col-lg-12 -->
</div>
</section>
