<style>

            #note_div{
    display: inline-block;
     padding-top:5%;
     padding-left: 5%;
     border-radius: 3px;

 }
 #note_div > p{
     font-size: small;
 }

 .clockdiv{
     color: #fff;
     display: inline-block;
     font-weight: 200;
     text-align: center;
     font-size: 26px;
 }

 .clockdiv > div{
     padding: 0px;
     border-radius: 3px;
     background: #d23b00;
     display: inline-block;
 }

.clockdiv div > span{
     padding: 6px;
     border-radius: 3px;
     background: #9c2c00;
     display: inline-block;
     font-weight:200;
 }
 .smalltext {
     font-size: 10px;
 }
</style>

<section id="top-box">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px">
                    <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;My Account</h4>
                </div>
                <!--<h4><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp; </h4>-->           	
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>               
        </div>
    </div>
</section>
<section id="middle">
    <div class="container">
        <div class="row">
            <div style="width: 100%" class="col-lg-9 form-set">
                <!--<div class="col-lg-12"><h2><strong><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;My Account</strong></h2></div>-->
                <div class="col-lg-12"></div>
                <?php if (isset($ApplyProRes)) {  ?>
                    <div class="col-lg-12">
                        <table id="cart" class="table table-hover table-condensed">
                            <thead>
                                <tr class="bordered-box">
<!--                                    <th style="width:30%"><h4><strong>Property Details</strong></h4></th>
                                    <th style="width:20%"><h4><strong>applied form</strong></h4></th>
                                    <th style="width:10%"><h4><strong>applied Username</strong></h4></th> 
                                    <th style="width:10%"><h4><strong>Price</strong></h4></th>
                                    <th style="width:20%"><h4><strong>Payment</strong></h4></th> 
                                    <th style="width:22%"><h4><strong>Last Date&Time</strong></h4></th> 
                                    <th style="width:20%" class="text-center" id="action"><h4><strong>Action</strong></h4></th>-->
                                    
                                    <th width="8%">Sr.No.</th>
                                   
                                    <th width="10%">main Form</th>
                                    <th width="12%">Private User Name</th>
                                    <th width="8%">Price</th>
                                    <th style="width:18%"><h4><strong>Payment</strong></h4></th> 
                                    <th width="10%" style="width:10%">applied form</th>
                                    <th width="10%">Date</th>
                                    <th width="25%" >LastDate</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                foreach ($ApplyProRes as $row) {
//                                for ($i = 0; $i < count(); $i++) {
                                    //$total_final += $row['pro_price'] * 1;
                                    ?> 
                                    <tr class="bordered-box">
                                       <td><?php echo $row['app_id']; ?></td>
                                        
                                       
                                        <td><?php $p = get_photo($row['proper_id']); ?>
                                            <a href="<?php echo base_url(); ?>home/proDetail/<?php echo $row['proper_id']; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/Pro_Imgupload/<?php echo $p; ?>" width="40%" style="width: 100%;"></a></td>
                                        <td><?php echo get_username($row['user_private_id']); ?></td>
                                        <td><?php echo '$100'; ?></td>
                                        <?php if ($row['is_payment'] == '1') { ?>
                                            <td data-th="Price"><h4><strong>&nbsp;<span class="btn btn-warning">PAID</span></strong></h4></td>
                                        <?php } else {
                                                $CurrDate = date('Y-m-d H:i:s');
                                                $date1=date_create($row['updated_date']);
                                                $date2=date_create($CurrDate);
                                                $diff=date_diff($date1,$date2);
                                                $DIF = $diff->format("%R%a");
                                                ?>
                                            <td data-th="Price"><h4><strong>&nbsp;
                                                        <?php if($DIF >= '0'){ ?>
                                                        <a class="btn btn-danger col-md-7" href="<?php echo base_url('home/paynow'); ?> ">EXPIRED</a>
                                                            <?php  }else{ ?><a class="btn btn-primary col-md-7" href="javascript:void(0)">PAY NOW</a>
                                                            <?php } ?>
                                                    </strong></h4></td>
                                        <?php } ?>
                                        <td><?php
                                            $qw = $row['own_property'];
                                            $warr = explode(",", $qw);
                                            for ($i = 0; $i < count($warr); $i++) {
                                                $ph = get_photo($warr[$i]);
                                                ?>
                                                <a href="<?php echo base_url(); ?>home/privateDetail/<?php echo $warr[$i]; ?>" target="_blank">
                                                    <img src="<?php echo base_url(); ?>assets/Pro_Imgupload/<?php echo $ph; ?>" width="25%" style="width: 100%;">
                                                </a>
                                            <?php }?>
                                        </td>
                                        <td><?php echo date('d M, Y', strtotime($row['created_date'])); ?></td>
                                        <?php if ($row['is_payment'] == '0') { ?>
                                        <td class="cntdwn" style="background:#c0392b;color:white;border: solid;">
                                            <?php
                                            date_default_timezone_set('Asia/Kolkata');
                                            $CurrDate = date('Y-m-d H:i:s');

                                            $date = new DateTime($row['updated_date']);
                                            $sdate1 = $date->format('M d, Y H:i:s');

                                            $date = new DateTime($row['created_date']);
                                            $credate1 = $date->format('M d, Y H:i:s');

                                            ?>
                                            <div class="col-lg-12">
                                                <h4><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;<strong>Expiry Timer</strong></h4>
                                                <div id="clockdiv<?php echo $row['app_id']; ?>" style="float: left" class="clockdiv">
                                                    <div>
                                                        <span class="days"></span>
                                                        <div class="smalltext">Days</div>
                                                    </div>
                                                    <div>
                                                        <span class="hours"></span>
                                                        <div class="smalltext">Hours</div>
                                                    </div>
                                                    <div>
                                                        <span class="minutes"></span>
                                                        <div class="smalltext">Minutes</div>
                                                    </div>
                                                    <div>
                                                        <span class="seconds"></span>
                                                        <div class="smalltext">Seconds</div>
                                                    </div>
                                                </div>
                                                <div id="note_div" style="float: left">
                                                    <!--<p>Amount will be locked after 5 days from investment.</p>-->
                                                </div>
                                            </div>
                                        </td>
 
                                <script type="text/javascript">                      
                                $(document).ready(function () {
                                    function getTimeRemaining(endtime) {
                                        var t = Date.parse(endtime) - Date.parse(new Date());
                                        //var t = 435600000;
                                        var days = Math.floor(t / (1000 * 60 * 60 * 24));
                                        var hours = Math.floor((t % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                        var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
                                        var seconds = Math.floor((t % (1000 * 60)) / 1000);

                                        return {
                                            'total': t,
                                            'days': days,
                                            'hours': hours,
                                            'minutes': minutes,
                                            'seconds': seconds
                                        };
                                    }

                                    function initializeClock(id, endtime) {

                                        var clock = document.getElementById(id);
                                        var daysSpan = clock.querySelector('.days');
                                        var hoursSpan = clock.querySelector('.hours');
                                        var minutesSpan = clock.querySelector('.minutes');
                                        var secondsSpan = clock.querySelector('.seconds');

                                        function updateClock() {
                                            var t = getTimeRemaining(endtime);
                                            daysSpan.innerHTML = t.days;
                                            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                                            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                                            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
                                          //  console.log(t.total);
                                            // if(){
                                            //     location.reload();

                                            // }
                                            if (t.total <= 0) {
                                                location.reload();
                                                clearInterval(timeinterval);
                                            }
                                        }

                                        // updateClock();
                                        var timeinterval = setInterval(updateClock, 1000);

                                    }


                                // var deadline = new Date(Date.parse(new Date()) + <?php //echo $a;    ?> * <?php //echo $b;    ?> * <?php //echo $c;    ?> * <?php //echo $d;    ?> * 1000);
                                // var countDownDate = new Date(value).getTime();

                                    var deadline = "<?php echo $sdate1; ?>";

                                    var credate1 = "<?php echo $credate1; ?>";
                                    var tm = Date.parse(deadline) - Date.parse(new Date());
                                    var initime = Date.parse(deadline) - Date.parse(credate1);
                                //   console.log(initime);
                                    //console.log(initime / 4);

                                  //  console.log(tm);
                                // console.log((tm / 4))
                                    if (tm > 0 && tm < (initime / 4)) {

                                        var product_id = 1;
                                        // alert(product_id);
                                        $.ajax({
                                            type: 'POST',
                                            url: 'date-fun.php',
                                            data: {product_id: product_id, pro_stock: 'Almost Closing'},
                                            success: function (response) {//response is value returned from php (for your example it's "bye bye"
                                             //   console.log(response);
                                            }
                                        });
                                        initializeClock('clockdiv<?php echo $row['app_id']; ?>', deadline);
                                    } else if (tm >= 0) {

                                        initializeClock('clockdiv<?php echo $row['app_id']; ?>', deadline);


                                    } else {
                                        var clock = document.getElementById('clockdiv<?php echo $row['app_id']; ?>');
                                        var daysSpan = clock.querySelector('.days');
                                        var hoursSpan = clock.querySelector('.hours');
                                        var minutesSpan = clock.querySelector('.minutes');
                                        var secondsSpan = clock.querySelector('.seconds');
                                        daysSpan.innerHTML = '00';
                                        hoursSpan.innerHTML = '00';
                                        minutesSpan.innerHTML = '00';
                                        secondsSpan.innerHTML = '00';

                                        var product_id = 1;
                                        $.ajax({
                                            type: 'POST',
                                            url: 'date-fun.php',
                                            data: {product_id: product_id, pro_stock: 'Closed'},
                                            success: function (response) {//response is value returned from php (for your example it's "bye bye"
                                                //alert(response);
                                            }
                                        });
                                        //  echo update_stock($ProRes[0]['pro_id']);
                                    }
                                });

                                </script> 
                                        <?php } ?>
                                        
                                    </tr>
                                <?php
                                //  $index++;
                            }
                            ?>
                            <tr class="bordered-box">
                                <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                <td rowspan="8" style="float: right"><a class="btn btn-success" href="<?php echo site_url('postproperty/myCheckOut'); ?>">CHECKOUT &nbsp;&nbsp;<i class="fa fa-angle-right"></i></a></td>
                            </tr>
                        </table>
                    </div>                                
                <?php } ?>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>
            </div><!--End col-lg-9 form-set-->                                                                                               
            <!-- </form> -->

        </div>
    </div>
</section>
<script type="text/javascript" src="http://rendro.github.io/countdown/javascripts/jquery.countdown.js"></script>
                                           <script type="text/javascript">
    $(document).ready(function () {

        // check all checkboxes in table
        if ($('.checkall').length > 0) {
            $('.checkall').click(function () {
                var parentTable = $(this).parents('table');
                var ch = parentTable.find('.checkbox1');
                if ($(this).is(':checked')) {

                    //check all rows in table
                    ch.each(function () {
                        $(this).attr('checked', true);
                        $(this).parent().addClass('checked');	//used for the custom checkbox style
                        $(this).parents('tr').addClass('selected'); // to highlight row as selected
                    });

                } else {

                    //uncheck all rows in table
                    ch.each(function () {
                        $(this).attr('checked', false);
                        $(this).parent().removeClass('checked');	//used for the custom checkbox style
                        $(this).parents('tr').removeClass('selected');
                    });

                }
            });
        }
        
    });
 </script>