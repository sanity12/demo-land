
<!--<link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/croppie.css">-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/croppie.css">

<section id="top-box">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px">
                    <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Register</h4>
                </div>
                <!--<h4><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp; </h4>-->           	
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>               
        </div>
    </div>
</section>
<section style="margin-bottom:40px;" id="product-details">
    <div class="container">
        <div class="row">



            <div style="border:solid 1px #aac6f5; padding:20px;" class="col-lg-12 form-set">

                <!-- --------- ------------------ -->

                <!--            <div class="form-group">-->
<!--                <div class="panel panel-default">-->
<!--                    <div class="panel-heading"> Profile Image Upload</div>-->
<!--                    <div class="panel-body">-->
                <div  class="well" style="border: 1px solid #e3e3e3;">
                        <div class="row" style="padding:19px">
                            <div class="col-md-4">
                                <div class="form-group" style="text-align: right;padding-top: 5px;">
                                    <label class="control-label"> Profile Image</label>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div id="upload-demo" style="width:200px"></div>
                            </div>
                            <div class="col-md-2" style="padding-top:10px;">
                                <strong>Select Image:</strong>
                                <br/>
                                <input type="file" id="upload">
                                <br/>
                                <button class="btn btn-success upload-result">Upload Image</button>
                            </div>
                            <div class="col-md-2" style="">
                                <div id="upload-demo-i" style="background:#e1e1e1;width:150px;padding:5px;height:150px;margin-top:10px"></div>
                            </div>
                        </div>
<!--                    </div>-->
<!--                </div>-->

                <!--   </div>-->
                <!---------------------------------- -->

                <form class="form-horizontal" action="<?php echo site_url('user/registration'); ?>" method="post"  id="user_form" name="user_form">
                    <input type="hidden" name="uploded_profile_pic" id="uploded_profile_pic" />

                    <div class="form-group">
                        <label class="col-md-4 control-label">User Type</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-bookmark"></i></span>
                                <input  name="user_type" value="<?php echo $user_type;?>" placeholder="" class="form-control"  type="text" style="WIDTH: 98%;" readonly=" disable">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">User Status</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-adjust"></i></span>
                                <select name="user_status" class="form-control selectpicker">
                                    <option value="">Select User Status..</option>
                                    <option value="dealer ">Dealer</option>
                                    <option value="owner">Owner</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">First Name</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input  name="first_name" placeholder="First Name(as per PAN Record)" class="form-control"  type="text" style="WIDTH: 98%;">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" >Last Name</label> 
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input name="last_name" placeholder="Last Name(as per PAN Record)" class="form-control"  type="text" style="WIDTH: 98%;">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">E-Mail</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input name="email" placeholder="E-Mail Address" class="form-control"  type="text" style="WIDTH: 98%;">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Contact No.</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                <input name="contact_no" placeholder="Mobile Number" class="form-control" type="Number" style="WIDTH: 98%;">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Website Link</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-link"></i></span>
                                <input name="website_link" placeholder="Website Link" class="form-control"  type="text" style="WIDTH: 98%;">
                            </div>
                        </div>
                    </div>

                    <div class="form-group"> 
                        <label class="col-md-4 control-label">Company Registered As</label>
                        <div class="col-md-8 selectContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                <select name="company_register" class="form-control selectpicker">
                                    <option value="">Select Company Registered As..</option>
                                    <option value="proprietorship">Proprietorship</option>
                                    <option value="partnership">Partnership</option>
                                    <option value="pvtltd">Pvt. Ltd.</option>
                                    <option value="onepercom">One Person Company</option>
                                    <option value="llp">LLP</option>
                                    <option value="lmtcom">Limited Company</option>
                                    <option value="not_regi">N/A</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">User Name</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input  name="user_name" placeholder="Username" class="form-control"  type="text" id="user_name" style="WIDTH: 98%;">
                                <div class="error error1"></div>
                                <!-- <div class="error"></label>	 -->
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" >Password</label> 
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-log-in"></i></span>
                                <input name="user_password" placeholder="Password" class="form-control"  type="password" id="user_password" style="WIDTH: 98%;">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" >Confirm Password</label> 
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-log-in"></i></span>
                                <input name="confirm_password" placeholder="Confirm Password" class="form-control"  type="password" style="WIDTH: 98%;">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">PAN Number</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                                <input  name="panno" id="panno" placeholder="PAN Number" class="form-control"  type="text" id="panno" style="WIDTH: 98%;">
                                <div class="error error1"></div>
                                <!-- <div class="error"></label>	 -->
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Additional Description</label>
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                                <input  name="additional_description" id="additional_description" placeholder="Additional Description" class="form-control"  type="text" style="WIDTH: 98%;">
                                <div class="error error1 error2"></div>

                                <!-- <div class="error"></label>	 -->
                            </div>
                        </div>
                    </div>




                    <div class="form-group sub">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-4"><br>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                            <button type="submit" name="submit" class="btn btn-warning submit" value="submit" onclick="">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                        </div>
<!--                    </div>
                    <div class="form-group sub">-->
                    <?php if($user_type == 'undetailed'){?>
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-4"><br>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                            <button type="submit" name="submit" class="btn btn-warning submit" value="accountdetails" style="margin-top: -13px;">
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspAccount Details 
                                <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                        </div>
                    <?php } ?>
                    </div>
                </form>
                </div> <!-- border div -->
            </div><!-- /.container -->
        </div><!-- /.container -->
    </div>	
</section>
<script type="text/javascript">
    $('#user_name').change(function () {
        $('.error1').html('');
        var user_name = $('input[name="user_name"]').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>user/user_name_exists",
            data: {user_name: user_name},
            success: function (data) {
                if (data == 'exist') {
                    $('.error1').html('Sorry, This username is already used by another user please select another one.');
                    $(".sub").hide();
                } else {
                    $(".sub").show();
                }
            }
        });
    });

    $('#additional_description').change(function () {
        $('.error2').html('');
        var profile_image=$('#uploded_profile_pic').val();
        var additional_description=$('#additional_description').val();
       // var ad_length=additional_description.length;
        var ad_length = additional_description.replace(/\s/g, "").length;
        if(ad_length>100)
        {
            $('.error2').html('Please Enter Only 100 Characters');
        }
        else if(profile_image=="")
        {
            $('.error2').html('Please Upload Profile Image');

        }

    });

</script>
<script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script>
<script type="text/javascript">
    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 100,
            height: 100,
            type: 'circle'
        },
       boundary: {
          width: 150,
          height: 150
       }
    });

    $('#upload').on('change', function () {
      var reader = new FileReader();
       reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
          }).then(function(){
              console.log('jQuery bind complete');
         });

      }
       reader.readAsDataURL(this.files[0]);
    });

   $('.upload-result').on('click', function (ev) {
       $uploadCrop.croppie('result', {
            type: 'canvas',
           size: 'viewport'
       }).then(function (resp) {

           $.ajax({
               url: "<?php echo base_url(); ?>User/upload_pic",
               type: "POST",
              data: {"image":resp},
             success: function (data) {
                 // alert(data);
                 $('#uploded_profile_pic').val(data);
                    html = '<img src="' + resp + '" />';
                    $("#upload-demo-i").html(html);
                }
            });
        });
   });

</script>



