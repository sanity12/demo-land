<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">		    
        <!-- Modal content-->
        <div class="modal-content modal-dialog">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">SELECT WHICH TYPE OF USER YOU ARE ! </h4>
            </div>
            <div class="modal-body">
                <form class="well form-horizontal" action="<?php echo site_url('user/register'); ?>" method="post"  id="msg_form" enctype="multipart/form-data">
                    <fieldset>
                        <center><h2>SELECT USER TYPE</h2></center>
                        <div class="form-group">
                            <label class="col-md-4 control-label" ></label> 
                            <div class="col-md-8 inputGroupContainer">
                                <div class="input-group col-md-8">
                                    <div  id="private_icon" class="col-md-1" style="display:none">
                                        <i class="fa fa-user-secret" aria-hidden="true" style="font-size: 20px !important;color: black;padding-top: 10px;"></i>
                                    </div>
                                    <div  id="public_icon" class="col-md-1" style="display:none">
                                        <i class="fa fa-users" aria-hidden="true" style="font-size: 20px !important;color: black;padding-top: 10px;"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <select name="user_type" class="form-control selectpicker" onchange="dispaly_public_private_icon(this.value);">
                                                <option value="">select user type</option>
                                                <option value="public">Public</option>
                                                <option value="private">Private</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-4"><br>
                                <button type="submit" class="btn btn-warning" >SUBMIT <span class="glyphicon glyphicon-send"></span></button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
        <!-- Modal content-->
    </div>		    
</div>
<script>
    $(window).on('load', function () {
        $('#myModal').modal('show');
    });

    function dispaly_public_private_icon(value)
    {
        if(value=="public")
        {
            $('#public_icon').css('display','block');
            $('#private_icon').css('display','none');
        }
        else
        {
            $('#public_icon').css('display','none');
            $('#private_icon').css('display','block');
        }
    }
</script>
