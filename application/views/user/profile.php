<?php //print_r($user_res);  ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/croppie.css">

<div id="content" class="content-container ng-scope">
    <section class="view-container animate-fade-up">
        <div class="container wid-init">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px">
                        <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Edit Your Details </h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="container wid-init ce-form well">
            <!-- --------------------------------------  -->
            <div class="row" style="padding:19px">
                <div class="col-md-4">
                    <div class="form-group" style="text-align: right;padding-top: 5px;">
                        <label class="control-label"> Profile Image</label>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div id="upload-demo" style="width:200px"></div>
                </div>
                <div class="col-md-2" style="padding-top:10px;">
                    <strong>Select Image:</strong>
                    <br/>
                    <input type="file" id="upload">
                    <br/>
                    <button class="btn btn-success upload-result">Upload Image</button>
                </div>
                <div class="col-md-2" style="">
                    <div id="upload-demo-i" style="background:#e1e1e1;width:150px;padding:5px;height:150px;margin-top:10px"></div>
                </div>
            </div>
            <!-- --------------------------------------- -->

            <form class="form-horizontal" action="<?php echo site_url('user/editprofile'); ?>" method="post"  id="user_form">
                <input type="hidden" name="uploded_profile_pic" id="uploded_profile_pic" />
                <fieldset>
                    <input type="hidden" name="uid" value="<?php echo $user_res[0]['user_id']; ?>">
                    <div class="form-group">
                        <label class="col-md-4 control-label">User Type</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-bookmark"></i></span>
                                <input  name="user_type"  class="form-control md-input"  type="text" value="<?php echo $user_res[0]['user_type']; ?>" readonly="" disabled="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">User Status</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-adjust"></i></span>
                                <input  name="user_status" class="form-control md-input"  type="text" value="<?php echo $user_res[0]['user_status']; ?>" readonly="" disabled="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">First Name</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input  name="first_name" placeholder="First Name(as per PAN Record)" class="form-control md-input"  type="text" value="<?php echo $user_res[0]['first_name']; ?>">
                            </div>
                        </div>
                    </div>

                    <!-- Text input-->

                    <div class="form-group">
                        <label class="col-md-4 control-label" >Last Name</label> 
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input name="last_name" placeholder="Last Name(as per PAN Record)" class="form-control"  type="text" value="<?php echo $user_res[0]['last_name']; ?>">
                            </div>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label">E-Mail</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input name="email" placeholder="E-Mail Address" class="form-control" type="text" value="<?php echo $user_res[0]['email']; ?>">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Website Link</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-link"></i></span>
                                <input name="website_link" placeholder="Website Link" class="form-control"  type="text" value="<?php echo $user_res[0]['website_link']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <label class="col-md-4 control-label">Company Registered As</label>
                        <div class="col-md-8 selectContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                <select name="company_register" class="form-control selectpicker">
                                    <option value="">Select Company Registered As..</option>
                                    <option value="proprietorship" <?php if ($user_res[0]['company_register'] == 'proprietorship') {
                                        echo 'selected';
                                    } ?> >Proprietorship</option>
                                    <option value="partnership" <?php if ($user_res[0]['company_register'] == 'partnership') {
                                        echo 'selected';
                                    } ?> >Partnership</option>
                                    <option value="pvtltd" <?php if ($user_res[0]['company_register'] == 'pvtltd') {
                                        echo 'selected';
                                    } ?> >Pvt. Ltd.</option>
                                    <option value="onepercom" <?php if ($user_res[0]['company_register'] == 'onepercom') {
                                        echo 'selected';
                                    } ?> >One Person Company</option>
                                    <option value="llp" <?php if ($user_res[0]['company_register'] == 'llp') {
                                        echo 'selected';
                                    } ?> >LLP</option>
                                    <option value="lmtcom" <?php if ($user_res[0]['company_register'] == 'lmtcom') {
                                        echo 'selected';
                                    } ?> >Limited Company</option>
                                    <option value="not_regi" <?php if ($user_res[0]['company_register'] == 'not_regi') {
                                            echo 'selected';
                                        } ?> >Not Registered</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <!-- Text input-->

                    <div class="form-group">
                        <label class="col-md-4 control-label">Username</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input  name="user_name" placeholder="Username" class="form-control"  type="text" value="<?php echo $user_res[0]['user_name']; ?>" readonly disable="" >
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">PAN Number</label>  
                        <div class="col-md-8 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                                <input  name="panno" placeholder="PAN Number" class="form-control"  type="text" id="panno" value="<?php echo $user_res[0]['panno']; ?>">
                                <div class="error error1"></div>
                                <!-- <div class="error"></label>	 -->
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-4"><br>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-warning" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                        </div>
                    </div>
                </fieldset>
            </form>

        </div><!-- /.container -->
</div>							
<!-- </md-card>end ngIf: eligibleStatus==='NONE' -->
<div class="row">
    <div class="col-md-12">
        <!-- ngIf: eligibleStatus==='ELIGIBLE' -->

        <!-- ngIf: eligibleStatus==='INELIGIBLE' -->
    </div>

</div>
</div>
</section>
</div>
<script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script>
<script type="text/javascript">
    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 100,
            height: 100,
            type: 'circle'
        },
        boundary: {
            width: 150,
            height: 150
        }
    });

    $('#upload').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });

        }
        reader.readAsDataURL(this.files[0]);
    });

    $('.upload-result').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {

            $.ajax({
                url: "<?php echo base_url(); ?>User/upload_pic",
                type: "POST",
                data: {"image":resp},
                success: function (data) {
                    // alert(data);
                    $('#uploded_profile_pic').val(data);
                    html = '<img src="' + resp + '" />';
                    $("#upload-demo-i").html(html);
                }
            });
        });
    });

</script>

