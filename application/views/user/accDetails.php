<section id="top-box">
    <div class="container">
        <div class="row">
            <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px">
                <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Bank Account Details</h4>
            </div>      
        </div>
    </div>
</section>
<section style="margin-bottom:40px;" id="product-details">
    <div class="container">
        <div class="row">
            <div style="border:solid 1px #aac6f5; padding:20px;" class="col-lg-12 form-set">
                    <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">		    
                            <!-- Modal content-->
                            <div class="modal-content modal-dialog modal-sm" style="/*! margin-right: 30px; */width: 100%;">
                                <div class="modal-header">
                                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                                    <h4 class="modal-title">Mobile Verification</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="well form-horizontal" action="<?php echo site_url('user/verifyOtp'); ?>" method="post"  id="contact_form">
                                        <fieldset>
                                            <center><span><h5>Sent OTP to your mobile number please check</h5></span></center>		
                                            <div class="form-group">
                                                <input  name="ism" class="form-control"  type="hidden" value="<?php echo $this->router->fetch_class(); ?>" placeholder="" >
                                                <input  name="user_id" class="form-control"  type="hidden" value="<?php echo $user_res[0]['user_id']; ?>" placeholder="" >
                                                <label class="col-md-4 control-label">Mobile Number</label>  
                                                <div class="col-md-8 inputGroupContainer">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"></span>
                                                        <input  name="user_mobile" class="form-control"  type="text" value="<?php echo $user_res[0]['contact_no']; ?>" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >OTP <span style="color: red">*</span></label> 
                                                <div class="col-md-8 inputGroupContainer">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"></span>
                                                        <input name="user_otp" placeholder="OTP to verify" class="form-control"  type="text" required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <table style="border:0px;float: right;" id="timer1">
                                                <tr>
                                                    <td style="text-align:center;">Min</td>
                                                    <td style="text-align:center;">Sec</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4"><span id="ms_timer"></span></td>
                                                </tr>
                                            </table>

                                            <div class="form-group" id="resend">
                                                <label class="col-md-4 control-label"></label>
                                                <div class="col-md-4"><br>
                                                    <button type="button" class="btn btn-warning" id="resendbtn">RESEND<span class="glyphicon glyphicon-send"></span></button>
                                                </div>
                                            </div>

                                            <!-- Button -->
                                            <div class="form-group" id="submit">
                                                <label class="col-md-4 control-label"></label>
                                                <div class="col-md-4"><br>
                                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-warning" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspVERIFY<span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>		
                                </div>
                            </div>
                            <!-- Modal content-->
                        </div>		    
                    </div>
                    <?php if (!empty($query)) { ?>
                        <div id="bankinfo">
                            <form class="well form-horizontal" action="<?php echo site_url('user/updateBankinfo'); ?>" method="post"  id="bankinfo_form">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color: black;">Bank Account Number <span style="color: red">*</span></label>  
                                        <div class="col-md-8 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"></span>
                                                <input  name="bank_acc_no" class="form-control"  type="text" value="<?php echo $query[0]['bank_acc_no'] ?>" placeholder="Bank Account Number" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color: black;">Bank Holder Name <span style="color: red">*</span></label>  
                                        <div class="col-md-8 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"></span>
                                                <input  name="bank_holder_name" class="form-control"  type="text" value="<?php echo $query[0]['bank_holder_name'] ?>" placeholder="Bank Holder Name" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color: black;">IFSC Code <span style="color: red">*</span></label>  
                                        <div class="col-md-8 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"></span>
                                                <input  name="ifsc_code" class="form-control"  type="text" value="<?php echo $query[0]['ifsc_code'] ?>" placeholder="IFSC Code" required=""> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color: black;">BRANCH Code <span style="color: red">*</span></label> 
                                        <div class="col-md-8 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"></span>
                                                <input name="branch_code" placeholder="BRANCH Code " class="form-control"  type="text" required="" value="<?php echo $query[0]['branch_code'] ?>">
                                            </div>
                                        </div>
                                    </div>						
                                    <!-- Button -->
                                    <div class="form-group" >
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-4"><br>
                                        <?php if(isset($edit_id)){?>
                                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-warning" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspUPDATE<span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                                        <?php }else { ?>
                                           <div class="col-md-12 col-sm-12">
                                               <a class="btn btn-warning" href="<?php echo base_url(); ?>user/bank_detail_otp">Click Here For Edit Bank Details</a>
                                           </div>
                                        <?php } ?>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>	
                        </div>	
                    <?php } else { ?>
                        <div id="bankinfo">
                            <form class="well form-horizontal" action="<?php echo site_url('user/inserBankinfo'); ?>" method="post"  id="bankinfo_form">
                                <fieldset>
                                    <!--<input  name="user_id" class="form-control"  type="hidden" value="<?php echo $user_res[0]['user_id']; ?>" placeholder="" >-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color: black;">Bank Account Number <span style="color: red">*</span></label>  
                                        <div class="col-md-8 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"></span>
                                                <input  name="bank_acc_no" class="form-control"  type="text" value="" placeholder="Bank Account Number" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color: black;">Bank Holder Name <span style="color: red">*</span></label>  
                                        <div class="col-md-8 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"></span>
                                                <input  name="bank_holder_name" class="form-control"  type="text" value="" placeholder="Bank Holder Name" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color: black;">IFSC Code <span style="color: red">*</span></label>  
                                        <div class="col-md-8 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"></span>
                                                <input  name="ifsc_code" class="form-control"  type="text" value="" placeholder="IFSC Code" required=""> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color: black;">BRANCH Code <span style="color: red">*</span></label> 
                                        <div class="col-md-8 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"></span>
                                                <input name="branch_code" placeholder="BRANCH Code " class="form-control"  type="text" required="">
                                            </div>
                                        </div>
                                    </div>						
                                    <!-- Button -->
                                    <div class="form-group" >
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-4"><br>
                                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-warning" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT<span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>	
                        </div>
                    <?php } ?>
                
            </div>							
            <!-- </md-card>end ngIf: eligibleStatus==='NONE' -->
            <div class="row"></div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $("#resend").hide();
<?php if ($otpverify == '1') { ?>
        $(window).on('load', function () {
            $('#myModal').modal('show');
        });

        $(document).ready(function () {
            $("#ms_timer").countdowntimer({
                minutes: 5,
                size: "md"
            });
        });
        $("#resendbtn").click(function () {
            location.reload(true);
        });
        var ajax_call = function () {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>welcome/DeleteOTP",
                data: {},
                // dataType: "json",
                success: function (data) {
                    // alert(data);
                    //location.reload(true);
                    $("#resend").show();
                    $("#timer1").hide();
                    $("#submit").hide();
                }
            });
            // location.reload(true);
        };
        var interval = 1000 * 60 * 5; // where X is your every X minutes
        setInterval(ajax_call, interval);
<?php } ?>
</script>

