<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h3><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;User Dashboard</h3>           	
        </div>
    </div>
</div>
<section id="middle">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <h5>
                    <ul class="list-group">
                        <li class="list-group-item"><a href="<?php echo base_url();?>user/profile"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;<strong>My Profile</strong></a></li>
                        <li class="list-group-item"><a href="<?php echo base_url();?>user/orders"><i class="fa fa-check-square-o" aria-hidden="true"></i>&nbsp;<strong>Order History</strong></a></li>
                        <li class="list-group-item"><a  href="<?php echo base_url();?>user/setting"><i class="fa fa-gears" aria-hidden="true"></i>&nbsp;<strong>Account Setting</strong></a></li>
                        <li class="list-group-item"><a class="black-text" href="<?php echo base_url();?>home/myaccount"><i class="fa fa-plus-square-o" aria-hidden="true"></i>&nbsp;<strong>My Account</strong></a></li>
                    </ul>
                </h5>                                                                            	
            </div>
            <div style="border:solid 1px #aac6f5; margin-bottom:40px;" class="col-lg-9 form-set">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>
                <div class="col-lg-12"><h4><strong><i class="fa fa-check-square-o" aria-hidden="true"></i>&nbsp;Order History</strong></h4></div>


                <div class="col-lg-12" style="text-align:center; font-size:18px; padding-bottom:25px;">Work In Process</div>

                <!--Table-->
                <div class="col-lg-12" style="display:none;">
                    <br/>
                    <h5>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="3"><h4><strong><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Your Order History</strong></h4></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><h4><a href="#">Product heading will be here</a></h4></td>
                                        <td width="10%"><center><a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-trash" aria-hidden="true"></i></a></center></td>                                                                
                                </tr>
                                <tr>
                                    <td><h4><a href="#">Product heading will be here</a></h4></td>
                                    <td width="10%"><center><a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-trash" aria-hidden="true"></i></a></center></td>                                                                
                                </tr>                            

                                </tbody>
                            </table>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="3"><h4><strong><i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;Order History Details</strong></h4></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="1"><h4>Product heading will be here</h4><h6>Category Name</h6></td>                                
                                    </tr> 
                                    <tr>
                                        <td><h6>Payment Details</h6></td>                                
                                    </tr>                                                                                   
                                </tbody>
                            </table> 

                        </div>
                    </h5>
                </div>
                <!--End Table-->                


            </div><!--End col-lg-9 form-set-->                                                                                               



        </div>
    </div>
</section>