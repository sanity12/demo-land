<style>
    body {
        color: #000000;
    }
</style>
<div class="content-wrapper"><br>
    <div class="container" style="width: 100%">
        <center><h2>Message List</h2></center>
        <div id="message">
            <h3 style="color: #3c8dbc;"><u>Received Message</u></h3>
            <ul class="grouped">
                <li style="margin-right:20px;"><a href="#" class="bttn yellow" data-toggle="modal" data-target="#myModal">COMPOSE</a></li>
                <li><a href="<?php echo base_url('home/message'); ?>" class="bttn icon refresh">Refresh</a></li>
                <li><a href="javascript:void()" class="bttn mark_read">Mark as Read</a></li>
                <li><a href="javascript:void()" class="bttn mark_unread">Mark as Unread</a></li>
                <li><a href="javascript:void()" class="bttn icon delete">Delete</a></li>
                <!-- <li><a href="#" class="bttn icon folder">Folder</a></li> -->
            </ul> 
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">		    
                    <!-- Modal content-->
                    <div class="modal-content modal-dialog modal-lg">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Send Message to Admin</h4>
                        </div>
                        <div class="modal-body">
                            <form class="well form-horizontal" action="<?php echo site_url('home/sent_msg_admin'); ?>" method="post"  id="msg_form" enctype="multipart/form-data">
                                <fieldset>
                                    <h4>New Message</h4>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" > Message Type*</label> 
                                        <div class="col-md-8 inputGroupContainer">
                                            <div class="input-group col-md-8">
                                                <input name="msg_type" id="msg_type" placeholder="Message Type" class="form-control"  type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Product Description*</label>  
                                        <div class="col-md-8 inputGroupContainer">
                                            <div class="input-group col-md-8">
                                                <span class="input-group col-md-8-addon"></span>                           
                                                <textarea class="ckeditor" name="message" id="editor" cols="26"></textarea>                  
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-4"><br>
                                            <button type="submit" class="btn btn-warning" >SUBMIT <span class="glyphicon glyphicon-send"></span></button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- Modal content-->
                </div>		    
            </div>

            <table class="table table-bordered mailinbox table-striped table-hover" id="dataTables4" style="border-collapse:collapse; margin-top:10px;" border="0" cellpadding="5" cellspacing="3">
                <thead>
                    <tr>
                        <th class="head1 aligncenter"><input type="checkbox" class="checkall" /></th>
                        <!-- <th class="head0">&nbsp;</th> -->
                        <th class="head1">Sender</th>
                        <th class="head1">Message Type</th>
                        <th class="head0" style="width: 45%;">Subject</th>
                        <!-- <th class="head1 attachement">&nbsp;</th> -->
                        <th class="head0">Date</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if (!empty($msgList)) {
                        $index = 1;
                        foreach ($msgList as $row) {
                            ?>
                            <tr <?php
                            if ($row['msg_status'] != '3') {
                                echo 'class="unread" ';
                            }
                            ?>>
                                <td class="aligncenter"><input type="checkbox" class="checkbox" name="" value="<?php echo $row['m_id'] ?>" /></td>
                                <!-- <td class="star"><a class="msgstar starred"></a></td> -->
                                <td><?php if(!empty($row['sent_user_id'])){ echo get_username($row['sent_user_id']); }else{ echo 'admin';}?></td>
                                <td><a href="" class="title" data-toggle="modal" id="" data-target="#myModal<?php echo $row['m_id'] ?>"><?php echo $row['msg_type'] ?></a>                                            	
                                </td>
                                <td style="width: 45%;"><?php $url = $row['message'];
                                    echo wordwrap($url, 15, "\n", 1);
                                    ?></td>
                                <!-- <td class="attachment"><img src="images/attachment.png" alt="" /></td> -->
                                <td class="date"><?php echo date('d F, Y H:i', strtotime($row['created_date'])); ?></td>
                            </tr>
                        <div class="modal fade" id="myModal<?php echo $row['m_id'] ?>" role="dialog">
                            <div class="modal-dialog">		    
                                <!-- Modal content-->
                                <div class="modal-content modal-dialog modal-lg">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">View Message</h4>
                                    </div>
                                    <div class="modal-body">				        	
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" > Message Type*</label> 
                                                <div class="col-md-8 inputGroupContainer">
                                                    <div class="input-group col-md-8">
                                                        <input name="msg_type" id="msg_type" placeholder="Message Type" class="form-control"  type="text" value="<?php echo $row['msg_type']; ?>" readonly disable="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Product Description*</label>  
                                                <div class="col-md-8 inputGroupContainer">
                                                    <div class="input-group col-md-8">
                                                        <span class="input-group col-md-8-addon"></span>                           
                                                        <textarea  name="message" id="editor<?php echo $row['m_id'] ?>" cols="26" readonly disable="" ><?php echo $row['message']; ?></textarea>                  
                                                    </div>
                                                </div>
                                            </div> 				                    
                                        </fieldset>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                    <script src="<?php echo base_url(); ?>assets/js/ckeditor.js"></script>
                                    <script src="<?php echo base_url(); ?>assets/js/ckfinder.js"></script>
                                    <script type="text/javascript">
                                        CKEDITOR.replace('editor<?php echo $row['m_id'] ?>', {
                                            fullPage: true,
                                            allowedContent: true,
                                            extraPlugins: 'wysiwygarea'

                                        });
                                    </script>
                                </div>
                                <!-- Modal content-->
                            </div>		    
                        </div> 
    <?php }
}
?> 
                </tbody>                        
            </table>             
        </div>
        <div id="admin_msg">
            <h3 style="color: #3c8dbc;"><u>Sent Messages</u></h3>
            <table class="table table-bordered mailinbox table-striped table-hover" id="dataTables5" style="border-collapse:collapse; margin-top:10px;" border="0" cellpadding="5" cellspacing="3">
                <thead>
                    <tr>
                        <th class="head1 aligncenter"><input type="checkbox" class="checkall" /></th>
                        <!-- <th class="head0">&nbsp;</th> -->
                        <th class="head1">Sender</th>
                        <th class="head1">Message Type</th>
                        <th class="head0" style="width: 44%;">Subject</th>
                        <!-- <th class="head1 attachement">&nbsp;</th> -->
                        <th class="head0">Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($msgList_admin)) {
                        $index = 1;
                        foreach ($msgList_admin as $row) {
                            ?>
                            <tr>
                                <td class="aligncenter"><input type="checkbox" class="checkbox" name="" value="<?php echo $row['m_id'] ?>" /></td>
                                <!-- <td class="star"><a class="msgstar starred"></a></td> -->
                                <td>You</td>
                                <td><a href="" class="title" data-toggle="modal" id="" data-target="#myModal<?php echo $row['m_id'] ?>"><?php echo $row['msg_type'] ?></a></td>
                                <td><?php $url = $row['message'];
                            echo wordwrap($url, 30, "\n", 1); ?></td>
                                <!-- <td class="attachment"><img src="images/attachment.png" alt="" /></td> -->
                                <td class="date"><?php echo date('d F, Y H:i', strtotime($row['created_date'])); ?></td>
                            </tr>
                        <div class="modal fade" id="myModal<?php echo $row['m_id'] ?>" role="dialog">
                            <div class="modal-dialog">		    
                                <!-- Modal content-->
                                <div class="modal-content modal-dialog modal-lg">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">View Message</h4>
                                    </div>
                                    <div class="modal-body">				        	
                                            <!-- <form class="well form-horizontal" action="<?php echo site_url('welcome/sent_msg_admin'); ?>" method="post"> -->
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" > Message Type*</label> 
                                                <div class="col-md-8 inputGroupContainer">
                                                    <div class="input-group col-md-8">
                                                        <input name="msg_type" id="msg_type" placeholder="Message Type" class="form-control"  type="text" value="<?php echo $row['msg_type']; ?>" readonly disable="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Product Description*</label>  
                                                <div class="col-md-8 inputGroupContainer">
                                                    <div class="input-group col-md-8">
                                                        <span class="input-group col-md-8-addon"></span>                           
                                                        <textarea  name="message" id="editor<?php echo $row['m_id'] ?>" cols="26" readonly disable="" ><?php echo $row['message']; ?></textarea>                  
                                                    </div>
                                                </div>
                                            </div> 				                    
                                        </fieldset>				           
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                    <script src="<?php echo base_url(); ?>assets/js/ckeditor.js"></script>
                                    <script src="<?php echo base_url(); ?>assets/js/ckfinder.js"></script>
                                    <script type="text/javascript">
                                    CKEDITOR.replace('editor<?php echo $row['m_id'] ?>', {
                                        fullPage: true,
                                        allowedContent: true,
                                        extraPlugins: 'wysiwygarea'

                                    });
                                    </script>
                                </div>
                                <!-- Modal content-->
                            </div>		    
                        </div>
    <?php }
} ?>                          
                </tbody>                        
            </table> 
        </div>
    </div>
</div>
<br><br><br><br><br><br>
<script>
//    
    $(document).ready(function () {

        $('#dataTables4').dataTable();
    });
</script> 
<script type="text/javascript">
    $(document).on("click", '.edit_button', function (e) {
        var name = $(this).data('name');
        var id = $(this).data('id');
        var content = $(this).data('content');
        var quote = $(this).data('quote');

        $(".business_skill_id").val(id);
        $(".business_skill_name").val(name);
        $(".business_skill_quote").val(quote);
        tinyMCE.get('business_skill_content').setContent(content);
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {

        // check all checkboxes in table
        if ($('.checkall').length > 0) {
            $('.checkall').click(function () {
                var parentTable = $(this).parents('table');
                var ch = parentTable.find('.checkbox');
                if ($(this).is(':checked')) {

                    //check all rows in table
                    ch.each(function () {
                        $(this).attr('checked', true);
                        $(this).parent().addClass('checked');	//used for the custom checkbox style
                        $(this).parents('tr').addClass('selected'); // to highlight row as selected
                    });

                } else {

                    //uncheck all rows in table
                    ch.each(function () {
                        $(this).attr('checked', false);
                        $(this).parent().removeClass('checked');	//used for the custom checkbox style
                        $(this).parents('tr').removeClass('selected');
                    });

                }
            });
        }


        if ($('.mailinbox').length > 0) {

            // star
            $('.msgstar').click(function () {
                if ($(this).hasClass('starred'))
                    $(this).removeClass('starred');
                else
                    $(this).addClass('starred');
            });

            //add class selected to table row when checked
            $('.mailinbox tbody input:checkbox').click(function () {
                if ($(this).is(':checked'))
                    $(this).parents('tr').addClass('selected');
                else
                    $(this).parents('tr').removeClass('selected');
            });

            // trash
            if ($('.delete').length > 0) {
                $('.delete').click(function () {
                    var c = false;
                    var cn = 0;
                    var o = new Array();
                    var fav = [];
                    $('.mailinbox input:checkbox').each(function () {
                        if ($(this).is(':checked')) {
                            c = true;
                            o[cn] = $(this);
                            cn++;
                            fav.push($(this).val());
                        }
                    });
                    if (!c) {
                        alert('No selected message');
                    } else {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>welcome/statusChange",
                            data: {status: '4', id: fav},
                            cache: false,
                            success: function (result) {
                                //location.reload(true);
                            },
                            error: function (result) {
                                alert(result);
                            }
                        });
                        var msg = (o.length > 1) ? 'messages' : 'message';
                        if (confirm('Delete ' + o.length + ' ' + msg + '?')) {
                            for (var a = 0; a < cn; a++) {
                                $(o[a]).parents('tr').remove();
                            }
                        }
                    }
                });
            }


            // mark as read
            $('.mark_read').click(function () {
                var c = false;
                var cn = 0;
                var o = new Array();
                var fav = [];
                $('.mailinbox input:checkbox').each(function () {
                    if ($(this).is(':checked')) {
                        c = true;
                        o[cn] = $(this);
                        cn++;
                        fav.push($(this).val());
                    }
                });

                if (!c) {
                    alert('No selected message');
                } else {

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>welcome/statusChange",
                        data: {status: '3', id: fav},
                        cache: false,
                        success: function (result) {
                            //location.reload(true);
                        },
                        error: function (result) {
                            alert(result);
                        }
                    });

                    var msg = (o.length > 1) ? 'messages' : 'message';
                    if (confirm('Mark ' + o.length + ' ' + msg + ' to read')) {
                        for (var a = 0; a < cn; a++) {
                            $(o[a]).parents('tr').removeClass('unread');
                        }
                    }
                }

            });

            // make messages to unread
            $('.mark_unread').click(function () {
                var c = false;
                var cn = 0;
                var o = new Array();
                var fav = [];
                $('.mailinbox input:checkbox').each(function () {
                    if ($(this).is(':checked')) {
                        c = true;
                        o[cn] = $(this);
                        cn++;
                        fav.push($(this).val());
                    }
                });
                if (!c) {
                    alert('No selected message');
                } else {

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>welcome/statusChange",
                        data: {status: '2', id: fav},
                        cache: false,
                        success: function (result) {
                            //location.reload(true);
                        },
                        error: function (result) {
                            alert(result);
                        }
                    });
                    var msg = (o.length > 1) ? 'messages' : 'message';
                    if (confirm('Mark ' + o.length + ' ' + msg + ' to unread')) {
                        for (var a = 0; a < cn; a++) {
                            $(o[a]).parents('tr').addClass('unread');
                        }
                    }
                }
            });

        }
    });

</script>
<script type="text/javascript">
    CKEDITOR.replace('editor', {
        fullPage: true,
        allowedContent: true,
        extraPlugins: 'wysiwygarea'

    });
</script>