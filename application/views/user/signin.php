<section id="top-box">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px">
                    <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Sign In</h4>
                </div>
                <!--<h4><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp; </h4>-->           	
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>               
        </div>
    </div>
</section>

<section style="margin-bottom:40px;" id="product-details">
    <div class="container">
        <div class="row">
            <div style="border:solid 1px #aac6f5; padding:20px;" class="col-lg-12 form-set">
                <form name="regform" action="<?php echo site_url('user/signIn'); ?>" method="post" onsubmit="return validation();">
                    <div class="col-lg-6">
                        <?php
                        if (isset($ErrorMsg)) {

                            if ($ErrorMsg != '') {
                                ?>
                                <div class="col-lg-10" style="color:#F00;"><?php echo $ErrorMsg; ?></div>
                                <?php
                            }
                        }
                        ?>
                        <div class="col-lg-10">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" placeholder="Enter the User Name" name="email">
                            </div>
                        </div>
                        <input type="hidden" name="signin" value="signin">
                        <div class="col-lg-10">
                            <div class="md-form" style="padding-top: 23px;">
                                <input type="password" id="pwd" class="form-control masked" placeholder="Enter Password" name="password">
                            </div>
                        </div> 
                                          
                        <div class="col-lg-12">
                            <div class="md-form" style="padding-top: 23px;">
                                <button style="margin-left:0px;" type="submit" class="btn btn-success" name="submit" value="Sign In"><strong>Sign in</strong></button>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="md-form">
                                <h5>Not a Register ?&nbsp;<a href="register.php">Click Here</a></h5>
                            </div>
                        </div>                                                        	
                    </div>
                    <div class="col-lg-6"><center><img class="img-responsive" src="img/advertise.jpg" alt="" title="" /></center></div>                                                                                               
                </form>
            </div><!-- End col-lg-12 -->
        </div>
    </div>
</section>
<script type="text/javascript" >
    function validation() {

        //var reg = "/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/";
        var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var phoneno = /^\d{10}$/;
        var email_mobile = document.regform.email.value;

        if (document.regform.email.value == "")
        {
            alert("Please enter email or mobile");
            document.regform.email.focus();
            document.regform.email.select();
            return false;
        } else if (document.regform.password.value == "")
        {
            alert("Please enter your password");
            document.regform.password.focus();
            document.regform.password.select();
            return false;
        }
    }
</script>
