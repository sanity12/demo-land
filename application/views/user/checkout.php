<style>
    .backChange{
        background:green;
    }
</style>
<section id="top-box">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px">
                    <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;My Checkout </h4>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>               
        </div>
    </div>
</section>
<section id="middle">
    <div class="container">
        <div class="row">
            <div style="width: 100%" class="col-lg-9 form-set">
                <div class="col-lg-12"></div>
                <?php if (isset($ApplyProRes)) { ?>
                    <div class="col-lg-12">
                        <table id="cart" class="table table-hover table-condensed">
                            <thead>
                                <tr class="bordered-box">
                                    <!--<th width="8%">Sr.No.</th>-->
                                    <th width="8%" class="aligncenter">Select For Payment</th>
                                    <th width="10%">main Form</th>
                                    <th width="12%">Private User Name</th>
                                    <th width="8%">Price</th>
                                    <th style="width:18%"><h4><strong>Payment</strong></h4></th> 
                                    <th width="10%" style="width:10%">Select for apply code</th>
                                    <th width="10%">Date</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $total_final = 0;
                                foreach ($ApplyProRes as $row) {
                                    $total_final += $row['pro_price'] * 1;
                                    $CurrDate = date('Y-m-d H:i:s');
                                    $date1=date_create($row['updated_date']);
                                    $date2=date_create($CurrDate);
                                    $diff=date_diff($date1,$date2);
                                    $DIF = $diff->format("%R%a");
                                    if($DIF < 0){
                                    ?> 
                                    <tr class="bordered-box checkbox2" >
                                        <?php if ($row['is_payment'] == '0') { ?>
                                            <td class=" head1 aligncenter"><label class="btn btn-primary active"><input type="checkbox" style="font-size: 2.5em" class="checkbox1" name="checkbox1[]" id="c<?php echo $row['app_id'] ?>" data-id="<?php echo $row['pro_price']; ?>" value="<?php echo $row['app_id'] ?>" /></label></td> 
                                        <?php } ?>
                                        <td><?php $p = get_photo($row['proper_id']); ?>
                                            <a href="<?php echo base_url(); ?>home/proDetail/<?php echo $row['proper_id']; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/Pro_Imgupload/<?php echo $p; ?>" width="40%" style="width: 100%;"></a></td>
                                        <td><?php echo get_username($row['user_private_id']); ?></td>
                                        <td>$<span class='rowprice1'><?php echo $row['pro_price']; ?></span></td>
                                        <?php if ($row['is_payment'] == '1') { ?>
                                            <td data-th="Price"><h4><strong>&nbsp;<span class="btn btn-info">PAID</span></strong></h4></td>
                                        <?php } else { ?>
                                            <td data-th="Price"><h4><strong>&nbsp;<a class="btn btn-info col-md-7" href="javascript:void(0)">PAY NOW</a></strong></h4></td>
                                        <?php } ?>
                                        <td class="visibleimg"><button type='button' data-id="<?php echo $row['app_id']; ?>" data-price="<?php echo $row['pro_price']; ?>" id="applynow<?php echo $row['app_id']; ?>" class="btn btn-warning applynow" >Select</button>
                                            <div class="img0 visible" style="margin-left: 35px;" id="img<?php echo $row['app_id']; ?>" >
                                                <img src="<?php echo base_url(); ?>/assets/images/rightgreen.png" style="width:20%">
                                            </div></td>
                                <script>
                                    $("#applynow<?php echo $row['app_id']; ?>").click(function () {
                                        var proper_id = $(this).attr("data-id");
                                        
                                        if (jQuery("#c<?php echo $row['app_id']; ?>").is(":checked")) {
                                            if ($('#img<?php echo $row['app_id']; ?>').hasClass('visible')) {
                                                $('#img<?php echo $row['app_id']; ?>').slideDown().removeClass('visible');
                                                arr.push(proper_id);
                                                $("#img" + proper_id).show();
                                                $("#applynow" + proper_id).html("Selected");
                                                totalp += parseFloat($(this).attr('data-price'));
                                                $("#properids").val(arr);
                                                $("#appval").val(totalp);
                                                getdiscount();
                                            } else {
                                                totalp = totalp - parseFloat($(this).attr('data-price'));
                                                $("#appval").val(totalp);
                                                arr.pop(proper_id);
                                                $('#img<?php echo $row['app_id']; ?>').slideUp().addClass('visible');
                                                $("#img<?php echo $row['app_id']; ?>").hide();
                                                $("#applynow" + proper_id).html('select');
                                                $("#properids").val(arr);
                                                getdiscount();
                                            }
                                            //  alert(arr);
                                        } else {
                                            alert('First Check the checkbox');
                                        }
                                    });
                                </script>
                                <td><?php echo $today = date("d M, Y"); ?></td>
                                </tr>
                                <?php
                                //  $index++;
                                } }
                            ?>
                            <tr class="bordered-box">
                                <td class="head1 aligncenter"></td>
                                <td style="" colspan="2">&nbsp;</td>
                                <!--<td style="float: right">&nbsp;</td>-->
                                <td class="head1 aligncenter"><label class="btn btn-success">Total : $<span id="total"><?php echo $total_final; ?></span></label></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>                                
                <?php } ?>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div>
            </div><!--End col-lg-9 form-set-->                                                                                               
            <!-- </form> -->
             <?php if (!empty($coupontext)) { ?>
                <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px;background: #575c9e;">
                    <h4><i class="fa fa-star-half-full" aria-hidden="true"></i>&nbsp;&nbsp;
                        <?php echo $coupontext[0]['description']; ?></h4>
                </div>
            <?php } ?>


            <?php if (!empty($type1)) { ?>
                <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px;background: #c9bebb;">
                    <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;
                        Coupon Code : <?php echo $type1[0]['code'] . " " . $type1[0]['frequency'] . "% off on the selected  bill amount "; ?><span class="float:right" style="margin-left: 551px;">Remaining : 2</span> </h4>
                </div>
            <?php } ?>
            <?php if (!empty($type2) && $type2[0]['frequency'] !='0') { ?>
                <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px;background: #c9bebb;">
                    <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;
                        <?php echo $type2[0]['code'] . " " . $type2[0]['frequency'] . " " . $type2[0]['duration']; ?> </h4>
                </div>
            <?php } ?>
            <?php if (!empty($type3) && $type3[0]['frequency'] !='0') { ?>
                <div class="nav nav-pills nav-stacked top-title" style="margin-top:12px;margin-bottom: 5px;background: #c9bebb;">
                    <h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;
                        <?php echo $type3[0]['code'] . " " . $type3[0]['frequency'] . " " . $type3[0]['duration']; ?> </h4>
                </div>
            <?php } ?>

            <!--<form class="well1 form-vertical" action="" method="post"  id="coupon_form" enctype="multipart/form-data" style="margin-left:15px;">-->
            <div class="form-group">
                <label class="col-md-1 control-label" >Coupon Code</label> 
                <div class="col-md-2 inputGroupContainer">
                    <div class="input-group col-md-10">
                        <input name="code"  placeholder="" id="code" class="form-control"  type="text" required='' value="">
                    </div>
                </div>
                <input type="hidden" value="0" id="appval">
                <input type="hidden" name="ftotal" value="" id="ftotal">
                <input type="hidden" name="properids" value="" id="properids">
                <div class="col-md-2 inputGroupContainer">      
                    <div class="input-group col-md-5">
                        <input name="Subcode2" id="Subcode1" class="btn btn-info btn-block"  type="button" value="SUBMIT" required='' onclick="return testcheck()">
                    </div>
                </div>
            </div>
            <!--</form>-->

           
            <div class="col-lg-3" style="margin-top: 15px;margin-bottom: 15px;float: right;">
                <form enctype="" method="post" action="<?php echo base_url() ?>home/insert_applyIds">

                    <input type="hidden" name="user_public_id" value="<?php //echo $user_public_id; ?>" id="user_public_id">
                    <button class="btn btn-success btn-block" href="javascript:void(0)" id="submit">Click For Payment
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var arr = [];
    var total = 0;
    var totalp = 0;
    $(document).ready(function () {
        $(".img0").hide();
        $('input[type=checkbox]').attr('checked', true);
        var total = 0;
        //    alert("we");
        $('.checkbox1:checked').each(function () {
            var rp = $(this).attr("data-id");
            total += parseFloat(rp);
        });
        // alert(total);
        $("#total").html(total);
        $("#ftotal").val(total);
        
    });


//    $(".checkall").on("click", function () {
//        var $tblChkBox = $(".checkbox2 input:checkbox");
//        $($tblChkBox).prop('checked', $(this).prop('checked'));
//        //  if($($tblChkBox).prop('checked') === 'true') {
//        if ($(".checkbox1").length === $(".checkbox1:checked").length) {
//            var total = 0;
//            //    alert("we");
//            $('.rowprice1').each(function () {
//                total += parseFloat(this.innerHTML)
//            });
//            // alert(total);
//            $("#total").html(total);
//            $("#ftotal").val(total);
//        } else {
//
//            //var total = 0;
//            $("#total").html('0');
//            $("#ftotal").val('0');
//            $('.img0').slideUp().addClass('visible');
//            $(".img0").hide();
//            $(".applynow").html('select');
//            $("#properids").val('');
//            $("#appval").val('0');
//
//        }
//    });

    $('.checkbox1').click(function(){
       
//        if($(".checkbox1").length === $(".checkbox1:checked").length) {
//           $(".checkall").prop("checked", true);
//    //            var total = 0;
//    //            $('.rowprice1').each(function(){
//    //                total += parseFloat(this.innerHTML)
//    //            });
//    //            $("#total").html(total);$("#ftotal").val(total);
//        }else {
//                $(".checkall").prop("checked", false);      
//    //            var total = 0;
//    //            $('.checkbox1:checked').each(function(){
//    //                var rp = $(this).attr("data-id");
//    //                    total += parseFloat(rp);
//    //            });
//    //            $("#total").html(total);    $("#ftotal").val(total);
//        }
            var total = 0;
            $('.checkbox1:checked').each(function () {
                var rp = $(this).attr("data-id");
                total += parseFloat(rp);
            });
            $("#total").html(total); 
            $("#ftotal").val(total);
            var app_id = $(this).val();
            
        if($(this).is(":checked")){
            
        }
        else{
           //alert('2')
            if (!$('#img'+app_id).hasClass('visible')) {
                //alert('3');
                arr.pop(app_id);
                $("#img"+app_id).slideUp().addClass('visible');
                $("#img"+app_id).hide();
                $("#applynow" + app_id).html('select');
                $("#properids").val(arr);
//               // alert($("#applynow" + app_id).attr('data-price'));
               totalp = totalp - parseFloat($("#applynow" + app_id).attr('data-price'));
            }
            
        }
        $("#appval").val(totalp);
        getdiscount();
    });
    function testcheck()
    {
        if (!jQuery(".checkbox1").is(":checked")) {
            alert("Please Select At Least one checkbox before apply code!");
            return false;
        } else if ($("#appval").val() === '') {
            alert("Please Select At Least one select button before apply code!");
            return false;
        } else if ($("#properids").val() === '') {
            alert("Please Select At Least one select btn before apply code!");
            return false;
        } else if ($("#code").val() === '') {
            alert("Please Enter Coupon Code!");
            return false;
        } else {
            getdiscount();
            //AJAX CODE FOR GET VALUE OF DISCOUNT
//            var code = $("#code").val();
//            var d = 0;
//            var selamt = $("#appval").val();
//            var total = $("#ftotal").val();
//            $.ajax({
//                type: 'POST',
//                url: '<?php echo base_url(); ?>postproperty/get_value',
//                data: {code: code},
//                success: function (response) {
//                    d = response;
//                    calFinalAmt(d,selamt,total);
//                }
//            });
        }
    }
    function getdiscount(){
        var code = $("#code").val();
        var d = 0;
        var selamt = $("#appval").val();
        var total = $("#ftotal").val();
        $(".loader").fadeIn();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url(); ?>postproperty/get_value',
            data: {code: code},
            success: function (response) {
                d = response;
                $(".loader").fadeOut("slow");
                calFinalAmt(d,selamt,total);
            }
        });
    }
    
    function calFinalAmt(disc,selamt,total) {
        var le = parseInt(total - selamt);
        var rt = parseInt(selamt - (selamt * (disc / 100)));
        var cal = parseInt(rt + le);
        $("#total").html(cal);
    }
</script>