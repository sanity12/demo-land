<?php

if (!function_exists('get_category')) {

    function get_category($user_id = false) {
        $CI = & get_instance();
        $CI->load->model('Crud');
        $CListRes = $CI->Crud->retrive_cat($where = array());
        foreach ($CListRes as $GetCatRes) {
            echo'<option value="' . $GetCatRes['cat_id'] . '" onclick="clickSearchButton()">' . $GetCatRes['cat_name'] . '</option>';
        }
    }

}

if (!function_exists('sendOTP')) {

    function sendOTP($contact_no) {
        $authKey = "188809AbCWbrWxV5a38e835"; //Your authentication key		183123ATM5ubE2p7u5a0593d1
        $mobileNumber = $contact_no;         //Multiple mobiles numbers separated by comma		
        $senderId = "Landkart";                //Sender ID,While using route4 sender id should be 6 characters long.	
        $otp = rand('100000', '999999');
        $message = urlencode("Your OTP is " . $otp); //Your message to send, Add URL encoding here.
        $route = "4";                        //Define route 
        $postData = array(//Prepare you post parameters
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );
        $url = "https://control.msg91.com/api/sendhttp.php"; //API URL		
        $ch = curl_init(); // init the resource
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
                //,CURLOPT_FOLLOWLOCATION => true
        ));
        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        $output = curl_exec($ch); //get response
        //Print error if any
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);

        // echo 'https://control.msg91.com/api/sendotp.php?authkey='.$authKey.'&mobile='.$mobileNumber.'&message='.$message.'&sender='.$senderId.'&otp='.$otp.'';
        // echo '';
//		$response = file_get_contents('https://control.msg91.com/api/sendotp.php?authkey='.$authKey.'&mobile='.$mobileNumber.'&message='.$message.'&sender='.$senderId.'&otp='.$otp.'');

        $my_array_data = json_decode($output, TRUE);
        // print_r($output);
        // print_r($my_array_data);
        $response = array('otp' => $otp, 'message' => $output);
        // exit;
        return $response;
    }

}

if (!function_exists('sendSMS')) {

    function sendSMS($contact_no, $time) {
        $time = '1 Day';
        $authKey = "188809AbCWbrWxV5a38e835"; //Your authentication key		183123ATM5ubE2p7u5a0593d1
        $mobileNumber = $contact_no;         //Multiple mobiles numbers separated by comma		
        $senderId = "Lndkrt";                //Sender ID,While using route4 sender id should be 6 characters long.	
        $message = urlencode("You have only " . $time . " left for payment,after time completed the property will automatic locked."); //Your message to send, Add URL encoding here.
        $route = "4";                        //Define route 
        $postData = array(//Prepare you post parameters
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );
        $url = "https://control.msg91.com/api/sendhttp.php"; //API URL		
        $ch = curl_init(); // init the resource
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
                //,CURLOPT_FOLLOWLOCATION => true
        ));
        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        $output = curl_exec($ch); //get response
        //Print error if any
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);

        // echo 'https://control.msg91.com/api/sendotp.php?authkey='.$authKey.'&mobile='.$mobileNumber.'&message='.$message.'&sender='.$senderId.'&otp='.$otp.'';
        // echo '';
//		$response = file_get_contents('https://control.msg91.com/api/sendotp.php?authkey='.$authKey.'&mobile='.$mobileNumber.'&message='.$message.'&sender='.$senderId.'&otp='.$otp.'');

        $my_array_data = json_decode($output, TRUE);
        // print_r($output);
        // print_r($my_array_data);
        $response = array('message' => $output);
        // exit;
        return $output;
    }

}

if (!function_exists('check_user')) {

    function check_user($user_id) {
        $CI = & get_instance();
        $CI->load->model('Crud');
        $CListRes = $CI->Crud->retrive_user($user_id);
        $response = $CListRes[0]['user_type'];
        return $response;
    }

}

if (!function_exists('get_username')) {

    function get_username($user_id) {
        $CI = & get_instance();
        $CI->load->model('Crud');
        $CListRes = $CI->Crud->retrive_user($user_id);
        $response = $CListRes[0]['user_name'];
        return $response;
    }

}

if (!function_exists('get_flname')) {

    function get_flname($user_id) {
        $CI = & get_instance();
        $CI->load->model('Crud');
        $CListRes = $CI->Crud->retrive_user($user_id);
        $response = $CListRes[0]['first_name'] . " " . $CListRes[0]['last_name'];
        return $response;
    }

}

if (!function_exists('get_photo')) {

    function get_photo($pro_id) {
        $CI = & get_instance();
        $CI->load->model('Crud');

        $CI->db->where('property_id', $pro_id);
        $query = $CI->db->get("property_details");
        $CListRes = $query->result_array();
        $response = $CListRes[0]['pro_photo'];
        return $response;
    }

}

if (!function_exists('get_oneprivatePro')) {

    function get_oneprivatePro() {
        $CI = & get_instance();
        $user_id = $CI->session->userdata('user_id');
        $CI->load->model('Crud');
        $CI->db->where('user_id', $user_id);
        $CI->db->where('property_type', 'private');
        $query = $CI->db->get("property_details");
        $CListRes = $query->num_rows();        
        return $CListRes;
    }

}


if (!function_exists('get_proname')) {

    function get_proname($pro_id) {
        $CI = & get_instance();
        $CI->load->model('Crud');

        $CI->db->where('property_id', $pro_id);
        $query = $CI->db->get("property_details");
        $CListRes = $query->result_array();
        $response = $CListRes[0]['project_name'];
        return $response;
    }

}

if (!function_exists('sent_msg_admin')) {

    function sent_msg_admin($receiveId, $msg_type, $message, $pro_id) {
        $CI = & get_instance();
        $user_id = $CI->session->userdata('user_id');
        $data = array(
            'pro_id' => $pro_id,
            'sent_user_id' => $user_id,
            'user_id' => $receiveId,
            'msg_type' => $msg_type,
            'message' => $message,
            'created_date' => date('Y-m-d H:i:s'),
            'msg_status' => '0' // 0-> admin message
        );

        $CI->db->insert('message', $data);
        $insert_id = $CI->db->insert_id();
        if ($insert_id) {
            return $insert_id;
        }
    }
}

if (!function_exists('sent_but_not_receive')) {

    function sent_but_not_receive() {

        $CI = & get_instance();
        $CI->load->model('Crud');
        $count = $CI->Crud->notReceiveMsg();
        echo $count;
    }

}
if (!function_exists('DuplicateMySQLRecord')) {

    function DuplicateMySQLRecord($table, $primary_key_field, $primary_key_val) {
        $CI = & get_instance();
        $CI->db->where($primary_key_field, $primary_key_val);
        $query = $CI->db->get($table);

        foreach ($query->result() as $row) {
            foreach ($row as $key => $val) {
                if ($key != $primary_key_field) {
                    /* $this->db->set can be used instead of passing a data array directly to the insert or update functions */
                    $CI->db->set($key, $val);
                }//endif              
            }//endforeach
        }//endforeach

        /* insert the new record into table */
        $CI->db->insert($table);
        $insert_id = $CI->db->insert_id();
        return $insert_id;
    }
}

if (!function_exists('DuplicateMySQLRecord1')) {

    function DuplicateMySQLRecord1($table, $primary_key_field, $primary_key_val, $newpro_id) {
        $CI = & get_instance();
        $CI->db->where($primary_key_field, $primary_key_val);
        $query = $CI->db->get($table);
        foreach ($query->result() as $row) {
           foreach ($row as $key => $val) {
                if ($key != 'gallery_id') {
                $data['dif'] = array($key,$val);
                }//endif              
            }//endforeach
            $data_diff = array ('pro_id' => $newpro_id,
                                'photo'  => $data['dif']['1']);
            
            $CI->db->insert($table,$data_diff);
        }//endforeach
        return true;
    }
}



if (!function_exists('get_profile_image')) {

    function get_profile_image() {

        $CI = & get_instance();
        $CI->load->model('Crud');
        $profile_image = $CI->Crud->get_profile_image();
        echo $profile_image[0]['profile_picture'];
    }

}
if (!function_exists('get_property_user_details')) {

    function get_property_user_details($p_user_id) {

        $CI = & get_instance();
        $CI->load->model('Crud');
        $CListRes = $CI->Crud->retrive_user($p_user_id);
        //  $response = $CListRes[0]['first_name'];
        return $CListRes;
    }

}

if (!function_exists('get_my_account_count')) {

    function get_my_account_count() {

        $CI = & get_instance();
        $CI->load->model('Crud');
        $count = $CI->Crud->getMyAccountCount();
        $i = "0";
        foreach ($count as $row) {
            $CurrDate = date('Y-m-d H:i:s');
            $date1=date_create($row['updated_date']);
            $date2=date_create($CurrDate);
            $diff=date_diff($date1,$date2);
            $DIF = $diff->format("%R%a");
            if($DIF < 0){
                $i++;
            }
        }
        echo $i;
    }

}


if (!function_exists('get_property_image')) {

    function get_property_image($property_id) {

        $CI = & get_instance();
        $CI->load->model('Crud');
        $img_res = $CI->Crud->getPropertyImage($property_id);
        return $img_res;
    }
}
if (!function_exists('get_property_user_details')) {

    function get_property_user_details($p_user_id) {

        $CI = & get_instance();
        $CI->load->model('Crud');
        $CListRes = $CI->Crud->retrive_user($p_user_id);
        //  $response = $CListRes[0]['first_name'];
        return $CListRes;
    }
}
if (!function_exists('get_city_name_for_preference')) {

    function get_city_name_for_preference($city_id_array) {

        $CI = & get_instance();
        $CI->load->model('Crud');
        $CListRes = $CI->Crud->get_city_name_for_preference($city_id_array);
        //  $response = $CListRes[0]['first_name'];
        return $CListRes;
    }
}
if (!function_exists('get_pincode_for_preference')) {

    function get_pincode_for_preference($zipcode_id_array) {

        $CI = & get_instance();
        $CI->load->model('Crud');
        $CListRes = $CI->Crud->get_pincode_for_preference($zipcode_id_array);
        //  $response = $CListRes[0]['first_name'];
        return $CListRes;
    }
}

if (!function_exists('get_property_images_from_gallery')) {

    function get_property_images_from_gallery($property_id) {

        $CI = & get_instance();
        $CI->load->model('Crud');
        $img_res = $CI->Crud->getPropertyImagesfromGallery($property_id);
        return $img_res;
    }
}
if (!function_exists('retrive_preference_details')) {

    function retrive_preference_details($property_id,$user_id) {

        $CI = & get_instance();
        $CI->load->model('Crud');
        $img_res = $CI->Crud->retrive_preference($user_id,$property_id);
        return $img_res;
    }
}
?>