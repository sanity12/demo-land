<?php
class MAutocomplete extends CI_Model{ 
    function lookup($keyword){ 
        $this->db->select('*')->from('specities'); 
        $this->db->like('district',$keyword,'after'); 
//        $this->db->or_like('iso',$keyword,'after'); 
        $query = $this->db->get();     
        return $query->result(); 
    }
    
    function get_city_of_property($keyword)
    {
        $this->db->distinct();
        $this->db->select('city');
        $this->db->where('property_type','public');
        $this->db->like('city',$keyword,'after');
        $query = $this->db->get('property_details');
        return $query->result();
    }
}