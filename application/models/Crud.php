<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Model {

    public function retrive_cat($where) {
        if (isset($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get("cat");
//        echo $this->db->last_query();
        return $query->result_array();
    }

    public function get_products() {
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');
//        if ($user_type == 'private') {
            $this->db->where("user_id !=", $user_id);
//        }
        $this->db->where("status !=", '3');
        $this->db->where("property_type", 'public');
        $this->db->where("primary_id", '0');
        $query = $this->db->get("property_details");
//          echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function get_tableData($table, $txn_no) {
        $this->db->where('txno', $txn_no);
        $query = $this->db->get('transactions');
        $this->db->limit(1);
        echo $this->db->last_query();
        return $query->result_array();
    }

    public function retrive_user($id) {
        $this->db->where('user_id', $id);
        $query = $this->db->get("users");
        return $query->result_array();
    }

    public function retrive_Prod($where = false) {
        // $this->db->order_by('ma_id','DESC');
        if (isset($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get("products");
        return $query->result_array();
    }

    public function retrive() {
        $query = $this->db->get("users");
        return $query->result_array();
    }

    public function retrive_message() {
        $this->db->order_by('m_id', 'DESC');
        //$this->db->where('deleted_type','1');
        $this->db->where("msg_status !=", '4');
        $this->db->where("msg_status !=", '5');
        $query = $this->db->get("message");
        return $query->result_array();
    }

    public function retrive_city($id) {
        $this->db->where('city_id', $id);
        $query = $this->db->get("cities");
        return $query->result_array();
    }

    public function retrive_bankinfo($id) {
        $this->db->where('user_id', $id);
        $query = $this->db->get("bank_details");
        return $query->result_array();
    }

    public function retrive_state($id) {
        $this->db->where('state_id', $id);
        $query = $this->db->get("states");
        return $query->result_array();
    }

    public function retrive_investPro($user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->where('lock_status', '1');
        $query = $this->db->get("invest_product");
        return $query->result_array();
    }

    public function register() {

        $data = array(
            'user_type' => $this->input->post('user_type'),
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'contact_no' => $this->input->post('contact_no'),
            'company_register' => $this->input->post('company_register'),
            'user_name' => $this->input->post('user_name'),
            'user_password' => md5($this->input->post('user_password')),
            'user_status' => $this->input->post('user_status'),
            'website_link' => $this->input->post('website_link'),
            'profile_picture' => $this->input->post('uploded_profile_pic'),
            'additional_description' => $this->input->post('additional_description')
        );

        $this->db->insert('users', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function login($where) {
        $this->db->where($where);
        $query = $this->db->get("users");
        if ($query->num_rows() > 0) {

            foreach ($query->result() as $rows) {
                //add all data to session
                $newdata = array(
                    'user_id' => $rows->user_id,
                    'user_mobileno' => $rows->contact_no,
                    'user_name' => $rows->name,
                    'user_fname' => $rows->first_name,
                    'user_lname' => $rows->last_name,
                    'user_type' => $rows->user_type,
                    'email' => $rows->email,
                    'logged_in' => TRUE
                );
            }
            $this->session->set_userdata($newdata);
            return true;
        }
    }

    public function editprofile($user_id) {

        $data = array(
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'website_link' => $this->input->post('website_link'),
            'company_register' => $this->input->post('company_register'),
            'panno' => $this->input->post('panno'),
            'profile_picture' => $this->input->post('uploded_profile_pic')
        );
        $this->db->where("user_id", $user_id);
        $this->db->update('users', $data);
    }

    public function get_applyPro() {
        $user_id = $this->session->userdata('user_id');
        $this->db->select('*,ap.created_date as date');
        $this->db->from('applyproperty_private ap');
        $this->db->join('property_details p', 'p.property_id = ap.proper_id', 'left');
        $this->db->where("ap.user_public_id", $user_id);
        $this->db->where("ap.status !=", '0');
        $this->db->order_by('ap.app_id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_applyPro_notPaid() {
        $user_id = $this->session->userdata('user_id');
        $this->db->select('*,ap.created_date as date');
        $this->db->from('applyproperty_private ap');
        $this->db->join('property_details p', 'p.property_id = ap.proper_id', 'left');
        $this->db->where("ap.user_public_id", $user_id);
        $this->db->where("ap.is_payment", '0');
        $this->db->order_by('ap.app_id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_applyPro_notPaid_id($subQuery) {
        $user_id = $this->session->userdata('user_id');
        $this->db->select('*,ap.created_date as date');
        $this->db->from('applyproperty_private ap');
        $this->db->join('property_details p', 'p.property_id = ap.proper_id', 'left');
        $this->db->where("ap.user_public_id", $user_id);
        $this->db->where("ap.is_payment", '0');
        $this->db->where($subQuery);
        $this->db->order_by('ap.app_id', 'DESC');
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function msgList($id) {
        $this->db->where("user_id", $id);
        $this->db->where("msg_status !=", '4');
        $this->db->where("msg_status !=", '5');
        $this->db->order_by('m_id', 'desc');
        $query = $this->db->get("message");
        if ($query->num_rows() > 0) {
            $query_result = $query->result_array();
            return $query_result;
        }
    }

    public function msgList_admin($id) {
        $this->db->where("user_id", $id);
        $this->db->where("msg_status", '5');
        $this->db->order_by('m_id', 'desc');
        $query = $this->db->get("message");
        if ($query->num_rows() > 0) {
            $query_result = $query->result_array();
            return $query_result;
        }
    }

    public function search($param) {
        $this->db->select('name');
//        $this->db->select('text');
        $this->db->like('district', $param, 'both');
        $query = $this->db->get('specities');
        if ($query->num_rows() > 0) {
            $query_result = $query->result_array();
            return $query_result;
        }
    }

    //------------------------------
    function getData($loadType, $loadId) {
        if ($loadType == "state") {
            $fieldList = 's_id as id,state_name as name';
            $table = 'states';
            $fieldName = 'cont_id';
            $orderByField = 'state_name';
        } else {
            $fieldList = 'city_id as id,city_name as name';
            $table = 'cities';
            $fieldName = 'state_id';
            $orderByField = 'city_name';
        }
        $this->db->select($fieldList);
        $this->db->from($table);
        $this->db->where($fieldName, $loadId);
        $this->db->order_by($orderByField, 'asc');
        $query = $this->db->get();
        return $query;
    }

    public function getDataZipcode($address, $id) {
        $this->db->select('*');
        $this->db->from('zipcode');
        $this->db->where('district_name', $address);
        $this->db->order_by('subdist_name', 'asc');
        $query = $this->db->get();
        return $query;
    }

//    public function save_property_details() {
//        $data = array(
//            'you_are' => $this->input->post('you_are'),
//            'city' => $this->input->post('city_dropdown'),
//            'project_name' => $this->input->post('project_name'),
//            'locality' => $this->input->post('locality'),
//            'address' => $this->input->post('address'),
//            'plot_area' => $this->input->post('plot_area'),
//            'length' => $this->input->post('length'),
//            'breadth' => $this->input->post('breadh'),
//            'no_of_bathrooms' => $this->input->post('no_of_bathrooms'),
//            'no_of_bedrooms' => $this->input->post('no_of_bedrooms'),
//            'no_of_balcony' => $this->input->post('no_of_balcony') ,
//            'complete_address' => $this->input->post('complete_address'),
//            'Garden' => $this->input->post('garden'),
//            'water_borewell' => $this->input->post('water_borewell'),
//            'electricity_backup' => $this->input->post('electricity_backup'),
//            'swimming_pool' => $this->input->post('swimming_pool'),
//            'age_of_property' => $this->input->post('age_of_property'),
//           'electricity' => $this->input->post('electricity')
//
//        );
//        $this->db->insert('property_details', $data);
//    }

    public function save_property_details() {

        $residential_type = $this->input->post('residential_type');
        $commercial_type = $this->input->post('commercial_type');
        $selected_property_type = $this->input->post('selected_property_type');
        $selected_property_type = trim($selected_property_type);
        //----------------------------

        $r_selected_radio = $this->input->post('residential_type');
        if ($r_selected_radio) {
            $commercial_type = "null";
        } else {
            $residential_type = "null";
        }

        //------------------------------

        $plot_measurement = $this->input->post('plot_area_measurement');
        $length_measurement = $this->input->post('select_length_measurement');
        $breadth_measurement = $this->input->post('select_breadth_measurement');


        $data = array(
            'user_id' => $this->session->userdata('user_id'),
            'user_type' => $this->session->userdata('user_type'),
            'you_are' => $this->input->post('you_are'),
            'list_property' => $selected_property_type,
            'property_type' => $this->input->post('list_property'),
            'residential_type' => $residential_type,
            'commercial_type' => $commercial_type,
            'city' => $this->input->post('city_dropdown'),
            'project_name' => $this->input->post('project_name'),
            'locality' => $this->input->post('locality'),
//            'address' => $this->input->post('address'),
            'plot_area' => $this->input->post('plot_area') . " " . $plot_measurement,
            'length' => $this->input->post('length') . " " . $length_measurement,
            'breadth' => $this->input->post('breadh') . " " . $breadth_measurement,
            'no_of_bathrooms' => $this->input->post('no_of_bathrooms'),
            'no_of_bedrooms' => $this->input->post('no_of_bedrooms'),
            'no_of_balcony' => $this->input->post('no_of_balcony'),
            'complete_address' => $this->input->post('complete_address'),
            'Garden' => $this->input->post('garden'),
            'water_borewell' => $this->input->post('water_borewell'),
            'electricity_backup' => $this->input->post('electricity_backup'),
            'swimming_pool' => $this->input->post('swimming_pool'),
            'age_of_property' => $this->input->post('age_of_property'),
            'electricity' => $this->input->post('electricity'),
            'no_of_hall' => $this->input->post('no_of_hall'),
            'no_of_kitchen' => $this->input->post('no_of_kitchen'),
            'possession' => $this->input->post('possession'),
            'status1' => $this->input->post('legal_status')
        );

        $this->db->insert('property_details', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function add_property_image($data) {
        $this->db->insert('gallery', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function save_preference_details() {

        //$user_id=
        $plot_measurement = $this->input->post('plot_area_measurement');
        $data = array(
            'country' => $this->input->post('country'),
            'city' => $this->input->post('selected_city_id'),
            'state' => $this->input->post('state'),
            'zipcode' => $this->input->post('selected_zipcode_id'),
            'no_of_hall' => $this->input->post('no_of_hall'),
            'no_of_kitchen' => $this->input->post('no_of_kitchen'),
            'plot_area' => $this->input->post('plot_area') . " " . $plot_measurement,
            'no_of_bathrooms' => $this->input->post('no_of_bathrooms'),
            'no_of_bedrooms' => $this->input->post('no_of_bedrooms'),
            'no_of_balcony' => $this->input->post('no_of_balcony'),
            'Garden' => $this->input->post('garden'),
            'water_borewell' => $this->input->post('water_borewell'),
            'electricity_backup' => $this->input->post('electricity_backup'),
            'swimming_pool' => $this->input->post('swimming_pool'),
            'age_of_property' => $this->input->post('age_of_property'),
            'electricity' => $this->input->post('electricity'),
            'possession' => $this->input->post('possession'),
            'property_id' => $this->input->post('property_id'),
            'user_id' => $this->session->userdata('user_id'),
            'status1' => $this->input->post('legal_status'),
        );

        $this->db->insert('preference_details', $data);
    }


    public function notReceiveMsg() {
        $user_id = $this->session->userdata('user_id');
        $this->db->where("user_id", $user_id);
        $this->db->where("msg_status", '0');
        $query = $this->db->get("message");
        return $query->num_rows();
    }

    public function get_profile_image() {
        $user_id = $this->session->userdata('user_id');
        $this->db->where("user_id", $user_id);
        $query = $this->db->get("users");
        return $query->result_array();
    }

    public function msgInsert() {

        $query = $this->db->get("users");
        $queryRes = $query->result_array();
        for ($i = 0; $i < count($queryRes); $i++) {
            $data = array(
                'user_id' => $queryRes[$i]['user_id'],
                'msg_type' => $this->input->post('msg_type'),
                'message' => $this->input->post('message'),
                'created_date' => date('Y-m-d H:i:s'),
                'msg_status' => '0' // 0-> sent message
            );

            $this->db->insert('message', $data);
        }
    }

    public function retrive_type($type) {
        $user_id = $this->session->userdata('user_id');
        $this->db->where("coupon_type", $type);
        $this->db->where("status", '0');
        $query = $this->db->get("coupon");
        return $query->result_array();
    }

    public function retrive_text($type) {
        //$user_id = $this->session->userdata('user_id');
        $this->db->where("status", $type);
        $query = $this->db->get("settings");
        return $query->result_array();
    }
    
    public function get_city_name_for_preference($city_id_array)
    {
        $this->db->where_in('city_id', $city_id_array);
        $query = $this->db->get("cities");
        return $query->result_array();
    }
    public function get_pincode_for_preference($zipcode_id_array)
    {
        $this->db->where_in('zip_id', $zipcode_id_array);
        $query = $this->db->get("zipcode");
        return $query->result_array();
    }
    public function retrive_preference($id,$property_id) {
        $this->db->where('user_id', $id);
        $this->db->where('property_id', $property_id);
        $query = $this->db->get("preference_details");
        return $query->result_array();
    }
    public function editpreference_details($user_id,$property_id) {

        $country=$this->input->post('country');
        $state=$this->input->post('state');

        if($country=="-1")
        {
            $country=$this->input->post('pre_country');
        }
        if($state=="-1")
        {
            $state=$this->input->post('pre_state');
        }

        $data = array(
            'country' => $country,
            'city' => $this->input->post('selected_city_id'),
            'state' => $state,
            'zipcode' => $this->input->post('selected_zipcode_id'),
            'no_of_hall' => $this->input->post('no_of_hall'),
            'no_of_kitchen' => $this->input->post('no_of_kitchen'),
            'plot_area' => $this->input->post('plot_area'),
            'no_of_bathrooms' => $this->input->post('no_of_bathrooms'),
            'no_of_bedrooms' => $this->input->post('no_of_bedrooms'),
            'no_of_balcony' => $this->input->post('no_of_balcony') ,
            'Garden' => $this->input->post('garden'),
            'water_borewell' => $this->input->post('water_borewell'),
            'electricity_backup' => $this->input->post('electricity_backup'),
            'swimming_pool' => $this->input->post('swimming_pool'),
            'age_of_property' => $this->input->post('age_of_property'),
            'electricity' => $this->input->post('electricity'),
            'possession' => $this->input->post('possession'),
            'property_id' => $this->input->post('property_id'),
            'user_id' => $this->session->userdata('user_id')

        );

        $this->db->where("user_id", $user_id);
        $this->db->where("property_id", $property_id);
        $this->db->update('preference_details', $data);
    }
    public function getMyAccountCount() {
        $user_id = $this->session->userdata('user_id');
        $this->db->where("user_public_id", $user_id);
        $this->db->where("is_payment", '0');
        $query = $this->db->get("applyproperty_private");
        return $query->result_array();
    }
    
    public function getPropertyImage($property_id)
    {
        $this->db->where('property_id', $property_id);
        $query = $this->db->get("property_details");
        return $query->result_array();
    }
    public function add_prop_image_in_property_details($data,$property_id)
    {
        $this->db->where('property_id', $property_id);
        $this->db->update('property_details', $data);


    }
    public function getPropertyImagesfromGallery($property_id)
    {
        $this->db->where('pro_id', $property_id);
        $query = $this->db->get("gallery");
        return $query->result_array();
    }
    public function update_prop_image_in_gallery($data,$property_id,$pro_photo_id)
    {
        $this->db->where('pro_id', $property_id);
        $this->db->where('gallery_id', $pro_photo_id);
        $this->db->update('gallery', $data);
    }

}

?>