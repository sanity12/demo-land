
$(document).ready(function(){  

	//APPLY PAGE FORM VALIDATION
  	var frontpage = $("#apply_form").validate({
	    rules: {
	        to_uni_purchased: {
	            required: true,
                number: true,
                //currency:true
	        },
	        price_per_unit: {
	            required: true,
                number: true,
	        },
            mrp_per_unit: {
                required: true,
                number: true,
            },
            end_con_sel_pri: {
                required: true,
                number: true,
            },
            min_guart_rtn: {
                required: true,
                min:0,
                max:100
            },
            per_over_invest: {
                required: true,max:12,min:1
            },
            no_of_cycles: {
                required: true,min:1,max:12
            },
            Flipkart_comm: {
                //required: true
                min:0,
                max:25
            },
            amazon_comm: {
                //required: true
                min:0,
                max:25
            },
            myntra_comm: {
                //required: true
                min:0,
                max:25
            },
            other_comm: {
                //required: true
                min:0,
                max:25
            },
	         disc_offred: {
                required: true,
                max:100,
                min:0
            },
	    },
	    messages: {
	        to_uni_purchased: {
	            required: "Please enter a Total Units being purchased"//,
	        },
	        price_per_unit: {
	            required: "Please enter a Price per unit"//,
	        },
            mrp_per_unit: {
                required: "Please enter a MRP per unit"//,
            },
            end_con_sel_pri: {
                required: "Please enter a End consumer selling price"//,
            },
	        min_guart_rtn: {
                 required: "Please enter a Min. guaranteed return"//,
	       } ,
           per_over_invest: {
                 required: "Please enter a Period over investment"//,
           },
           no_of_cycles: {
                 required: "Please enter a No. of cycles/Year"//,
           },
           // ecomm_used: {
           //  required: "Please select E-Commerce platform used or not"
           // },   
           disc_offred:{
            required: "Please enter a Discount offered"

           },
        }
	});    
    jQuery.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    }, "Letters only please"); 
    var bankinfo_form = $("#bankinfo_form").validate({
        rules: {
            bank_acc_no: {
                required: true,
                minlength: 11,
                number:true
                //maxlength: 16
            },
            bank_holder_name: {
                required: true,
                lettersonly: true
            },
            ifsc_code: {
                required: true                
            },
            branch_code: {
                required: true               
            }
            
        },
        messages: {
             bank_acc_no: {
                required: "Please provide a Bank Account number",
                minlength: "Please provide a number at least 11 number",
                //maxlength: "Please provide a number only up to 10 number"
            },
             bank_holder_name: {
                required: 'Please enter your Bank Holder Name'
            },
            ifsc_code: {
                required: 'Please enter your IFSC Code'
            },           
            branch_code: {
                required: 'Please enter your BRANCH Code',
                
            },
           
        }               
    });
    //CONTACT FORM VALIDATION
    var contact_form = $("#contact_form").validate({
        rules: {
            fname: {
                required: true
            },
            lname: {
                required: true
            },
            email: {
                required: true,
                email: true/*,
                remote: "checkEmail"*/
            },
            contact_num: {
                required: true,
                minlength: 10,
                maxlength: 10
            }
            
        },
        messages: {
            fname: {
                required: 'Please enter your firstname'
            },
            lname: {
                required: 'Please enter your lastname'
            },
            email: {
                required: 'Please enter your email',
                email: 'Please enter a valid email address'
            },
            contact_num: {
                required: "Please provide a number",
                minlength: "Please provide a number at least 10 number",
                maxlength: "Please provide a number only up to 10 number"
            }
        }               
    });
    jQuery.validator.addMethod("noSpace", function(value, element) { 
    return value.indexOf(" ") < 0 && value != ""; 
  }, "Space are not allowed");
    jQuery.validator.addMethod("regx", function(value, element, regexpr) {          
        return regexpr.test(value);
    }, "Please enter a valid pasword, * and / are not allowed.");
    jQuery.validator.addMethod("regx1", function(value, element, regexpr) {          
        return regexpr.test(value);
    }, "Please enter a valid Pan Number, * , ., % and / are not allowed.");
    var user_form = $("#user_form").validate({
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            user_status: {
                required: true
            },
            email: {
                required: true,
                email: true/*,
                remote: "checkEmail"*/
            },
            contact_no: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            company_register: {
                required: true
            },
            website_link: {
                required: true,
                url: true
            },
            user_name: {
                required: true,
                // remote: {
                //     url: "/user/register_username_exists",
                //     type: "post",
                //     data: {
                //         user_name: function(){ return $("#user_name").val(); }
                //     }
                // }
            },
            user_password: {
                noSpace: true,
                minlength: 5,
                maxlength: 12,
                regx: /^[a-zA-Z0-9"!?.-]+$/
            },
            confirm_password: {
                equalTo: "#user_password"
            },
            panno: {
                required: true,
               // number: true,
               noSpace: true,
                regx1: /^[a-zA-Z0-9]+$/
            },
        },
        messages: {
            first_name: {
                required: 'Please enter your firstname'
            },
            last_name: {
                required: 'Please enter your lastname'
            },
            email: {
                required: 'Please enter your email',
                email: 'Please enter a valid email address'
            },
            contact_no: {
                required: "Please provide a number",
                minlength: "Please provide a number at least 10 number",
                maxlength: "Please provide a number only up to 10 number"
            },
            // user_name: {
            //     required: 'User Name is required',
            //     // remote: 'User Name is already exist.'
            // },
             user_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                maxlength: "Your password must be 12 characters long"
            },
            confirm_password: {
                required: 'Please provide a confirm password',
                equalTo: "password not match"
            }
        }               
    });

    var account_form = $("#account_form").validate({
        rules: {
            pass: {
                
                minlength: 6
            },
            repass: {
                equalTo: "#pwd"
            }
        },
        messages: {
             pass: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            },
            repass: {
                required: 'Please provide a confirm password',
                equalTo: "password not match"
            }
        }               
    });

     var chn_mobile_form = $("#chn_mobile_form").validate({
        rules: {
            contact_no: {
                required: true,
                minlength: 10,
                maxlength: 10
            }
            
        },
        messages: {
            contact_no: {
                required: "Please provide a number",
                minlength: "Please provide a number at least 10 number",
                maxlength: "Please provide a number only up to 10 number"
            }
        }         
    });

    
 
    //LOGIN FORM VALIDATION
    var login_form = $("#login_form").validate({
	    rules: {
	        username2: {
	            required: true
	        },
	        password2: {
	            required: true
	        }
	        
	    },
	    messages: {
	        username2: {
	            required: "Please enter an email"
	        },
	        password2: {
	            required: "Please enter password"
	        }
	    }               
	});

    var msg_form = $("#msg_form").validate({
        rules: {
            msg_type: {
                required: true
            },
            message: {
                required: true
            }
            
        },
        messages: {
            msg_type: {
                required: "Please enter an Message Type "
            },
            message: {
                required: "Please enter message description"
            }
        }               
    });
  
});
